#include <iostream>
#include <sys/time.h>
#include <cuda_runtime.h>

#include "Declaration.h"
#include "pairing.h"
#include "timing.h"

void pairing_timing() {

  const int Count = 10;
  struct timeval start, end;
  double timing;

  F2_487_u64 xP, xQ, yP, yQ;
  ExtField6_2<F2_487_u64> pairing6_2;
  ExtField3_2_2<F2_487_u64> pairing3_2_2;

  F2_967_u64 xP_967, xQ_967, yP_967, yQ_967;
  ExtField6_2<F2_967_u64> pairing6_2_967;
  ExtField3_2_2<F2_967_u64> pairing3_2_2_967;



  /******************** using window method ********************/
  const int window_size = 9;

  // ext6_2 window method (host)
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP.setRandom();
    xQ.setRandom();
    yP.setRandom();
    yQ.setRandom();

    gettimeofday(&start, 0);
    pairing6_2 = etaT_pairing6_2<F2_487_u64, window_size>(xP, xQ, yP, yQ);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }

  std::cout << "pairing (base field: F2_487_u74) over ext6_2 using window method with window size: " << window_size << " (host code): " << Count << "回平均" << std::endl;
  std::cout << timing / Count << " (microsec)" << std::endl;



  // ext3_2_2 window method (host)
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP.setRandom();
    xQ.setRandom();
    yP.setRandom();
    yQ.setRandom();

    gettimeofday(&start, 0);
    pairing3_2_2 = etaT_pairing3_2_2<F2_487_u64, window_size>(xP, xQ, yP, yQ);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }

  std::cout << "pairing (base field: F2_487_u74) over ext3_2_2 using window method with window size: " << window_size << " (host code): " << Count << "回平均" << std::endl;
  std::cout << timing / Count << " (microsec)" << std::endl;



  // m=967
  //ext6_2 window method (host)
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP_967.setRandom();
    xQ_967.setRandom();
    yP_967.setRandom();
    yQ_967.setRandom();

    gettimeofday(&start, 0);
    pairing6_2_967 = etaT_pairing6_2<F2_967_u64, window_size>(xP_967, xQ_967, yP_967, yQ_967);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }

  std::cout << "pairing (base field: F2_967_u74) over ext6_2 using window method with window size: " << window_size << " (host code): " << Count << "回平均" << std::endl;
  std::cout << timing / Count << " (microsec)" << std::endl;



  // ext3_2_2 window method (host)
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP_967.setRandom();
    xQ_967.setRandom();
    yP_967.setRandom();
    yQ_967.setRandom();

    gettimeofday(&start, 0);
    pairing3_2_2_967 = etaT_pairing3_2_2<F2_967_u64, window_size>(xP_967, xQ_967, yP_967, yQ_967);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }

  std::cout << "pairing (base field: F2_967_u74) over ext3_2_2 using window method with window size: " << window_size << " (host code): " << Count << "回平均" << std::endl;
  std::cout << timing / Count << " (microsec)" << std::endl;
}

/* 上のpairing_timing()と同じ */
void pairingCPU_timing() {

  const int Count = 10;
  struct timeval start, end;
  double timing;

  const int wSize = 4;

  F2_487_u64 xP_H, xQ_H, yP_H, yQ_H;
  ExtField6_2<F2_487_u64> pairing6_2_H;
  ExtField3_2_2<F2_487_u64> pairing3_2_2_H;

  F2_967_u64 xP_H9, xQ_H9, yP_H9, yQ_H9;
  ExtField6_2<F2_967_u64> pairing6_2_H9;
  ExtField3_2_2<F2_967_u64> pairing3_2_2_H9;

  std::cout << "window size: " << wSize << std::endl;

  // ext6_2 host
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {
    xP_H.setRandom();
    xQ_H.setRandom();
    yP_H.setRandom();
    yQ_H.setRandom();

    gettimeofday(&start, 0);
    pairing6_2_H = etaT_pairing6_2<F2_487_u64, wSize>(xP_H, xQ_H, yP_H, yQ_H);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }

  std::cout << "pairing (base field: F2_487_u64) over ext6_2 (host code): " << Count << "回平均" << std::endl;
  std::cout << timing / Count << " (microsec)" << std::endl;



  // ext3_2_2 host
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {
    xP_H.setRandom();
    xQ_H.setRandom();
    yP_H.setRandom();
    yQ_H.setRandom();

    gettimeofday(&start, 0);
    pairing3_2_2_H = etaT_pairing3_2_2<F2_487_u64, wSize>(xP_H, xQ_H, yP_H, yQ_H);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }

  std::cout << "pairing (base field: F2_487_u64) over ext3_2_2 (host code): " << Count << "回平均" << std::endl;
  std::cout << timing / Count << " (microsec)" << std::endl;



  // m=967
  // ext6_2 host
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {
    xP_H9.setRandom();
    xQ_H9.setRandom();
    yP_H9.setRandom();
    yQ_H9.setRandom();

    gettimeofday(&start, 0);
    pairing6_2_H9 = etaT_pairing6_2<F2_967_u64, wSize>(xP_H9, xQ_H9, yP_H9, yQ_H9);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }

  std::cout << "pairing (base field: F2_967_u64) over ext6_2 (host code): " << Count << "回平均" << std::endl;
  std::cout << timing / Count << " (microsec)" << std::endl;



  // ext3_2_2 host
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {
    xP_H9.setRandom();
    xQ_H9.setRandom();
    yP_H9.setRandom();
    yQ_H9.setRandom();

    gettimeofday(&start, 0);
    pairing3_2_2_H9 = etaT_pairing3_2_2<F2_967_u64, wSize>(xP_H9, xQ_H9, yP_H9, yQ_H9);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }

  std::cout << "pairing (base field: F2_967_u64) over ext3_2_2 (host code): " << Count << "回平均" << std::endl;
  std::cout << timing / Count << " (microsec)" << std::endl;
}

void pairingMUltiple_timing() {

  const int Count = 1;
  const int number = 1;
  struct timeval start, end;
  double timing;

  const int wSize = 5;

  BaseField_Multiple<F2_487_u64, number> xP, xQ, yP, yQ;
  ExtField6_2_Multiple<F2_487_u64, number> pairing6_2;
  ExtField3_2_2_Multiple<F2_487_u64, number> pairing3_2_2;

  F2_487_u64 xP_H, xQ_H, yP_H, yQ_H;
  ExtField6_2<F2_487_u64> pairing6_2_H;
  ExtField3_2_2<F2_487_u64> pairing3_2_2_H;

  BaseField_Multiple<F2_967_u64, number> xP9, xQ9, yP9, yQ9;
  ExtField6_2_Multiple<F2_967_u64, number> pairing6_29;
  ExtField3_2_2_Multiple<F2_967_u64, number> pairing3_2_29;

  F2_967_u64 xP_H9, xQ_H9, yP_H9, yQ_H9;
  ExtField6_2<F2_967_u64> pairing6_2_H9;
  ExtField3_2_2<F2_967_u64> pairing3_2_2_H9;



  int *testGPU_;
  cudaMalloc((void**)&testGPU_, sizeof(int));

  std::cout << "window size: " << wSize << std::endl;

  // ext6_2 host
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    for (unsigned int j = 0; j < number; ++j) {
      xP_H.setRandom();
      xQ_H.setRandom();
      yP_H.setRandom();
      yQ_H.setRandom();

      gettimeofday(&start, 0);
      pairing6_2_H = etaT_pairing6_2<F2_487_u64, wSize>(xP_H, xQ_H, yP_H, yQ_H);
      gettimeofday(&end, 0);
      timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
    }
  }

  std::cout << "pairing (base field: F2_487_u74) over ext6_2 (host code): " << Count << "回平均, number of pairing: " << number << std::endl;
  std::cout << timing / Count << " (microsec)" << std::endl;



  // ext6_2 GPU
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP.setRandom();
    xQ.setRandom();
    yP.setRandom();
    yQ.setRandom();

    gettimeofday(&start, 0);
    pairing6_2 = pairing6_2_multipleP<F2_487_u64, wSize, number>(xP, xQ, yP, yQ);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }

  std::cout << "multiple pairing (base field: F2_487_u74) over ext6_2 on GPU: " << Count << "回平均, number of pairing: " << number << std::endl;
  std::cout << timing / Count << " (microsec)" << std::endl;



  // ext3_2_2 host
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    for (unsigned int j = 0; j < number; ++j) {
      xP_H.setRandom();
      xQ_H.setRandom();
      yP_H.setRandom();
      yQ_H.setRandom();

      gettimeofday(&start, 0);
      pairing3_2_2_H = etaT_pairing3_2_2<F2_487_u64, wSize>(xP_H, xQ_H, yP_H, yQ_H);
      gettimeofday(&end, 0);
      timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
    }
  }

  std::cout << "pairing (base field: F2_487_u74) over ext3_2_2 (host code): " << Count << "回平均, number of pairing: " << number << std::endl;
  std::cout << timing / Count << " (microsec)" << std::endl;



  // ext3_2_2 GPU
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP.setRandom();
    xQ.setRandom();
    yP.setRandom();
    yQ.setRandom();

    gettimeofday(&start, 0);
    pairing3_2_2 = pairing3_2_2_multipleP<F2_487_u64, wSize, number>(xP, xQ, yP, yQ);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }

  std::cout << "multiple pairing (base field: F2_487_u74) over ext3_2_2 on GPU: " << Count << "回平均, number of pairing: " << number << std::endl;
  std::cout << timing / Count << " (microsec)" << std::endl;



  // m=967
  // ext6_2 host
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    for (unsigned int j = 0; j < number; ++j) {
      xP_H.setRandom();
      xQ_H.setRandom();
      yP_H.setRandom();
      yQ_H.setRandom();

      gettimeofday(&start, 0);
      pairing6_2_H9 = etaT_pairing6_2<F2_967_u64, wSize>(xP_H9, xQ_H9, yP_H9, yQ_H9);
      gettimeofday(&end, 0);
      timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
    }
  }

  std::cout << "pairing (base field: F2_967_u74) over ext6_2 (host code): " << Count << "回平均, number of pairing: " << number << std::endl;
  std::cout << timing / Count << " (microsec)" << std::endl;

  cudaFree(testGPU_);
}

void pairingMUltiple_timingProfile() {

  const int Count = 1;
  const int number = 1;
  struct timeval start, end;
  double timing;



  int *testGPU_;
  cudaMalloc((void**)&testGPU_, sizeof(int));

  const int wSize = 5;

  BaseField_Multiple<F2_487_u64, number> xP, xQ, yP, yQ;
  ExtField6_2_Multiple<F2_487_u64, number> pairing6_2;
  ExtField3_2_2_Multiple<F2_487_u64, number> pairing3_2_2;

  F2_487_u64 xP_H, xQ_H, yP_H, yQ_H;
  ExtField6_2<F2_487_u64> pairing6_2_H;
  ExtField3_2_2<F2_487_u64> pairing3_2_2_H;

  BaseField_Multiple<F2_967_u64, number> xP9, xQ9, yP9, yQ9;
  ExtField6_2_Multiple<F2_967_u64, number> pairing6_29;
  ExtField3_2_2_Multiple<F2_967_u64, number> pairing3_2_29;

  F2_967_u64 xP_H9, xQ_H9, yP_H9, yQ_H9;
  ExtField6_2<F2_967_u64> pairing6_2_H9;
  ExtField3_2_2<F2_967_u64> pairing3_2_2_H9;

  std::cout << "window size: " << wSize << std::endl;

  // m=487
  // ext3_2_2 GPU
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP.setRandom();
    xQ.setRandom();
    yP.setRandom();
    yQ.setRandom();

    gettimeofday(&start, 0);
    pairing3_2_2 = pairing3_2_2_multipleP<F2_487_u64, wSize, number>(xP, xQ, yP, yQ);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }

  std::cout << "multiple pairing (base field: F2_487_u74) over ext3_2_2 on GPU: " << Count << "回平均, number of pairing: " << number << std::endl;
  std::cout << timing / Count << " (microsec)" << std::endl;



  // m=967
  // ext3_2_2 GPU
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP9.setRandom();
    xQ9.setRandom();
    yP9.setRandom();
    yQ9.setRandom();

    gettimeofday(&start, 0);
    pairing3_2_29 = pairing3_2_2_multipleP<F2_967_u64, wSize, number>(xP9, xQ9, yP9, yQ9);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }

  std::cout << "multiple pairing (base field: F2_967_u74) over ext3_2_2 on GPU: " << Count << "回平均, number of pairing: " << number << std::endl;
  std::cout << timing / Count << " (microsec)" << std::endl;

  cudaFree(testGPU_);
}

void pairing_getTiming() {

  int *testGPU_;
  cudaMalloc((void**)&testGPU_, sizeof(int));

  const int wSize = 6;
  std::cout << "window size: " << wSize << std::endl;
#if 1
  std::cout << "number    m=487-6_2-host,per P    m=487-6_2-GPU,per P    m=487-3_2_2-host,per P    m=487-3_2_2-GPU,per P" << std::endl;
  pairing487Multiple_getTiming<wSize,1>();
  pairing487Multiple_getTiming<wSize,64>();
  pairing487Multiple_getTiming<wSize,128>();
  pairing487Multiple_getTiming<wSize,256>();
  pairing487Multiple_getTiming<wSize,384>();
  pairing487Multiple_getTiming<wSize,512>();
  pairing487Multiple_getTiming<wSize,640>();
  pairing487Multiple_getTiming<wSize,768>();
  pairing487Multiple_getTiming<wSize,896>();
  pairing487Multiple_getTiming<wSize,1024>();
  pairing487Multiple_getTiming<wSize,1152>();
  pairing487Multiple_getTiming<wSize,1280>();
  pairing487Multiple_getTiming<wSize,1408>();

#else
  std::cout << "number    m=967-6_2-host,per P    m=967-6_2-GPU,per P    m=967-3_2_2-host,per P    m=967-3_2_2-GPU,per  P" << std::endl;
  pairing967Multiple_getTiming<wSize,1>();
  pairing967Multiple_getTiming<wSize,64>();
  pairing967Multiple_getTiming<wSize,128>();
  pairing967Multiple_getTiming<wSize,256>();
  pairing967Multiple_getTiming<wSize,384>();
  pairing967Multiple_getTiming<wSize,512>();
  pairing967Multiple_getTiming<wSize,640>();
  pairing967Multiple_getTiming<wSize,768>();
  pairing967Multiple_getTiming<wSize,896>();
  pairing967Multiple_getTiming<wSize,1024>();
  pairing967Multiple_getTiming<wSize,1152>();
  pairing967Multiple_getTiming<wSize,1280>();
  pairing967Multiple_getTiming<wSize,1408>();
#endif

  cudaFree(testGPU_);
}

void pairingHost_getTiming() {

  int *testGPU_;
  cudaMalloc((void**)&testGPU_, sizeof(int));

  const int wSize = 8;
#if 0
  std::cout << "!! dummy !!" << std::endl;
  std::cout << "window size: " << wSize << std::endl;
  std::cout << "number    m=487-6_2-host,per P    m=487-3_2_2-host,per P" << std::endl;
  pairing487Host_getTiming<wSize,1>();
  pairing487Host_getTiming<wSize,32>();
  pairing487Host_getTiming<wSize,64>();
  pairing487Host_getTiming<wSize,96>();
  pairing487Host_getTiming<wSize,128>();
  pairing487Host_getTiming<wSize,160>();
  pairing487Host_getTiming<wSize,192>();
  pairing487Host_getTiming<wSize,224>();
  pairing487Host_getTiming<wSize,256>();
  pairing487Host_getTiming<wSize,288>();
  pairing487Host_getTiming<wSize,320>();
  pairing487Host_getTiming<wSize,352>();
  pairing487Host_getTiming<wSize,384>();
  pairing487Host_getTiming<wSize,416>();
  pairing487Host_getTiming<wSize,448>();
  pairing487Host_getTiming<wSize,480>();
  pairing487Host_getTiming<wSize,512>();
  pairing487Host_getTiming<wSize,544>();
  pairing487Host_getTiming<wSize,576>();
  pairing487Host_getTiming<wSize,608>();
  pairing487Host_getTiming<wSize,640>();
  pairing487Host_getTiming<wSize,672>();
  pairing487Host_getTiming<wSize,704>();
  pairing487Host_getTiming<wSize,736>();
  pairing487Host_getTiming<wSize,768>();
  pairing487Host_getTiming<wSize,800>();
  pairing487Host_getTiming<wSize,832>();
  pairing487Host_getTiming<wSize,864>();
  pairing487Host_getTiming<wSize,896>();
  pairing487Host_getTiming<wSize,928>();
  pairing487Host_getTiming<wSize,960>();
  pairing487Host_getTiming<wSize,992>();
  pairing487Host_getTiming<wSize,1024>();
  pairing487Host_getTiming<wSize,1056>();
  pairing487Host_getTiming<wSize,1088>();
  pairing487Host_getTiming<wSize,1120>();
  pairing487Host_getTiming<wSize,1152>();
  pairing487Host_getTiming<wSize,1184>();
  pairing487Host_getTiming<wSize,1216>();
  pairing487Host_getTiming<wSize,1248>();
  pairing487Host_getTiming<wSize,1280>();
  pairing487Host_getTiming<wSize,1312>();
  pairing487Host_getTiming<wSize,1344>();
  pairing487Host_getTiming<wSize,1376>();
  pairing487Host_getTiming<wSize,1408>();

#else
  std::cout << "!! dummy !!" << std::endl;
  std::cout << "window size: " << wSize << std::endl;
  std::cout << "number    m=967-6_2-host,per P    m=967-3_2_2-host,per P" << std::endl;
  pairing967Host_getTiming<wSize,1>();
  pairing967Host_getTiming<wSize,32>();
  pairing967Host_getTiming<wSize,64>();
  pairing967Host_getTiming<wSize,96>();
  pairing967Host_getTiming<wSize,128>();
  pairing967Host_getTiming<wSize,160>();
  pairing967Host_getTiming<wSize,192>();
  pairing967Host_getTiming<wSize,224>();
  pairing967Host_getTiming<wSize,256>();
  pairing967Host_getTiming<wSize,288>();
  pairing967Host_getTiming<wSize,320>();
  pairing967Host_getTiming<wSize,352>();
  pairing967Host_getTiming<wSize,384>();
  pairing967Host_getTiming<wSize,416>();
  pairing967Host_getTiming<wSize,448>();
  pairing967Host_getTiming<wSize,480>();
  pairing967Host_getTiming<wSize,512>();
  pairing967Host_getTiming<wSize,544>();
  pairing967Host_getTiming<wSize,576>();
  pairing967Host_getTiming<wSize,608>();
  pairing967Host_getTiming<wSize,640>();
  pairing967Host_getTiming<wSize,672>();
  pairing967Host_getTiming<wSize,704>();
  pairing967Host_getTiming<wSize,736>();
  pairing967Host_getTiming<wSize,768>();
  pairing967Host_getTiming<wSize,800>();
  pairing967Host_getTiming<wSize,832>();
  pairing967Host_getTiming<wSize,864>();
  pairing967Host_getTiming<wSize,896>();
  pairing967Host_getTiming<wSize,928>();
  pairing967Host_getTiming<wSize,960>();
  pairing967Host_getTiming<wSize,992>();
  pairing967Host_getTiming<wSize,1024>();
  pairing967Host_getTiming<wSize,1056>();
  pairing967Host_getTiming<wSize,1088>();
  pairing967Host_getTiming<wSize,1120>();
  pairing967Host_getTiming<wSize,1152>();
  pairing967Host_getTiming<wSize,1184>();
  pairing967Host_getTiming<wSize,1216>();
  pairing967Host_getTiming<wSize,1248>();
  pairing967Host_getTiming<wSize,1280>();
  pairing967Host_getTiming<wSize,1312>();
  pairing967Host_getTiming<wSize,1344>();
  pairing967Host_getTiming<wSize,1376>();
  pairing967Host_getTiming<wSize,1408>();
#endif

  cudaFree(testGPU_);
}

void dummy_pairing_getTiming() {

  int *testGPU_;
  cudaMalloc((void**)&testGPU_, sizeof(int));

  const int wSize = 4;
#if 1
  std::cout << "!! dummy !!" << std::endl;
  std::cout << "window size: " << wSize << std::endl;
  std::cout << "number    m=487-6_2-GPU,per P    m=487-3_2_2-GPU,per P" << std::endl;
  dummy_pairing487Multiple_getTiming<wSize,1>();
  dummy_pairing487Multiple_getTiming<wSize,32>();
  dummy_pairing487Multiple_getTiming<wSize,64>();
  dummy_pairing487Multiple_getTiming<wSize,96>();
  dummy_pairing487Multiple_getTiming<wSize,128>();
  dummy_pairing487Multiple_getTiming<wSize,160>();
  dummy_pairing487Multiple_getTiming<wSize,192>();
  dummy_pairing487Multiple_getTiming<wSize,224>();
  dummy_pairing487Multiple_getTiming<wSize,256>();
  dummy_pairing487Multiple_getTiming<wSize,288>();
  dummy_pairing487Multiple_getTiming<wSize,320>();
  dummy_pairing487Multiple_getTiming<wSize,352>();
  dummy_pairing487Multiple_getTiming<wSize,384>();
  dummy_pairing487Multiple_getTiming<wSize,416>();
  dummy_pairing487Multiple_getTiming<wSize,448>();
  dummy_pairing487Multiple_getTiming<wSize,480>();
  dummy_pairing487Multiple_getTiming<wSize,512>();
  dummy_pairing487Multiple_getTiming<wSize,544>();
  dummy_pairing487Multiple_getTiming<wSize,576>();
  dummy_pairing487Multiple_getTiming<wSize,608>();
  dummy_pairing487Multiple_getTiming<wSize,640>();
  dummy_pairing487Multiple_getTiming<wSize,672>();
  dummy_pairing487Multiple_getTiming<wSize,704>();
  dummy_pairing487Multiple_getTiming<wSize,736>();
  dummy_pairing487Multiple_getTiming<wSize,768>();
  dummy_pairing487Multiple_getTiming<wSize,800>();
  dummy_pairing487Multiple_getTiming<wSize,832>();
  dummy_pairing487Multiple_getTiming<wSize,864>();
  dummy_pairing487Multiple_getTiming<wSize,896>();
  dummy_pairing487Multiple_getTiming<wSize,928>();
  dummy_pairing487Multiple_getTiming<wSize,960>();
  dummy_pairing487Multiple_getTiming<wSize,992>();
  dummy_pairing487Multiple_getTiming<wSize,1024>();
  dummy_pairing487Multiple_getTiming<wSize,1056>();
  dummy_pairing487Multiple_getTiming<wSize,1088>();
  dummy_pairing487Multiple_getTiming<wSize,1120>();
  dummy_pairing487Multiple_getTiming<wSize,1152>();
  dummy_pairing487Multiple_getTiming<wSize,1184>();
  dummy_pairing487Multiple_getTiming<wSize,1216>();
  dummy_pairing487Multiple_getTiming<wSize,1248>();
  dummy_pairing487Multiple_getTiming<wSize,1280>();
  dummy_pairing487Multiple_getTiming<wSize,1312>();
  dummy_pairing487Multiple_getTiming<wSize,1344>();
  dummy_pairing487Multiple_getTiming<wSize,1376>();
  dummy_pairing487Multiple_getTiming<wSize,1408>();



  std::cout << "!! dummy !!" << std::endl;
  std::cout << "window size: " << wSize << std::endl;
  std::cout << "number    m=967-6_2-GPU,per P    m=967-3_2_2-GPU,per  P" << std::endl;
  dummy_pairing967Multiple_getTiming<wSize,1>();
  dummy_pairing967Multiple_getTiming<wSize,32>();
  dummy_pairing967Multiple_getTiming<wSize,64>();
  dummy_pairing967Multiple_getTiming<wSize,96>();
  dummy_pairing967Multiple_getTiming<wSize,128>();
  dummy_pairing967Multiple_getTiming<wSize,160>();
  dummy_pairing967Multiple_getTiming<wSize,192>();
  dummy_pairing967Multiple_getTiming<wSize,224>();
  dummy_pairing967Multiple_getTiming<wSize,256>();
  dummy_pairing967Multiple_getTiming<wSize,288>();
  dummy_pairing967Multiple_getTiming<wSize,320>();
  dummy_pairing967Multiple_getTiming<wSize,352>();
  dummy_pairing967Multiple_getTiming<wSize,384>();
  dummy_pairing967Multiple_getTiming<wSize,416>();
  dummy_pairing967Multiple_getTiming<wSize,448>();
  dummy_pairing967Multiple_getTiming<wSize,480>();
  dummy_pairing967Multiple_getTiming<wSize,512>();
  dummy_pairing967Multiple_getTiming<wSize,544>();
  dummy_pairing967Multiple_getTiming<wSize,576>();
  dummy_pairing967Multiple_getTiming<wSize,608>();
  dummy_pairing967Multiple_getTiming<wSize,640>();
  dummy_pairing967Multiple_getTiming<wSize,672>();
  dummy_pairing967Multiple_getTiming<wSize,704>();
  dummy_pairing967Multiple_getTiming<wSize,736>();
  dummy_pairing967Multiple_getTiming<wSize,768>();
  dummy_pairing967Multiple_getTiming<wSize,800>();
  dummy_pairing967Multiple_getTiming<wSize,832>();
  dummy_pairing967Multiple_getTiming<wSize,864>();
  dummy_pairing967Multiple_getTiming<wSize,896>();
  dummy_pairing967Multiple_getTiming<wSize,928>();
  dummy_pairing967Multiple_getTiming<wSize,960>();
  dummy_pairing967Multiple_getTiming<wSize,992>();
  dummy_pairing967Multiple_getTiming<wSize,1024>();
  dummy_pairing967Multiple_getTiming<wSize,1056>();
  dummy_pairing967Multiple_getTiming<wSize,1088>();
  dummy_pairing967Multiple_getTiming<wSize,1120>();
  dummy_pairing967Multiple_getTiming<wSize,1152>();
  dummy_pairing967Multiple_getTiming<wSize,1184>();
  dummy_pairing967Multiple_getTiming<wSize,1216>();
  dummy_pairing967Multiple_getTiming<wSize,1248>();
  dummy_pairing967Multiple_getTiming<wSize,1280>();
  dummy_pairing967Multiple_getTiming<wSize,1312>();
  dummy_pairing967Multiple_getTiming<wSize,1344>();
  dummy_pairing967Multiple_getTiming<wSize,1376>();
  dummy_pairing967Multiple_getTiming<wSize,1408>();
#else
#endif

  cudaFree(testGPU_);
}

void dummy_pairingMultiple_getTiming() {

  int *testGPU_;
  cudaMalloc((void**)&testGPU_, sizeof(int));

  const int wSize = 4;
  std::cout << "!! dummy !!" << std::endl;
  std::cout << "window size: " << wSize << std::endl;
  std::cout << "number    m=487-6_2-GPU,per P    m=487-3_2_2-GPU,per P    m=967-6_2-GPU,per P    m=967-3_2_2-GPU,per P" << std::endl;

  dummy_pairingMultiple_getTiming<wSize,1>();
  dummy_pairingMultiple_getTiming<wSize,64>();
  dummy_pairingMultiple_getTiming<wSize,1024>();

  cudaFree(testGPU_);
}
