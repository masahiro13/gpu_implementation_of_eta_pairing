#pragma once

#include <stdlib.h>
#include <sys/time.h>

//#include <random> //c++11

//#include <boost/random.hpp>
#include <iostream>
#include "standard.h"
#include "F2_487.h"

template<class T, int m> class BaseField {

public:

  static const unsigned int ext_degree = m;
  static const unsigned int nUint = (m + T::wordSize - 1) / T::wordSize;
  static const unsigned int wordSize = T::wordSize;
  typedef typename T::uint_type uint_type;

  uint_type x[nUint];



  __host__ __device__ BaseField() {}

  __forceinline__ __host__ __device__ void clear() {

    for (unsigned int i = 0; i < nUint; ++i) x[i] = 0;
  }

  __forceinline__ __host__ __device__ void copy(const BaseField& a) {
    for (unsigned int i = 0; i < nUint; ++i) x[i] = a.x[i];
  }

  __forceinline__ __host__ __device__ void setAll1Bit() {

    uint_type max = 0;
    --max;
    for (unsigned int i = 0; i < nUint; ++i) x[i] = max;
  }

  __forceinline__ __host__ __device__
  void clearUpDeg() {

    //const int remUint = ext_degree - wordSize * (nUint - 1); // ext_degree % wordSize
    const int remUint = ext_degree % wordSize; // ext_degree % wordSize
    x[nUint - 1] <<= (wordSize - remUint);
    x[nUint - 1] >>= (wordSize - remUint);
  }

  __forceinline__ void setRandom() {

#if 0

    std::random_device rd;
    std::mt19937 mt(rd());
    for (unsigned int i = 0; i < nUint; ++i) x[i] = rd();
    clearUpDeg();

#elif 0

    struct timeval now;
    gettimeofday(&now, 0);
    uint_type max = 0;

    boost::mt19937 gen( static_cast<unsigned long>(now.tv_usec) );
    boost::uniform_int<uint_type> dist(1, max - 1);
    boost::variate_generator< boost::mt19937, boost::uniform_int<uint_type> > rand(gen, dist);

    for (unsigned int i = 0; i < nUint; ++i) x[i] = rand();
    clearUpDeg();

#else

    struct timeval now;
    gettimeofday(&now, 0);
    srand( static_cast<unsigned long>(now.tv_usec) );

    for (unsigned int i = 0; i < nUint; ++i) {

      x[i] = rand();
      if (wordSize > 32) {

        x[i] <<= 31;
        x[i] <<= 1;
        x[i] += rand();
      }
    }
    clearUpDeg();

#endif
  }

  __forceinline__ __host__ __device__ void plusOne() {
      x[0] ^= 1;
  }

  __forceinline__ __host__ __device__
  bool testZero() const {

    bool result = true;
    for (unsigned int i = 0; i < nUint; ++i) result &= (x[i] == 0);
    return result;
  }

  __forceinline__ __host__ __device__
  uint_type getBit(const unsigned int position) const {

    const unsigned int nUintFloor = position / wordSize;
    //const unsigned int posRem = position - nUintFloor * wordSize;
    const unsigned int posRem = position % wordSize;

    //uint_type ret = x[nUintFloor];
    return (x[nUintFloor] >> posRem) & 1;
  }

  __forceinline__ __host__ __device__ friend
  BaseField operator&(const BaseField& a, const BaseField& b) {

    BaseField c;
    for (unsigned int i = 0; i < nUint; ++i) c.x[i] = a.x[i] & b.x[i];
    return c;
  }

  __forceinline__ __host__ __device__ friend
  BaseField operator|(const BaseField& a, const BaseField& b) {

    BaseField c;
    for (unsigned int i = 0; i < nUint; ++i) c.x[i] = a.x[i] | b.x[i];
    return c;
  }

  __forceinline__ __host__ __device__ friend
  BaseField operator^(const BaseField& a, const BaseField& b) {

    BaseField c;
    for (unsigned int i = 0; i < nUint; ++i) c.x[i] = a.x[i] ^ b.x[i];
    return c;
  }

  __forceinline__ __host__ __device__ friend
  BaseField operator+(const BaseField& a, const BaseField& b) {

    return a ^ b;
  }

  __forceinline__ __host__ __device__ friend
  BaseField operator-(const BaseField& a, const BaseField& b) {

    return a ^ b;
  }

  __forceinline__ __host__ __device__ friend
  bool operator==(const BaseField& a, const BaseField& b) {

    bool result = true;
    for (unsigned int i = 0; i < nUint; ++i) result &= (a.x[i] == b.x[i]);
    return result;
  }

  __forceinline__ __host__ __device__
  BaseField operator&=(const BaseField& b) {

    for (unsigned int i = 0; i < nUint; ++i) x[i] &= b.x[i];
    return *this;
  }

  __forceinline__ __host__ __device__
  BaseField operator|=(const BaseField& b) {

    for (unsigned int i = 0; i < nUint; ++i) x[i] |= b.x[i];
    return *this;
  }

  __forceinline__ __host__ __device__
  BaseField operator^=(const BaseField& b) {

    for (unsigned int i = 0; i < nUint; ++i) x[i] ^= b.x[i];
    return *this;
  }

  __forceinline__ __host__ __device__
  BaseField operator+=(const BaseField& b) {

    return *this ^= b;
  }

  __forceinline__ __host__ __device__
  BaseField operator-=(const BaseField& b) {

    return *this ^= b;
  }

  template<int N>
  __host__ __device__ void shiftLeftBit() {

    const unsigned int nUintFloor = N / wordSize;
    //const unsigned int remainder = N - nUintFloor * wordSize;
    const unsigned int remainder = N % wordSize;

    if (N <= 0) return;
    if (nUintFloor >= nUint) {

      this->clear();
      return;
    }

    for (int i = 0; i < nUint - nUintFloor; ++i) this->x[nUint - 1 - i] = this->x[nUint - 1 - nUintFloor - i];
    for (int i = 0; i < nUintFloor; ++i) this->x[i] = 0;

    if (remainder == 0) return;
    for (int i = nUint - 1; i > nUintFloor; --i) this->x[i] = (this->x[i] << remainder) | (this->x[i - 1] >> wordSize - remainder);
    this->x[nUintFloor] <<= remainder;
  }

  template<int N>
  __host__ __device__ void shiftRightBit() {

    const unsigned int nUintFloor = N / wordSize;
    //const unsigned int remainder = N - nUintFloor * wordSize;
    const unsigned int remainder = N % wordSize;

    if (N <= 0) return;
    if (nUintFloor >= nUint) {

      this->clear();
      return;
    }

    for (int i = 0; i < nUint - nUintFloor; ++i) this->x[i] = this->x[nUintFloor + i];
    for (int i = nUint - nUintFloor; i < nUint; ++i) this->x[i] = 0;

    if (remainder == 0) return;
    for (int i = 0; i < nUint - nUintFloor - 1; ++i) this->x[i] = (this->x[i] >> remainder) | (this->x[i + 1] << wordSize - remainder);
    this->x[nUint - nUintFloor - 1] >>= remainder;
  }

  template<int N>
  __host__ __device__ static BaseField shiftLeftBit(const BaseField& a) {

    BaseField c = a;
    c.shiftLeftBit<N>();
    return c;
  }

  template<int N>
  __host__ __device__ static BaseField shiftRightBit(const BaseField& a) {

    BaseField c = a;
    c.shiftRightBit<N>();
    return c;
  }

  template<int N>
  __host__ __device__ static void shiftLeftBit(BaseField &c, const BaseField& a) {

    c = a;
    c.shiftLeftBit<N>();
  }

  template<int N>
  __host__ __device__ static void shiftRightBit(BaseField &c, const BaseField& a) {

    c = a;
    c.shiftRightBit<N>();
  }

  __host__ __device__ void shiftLeftBit(const int N) {

    const unsigned int nUintFloor = N / wordSize;
    //const unsigned int remainder = N - nUintFloor * wordSize;
    const unsigned int remainder = N % wordSize;

    if (N <= 0) return;
    if (nUintFloor >= nUint) {

      this->clear();
      return;
    }

    for (int i = 0; i < nUint - nUintFloor; ++i) this->x[nUint - 1 - i] = this->x[nUint - 1 - nUintFloor - i];
    for (int i = 0; i < nUintFloor; ++i) this->x[i] = 0;

    if (remainder == 0) return;
    for (int i = nUint - 1; i > nUintFloor; --i) this->x[i] = (this->x[i] << remainder) | (this->x[i - 1] >> wordSize - remainder);
    this->x[nUintFloor] <<= remainder;
  }

  __host__ __device__ void shiftRightBit(const int N) {

    const unsigned int nUintFloor = N / wordSize;
    //const ungisned int remainder = N - nUintFloor * wordSize;
    const unsigned int remainder = N % wordSize;

    if (N <= 0) return;
    if (nUintFloor >= nUint) {

      this->clear();
      return;
    }

    for (int i = 0; i < nUint - nUintFloor; ++i) this->x[i] = this->x[nUintFloor + i];
    for (int i = nUint - nUintFloor; i < nUint; ++i) this->x[i] = 0;

    if (remainder == 0) return;
    for (int i = 0; i < nUint - nUintFloor - 1; ++i) this->x[i] = (this->x[i] >> remainder) | (this->x[i + 1] << wordSize - remainder);
    this->x[nUint - nUintFloor - 1] >>= remainder;
  }

  __host__ __device__ static BaseField shiftLeftBit(const BaseField& a, const int N) {

    BaseField c = a;
    c.shiftLeftBit(N);
    return c;
  }

  __host__ __device__ static BaseField shiftRightBit(const BaseField& a, const int N) {

    BaseField c = a;
    c.shiftRightBit(N);
    return c;
  }

  __host__ __device__ static void shiftLeftBit(BaseField &c, const BaseField& a, const int N) {

    c = a;
    c.shiftLeftBit(N);
  }

  __host__ __device__ static void shiftRightBit(BaseField &c, const BaseField& a, const int N) {

    c = a;
    c.shiftRightBit(N);
  }

  __host__ __device__ void shiftLeftWordSize(const int N) {

    if (N >= nUint) {

      this->clear();
      return;
    }

    for (int i = 0; i < nUint - N; ++i) this->x[nUint - 1 - i] = this->x[nUint - 1 - N - i];
    for (int i = 0; i < N; ++i) this->x[i] = 0;
  }

  __host__ __device__ void shiftRightWordSize(const int N) {

    if (N >= nUint) {

      this->clear();
      return;
    }

    for (int i = 0; i < nUint - N; ++i) this->x[i] = this->x[N + i];
    for (int i = nUint - N; i < nUint; ++i) this->x[i] = 0;
  }

  __host__ __device__ static
  BaseField shiftLeftWordSize(const BaseField& a, const int N) {

    BaseField c = a;
    c.shiftLeftWordSize(N);
    return c;
  }

  __host__ __device__ static
  BaseField shiftRightWordSize(const BaseField& a, const int N) {

    BaseField c = a;
    c.shiftRightWordSize(N);
    return c;
  }

  __host__ __device__ static
  void shiftLeftWordSize(BaseField& c, const BaseField& a, const int N) {

    c = a;
    c.shiftLeftWordSize(N);
  }

  __host__ __device__ static
  void shiftRightWordSize(BaseField& c, const BaseField& a, const int N) {

    c = a;
    c.shiftRightWordSize(N);
  }

  friend std::ostream& operator<<(std::ostream& os, const BaseField& a) {

    os << std::hex;
    os << "-------------------- data --------------------" << std::endl;
    for (unsigned int i = 0; i < nUint; ++i) os << "x[" << i << "]: 0x" << a.x[i] << std::endl;
    os << "-------------------- data --------------------" << std::endl;
    return os;
  }

  template<int wSize>
  __forceinline__ __host__ __device__
  static uint_type index(const BaseField& a, const unsigned int position) {

    const unsigned int posWS = position / wordSize;
    //const int rem32 = position % 32;

    //const unsigned int remWS = position - wordSize * posWS;
    const unsigned int remWS = position % wordSize;

    //const int end_index = (rem32 + wSize - 1) % 32;
    unsigned int tmp = remWS + wSize - 1;
    unsigned int end_index;
    if (tmp >= wordSize) end_index = tmp - wordSize; /* !!! wSizeはwordSize以下の必要がある !!! */
    else end_index = tmp;

    if (tmp < wordSize) return (a.x[posWS] << wordSize - 1 - end_index) >> wordSize - 1 - wSize + 1;
    else return (a.x[posWS] >> remWS) + ((a.x[posWS + 1] << wordSize - 1 - end_index) >> remWS - end_index - 1);
  }

  __forceinline__ __host__ __device__
  static uint_type index(const BaseField& a, const unsigned int position, const int wSize) {

    const unsigned int posWS = position / wordSize;
    //const int rem32 = position % 32;

    //const unsigned int remWS = position - wordSize * posWS;
    const unsigned int remWS = position % wordSize;

    //const int end_index = (rem32 + wSize - 1) % 32;
    unsigned int tmp = remWS + wSize - 1;
    unsigned int end_index;
    if (tmp >= wordSize) end_index = tmp - wordSize; /* !!! wSizeはwordSize以下の必要がある !!! */
    else end_index = tmp;

    if (tmp < wordSize) return (a.x[posWS] << wordSize - 1 - end_index) >> wordSize - 1 - wSize + 1;
    else return (a.x[posWS] >> remWS) + ((a.x[posWS + 1] << wordSize - 1 - end_index) >> remWS - end_index - 1);
  }

  __forceinline__ __host__ __device__
  static unsigned int pow2func(const int wSize) {

    return static_cast<unsigned int>(1 << wSize);
  }

  __host__ __device__ static void mulShAdd(BaseField& c, const BaseField& a, const BaseField& b);
  __host__ __device__ static void mulCombR2L_uintSize(BaseField& c, const BaseField& a, const BaseField& b);
  __host__ __device__ static void square(BaseField& c, const BaseField& a);

  template<int degree>
  __host__ __device__ static void mulMonomial(BaseField& c, const BaseField& a);
  __host__ __device__ static void mulMonomial_NT(BaseField& c, const BaseField& a, const int degree);

  template<int wSize>
  __forceinline__ __host__ __device__ static void precomputeTable(BaseField *table, const BaseField& a);
  __forceinline__ __host__ __device__ static void precomputeTable(BaseField *table, const BaseField& a, const int wSize);

  template<int wSize>
  __host__ __device__ static void mulCombL2RwithWindow(BaseField& c, const BaseField& a, const BaseField& b);
  __host__ __device__ static void mulCombL2RwithWindow(BaseField& c, const BaseField& a, const BaseField& b, const int wSize);
};

template<class T> class BaseField_Double {

  static const int ext_degree = T::ext_degree;
  static const int nUint = T::nUint;
  static const int wordSize = T::wordSize;
  typedef typename T::uint_type uint_type;

public:

  T low, high;



  __host__ __device__ BaseField_Double() {}

  __host__ __device__ void clear() {

    low.clear();
    high.clear();
  }

  __host__ __device__ void clearUpDeg() {

    low.clearUpDeg();
    high.clearUpDeg();
  }

  __host__ __device__ void reduction(T& c);
};

template<class T> class ExtField_Double {

  static const int ext_degree = T::ext_degree;
  static const int base_degree = T::base_degree;
  static const int nUint = T::nUint;
  static const int wordSize = T::wordSize;
  typedef typename T::uint_type uint_type;

public:

  T low, high;



  __host__ __device__ ExtField_Double() {}

  __host__ __device__ void clear() {

    low.clear();
    high.clear();
  }
};

template<class T, int number> class BaseField_Multiple {

  static const int ext_degree = T::ext_degree;
  static const int nUint = T::nUint;
  static const int wordSize = T::wordSize;
  typedef typename T::uint_type uint_type;

public:

  T xBF[number];
  //T *xBF;



  __host__ __device__ BaseField_Multiple() {}
#if 0
  __host__ __device__ BaseField_Multiple() {
    xBF = new T[number];
  }
  __host__ __device__ ~BaseField_Multiple() {
    if (xBF != NULL) delete[] xBF;
  }
#endif

  __host__ __device__ void clear() {
    for (unsigned int i = 0; i < number; ++i) xBF[i].clear();
  }

  __host__ __device__ void clearUpDeg() {
    for (unsigned int i = 0; i < number; ++i) xBF[i].clearUpDeg();
  }

  void setRandom() {
    for (unsigned int i = 0; i < number; ++i) xBF[i].setRandom();
  }

  __host__ __device__ void plusOne() {
    for (unsigned int i = 0; i < number; ++i) xBF[i].plusOne();
  }

  __forceinline__ __host__ __device__ friend
  bool operator==(const BaseField_Multiple& a, const BaseField_Multiple& b) {

    bool result = true;
    for (unsigned int i = 0; i < number; ++i) result &= (a.xBF[i] == b.xBF[i]);
    return result;
  }
};

template<class T, int number> class BaseField_Double_Multiple {

  static const int ext_degree = T::ext_degree;
  static const int nUint = T::nUint;
  static const int wordSize = T::wordSize;
  typedef typename T::uint_type uint_type;

public:

  T low[number], high[number];
  //T *low, *high;


  __host__ __device__ BaseField_Double_Multiple() {}

#if 0
  __host__ __device__ BaseField_Double_Multiple() {
    low = new T[number];
    high = new T[number];
  }

  __host__ __device__ ~BaseField_Double_Multiple() {
    if (low != NULL) delete[] low;
    if (high != NULL) delete[] high;
  }
#endif

  __host__ __device__ void clear() {
    for (unsigned int i = 0; i < number; ++i) {

      low[i].clear();
      high[i].clear();
    }
  }

  __host__ __device__ void clearUpDeg() {
    for (unsigned int i = 0; i < number; ++i) {

      low[i].clearUpDeg();
      high[i].clearUpDeg();
    }
  }

  void setRandom() {
    for (unsigned int i = 0; i < number; ++i) {

      low[i].setRandom();
      high[i].setRandom();
    }
  }
};
