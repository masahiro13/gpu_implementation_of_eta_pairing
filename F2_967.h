#pragma once

#include "base_field.h"

// $\mathbb F_{2^967}\simeq\mathbb F_2[x]/(x^967+x^36+1)$

typedef BaseField<uint_64, 967> F2_967_u64;


/* !!! dammy !!! */

template<>
__forceinline__ __host__ __device__ F2_967_u64::BaseField() {}

template<>
__forceinline__ __host__ __device__
void BaseField_Double<F2_967_u64>::reduction(F2_967_u64 & c) {

    F2_967_u64 tmp;
    F2_967_u64::uint_type mask = 0x000000000000007f;
    c = this->low;

    c += this->high;
    c += F2_967_u64::shiftLeftBit<94>(this->high);

    tmp = F2_967_u64::shiftRightBit<393>(this->high);
    //F2_967_u64::shiftRightBit<393>(tmp, this->high);
    c += tmp;

    c += F2_967_u64::shiftLeftBit<94>(tmp);
    //F2_967_u64::shiftLeftBit<94>(tmp2, tmp);
    //c += tmp2;
    c.x[F2_967_u64::nUint - 1] &= mask;
}

template<>
__forceinline__ __host__ __device__
//void F2_967_u64::mulShAdd(BaseField& c, const BaseField& a, const BaseField& b) {
void F2_967_u64::mulShAdd(F2_967_u64& c, const F2_967_u64& a, const F2_967_u64& b) {

//#if 0
    if (a.testZero() || b.testZero()) {

        c.clear();
        return;
    }
//#endif

    BaseField_Double<F2_967_u64> result;
    result.clear();

    F2_967_u64 tmp;

    if (a.getBit(0) == 1) result.low = b;

    F2_967_u64 bL = b;
    F2_967_u64 bH = b;
    bL.shiftLeftBit<1>();

    for (int i = 1; i < 967; ++i) {

        if ( a.getBit(i) == 1 ) result.low ^= bL;
        bL.shiftLeftBit<1>();
    }

    bH.shiftRightBit<1>();
    for (int i = 966; i >= 1; --i) {

        if ( a.getBit(i) == 1 ) result.high ^= bH;
        bH.shiftRightBit<1>();
    }

    //F2_967_u64::uint_type mask = 0x0000007fffffffff;
    F2_967_u64::uint_type mask = 0x000000000000007f;
    result.low.x[F2_967_u64::nUint - 1] &= mask;
    result.reduction(c);
}

template<>
__forceinline__ __host__ __device__
void F2_967_u64::mulCombR2L_uintSize(F2_967_u64& c, const F2_967_u64& a, const F2_967_u64& b) {

//#if 0
  if (a.testZero() || b.testZero()) {
    c.clear();
    return;
  }
//#endif

  const int nW = F2_967_u64::nUint;

  BaseField_Double<F2_967_u64> result;
  result.clear();

  //F2_967_u64 tmpL, tmpR;

  F2_967_u64 bL = b;
  F2_967_u64 bH = b;
  F2_967_u64 tmpBF;


  for (int i = 0; i < F2_967_u64::wordSize; ++i) {
    for (int j = 0; j < nW; ++j) {

      /* aは定められた次数より大きい項の係数が0であることを仮定する */
      if ( a.getBit(i + (j * F2_967_u64::wordSize)) == 1 ) {

        //result.low ^= F2_967_u64::shiftWordRegSize(bL, j);
        F2_967_u64::shiftLeftWordSize(tmpBF, bL, j);
        result.low ^= tmpBF;
      }
    }

    bL.shiftLeftBit<1>();
  }

  /* 上位1ビットを空けてbの先頭係数を代入しておくと下記の実装でO.K. */
  bH.shiftLeftBit<24>();
  for (int i = F2_967_u64::wordSize - 1; i >= 0; --i) {

    for (int j = 0; j < nW; ++j) {

      /* aは定められた次数より大きい項の係数が0であることを仮定する */
      if ( a.getBit(i + (j * F2_967_u64::wordSize)) == 1 ) {

        //result.high ^= F2_967_u64::shiftRightWordSize(bH, nW - j - 1);
        F2_967_u64::shiftRightWordSize(tmpBF, bH, nW - j - 1);
        result.high ^= tmpBF;
      }
    }

    bH.shiftRightBit<1>();
  }

  //result.clearUpDeg();
  //result.low.clearUpDeg();

  //F2_967_u64::uint_type mask = 0x0000007fffffffff;
  F2_967_u64::uint_type mask = 0x000000000000007f;
  result.low.x[F2_967_u64::nUint - 1] &= mask;
  result.reduction(c);
}

template<>
__forceinline__ __host__ __device__ void F2_967_u64::square(F2_967_u64& c, const F2_967_u64& a) {

//#if 0
  if (a.testZero()) {

    c.clear();
    return;
  }
//#endif

  int tmpN, tmpR;
  BaseField_Double<F2_967_u64> result;
  result.clear();

#if 1

  uint64_t one64 = 1;
  uint64_t lowArr[16], highArr[16];
  for (int i = 0; i < 16; ++i) {
    lowArr[i] = 0;
    highArr[i] = 0;
  }

  // low; index = 0 to 483
  for (int i = 0; i < 484; ++i) {

    //if (getBit32(arr32, i) == 1) lowArr[(2 * i) / 32] ^= 1 << ((2 * i) % 32);

    tmpN = (2 * i) / 64;
    tmpR = (2 * i) - (64 * tmpN);
    if (a.getBit(i) == 1) lowArr[tmpN] ^= (one64 << tmpR);
  }

  // high; index = 484 to 966
  for (int i = 0; i < 483; ++i) {

    //if (getBit32(arr32, i + 244) == 1) highArr[(2 * i + 1) / 32] ^= 1 << ((2 * i + 1) % 32); /* 1!! 487で区切っているため，highの0番目のビットは0（x^487の係数）から始まる !!!*/

    tmpN = (2 * i + 1) / 64;
    tmpR = (2 * i + 1) - (64 * tmpN);
    if (a.getBit(i + 484) == 1) highArr[tmpN] ^= (one64 << tmpR);
  }

  for (int i = 0; i < 16; ++i) {
    result.low.x[i] = lowArr[i];
    result.high.x[i] = highArr[i];
  }

#else

    // low; index = 0 to 243
    for (int i = 0; i < 244; ++i) {

        if (getBit32(arr32, i) == 1) result.low.setBit1(2 * i);
    }

    // high; index = 244 to 486
    for (int i = 0; i < 243; ++i) {

        if (getBit32(arr32, i + 244) == 1) result.high.setBit1(2 * i + 1); /* 1!! 487で区切っているため，highの0番目のビットは0（x^487の係数）から始まる !!!*/
    }

#endif

  // no need to clearUpDeg
  result.reduction(c);
}

template<>
template<int degree>
__forceinline__ __host__ __device__
void F2_967_u64::mulMonomial(F2_967_u64& c, const F2_967_u64& a) {

  //#if 0
  if (a.testZero()) {

    c.clear();
    return;
  }
  //#endif

  BaseField_Double<F2_967_u64> result;
  result.clear();

  result.low.copy(F2_967_u64::shiftLeftBit<degree>(a));
  result.high.copy(F2_967_u64::shiftRightBit<967 - degree>(a));

  //result.clearUpDeg();
  //result.low.clearUpDeg();

  //F2_967_u64::uint_type mask = 0x0000007fffffffff;
  F2_967_u64::uint_type mask = 0x000000000000007f;
  result.low.x[F2_967_u64::nUint - 1] &= mask;

  result.reduction(c);
}

template<>
__forceinline__ __host__ __device__
void F2_967_u64::mulMonomial_NT(F2_967_u64& c, const F2_967_u64& a, const int degree) {

  //#if 0
  if (a.testZero()) {

    c.clear();
    return;
  }
  //#endif

  BaseField_Double<F2_967_u64> result;
  result.clear();

  result.low.copy(F2_967_u64::shiftLeftBit(a, degree));
  result.high.copy(F2_967_u64::shiftRightBit(a, 487 - degree));

  //result.clearUpDeg();
  //result.low.clearUpDeg();

  //F2_967_u64::uint_type mask = 0x0000007fffffffff;
  F2_967_u64::uint_type mask = 0x000000000000007f;
  result.low.x[F2_967_u64::nUint - 1] &= mask;

  result.reduction(c);
}

/* Comb multiplication with window method */
/*
precompute table for multiplication with window of width wSize

     coefficient | a0 a1 a2 a3 a4
index            |
---------------------------------
  0              |  0  0  0  0  0
  1              |  1  0  0  0  0
  2              |  0  1  0  0  0
  3              |  1  1  0  0  0
- - - - - - - - - - - - - - - - -
  4              |  0  0  1  0  0
  5              |  1  0  1  0  0
  6              |  0  1  1  0  0
  7              |  1  1  1  0  0
 -  -  -  -  -  -  -  -  -  -  -
  8              |  0  0  0  1  0
  9              |  1  0  0  1  0
 10              |  0  1  0  1  0
 11              |  1  1  0  1  0
 12              |  0  0  1  1  0
 13              |  1  0  1  1  0
 14              |  0  1  1  1  0
 15              |  1  1  1  1  0
  -   -   -   -   -   -   -   -
 16              |  0  0  0  0  1
 17              |  1  0  0  0  1
 18              |  0  1  0  0  1
 19              |  1  1  0  0  1
 20              |  0  0  1  0  1
 21              |  1  0  1  0  1
 22              |  0  1  1  0  1
 23              |  1  1  1  0  1
 24              |  0  0  0  1  1
 25              |  1  0  0  1  1
 26              |  0  1  0  1  1
 27              |  1  1  0  1  1
 28              |  0  0  1  1  1
 29              |  1  0  1  1  1
 30              |  0  1  1  1  1
 31              |  1  1  1  1  1
---------------------------------
 */

template<>
template<int wSize>
__forceinline__ __host__ __device__ void F2_967_u64::precomputeTable(F2_967_u64 *table, const F2_967_u64& a) {

  for (int i = 0; i < power2<wSize>::num; ++i) table[i].clear();
  table[1].copy(a);

  if (wSize > 1) {

    F2_967_u64::mulMonomial<1>(table[2], a); // a * x
    table[3] = table[1] ^ table[2];
  }

  if (wSize > 2) {

    F2_967_u64::mulMonomial<2>(table[4], a); // a * x^2
    table[5] = table[1] ^ table[4];
    table[6] = table[2] ^ table[4];
    table[7] = table[3] ^ table[4];
  }

  if (wSize > 3) {

    F2_967_u64::mulMonomial<3>(table[8], a); // a * x^3
    for (int i = 1; i < 8; ++i) table[8 + i] = table[i] ^ table[8];
  }

  if (wSize > 4) {

    F2_967_u64::mulMonomial<4>(table[16], a); // a * x^4
    for (int i = 1; i < 16; ++i) table[16 + i] = table[i] ^ table[16];
  }

  if (wSize > 5) {

    F2_967_u64::mulMonomial<5>(table[32], a); // a * x^5
    for (int i = 1; i < 32; ++i) table[32 + i] = table[i] ^ table[32];
  }

  if (wSize > 6) {

    F2_967_u64::mulMonomial<6>(table[64], a); // a * x^6
    for (int i = 1; i < 64; ++i) table[64 + i] = table[i] ^ table[64];
  }

  if (wSize > 7) {

    F2_967_u64::mulMonomial<7>(table[128], a); // a * x^7
    for (int i = 1; i < 128; ++i) table[128 + i] = table[i] ^ table[128];
  }

  if (wSize > 8) {

    F2_967_u64::mulMonomial<8>(table[256], a); // a * x^8
    for (int i = 1; i < 256; ++i) table[256 + i] = table[i] ^ table[256];
  }
}

template<>
__forceinline__ __host__ __device__ void F2_967_u64::precomputeTable(F2_967_u64 *table, const F2_967_u64& a, const int wSize) {

  for (int i = 0; i < F2_967_u64::pow2func(wSize); ++i) table[i].clear();
  table[1].copy(a);

  if (wSize > 1) {

    F2_967_u64::mulMonomial<1>(table[2], a); // a * x
    table[3] = table[1] ^ table[2];
  }

  if (wSize > 2) {

    F2_967_u64::mulMonomial<2>(table[4], a); // a * x^2
    table[5] = table[1] ^ table[4];
    table[6] = table[2] ^ table[4];
    table[7] = table[3] ^ table[4];
  }

  if (wSize > 3) {

    F2_967_u64::mulMonomial<3>(table[8], a); // a * x^3
    for (int i = 1; i < 8; ++i) table[8 + i] = table[i] ^ table[8];
  }

  if (wSize > 4) {

    F2_967_u64::mulMonomial<4>(table[16], a); // a * x^4
    for (int i = 1; i < 16; ++i) table[16 + i] = table[i] ^ table[16];
  }

  if (wSize > 5) {

    F2_967_u64::mulMonomial<5>(table[32], a); // a * x^5
    for (int i = 1; i < 32; ++i) table[32 + i] = table[i] ^ table[32];
  }

  if (wSize > 6) {

    F2_967_u64::mulMonomial<6>(table[64], a); // a * x^6
    for (int i = 1; i < 64; ++i) table[64 + i] = table[i] ^ table[64];
  }

  if (wSize > 7) {

    F2_967_u64::mulMonomial<7>(table[128], a); // a * x^7
    for (int i = 1; i < 128; ++i) table[128 + i] = table[i] ^ table[128];
  }

  if (wSize > 8) {

    F2_967_u64::mulMonomial<8>(table[256], a); // a * x^8
    for (int i = 1; i < 256; ++i) table[256 + i] = table[i] ^ table[256];
  }
}

/* 最適化時inline展開するとコード生成にかなりの時間が掛かる */
template<>
template<int wSize>
//__forceinline __host__ __device__ void F2_967_u64::mulCombL2RwithWindow(F2_967_u64& c, const F2_967_u64& a, const F2_967_u64& b) {
void __host__ __device__ F2_967_u64::mulCombL2RwithWindow(F2_967_u64& c, const F2_967_u64& a, const F2_967_u64& b) {

  if (a.testZero() || b.testZero()) {

    c.clear();
    //puts("MASA");
    return;
  }

  //static const int nW = (487 + F2_967_u64::regLength - 1) / F2_967_u64::regLength;
  const int nW = F2_967_u64::nUint;
  const int n_wSize = F2_967_u64::wordSize / wSize;
  //static const int rem_wSize = F2_967_u64::regLength % wSize;
  const int rem_wSize = F2_967_u64::wordSize - wSize * n_wSize;

  BaseField_Double<F2_967_u64> result;
  result.clear();

  F2_967_u64 bTable[power2<wSize>::num];

  //F2_967_u64  tmpL, tmpH;

  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  //F2_967_u64 bTable[power2Arr[wSize]];
  precomputeTable<wSize>(bTable, b);

  /*
  F2_BaseField_Doubleを二つのF2_967_u64型の値を持つものとは見做さず，low値はフルで512 bit使い，
  最後のreductionの前にshiftして整形する
  */

  // remainder
  if (rem_wSize != 0) {
    for (int j = 0; j < nW; ++j) {

        result.low ^= F2_967_u64::shiftLeftWordSize(bTable[F2_967_u64::index<rem_wSize>(a, n_wSize*wSize + j*F2_967_u64::wordSize)], j);
        //F2_967_u64::shiftLeftWordSize(tmpL, bTable[index<rem_wSize>(arr32, n_wSize*wSize + j*F2_967_u64::wordSize)], j);
        //result.low ^= tmpL;

        result.high ^= F2_967_u64::shiftRightWordSize(bTable[F2_967_u64::index<rem_wSize>(a, n_wSize*wSize + j*F2_967_u64::wordSize)], nW - j);
        //F2_967_u64::shiftRightWordSize(tmpH, bTable[index<rem_wSize>(arr32, n_wSize*wSize + j*F2_967_u64::wordSize)], nW - j);
        //result.high ^= tmpH;
    }

    // result *= x^wSize (Left to Right Comb method)
    result.high.shiftLeftBit<wSize>();

    result.high ^= F2_967_u64::shiftRightBit<512 - wSize>(result.low); /*上記理由から487でなく512*/
    //F2_967_u64::shiftRightBit<512 - wSize>(tmpH, result.low);
    //result.high ^= tmpH;

    result.low.shiftLeftBit<wSize>();
  }

  for (int i = n_wSize-1; i >= 0; --i) {
    for (int j = 0; j < nW; ++j) {

      result.low ^= F2_967_u64::shiftLeftWordSize(bTable[F2_967_u64::index<wSize>(a, i*wSize+j*F2_967_u64::wordSize)], j);
      //F2_967_u64::shiftLeftWordSize(tmpL, bTable[index<wSize>(arr32, i*wSize + j*F2_967_u64::wordSize)], j);
      //result.low ^= tmpL;

      result.high ^= F2_967_u64::shiftRightWordSize(bTable[F2_967_u64::index<wSize>(a, i*wSize+j*F2_967_u64::wordSize)], nW - j);
      //F2_967_u64::shiftRightWordSize(tmpH, bTable[index<wSize>(arr32, i*wSize + j*F2_967_u64::wordSize)], nW - j);
      //result.high ^= tmpH;
    }

    if (i != 0) {

      // result *= x^wSize (Left to Right Comb method)
      result.high.shiftLeftBit<wSize>();

      result.high ^= F2_967_u64::shiftRightBit<512 - wSize>(result.low); /*上記理由から487でなく512*/
      //F2_967_u64::shiftRightBit<512 - wSize>(tmpH, result.low);
      //result.high ^= tmpH;

      result.low.shiftLeftBit<wSize>();
    }
  }

  /*
  F2_BaseField_Double<F2_967_u64>としての表現に合わせて整形
  */
  /* 25ではない! 要修正! */
  result.high.shiftLeftBit<25>();

  result.high ^= F2_967_u64::shiftRightBit<967>(result.low);
  //F2_967_u64::shiftRightBit<487>(tmpH, result.low);
  //result.high ^= tmpH;

  // clear coefficients of upper degree
  //F2_967_u64::uint_type mask = 0x0000007fffffffff;
  F2_967_u64::uint_type mask = 0x000000000000007f;
  result.low.x[F2_967_u64::nUint - 1] &= mask;

  //result.clearUpDeg();
  //result.low.clearUpDeg();

  result.reduction(c);
}



/* !! 何故かtemplate函数のものと計算結果合わず..? !! */
/* duplicated symbol error 注意 */
template<>
void __forceinline__ __host__ __device__ F2_967_u64::mulCombL2RwithWindow(F2_967_u64& c, const F2_967_u64& a, const F2_967_u64& b, const int wSize) {

  if (a.testZero() || b.testZero()) {

    c.clear();
    return;
  }

  //static const int nW = (487 + F2_967_u64::regLength - 1) / F2_967_u64::regLength;
  const int nW = F2_967_u64::nUint;
  const int n_wSize = F2_967_u64::wordSize / wSize;
  //static const int rem_wSize = F2_967_u64::regLength % wSize;
  const int rem_wSize = F2_967_u64::wordSize - wSize * n_wSize;

  BaseField_Double<F2_967_u64> result;
  result.clear();

  //F2_967_u64 bTable[power2Arr[wSize]];
  F2_967_u64 *bTable = new F2_967_u64[power2Arr[wSize]];

  //F2_967_u64  tmpL, tmpH;

  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  //F2_967_u64 bTable[power2Arr[wSize]];
  precomputeTable(bTable, b, wSize);

  /*
  F2_BaseField_Doubleを二つのF2_967_u64型の値を持つものとは見做さず，low値はフルで512 bit使い，
  最後のreductionの前にshiftして整形する
  */

  // remainder
  if (rem_wSize != 0) {
    for (int j = 0; j < nW; ++j) {

        result.low ^= F2_967_u64::shiftLeftWordSize(bTable[F2_967_u64::index(a, n_wSize*wSize + j*F2_967_u64::wordSize, rem_wSize)], j);
        //F2_967_u64::shiftLeftWordSize(tmpL, bTable[index<rem_wSize>(arr32, n_wSize*wSize + j*F2_967_u64::wordSize)], j);
        //result.low ^= tmpL;

        result.high ^= F2_967_u64::shiftRightWordSize(bTable[F2_967_u64::index(a, n_wSize*wSize + j*F2_967_u64::wordSize, rem_wSize)], nW - j);
        //F2_967_u64::shiftRightWordSize(tmpH, bTable[index<rem_wSize>(arr32, n_wSize*wSize + j*F2_967_u64::wordSize)], nW - j);
        //result.high ^= tmpH;
    }

    // result *= x^wSize (Left to Right Comb method)
    result.high.shiftLeftBit(wSize);

    result.high ^= F2_967_u64::shiftRightBit(result.low, 512 - wSize); /*上記理由から487でなく512*/
    //F2_967_u64::shiftRightBit<512 - wSize>(tmpH, result.low);
    //result.high ^= tmpH;

    result.low.shiftLeftBit(wSize);
  }

  for (int i = n_wSize-1; i >= 0; --i) {
    for (int j = 0; j < nW; ++j) {

      result.low ^= F2_967_u64::shiftLeftWordSize(bTable[F2_967_u64::index(a, i*wSize+j*F2_967_u64::wordSize, wSize)], j);
      //F2_967_u64::shiftLeftWordSize(tmpL, bTable[index<wSize>(arr32, i*wSize + j*F2_967_u64::wordSize)], j);
      //result.low ^= tmpL;

      result.high ^= F2_967_u64::shiftRightWordSize(bTable[F2_967_u64::index(a, i*wSize+j*F2_967_u64::wordSize, wSize)], nW - j);
      //F2_967_u64::shiftRightWordSize(tmpH, bTable[index<wSize>(arr32, i*wSize + j*F2_967_u64::wordSize)], nW - j);
      //result.high ^= tmpH;
    }

    if (i != 0) {

      // result *= x^wSize (Left to Right Comb method)
      result.high.shiftLeftBit(wSize);

      result.high ^= F2_967_u64::shiftRightBit(result.low, 512 - wSize); /*上記理由から487でなく512*/
      //F2_967_u64::shiftRightBit<512 - wSize>(tmpH, result.low);
      //result.high ^= tmpH;

      result.low.shiftLeftBit(wSize);
    }
  }

  /*
  F2_BaseField_Double<F2_967_u64>としての表現に合わせて整形
  */
  /* 25ではない! 要修正! */
  result.high.shiftLeftBit<25>();

  result.high ^= F2_967_u64::shiftRightBit<967>(result.low);
  //F2_967_u64::shiftRightBit<487>(tmpH, result.low);
  //result.high ^= tmpH;

  // clear coefficients of upper degree
  //F2_967_u64::uint_type mask = 0x0000007fffffffff;
  F2_967_u64::uint_type mask = 0x000000000000007f;
  result.low.x[F2_967_u64::nUint - 1] &= mask;

  //result.clearUpDeg();
  //result.low.clearUpDeg();

  result.reduction(c);
  delete[] bTable;
}
