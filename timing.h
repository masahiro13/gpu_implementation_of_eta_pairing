#pragma once

#include "pairing.h"
#include "pairingGpu.h"
#include "dummy_pairingGpu.h"

template<int wSize, int number>
void pairing487Multiple_getTiming() {

  const int Count = 1;
  struct timeval start, end;
  double timing;

  //const int wSize = 5;

  BaseField_Multiple<F2_487_u64, number> xP, xQ, yP, yQ;
  ExtField6_2_Multiple<F2_487_u64, number> pairing6_2;
  ExtField3_2_2_Multiple<F2_487_u64, number> pairing3_2_2;

  F2_487_u64 xP_H, xQ_H, yP_H, yQ_H;
  ExtField6_2<F2_487_u64> pairing6_2_H;
  ExtField3_2_2<F2_487_u64> pairing3_2_2_H;



  // ext6_2 host
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {
    for (unsigned int j = 0; j < number; ++j) {
      xP_H.setRandom();
      xQ_H.setRandom();
      yP_H.setRandom();
      yQ_H.setRandom();

      gettimeofday(&start, 0);
      pairing6_2_H = etaT_pairing6_2<F2_487_u64, wSize>(xP_H, xQ_H, yP_H, yQ_H);
      gettimeofday(&end, 0);
      timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
    }
  }
  std::cout << number << "    " << timing / Count / 1.0e+6 << ",    " << timing / Count / 1.0e+6 / number << "        ";



  // ext6_2 GPU
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP.setRandom();
    xQ.setRandom();
    yP.setRandom();
    yQ.setRandom();

    gettimeofday(&start, 0);
    pairing6_2 = pairing6_2_multipleP<F2_487_u64, wSize, number>(xP, xQ, yP, yQ);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << timing / Count / 1.0e+6 << ",    "  << timing / Count / 1.0e+6 / number << "        ";

  // ext3_2_2 host
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {
    for (unsigned int j = 0; j < number; ++j) {
      xP_H.setRandom();
      xQ_H.setRandom();
      yP_H.setRandom();
      yQ_H.setRandom();

      gettimeofday(&start, 0);
      pairing3_2_2_H = etaT_pairing3_2_2<F2_487_u64, wSize>(xP_H, xQ_H, yP_H, yQ_H);
      gettimeofday(&end, 0);
      timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
    }
  }
  std::cout << timing / Count / 1.0e+6 << ",    " << timing / Count / 1.0e+6 / number << "        ";

  // ext3_2_2 GPU
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP.setRandom();
    xQ.setRandom();
    yP.setRandom();
    yQ.setRandom();

    gettimeofday(&start, 0);
    pairing3_2_2 = pairing3_2_2_multipleP<F2_487_u64, wSize, number>(xP, xQ, yP, yQ);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << timing / Count / 1.0e+6 << ",    " << timing / Count / 1.0e+6 / number << "        " << std::endl;
}



template<int wSize, int number>
void pairing967Multiple_getTiming() {

  const int Count = 1;
  struct timeval start, end;
  double timing;

  //const int wSize = 5;

  BaseField_Multiple<F2_967_u64, number> xP, xQ, yP, yQ;
  ExtField6_2_Multiple<F2_967_u64, number> pairing6_2;
  ExtField3_2_2_Multiple<F2_967_u64, number> pairing3_2_2;

  F2_967_u64 xP_H, xQ_H, yP_H, yQ_H;
  ExtField6_2<F2_967_u64> pairing6_2_H;
  ExtField3_2_2<F2_967_u64> pairing3_2_2_H;



  // m=967
  // ext6_2 host
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {
    for (unsigned int j = 0; j < number; ++j) {
      xP_H.setRandom();
      xQ_H.setRandom();
      yP_H.setRandom();
      yQ_H.setRandom();

      gettimeofday(&start, 0);
      pairing6_2_H = etaT_pairing6_2<F2_967_u64, wSize>(xP_H, xQ_H, yP_H, yQ_H);
      gettimeofday(&end, 0);
      timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
    }
  }
  std::cout << number << "    " << timing / Count / 1.0e+6 << ",    " << timing / Count / 1.0e+6 / number << "        ";



  // ext6_2 GPU
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP.setRandom();
    xQ.setRandom();
    yP.setRandom();
    yQ.setRandom();

    gettimeofday(&start, 0);
    pairing6_2 = pairing6_2_multipleP<F2_967_u64, wSize, number>(xP, xQ, yP, yQ);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << timing / Count / 1.0e+6 << ",    "  << timing / Count / 1.0e+6 / number << "        ";


  // ext3_2_2 host
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {
    for (unsigned int j = 0; j < number; ++j) {
      xP_H.setRandom();
      xQ_H.setRandom();
      yP_H.setRandom();
      yQ_H.setRandom();

      gettimeofday(&start, 0);
      pairing3_2_2_H = etaT_pairing3_2_2<F2_967_u64, wSize>(xP_H, xQ_H, yP_H, yQ_H);
      gettimeofday(&end, 0);
      timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
    }
  }
  std::cout << timing / Count / 1.0e+6 << ",    "  << timing / Count / 1.0e+6 / number << "        ";

  // ext3_2_2 GPU
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP.setRandom();
    xQ.setRandom();
    yP.setRandom();
    yQ.setRandom();

    gettimeofday(&start, 0);
    pairing3_2_2 = pairing3_2_2_multipleP<F2_967_u64, wSize, number>(xP, xQ, yP, yQ);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << timing / Count / 1.0e+6 << ",    "  << timing / Count / 1.0e+6 / number << "        " << std::endl;
}

template<int wSize, int number>
void dummy_pairing487Multiple_getTiming() {

  const int Count = 1;
  struct timeval start, end;
  double timing;

  BaseField_Multiple<F2_487_u64, number> xP, xQ, yP, yQ;
  ExtField6_2_Multiple<F2_487_u64, number> pairing6_2;
  ExtField3_2_2_Multiple<F2_487_u64, number> pairing3_2_2;



  // ext6_2 GPU
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP.setRandom();
    xQ.setRandom();
    yP.setRandom();
    yQ.setRandom();

    gettimeofday(&start, 0);
    pairing6_2 = dummy_pairing6_2_multipleP<F2_487_u64, wSize, number>(xP, xQ, yP, yQ);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << number << "    " << timing / Count / 1.0e+6 << ",    "  << timing / Count / 1.0e+6 / number << "        ";



  // ext3_2_2 GPU
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP.setRandom();
    xQ.setRandom();
    yP.setRandom();
    yQ.setRandom();

    gettimeofday(&start, 0);
    pairing3_2_2 = dummy_pairing3_2_2_multipleP<F2_487_u64, wSize, number>(xP, xQ, yP, yQ);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << timing / Count / 1.0e+6 << ",    " << timing / Count / 1.0e+6 / number << "        " << std::endl;
}



template<int wSize, int number>
void dummy_pairing967Multiple_getTiming() {

  const int Count = 1;
  struct timeval start, end;
  double timing;

  BaseField_Multiple<F2_967_u64, number> xP, xQ, yP, yQ;
  ExtField6_2_Multiple<F2_967_u64, number> pairing6_2;
  ExtField3_2_2_Multiple<F2_967_u64, number> pairing3_2_2;



  // ext6_2 GPU
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP.setRandom();
    xQ.setRandom();
    yP.setRandom();
    yQ.setRandom();

    gettimeofday(&start, 0);
    pairing6_2 = dummy_pairing6_2_multipleP<F2_967_u64, wSize, number>(xP, xQ, yP, yQ);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << number << "    " << timing / Count / 1.0e+6 << ",    "  << timing / Count / 1.0e+6 / number << "        ";



  // ext3_2_2 GPU
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP.setRandom();
    xQ.setRandom();
    yP.setRandom();
    yQ.setRandom();

    gettimeofday(&start, 0);
    pairing3_2_2 = dummy_pairing3_2_2_multipleP<F2_967_u64, wSize, number>(xP, xQ, yP, yQ);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << timing / Count / 1.0e+6 << ",    "  << timing / Count / 1.0e+6 / number << "        " << std::endl;
}



template<int wSize, int number>
void pairing487Host_getTiming() {

  const int Count = 1;
  struct timeval start, end;
  double timing;



  F2_487_u64 xP_H, xQ_H, yP_H, yQ_H;
  ExtField6_2<F2_487_u64> pairing6_2_H;
  ExtField3_2_2<F2_487_u64> pairing3_2_2_H;



  // ext6_2 host
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {
    for (unsigned int j = 0; j < number; ++j) {
      xP_H.setRandom();
      xQ_H.setRandom();
      yP_H.setRandom();
      yQ_H.setRandom();

      gettimeofday(&start, 0);
      pairing6_2_H = etaT_pairing6_2<F2_487_u64, wSize>(xP_H, xQ_H, yP_H, yQ_H);
      gettimeofday(&end, 0);
      timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
    }
  }
  std::cout << number << "    " << timing / Count / 1.0e+6 << ",    " << timing / Count / 1.0e+6 / number << "        ";



  // ext3_2_2 host
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {
    for (unsigned int j = 0; j < number; ++j) {
      xP_H.setRandom();
      xQ_H.setRandom();
      yP_H.setRandom();
      yQ_H.setRandom();

      gettimeofday(&start, 0);
      pairing3_2_2_H = etaT_pairing3_2_2<F2_487_u64, wSize>(xP_H, xQ_H, yP_H, yQ_H);
      gettimeofday(&end, 0);
      timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
    }
  }
  std::cout << timing / Count / 1.0e+6 << ",    " << timing / Count / 1.0e+6 / number << "        " << std::endl;
}



template<int wSize, int number>
void pairing967Host_getTiming() {

  const int Count = 1;
  struct timeval start, end;
  double timing;



  F2_967_u64 xP_H, xQ_H, yP_H, yQ_H;
  ExtField6_2<F2_967_u64> pairing6_2_H;
  ExtField3_2_2<F2_967_u64> pairing3_2_2_H;



  // m=967
  // ext6_2 host
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {
    for (unsigned int j = 0; j < number; ++j) {
      xP_H.setRandom();
      xQ_H.setRandom();
      yP_H.setRandom();
      yQ_H.setRandom();

      gettimeofday(&start, 0);
      pairing6_2_H = etaT_pairing6_2<F2_967_u64, wSize>(xP_H, xQ_H, yP_H, yQ_H);
      gettimeofday(&end, 0);
      timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
    }
  }
  std::cout << number << "    " << timing / Count / 1.0e+6 << ",    " << timing / Count / 1.0e+6 / number << "        ";



  // ext3_2_2 host
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {
    for (unsigned int j = 0; j < number; ++j) {
      xP_H.setRandom();
      xQ_H.setRandom();
      yP_H.setRandom();
      yQ_H.setRandom();

      gettimeofday(&start, 0);
      pairing3_2_2_H = etaT_pairing3_2_2<F2_967_u64, wSize>(xP_H, xQ_H, yP_H, yQ_H);
      gettimeofday(&end, 0);
      timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
    }
  }
  std::cout << timing / Count / 1.0e+6 << ",    "  << timing / Count / 1.0e+6 / number << "        " << std::endl;
}



template<int wSize, int number>
void dummy_pairingMultiple_getTiming() {

  const int Count = 1;
  struct timeval start, end;
  double timing;


  BaseField_Multiple<F2_487_u64, number> xP, xQ, yP, yQ;
  ExtField6_2_Multiple<F2_487_u64, number> pairing6_2;
  ExtField3_2_2_Multiple<F2_487_u64, number> pairing3_2_2;

  BaseField_Multiple<F2_967_u64, number> xP9, xQ9, yP9, yQ9;
  ExtField6_2_Multiple<F2_967_u64, number> pairing6_29;
  ExtField3_2_2_Multiple<F2_967_u64, number> pairing3_2_29;



  // m=487
  // ext6_2 GPU
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP.setRandom();
    xQ.setRandom();
    yP.setRandom();
    yQ.setRandom();

    gettimeofday(&start, 0);
    pairing6_2 = dummy_pairing6_2_multipleP<F2_487_u64, wSize, number>(xP, xQ, yP, yQ);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << number << "    "  << timing / Count / 1.0e+6 << ",    "  << timing / Count / 1.0e+6 / number << "        ";


  // ext3_2_2 GPU
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP.setRandom();
    xQ.setRandom();
    yP.setRandom();
    yQ.setRandom();

    gettimeofday(&start, 0);
    pairing3_2_2 = dummy_pairing3_2_2_multipleP<F2_487_u64, wSize, number>(xP, xQ, yP, yQ);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << timing / Count / 1.0e+6 << ",    " << timing / Count / 1.0e+6 / number << "        ";



  // m=967
  // ext6_2 GPU
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP9.setRandom();
    xQ9.setRandom();
    yP9.setRandom();
    yQ9.setRandom();

    gettimeofday(&start, 0);
    pairing6_29 = dummy_pairing6_2_multipleP<F2_967_u64, wSize, number>(xP9, xQ9, yP9, yQ9);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << timing / Count / 1.0e+6 << ",    "  << timing / Count / 1.0e+6 / number << "        ";


  // ext3_2_2 GPU
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP9.setRandom();
    xQ9.setRandom();
    yP9.setRandom();
    yQ9.setRandom();

    gettimeofday(&start, 0);
    pairing3_2_29 = dummy_pairing3_2_2_multipleP<F2_967_u64, wSize, number>(xP9, xQ9, yP9, yQ9);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << timing / Count / 1.0e+6 << ",    " << timing / Count / 1.0e+6 / number << "        " << std::endl;
}
