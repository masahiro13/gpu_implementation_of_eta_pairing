#include <stdio.h>
#include <sys/time.h>
//#include <gmp.h>
#include <stdint.h>
//#include <stdbool.h>

#include <iostream>
#include "Declaration.h"



// test用
__global__ void test_kernel() {}

/*** 
shared memory. 各ブロックでコピーが生成される（kernel函数で使用される全ての領域を確保するのではなく1 block分で確保すれば良い）．
恐らく1 block当り16KBしかない．
***/
/*** 多項式演算のGPU実装 ***/

__global__ void mul_uint64(uint64_t *C, uint64_t *A, uint64_t *B) {

  *C = *A * *B;
}

__global__ void mul_uint64High(uint64_t *C, uint64_t *A, uint64_t *B) {

  asm("mul.hi.u64 %0, %1, %2;"
      : "=l"(*C)
      : "l"(*A), "l"(*B));
}

__global__ void mul_uint32Wide(uint64_t *C, uint32_t *A, uint32_t *B) {

  asm("mul.wide.u32 %0, %1, %2;"
      : "=l"(*C)
      : "r"(*A), "r"(*B));
}

__global__ void mul_uint32(uint32_t *C, uint32_t *A, uint32_t *B) {

  *C = *A * *B;
}



extern "C"
void mulGPU64() {

  uint64_t a, b, c;
  a = 0xffffffffffffffff;
  //b = 0xffffffffffffffff;
  b = 0x0000000000000100;

  uint64_t *a_dev, *b_dev, *c_dev;

  cudaMalloc((void**)&a_dev, sizeof(uint64_t));
  cudaMalloc((void**)&b_dev, sizeof(uint64_t));
  cudaMalloc((void**)&c_dev, sizeof(uint64_t));

  cudaMemcpy(a_dev, &a, sizeof(uint64_t), cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, &b, sizeof(uint64_t), cudaMemcpyHostToDevice);

  dim3 blocks(1,1);
  dim3 threads(1,1);

  mul_uint64<<< blocks, threads >>>(c_dev,a_dev,b_dev);

  cudaMemcpy(&c, c_dev, sizeof(uint64_t), cudaMemcpyDeviceToHost);

  std::cout << std::hex;
  std::cout << "c: " << c << std::endl;

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(c_dev);  
}

extern "C"
void mulGPU64High() {

  uint64_t a, b, c;
  a = 0xffffffffffffffff;
  //b = 0xffffffffffffffff;
  b = 0x0000000000000100;

  uint64_t *a_dev, *b_dev, *c_dev;

  cudaMalloc((void**)&a_dev, sizeof(uint64_t));
  cudaMalloc((void**)&b_dev, sizeof(uint64_t));
  cudaMalloc((void**)&c_dev, sizeof(uint64_t));

  cudaMemcpy(a_dev, &a, sizeof(uint64_t), cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, &b, sizeof(uint64_t), cudaMemcpyHostToDevice);

  dim3 blocks(1,1);
  dim3 threads(1,1);

  mul_uint64High<<< blocks, threads >>>(c_dev,a_dev,b_dev);

  cudaMemcpy(&c, c_dev, sizeof(uint64_t), cudaMemcpyDeviceToHost);

  std::cout << std::hex;
  std::cout << "c: " << c << std::endl;

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(c_dev);  
}

extern "C"
void mulGPU32Wide() {

  uint32_t a, b;
  uint64_t c;

  a = 0xffffffff;
  //b = 0xffffffff;
  b = 0x00000010;

  uint32_t *a_dev, *b_dev;
  uint64_t *c_dev;

  cudaMalloc((void**)&a_dev, sizeof(uint32_t));
  cudaMalloc((void**)&b_dev, sizeof(uint32_t));
  cudaMalloc((void**)&c_dev, sizeof(uint64_t));

  cudaMemcpy(a_dev, &a, sizeof(uint32_t), cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, &b, sizeof(uint32_t), cudaMemcpyHostToDevice);

  dim3 blocks(1,1);
  dim3 threads(1,1);

  mul_uint32Wide<<< blocks, threads >>>(c_dev,a_dev,b_dev);

  cudaMemcpy(&c, c_dev, sizeof(uint64_t), cudaMemcpyDeviceToHost);

  std::cout << std::hex;
  std::cout << "c: " << c << std::endl;

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(c_dev);  
}

extern "C"
void mulGPU32() {

  uint32_t a, b, c;
  a = 0xffffffff;
  //b = 0xffffffff;
  b = 0x00000010;

  uint32_t *a_dev, *b_dev, *c_dev;

  cudaMalloc((void**)&a_dev, sizeof(uint32_t));
  cudaMalloc((void**)&b_dev, sizeof(uint32_t));
  cudaMalloc((void**)&c_dev, sizeof(uint32_t));

  cudaMemcpy(a_dev, &a, sizeof(uint32_t), cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, &b, sizeof(uint32_t), cudaMemcpyHostToDevice);

  dim3 blocks(1,1);
  dim3 threads(1,1);

  mul_uint32<<< blocks, threads >>>(c_dev,a_dev,b_dev);

  cudaMemcpy(&c, c_dev, sizeof(uint32_t), cudaMemcpyDeviceToHost);

  std::cout << std::hex;
  std::cout << "c: " << c << std::endl;

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(c_dev);  
}
