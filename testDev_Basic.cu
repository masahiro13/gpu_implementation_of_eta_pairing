#include <sys/time.h>
#include <stdint.h>

#include <iostream>

#include "Declaration.h"
#include "kernel.h"
#include "F2_487.h"
#include "ext_field6_2.h"
#include "ext_field3_2_2.h"



/****************************************        ****************************************/
/**************************************** Kernel ****************************************/
/****************************************        ****************************************/

__global__ void test_basicKernel(BaseField<uint_64,487>::uint_type *C, BaseField<uint_64,487>::uint_type *A, BaseField<uint_64,487>::uint_type *B) {

  for (unsigned int i = 0; i < BaseField<uint_64,487>::nUint; ++i) C[i] = A[i] ^ B[i];
}

__global__ void test_basicKernel2(BaseField<uint_64,487> *C, BaseField<uint_64,487> *A, BaseField<uint_64,487> *B) {

  *C = *A + *B;
}



__global__ void test_add(F2_487_u64 *C, F2_487_u64 *A, F2_487_u64 *B) {

  *C = *A + *B;
}

__global__ void test_mul(F2_487_u64 *C, F2_487_u64 *A, F2_487_u64 *B) {

  F2_487_u64::mulCombR2L_uintSize(*C, *A, *B);
}

template<class Basefield, int number>
__global__ void testMultiple(BaseField_Multiple<Basefield, number> *c, BaseField_Multiple<Basefield, number> *a, BaseField_Multiple<Basefield, number> *b) {

  c->xBF[blockIdx.x].x[threadIdx.x] = a->xBF[blockIdx.x].x[threadIdx.x] ^ b->xBF[blockIdx.x].x[threadIdx.x];
}

template<class Basefield>
__global__ void mulCombR2L_uintSizeKernel2(BaseField_Double<Basefield> *result, Basefield *a, Basefield *b) {

  __shared__ Basefield bL, bH;
  bL = *b;
  bH = bL;

  for (int i = 0; i < Basefield::wordSize; ++i) {
    for (int j = 0; j < Basefield::nUint; ++j) {

      /* aは定められた次数より大きい項の係数が0であることを仮定する */
      if ( a->getBit(i + (j * Basefield::wordSize)) == 1 ) {
        if (threadIdx.x >= j) result->low.x[threadIdx.x] ^= bL.x[threadIdx.x - j];
      }
    }
    bL.template shiftLeftBit<1>();
  }

  /* 上位1ビットを空けてbの先頭係数を代入しておくと下記の実装でO.K. */
  bH.template shiftLeftBit<24>();
  for (int i = Basefield::wordSize - 1; i >= 0; --i) {
    for (int j = 0; j < Basefield::nUint; ++j) {

      /* aは定められた次数より大きい項の係数が0であることを仮定する */
      if ( a->getBit(i + (j * Basefield::wordSize)) == 1 ) {
        //threadIdx.x + Basefield::nUint - 1 - j < Basefield::nUint
        if (threadIdx.x < 1 + j) result->high.x[threadIdx.x] ^= bH.x[threadIdx.x + Basefield::nUint - 1 - j];
      }
    }
    bH.template shiftRightBit<1>();
  }
}



/****************************************             ****************************************/
/**************************************** Kernel Call ****************************************/
/****************************************             ****************************************/
extern "C"
void test_basic() {

  BaseField<uint_64,487> a, b, c;
  a.setRandom();
  b.clear();

  BaseField<uint_64,487>::uint_type *a_dev, *b_dev, *c_dev;
  const int numUint = BaseField<uint_64,487>::nUint;

  cudaMalloc((void**)&a_dev, numUint * sizeof(BaseField<uint_64,487>::uint_type));
  cudaMalloc((void**)&b_dev, numUint * sizeof(BaseField<uint_64,487>::uint_type));
  cudaMalloc((void**)&c_dev, numUint * sizeof(BaseField<uint_64,487>::uint_type));

  cudaMemcpy(a_dev, a.x, numUint * sizeof(BaseField<uint_64,487>::uint_type), cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, b.x, numUint * sizeof(BaseField<uint_64,487>::uint_type), cudaMemcpyHostToDevice);

  dim3 blocks(1,1);
  dim3 threads(1,1);

  test_basicKernel<<< blocks, threads >>>(c_dev,a_dev,b_dev);

  cudaMemcpy(c.x, c_dev, numUint * sizeof(BaseField<uint_64,487>::uint_type), cudaMemcpyDeviceToHost);

  std::cout << std::hex;
  for (unsigned int i = 0; i < BaseField<uint_64,487>::nUint; ++i) std::cout << "a[" << i << "]: " << a.x[i] << std::endl;

  std::cout << std::hex;
  for (unsigned int i = 0; i < BaseField<uint_64,487>::nUint; ++i) std::cout << "c[" << i << "]: " << c.x[i] << std::endl;

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(c_dev);
}

extern "C"
void test_basic2() {

  BaseField<uint_64,487> a, b, c;
  a.setRandom();
  b.clear();

  BaseField<uint_64,487> *a_dev, *b_dev, *c_dev;
  const static int numUint = BaseField<uint_64,487>::nUint;

  cudaMalloc((void**)&a_dev, sizeof(BaseField<uint_64,487>));
  cudaMalloc((void**)&b_dev, sizeof(BaseField<uint_64,487>));
  cudaMalloc((void**)&c_dev, sizeof(BaseField<uint_64,487>));

  cudaMemcpy(a_dev, &a, sizeof(BaseField<uint_64,487>), cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, &b, sizeof(BaseField<uint_64,487>), cudaMemcpyHostToDevice);

  dim3 blocks(1,1);
  dim3 threads(1,1);

  test_basicKernel2<<< blocks, threads >>>(c_dev,a_dev,b_dev);

  cudaMemcpy(&c, c_dev, sizeof(BaseField<uint_64,487>), cudaMemcpyDeviceToHost);

  std::cout << std::hex;
  for (unsigned int i = 0; i < numUint; ++i) std::cout << "a[" << i << "]: " << a.x[i] << std::endl;

  std::cout << std::endl;
  for (unsigned int i = 0; i < numUint; ++i) std::cout << "c[" << i << "]: " << c.x[i] << std::endl;

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(c_dev);
}

extern "C"
void test_AddMul_BF() {

  F2_487_u64 a, b, c, add, mul;
  a.setRandom();
  b.setRandom();
  a.clearUpDeg();
  b.clearUpDeg();

  F2_487_u64 *a_dev, *b_dev, *add_dev, *mul_dev;

  cudaMalloc((void**)&a_dev, sizeof(F2_487_u64));
  cudaMalloc((void**)&b_dev, sizeof(F2_487_u64));
  cudaMalloc((void**)&add_dev, sizeof(F2_487_u64));
  cudaMalloc((void**)&mul_dev, sizeof(F2_487_u64));

  cudaMemcpy(a_dev, &a, sizeof(F2_487_u64), cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, &b, sizeof(F2_487_u64), cudaMemcpyHostToDevice);

  dim3 blocks(1,1);
  dim3 threads(1,1);

  test_add<<< blocks, threads >>>(add_dev,a_dev,b_dev);
  test_mul<<< blocks, threads >>>(mul_dev,a_dev,b_dev);

  cudaMemcpy(&add, add_dev, sizeof(F2_487_u64), cudaMemcpyDeviceToHost);
  cudaMemcpy(&mul, mul_dev, sizeof(F2_487_u64), cudaMemcpyDeviceToHost);

  std::cout << "a:" << a;
  std::cout << "b:" << b;
  std::cout << "-------------------------------------------------" << std::endl;

  std::cout << "add on device (serial)" << std::endl;
  std::cout << add;

  std::cout << "mul on device (serial)" << std::endl;
  std::cout << mul;

  std::cout << "add on host" << std::endl;
  std::cout << a + b;

  F2_487_u64::mulCombR2L_uintSize(c, a, b);
  std::cout << "mul on host" << std::endl;
  std::cout << c;

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(add_dev);
  cudaFree(mul_dev);
}

extern "C"
void testMultipleCall() {

  const int number = 2;
  BaseField_Multiple<F2_487_u64, number> a, b, c, cHost;
  a.setRandom();
  b.setRandom();

  BaseField_Multiple<F2_487_u64, number> *a_dev, *b_dev, *c_dev;

  const static int numUint = F2_487_u64::nUint;

  cudaMalloc((void**)&a_dev, sizeof(BaseField_Multiple<F2_487_u64, number>));
  cudaMalloc((void**)&b_dev, sizeof(BaseField_Multiple<F2_487_u64, number>));
  cudaMalloc((void**)&c_dev, sizeof(BaseField_Multiple<F2_487_u64, number>));

  cudaMemcpy(a_dev, &a, sizeof(BaseField_Multiple<F2_487_u64, number>), cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, &b, sizeof(BaseField_Multiple<F2_487_u64, number>), cudaMemcpyHostToDevice);

  dim3 blocks(number,1);
  dim3 threads(numUint,1);

  testMultiple<<< blocks, threads >>>(c_dev,a_dev,b_dev);

  cudaMemcpy(&c, c_dev, sizeof(BaseField_Multiple<F2_487_u64, number>), cudaMemcpyDeviceToHost);

  for (int i = 0; i < number; ++i) {
    cHost.xBF[i] = a.xBF[i] + b.xBF[i];

    std::cout << i << "-th element; c:" << std::endl << c.xBF[i];
    std::cout << "c (host):" << std::endl << cHost.xBF[i];
    std::cout << std::endl;
  }

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(c_dev);
}

template<class Basefield, int number>
void testMultipleCall_Template() {

  BaseField_Multiple<Basefield, number> a, b, c, cHost;
  a.setRandom();
  b.setRandom();

  BaseField_Multiple<Basefield, number> *a_dev, *b_dev, *c_dev;

  const static int numUint = Basefield::nUint;

  cudaMalloc((void**)&a_dev, sizeof(BaseField_Multiple<Basefield, number>));
  cudaMalloc((void**)&b_dev, sizeof(BaseField_Multiple<Basefield, number>));
  cudaMalloc((void**)&c_dev, sizeof(BaseField_Multiple<Basefield, number>));

  cudaMemcpy(a_dev, &a, sizeof(BaseField_Multiple<Basefield, number>), cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, &b, sizeof(BaseField_Multiple<Basefield, number>), cudaMemcpyHostToDevice);

  dim3 blocks(number,1);
  dim3 threads(numUint,1);

  testMultiple<<< blocks, threads >>>(c_dev,a_dev,b_dev);

  cudaMemcpy(&c, c_dev, sizeof(BaseField_Multiple<Basefield, number>), cudaMemcpyDeviceToHost);

  for (int i = 0; i < number; ++i) {
    cHost.xBF[i] = a.xBF[i] + b.xBF[i];

    std::cout << i << "-th element; c:" << std::endl << c.xBF[i];
    std::cout << "c (host):" << std::endl << cHost.xBF[i];
    std::cout << std::endl;
  }

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(c_dev);
}
template void testMultipleCall_Template<F2_487_u64, 4>();

template<class Basefield>
void mulCombR2L_uintSizeP2(Basefield& c, const Basefield& a, const Basefield& b) {

#if 0
  if (a.testZero() || b.testZero()) {

    c.clear();
    return;
  }
#endif

  BaseField_Double<Basefield> result;
  result.clear();

  Basefield *a_dev, *b_dev;
  BaseField_Double<Basefield> *result_dev;

  cudaMalloc((void**)&a_dev, sizeof(Basefield));
  cudaMalloc((void**)&b_dev, sizeof(Basefield));
  cudaMalloc((void**)&result_dev, sizeof(BaseField_Double<Basefield>));

  cudaMemcpy(a_dev, &a, sizeof(Basefield), cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, &b, sizeof(Basefield), cudaMemcpyHostToDevice);
  cudaMemcpy(result_dev, &result, sizeof(BaseField_Double<Basefield>), cudaMemcpyHostToDevice);

  dim3 blocks(1,1);
  dim3 threads(Basefield::nUint,1);

  mulCombR2L_uintSizeKernel2<<< blocks, threads >>>(result_dev,a_dev,b_dev);

  cudaMemcpy(&result, result_dev, sizeof(BaseField_Double<Basefield>), cudaMemcpyDeviceToHost);

  typename Basefield::uint_type mask = 0x0000007fffffffff;
  result.low.x[Basefield::nUint - 1] &= mask;
  result.reduction(c);

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(result_dev);
}
template void mulCombR2L_uintSizeP2<F2_487_u64>(F2_487_u64& c, const F2_487_u64& a, const F2_487_u64& b);
