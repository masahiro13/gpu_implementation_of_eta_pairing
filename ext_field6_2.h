#pragma once

#include "base_field.h"

/*
extension field: $\mathbb F_{2^{6m}}$の定義
\mathbb F_{2^{6m}} = \mathbb F_2[x]/(x^6+x^5+x^3+x^2+1)
*/
template<class T> class ExtField6 {

public:

  static const int ext_degree = 6;
  static const int base_degree = T::ext_degree;
  static const int wordSize = T::wordSize;
  static const int nUint = T::nUint;
  typedef typename T::uint_type uint_type;

  T xBF[6];



  __host__ __device__ ExtField6() {}

#if 0
  __host__ __device__ ExtField6(const T& a0, const T& a1, const T& a2, const T& a3, const T& a4, const T& a5)
    : xBF[0](a0)
    , xBF[1](a1)
    , xBF[2](a2)
    , xBF[3](a3)
    , xBF[4](a4)
    , xBF[5](a5)
  {}
#endif

  __host__ __device__ void clear() {
    for (unsigned int i = 0; i < 6; ++i) xBF[i].clear();
  }

  __host__ __device__ void clearUpDeg() {
    for (unsigned int i = 0; i < 6; ++i) xBF[i].clearUpDeg();
  }

  void setRandom() {
    for (unsigned int i = 0; i < 6; ++i) xBF[i].setRandom();
  }

  __host__ __device__ void copy(const ExtField6& a) {
    for (unsigned int i = 0; i < 6; ++i) xBF[i] = a.xBF[i];
  }

  __host__ __device__ void plusOne() {
      xBF[0].plusOne();
  }

  __host__ __device__ friend
  ExtField6 operator&(const ExtField6& a, const ExtField6& b) {

    ExtField6 c;
    for (unsigned int i = 0; i < 6; ++i) c.xBF[i] = a.xBF[i] & b.xBF[i];
    return c;
  }

  __host__ __device__ friend
  ExtField6 operator|(const ExtField6& a, const ExtField6& b) {

    ExtField6 c;
    for (unsigned int i = 0; i < 6; ++i) c.xBF[i] = a.xBF[i] | b.xBF[i];
    return c;
  }

  __host__ __device__ friend
  ExtField6 operator^(const ExtField6& a, const ExtField6& b) {

    ExtField6 c;
    for (unsigned int i = 0; i < 6; ++i) c.xBF[i] = a.xBF[i] ^ b.xBF[i];
    return c;
  }

  __host__ __device__ friend
  ExtField6 operator+(const ExtField6& a, const ExtField6& b) {

    return a ^ b;
  }

  __host__ __device__ friend
  ExtField6 operator-(const ExtField6& a, const ExtField6& b) {

    return a ^ b;
  }

  __host__ __device__ friend bool operator==(const ExtField6& a, const ExtField6& b) {

    bool result = true;
    for (unsigned int i = 0; i < 6; ++i) result &= (a.xBF[i] == b.xBF[i]);
      return result;
  }


  __host__ __device__
  ExtField6 operator&=(const ExtField6& b) {

    for (unsigned int i = 0; i < 6; ++i) xBF[i] &= b.xBF[i];
    return *this;
  }

  __host__ __device__
  ExtField6 operator|=(const ExtField6& b) {

    for (unsigned int i = 0; i < 6; ++i) xBF[i] |= b.xBF[i];
    return *this;
  }

  __host__ __device__
  ExtField6 operator^=(const ExtField6& b) {

    for (unsigned int i = 0; i < 6; ++i) xBF[i] ^= b.xBF[i];
    return *this;
  }

  __host__ __device__
  ExtField6 operator+=(const ExtField6& b) {

    return *this ^= b;
  }

  __host__ __device__
  ExtField6 operator-=(const ExtField6& b) {

    return *this ^= b;
  }

  /*

  reduction with difining polynomial w^6 + w^5 + w^3 + w^2 + 1

  a_10w^10 + a_9w^9 + a_8w^8 + a_7w^7 + a_6w^6 + a_5w^5 + a_4w^4 + a_3w^3 + a_2w^2 + a_1w + a_0
  = (a_7 + a_6 + a_5)w^5 + (a_7 + a_4)w^4 + (a_10 + a_6 + a_3)w^3 + (a_10 + a_9 + a_7 + a_6 + a_2)w^2 + (a_9 + a_8 + a_7 + a_1)w + a_8 + a_7 + a_6 + a_0

  */
  __host__ __device__ void reduction(const ExtField_Double<ExtField6>& c) {

    T a67, tmp;

    // a6 + a7
    a67.copy(c.high.xBF[0] + c.high.xBF[1]);

    // (a7 + a6 + a5)w^5
    this->xBF[5] = a67 + c.low.xBF[5];

    // (a7 + a4)w^4
    this->xBF[4] = c.high.xBF[1] + c.low.xBF[4];

    // (a10 + a6 + a3)w^3
    tmp = c.high.xBF[4] + c.high.xBF[0];
    this->xBF[3] = tmp + c.low.xBF[3];

    // (a_10 + a_9 + a_7 + a_6 + a_2)w^2
    tmp = a67 + c.low.xBF[2];
    tmp += c.high.xBF[3];
    this->xBF[2] = tmp + c.high.xBF[4];

    // (a_9 + a_8 + a_7 + a_1)w
    tmp = c.low.xBF[1] + c.high.xBF[1];
    tmp += c.high.xBF[2];
    this->xBF[1] = tmp + c.high.xBF[3];

    // a_8 + a_7 + a_6 + a_0
    tmp = a67 + c.low.xBF[0];
    this->xBF[0] = tmp + c.high.xBF[2];
  }

  // Karatsuba 6
  __host__ __device__ static void mulShAdd6(ExtField6& c, const ExtField6& a, const ExtField6& b) {

    T v[6], vST[15];
    ExtField_Double<ExtField6> result;

    // v[i] = a[i]b[i], not use v[0], v[5]
    for (int i = 1; i < 5; ++i) T::mulShAdd(v[i], a.xBF[i], b.xBF[i]);

    // vST[k] = (a[i]+a[j])(b[i]+b[j])
    T::mulShAdd(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
    T::mulShAdd(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
    T::mulShAdd(vST[2], a.xBF[0] + a.xBF[3], b.xBF[0] + b.xBF[3]);
    T::mulShAdd(vST[3], a.xBF[0] + a.xBF[4], b.xBF[0] + b.xBF[4]);
    T::mulShAdd(vST[4], a.xBF[0] + a.xBF[5], b.xBF[0] + b.xBF[5]);
    T::mulShAdd(vST[5], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);
    T::mulShAdd(vST[6], a.xBF[1] + a.xBF[3], b.xBF[1] + b.xBF[3]);
    T::mulShAdd(vST[7], a.xBF[1] + a.xBF[4], b.xBF[1] + b.xBF[4]);
    T::mulShAdd(vST[8], a.xBF[1] + a.xBF[5], b.xBF[1] + b.xBF[5]);
    T::mulShAdd(vST[9], a.xBF[2] + a.xBF[3], b.xBF[2] + b.xBF[3]);
    T::mulShAdd(vST[10], a.xBF[2] + a.xBF[4], b.xBF[2] + b.xBF[4]);
    T::mulShAdd(vST[11], a.xBF[2] + a.xBF[5], b.xBF[2] + b.xBF[5]);
    T::mulShAdd(vST[12], a.xBF[3] + a.xBF[4], b.xBF[3] + b.xBF[4]);
    T::mulShAdd(vST[13], a.xBF[3] + a.xBF[5], b.xBF[3] + b.xBF[5]);
    T::mulShAdd(vST[14], a.xBF[4] + a.xBF[5], b.xBF[4] + b.xBF[5]);



    // res[0] = a[0]b[0]
    T::mulShAdd(result.low.xBF[0], a.xBF[0], b.xBF[0]);

    // res[10] = a[5]b[5]
    T::mulShAdd(result.high.xBF[4], a.xBF[5], b.xBF[5]);

    // res[1]
    result.low.xBF[1] = vST[0] - result.low.xBF[0];
    result.low.xBF[1] -= v[1];

    // res[2]
    result.low.xBF[2] = vST[1] - result.low.xBF[0];
    result.low.xBF[2] -= v[2];
    result.low.xBF[2] += v[1];

    // res[3]
    result.low.xBF[3] = vST[2] + vST[5];
    result.low.xBF[3] -= result.low.xBF[0];
    result.low.xBF[3] -= v[3];
    result.low.xBF[3] -= v[1];
    result.low.xBF[3] -= v[2];

    // res[4]
    result.low.xBF[4] = vST[3] + vST[6];
    result.low.xBF[4] -= result.low.xBF[0];
    result.low.xBF[4] -= v[4];
    result.low.xBF[4] -= v[1];
    result.low.xBF[4] -= v[3];
    result.low.xBF[4] += v[2];

    // res[5]
    result.low.xBF[5] = vST[4] + vST[7];
    result.low.xBF[5] += vST[9];
    result.low.xBF[5] -= result.low.xBF[0];
    result.low.xBF[5] -= result.high.xBF[4];
    result.low.xBF[5] -= v[1];
    result.low.xBF[5] -= v[4];
    result.low.xBF[5] -= v[2];
    result.low.xBF[5] -= v[3];

    // res[6]
    result.high.xBF[0] = vST[8] + vST[10];
    result.high.xBF[0] -= v[1];
    result.high.xBF[0] -= result.high.xBF[4];
    result.high.xBF[0] -= v[2];
    result.high.xBF[0] -= v[4];
    result.high.xBF[0] += v[3];

    // res[7]
    result.high.xBF[1] = vST[11] + vST[12];
    result.high.xBF[1] -= v[2];
    result.high.xBF[1] -= result.high.xBF[4];
    result.high.xBF[1] -= v[3];
    result.high.xBF[1] -= v[4];

    // res[8]
    result.high.xBF[2] = vST[13] - v[3];
    result.high.xBF[2] -= result.high.xBF[4];
    result.high.xBF[2] += v[4];

    // res[9]
    result.high.xBF[3] = vST[14] - v[4];
    result.high.xBF[3] -= result.high.xBF[4];

    c.reduction(result);
  }

  __host__ __device__ static void mulCombR2L_uintSize6(ExtField6& c, const ExtField6& a, const ExtField6& b) {

    T v[6], vST[15];
    ExtField_Double<ExtField6> result;

    // v[i] = a[i]b[i], not use v[0], v[5]
    for (int i = 1; i < 5; ++i) T::mulCombR2L_uintSize(v[i], a.xBF[i], b.xBF[i]);

    // vST[k] = (a[i]+a[j])(b[i]+b[j])
    T::mulCombR2L_uintSize(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
    T::mulCombR2L_uintSize(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
    T::mulCombR2L_uintSize(vST[2], a.xBF[0] + a.xBF[3], b.xBF[0] + b.xBF[3]);
    T::mulCombR2L_uintSize(vST[3], a.xBF[0] + a.xBF[4], b.xBF[0] + b.xBF[4]);
    T::mulCombR2L_uintSize(vST[4], a.xBF[0] + a.xBF[5], b.xBF[0] + b.xBF[5]);
    T::mulCombR2L_uintSize(vST[5], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);
    T::mulCombR2L_uintSize(vST[6], a.xBF[1] + a.xBF[3], b.xBF[1] + b.xBF[3]);
    T::mulCombR2L_uintSize(vST[7], a.xBF[1] + a.xBF[4], b.xBF[1] + b.xBF[4]);
    T::mulCombR2L_uintSize(vST[8], a.xBF[1] + a.xBF[5], b.xBF[1] + b.xBF[5]);
    T::mulCombR2L_uintSize(vST[9], a.xBF[2] + a.xBF[3], b.xBF[2] + b.xBF[3]);
    T::mulCombR2L_uintSize(vST[10], a.xBF[2] + a.xBF[4], b.xBF[2] + b.xBF[4]);
    T::mulCombR2L_uintSize(vST[11], a.xBF[2] + a.xBF[5], b.xBF[2] + b.xBF[5]);
    T::mulCombR2L_uintSize(vST[12], a.xBF[3] + a.xBF[4], b.xBF[3] + b.xBF[4]);
    T::mulCombR2L_uintSize(vST[13], a.xBF[3] + a.xBF[5], b.xBF[3] + b.xBF[5]);
    T::mulCombR2L_uintSize(vST[14], a.xBF[4] + a.xBF[5], b.xBF[4] + b.xBF[5]);



    // res[0] = a[0]b[0]
    T::mulCombR2L_uintSize(result.low.xBF[0], a.xBF[0], b.xBF[0]);

    // res[10] = a[5]b[5]
    T::mulCombR2L_uintSize(result.high.xBF[4], a.xBF[5], b.xBF[5]);

    // res[1]
    result.low.xBF[1] = vST[0] - result.low.xBF[0];
    result.low.xBF[1] -= v[1];

    // res[2]
    result.low.xBF[2] = vST[1] - result.low.xBF[0];
    result.low.xBF[2] -= v[2];
    result.low.xBF[2] += v[1];

    // res[3]
    result.low.xBF[3] = vST[2] + vST[5];
    result.low.xBF[3] -= result.low.xBF[0];
    result.low.xBF[3] -= v[3];
    result.low.xBF[3] -= v[1];
    result.low.xBF[3] -= v[2];

    // res[4]
    result.low.xBF[4] = vST[3] + vST[6];
    result.low.xBF[4] -= result.low.xBF[0];
    result.low.xBF[4] -= v[4];
    result.low.xBF[4] -= v[1];
    result.low.xBF[4] -= v[3];
    result.low.xBF[4] += v[2];

    // res[5]
    result.low.xBF[5] = vST[4] + vST[7];
    result.low.xBF[5] += vST[9];
    result.low.xBF[5] -= result.low.xBF[0];
    result.low.xBF[5] -= result.high.xBF[4];
    result.low.xBF[5] -= v[1];
    result.low.xBF[5] -= v[4];
    result.low.xBF[5] -= v[2];
    result.low.xBF[5] -= v[3];

    // res[6]
    result.high.xBF[0] = vST[8] + vST[10];
    result.high.xBF[0] -= v[1];
    result.high.xBF[0] -= result.high.xBF[4];
    result.high.xBF[0] -= v[2];
    result.high.xBF[0] -= v[4];
    result.high.xBF[0] += v[3];

    // res[7]
    result.high.xBF[1] = vST[11] + vST[12];
    result.high.xBF[1] -= v[2];
    result.high.xBF[1] -= result.high.xBF[4];
    result.high.xBF[1] -= v[3];
    result.high.xBF[1] -= v[4];

    // res[8]
    result.high.xBF[2] = vST[13] - v[3];
    result.high.xBF[2] -= result.high.xBF[4];
    result.high.xBF[2] += v[4];

    // res[9]
    result.high.xBF[3] = vST[14] - v[4];
    result.high.xBF[3] -= result.high.xBF[4];

    c.reduction(result);
  }

  template<int wSize>
  __host__ __device__ static void mulCombL2RwithWindow6(ExtField6& c, const ExtField6& a, const ExtField6& b) {

    T v[6], vST[15];
    ExtField_Double<ExtField6> result;

    // v[i] = a[i]b[i], not use v[0], v[5]
    for (int i = 1; i < 5; ++i) T::template mulCombL2RwithWindow<wSize>(v[i], a.xBF[i], b.xBF[i]);

    // vST[k] = (a[i]+a[j])(b[i]+b[j])
    T::template mulCombL2RwithWindow<wSize>(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
    T::template mulCombL2RwithWindow<wSize>(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
    T::template mulCombL2RwithWindow<wSize>(vST[2], a.xBF[0] + a.xBF[3], b.xBF[0] + b.xBF[3]);
    T::template mulCombL2RwithWindow<wSize>(vST[3], a.xBF[0] + a.xBF[4], b.xBF[0] + b.xBF[4]);
    T::template mulCombL2RwithWindow<wSize>(vST[4], a.xBF[0] + a.xBF[5], b.xBF[0] + b.xBF[5]);
    T::template mulCombL2RwithWindow<wSize>(vST[5], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);
    T::template mulCombL2RwithWindow<wSize>(vST[6], a.xBF[1] + a.xBF[3], b.xBF[1] + b.xBF[3]);
    T::template mulCombL2RwithWindow<wSize>(vST[7], a.xBF[1] + a.xBF[4], b.xBF[1] + b.xBF[4]);
    T::template mulCombL2RwithWindow<wSize>(vST[8], a.xBF[1] + a.xBF[5], b.xBF[1] + b.xBF[5]);
    T::template mulCombL2RwithWindow<wSize>(vST[9], a.xBF[2] + a.xBF[3], b.xBF[2] + b.xBF[3]);
    T::template mulCombL2RwithWindow<wSize>(vST[10], a.xBF[2] + a.xBF[4], b.xBF[2] + b.xBF[4]);
    T::template mulCombL2RwithWindow<wSize>(vST[11], a.xBF[2] + a.xBF[5], b.xBF[2] + b.xBF[5]);
    T::template mulCombL2RwithWindow<wSize>(vST[12], a.xBF[3] + a.xBF[4], b.xBF[3] + b.xBF[4]);
    T::template mulCombL2RwithWindow<wSize>(vST[13], a.xBF[3] + a.xBF[5], b.xBF[3] + b.xBF[5]);
    T::template mulCombL2RwithWindow<wSize>(vST[14], a.xBF[4] + a.xBF[5], b.xBF[4] + b.xBF[5]);



    // res[0] = a[0]b[0]
    T::template mulCombL2RwithWindow<wSize>(result.low.xBF[0], a.xBF[0], b.xBF[0]);

    // res[10] = a[5]b[5]
    T::template mulCombL2RwithWindow<wSize>(result.high.xBF[4], a.xBF[5], b.xBF[5]);

    // res[1]
    result.low.xBF[1] = vST[0] - result.low.xBF[0];
    result.low.xBF[1] -= v[1];

    // res[2]
    result.low.xBF[2] = vST[1] - result.low.xBF[0];
    result.low.xBF[2] -= v[2];
    result.low.xBF[2] += v[1];

    // res[3]
    result.low.xBF[3] = vST[2] + vST[5];
    result.low.xBF[3] -= result.low.xBF[0];
    result.low.xBF[3] -= v[3];
    result.low.xBF[3] -= v[1];
    result.low.xBF[3] -= v[2];

    // res[4]
    result.low.xBF[4] = vST[3] + vST[6];
    result.low.xBF[4] -= result.low.xBF[0];
    result.low.xBF[4] -= v[4];
    result.low.xBF[4] -= v[1];
    result.low.xBF[4] -= v[3];
    result.low.xBF[4] += v[2];

    // res[5]
    result.low.xBF[5] = vST[4] + vST[7];
    result.low.xBF[5] += vST[9];
    result.low.xBF[5] -= result.low.xBF[0];
    result.low.xBF[5] -= result.high.xBF[4];
    result.low.xBF[5] -= v[1];
    result.low.xBF[5] -= v[4];
    result.low.xBF[5] -= v[2];
    result.low.xBF[5] -= v[3];

    // res[6]
    result.high.xBF[0] = vST[8] + vST[10];
    result.high.xBF[0] -= v[1];
    result.high.xBF[0] -= result.high.xBF[4];
    result.high.xBF[0] -= v[2];
    result.high.xBF[0] -= v[4];
    result.high.xBF[0] += v[3];

    // res[7]
    result.high.xBF[1] = vST[11] + vST[12];
    result.high.xBF[1] -= v[2];
    result.high.xBF[1] -= result.high.xBF[4];
    result.high.xBF[1] -= v[3];
    result.high.xBF[1] -= v[4];

    // res[8]
    result.high.xBF[2] = vST[13] - v[3];
    result.high.xBF[2] -= result.high.xBF[4];
    result.high.xBF[2] += v[4];

    // res[9]
    result.high.xBF[3] = vST[14] - v[4];
    result.high.xBF[3] -= result.high.xBF[4];

    c.reduction(result);
  }

  /*
  (a_5w^5 + a_4w^4 + a_3w^3 + a_2w^2 + a_1w + a_0)^2 = a_5^2w^10 + a_4^2w^8 + a_3^2w^6 + a_2^2w^4 + a_1^2w^1 + a_0^2
                                                     = (a_3^2)w^5 + (a_2^2)w^4 + (a_5^2 + a_3^2)w^3 + (a_5^2 + a_3^2 + a_1^2)w^2 + (a_4^2)w + a_4^2 + a_3^2 + a_0^2
  */
  __host__ __device__ static void square6(ExtField6& c, const ExtField6& a) {

    // (fix) c_5 = a_3^2
    T::square(c.xBF[5], a.xBF[3]);

    // (fix) c_4 = a_2^2
    T::square(c.xBF[4], a.xBF[2]);

    // c_3 = a_5^2
    T::square(c.xBF[3], a.xBF[5]);

    // (fix) c_3 = a_5^2 + a_3^2
    c.xBF[3] += c.xBF[5];

    // c_0 = a_1^2
    T::square(c.xBF[0], a.xBF[1]);

    // (fix) c_2 = a_5^2 + a_3^2 + a_1^2
    c.xBF[2] = c.xBF[3] + c.xBF[0];

    // (fix) c_1 = a_4^2
    T::square(c.xBF[1], a.xBF[4]);

    // c_0 = a_0^2
    T::square(c.xBF[0], a.xBF[0]);

    // c_0 = a_4^2 + a_3^2 + a_0^2
    c.xBF[0] += c.xBF[1];
    c.xBF[0] += c.xBF[5];
  }

  /*
  (a_5w^5 + a_4w^4 + a_3w^3 + a_2w^2 + a_1w + a_0) * (w^5 + w^3)
  = (a_0 + a_1 + a_3 + a_4)w^5 + (a_1 + a_2 + a_4)w^4 + (a_0 + a_1 + a_3 + a_5)w^3 + (a_1 + a_2 + a_3 + a_5)w^2 + (a_2 + a_3 + a_5)w + a_1 + a_2 + a_4 + a_5
  */
  __host__ __device__ static void mulConstTerm6(ExtField6& c, const ExtField6& a) {

    T tmp;

    // tmp = a_0 + a_1 + a_3
    tmp = a.xBF[0] + a.xBF[1];
    tmp += a.xBF[3];

    c.xBF[5] = tmp + a.xBF[4];
    c.xBF[3] = tmp + a.xBF[5];

    // tmp = a_1 + a_2 + a_4
    tmp = a.xBF[1] + a.xBF[2];
    tmp += a.xBF[4];

    c.xBF[0] = tmp + a.xBF[5];
    c.xBF[4] = tmp;

    // tmp = a_2 + a_3 + a_5
    tmp = a.xBF[2] + a.xBF[3];
    tmp += a.xBF[5];

    c.xBF[2] = tmp + a.xBF[1];
    c.xBF[1] = tmp;
  }
};


  /*************************************************************************************/
  /*************************************************************************************/

/*
extension field: $\mathbb F_{2^{12m}}$の定義
\mathbb F_{2^{12m}} = \mathbb F_{2^{12m}}[y]/(y^2+y+w^5+w^3), w^6+w^5+w^3+w^2+1=0
*/
template<class T> class ExtField6_2 {

public:

  static const int ext_degree = 12;
  static const int base_degree = T::ext_degree;
  static const int wordSize = T::wordSize;
  static const int nUint = T::nUint;
  typedef typename T::uint_type uint_type;

  ExtField6<T> xF6[2];



  ExtField6_2() {}

#if 0
  ExtField6_2(const ExtField6<T>& a0, const ExtField6<T>& a1)
      : xF6[0](a0)
      , xF6[1](a1)
  {}
#endif

  __host__ __device__ void clear() {
    xF6[0].clear();
    xF6[1].clear();
  }

  __host__ __device__ void clearUpDeg() {
    xF6[0].clearUpDeg();
    xF6[1].clearUpDeg();
  }

  void setRandom() {
    xF6[0].setRandom();
    xF6[1].setRandom();
  }

  __host__ __device__ void copy(const ExtField6_2& a) {
    xF6[0].copy(a.xF6[0]);
    xF6[1].copy(a.xF6[1]);
  }

  __host__ __device__ void plusOne() {
    xF6[0].plusOne();
  }

  __host__ __device__ friend ExtField6_2 operator+(const ExtField6_2& a, const ExtField6_2& b) {

    ExtField6_2 c;
    c.xF6[0] = a.xF6[0] + b.xF6[0];
    c.xF6[1] = a.xF6[1] + b.xF6[1];
    return c;
  }

  __host__ __device__ friend bool operator==(const ExtField6_2& a, const ExtField6_2& b) {

    return (a.xF6[0] == b.xF6[0]) && (a.xF6[1] == b.xF6[1]);
  }

  ExtField6_2 operator+=(const ExtField6_2& b) {

    xF6[0] ^= b.xF6[0];
    xF6[1] ^= b.xF6[1];
    return *this;
  }

  /*
  reduction with difining polynomial s^2 + s + w^5 + w^3

  a_2s^2 + a_1s + a_0
  = (a_2 + a_1)s + a_2(w^5 + w^3) + a_0,
  */
  __host__ __device__ void reduction(const ExtField_Double<ExtField6_2>& c) {

      ExtField6<T> tmp;
      ExtField6<T>::mulConstTerm6(tmp, c.high.xF6[0]);
      this->xF6[0].copy(tmp + c.low.xF6[0]);

      this->xF6[1].copy(c.high.xF6[0] + c.low.xF6[1]);
  }

  // Karatsuba 2
  __host__ __device__ static void mulShAdd6_2(ExtField6_2& c, const ExtField6_2& a, const ExtField6_2& b) {

    ExtField_Double<ExtField6_2> result;

    ExtField6<T>::mulShAdd6(result.low.xF6[0], a.xF6[0], b.xF6[0]);
    ExtField6<T>::mulShAdd6(result.high.xF6[0], a.xF6[1], b.xF6[1]);

    ExtField6<T>::mulShAdd6(result.low.xF6[1], a.xF6[0] + a.xF6[1], b.xF6[0] + b.xF6[1]);
    result.low.xF6[1] -= result.low.xF6[0];
    result.low.xF6[1] -= result.high.xF6[0];

    c.reduction(result);
  }

  __host__ __device__ static void mulCombR2L_uintSize6_2(ExtField6_2& c, const ExtField6_2& a, const ExtField6_2& b) {

    ExtField_Double<ExtField6_2> result;

    ExtField6<T>::mulCombR2L_uintSize6(result.low.xF6[0], a.xF6[0], b.xF6[0]);
    ExtField6<T>::mulCombR2L_uintSize6(result.high.xF6[0], a.xF6[1], b.xF6[1]);

    ExtField6<T>::mulCombR2L_uintSize6(result.low.xF6[1], a.xF6[0] + a.xF6[1], b.xF6[0] + b.xF6[1]);
    result.low.xF6[1] -= result.low.xF6[0];
    result.low.xF6[1] -= result.high.xF6[0];

    c.reduction(result);
  }

  template<int wSize>
  __host__ __device__ static void mulCombL2RwithWindow6_2(ExtField6_2& c, const ExtField6_2& a, const ExtField6_2& b) {

    ExtField_Double<ExtField6_2> result;

    ExtField6<T>::template mulCombL2RwithWindow6<wSize>(result.low.xF6[0], a.xF6[0], b.xF6[0]);
    ExtField6<T>::template mulCombL2RwithWindow6<wSize>(result.high.xF6[0], a.xF6[1], b.xF6[1]);

    ExtField6<T>::template mulCombL2RwithWindow6<wSize>(result.low.xF6[1], a.xF6[0] + a.xF6[1], b.xF6[0] + b.xF6[1]);
    result.low.xF6[1] -= result.low.xF6[0];
    result.low.xF6[1] -= result.high.xF6[0];

    c.reduction(result);
  }

  /*
  (a_1s + a_0)^2
  = a_1^2s^2 + a_0^2
  = (a_1^2)s + a_1^2(w^5 + w^3) + a_0^2,
  */
  __host__ __device__ static void square6_2(ExtField6_2& c, const ExtField6_2& a) {

    ExtField6<T> a0sq;

    // c_1 = a_1^2
    ExtField6<T>::square6(c.xF6[1], a.xF6[1]);

    ExtField6<T>::square6(a0sq, a.xF6[0]);

    // c_0 = a_1^2(w^5 + w^3)
    ExtField6<T>::mulConstTerm6(c.xF6[0], c.xF6[1]);
    // c_0 = a_1^2(w^5 + w^3) + a_0^2
    c.xF6[0] += a0sq;
  }

  // \alpha\beta
  /*
  !! 乗算はuintSizeのComb methodで行う !!
  (a0,a1,a2,a3,a4,a5) = a0 + a1w+ a2w^2 + a3w^3 + a4w^4 + a5w^5とする

  a=(a0,a1,a2,0,a4,0, (1,0,0,0,0,0)) = (f,1)
  b=(b0,b1,b2,0,b4,0, (1,0,0,0,0,0)) = (g,1)
  ==> ab = (fg,f+g,1) = (fg + w^5 + w^3, f + g + 1)
         = (f + g + 1)s + fg + w^5 + w^3

  fg = (a0,a1,a2,0,a4,0)(b0,b1,b2,0,b4,0)
     = (a0,a1,a2)(b0,b1,b2) + a4w^4(b0,b1,b2) + b4w^4(a0,a1,a2) + a4b4w^8
     = (a0,a1,a2)(b0,b1,b2) + (a1b4 + a2b4 + a4b1 + a4b2)w^5 + (a0b4 + a4b0)w^4 + (a2b4 + a4b2)w^3 + (a2b4 + a4b2)w^2 + (a4b4)w + a2b4 + a4b2 + a4b4

  (a0,a1,a2)(b0,b1,b2)を3次のKaratsuba methodを用いて計算する
  */
  __host__ __device__ static void alpha_beta6_2(ExtField6_2& c, const ExtField6<T>& a, const ExtField6<T>& b) {

    // f + g
    c.xF6[1] = a + b;
    // (fix) c_1 = f + g + 1
    c.xF6[1].xBF[0].plusOne();

    // c_0 := (a0,a1,a2)(b0,b1,b2)
    // Karatsuba 3
    T v1, vST[3];

    T::mulCombR2L_uintSize(c.xF6[0].xBF[0], a.xBF[0], b.xBF[0]);
    T::mulCombR2L_uintSize(v1, a.xBF[1], b.xBF[1]);
    T::mulCombR2L_uintSize(c.xF6[0].xBF[4], a.xBF[2], b.xBF[2]);

    // vST[k] = (a[i]+a[j])(b[i]+b[j])
    // c_0_0
    T::mulCombR2L_uintSize(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
    T::mulCombR2L_uintSize(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
    // c_0_4
    T::mulCombR2L_uintSize(vST[2], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);

    // c_0_1
    c.xF6[0].xBF[1] = vST[0] - c.xF6[0].xBF[0];
    c.xF6[0].xBF[1] -= v1;

    // c_0_2
    c.xF6[0].xBF[2] = vST[1] - c.xF6[0].xBF[0];
    c.xF6[0].xBF[2] -= c.xF6[0].xBF[4];
    c.xF6[0].xBF[2] += v1;

    // c_0_3
    c.xF6[0].xBF[3] = vST[2] - v1;
    c.xF6[0].xBF[3] -= c.xF6[0].xBF[4];

    //(a0,a1,a2)(b0,b1,b2)  + (a1b4 + a2b4 + a4b1 + a4b2)w^5 + (a0b4 + a4b0)w^4 + (a2b4 + a4b2)w^3 + (a2b4 + a4b2)w^2 + (a4b4)w + a2b4 + a4b2 + a4b4
    // (fix) v1 = a4b4
    T::mulCombR2L_uintSize(v1, a.xBF[4], b.xBF[4]);
    // (fix) vST[0] = a2b4 + a4b2
    T::mulCombR2L_uintSize(vST[0], a.xBF[2], b.xBF[4]);
    T::mulCombR2L_uintSize(vST[1], a.xBF[4], b.xBF[2]);
    vST[0] += vST[1];

    // c_0_5 = a1b4（初代入）
    T::mulCombR2L_uintSize(c.xF6[0].xBF[5], a.xBF[1], b.xBF[4]);
    T::mulCombR2L_uintSize(vST[1], a.xBF[4], b.xBF[1]);
    // c_0_5 = a1b4 + a4b1
    c.xF6[0].xBF[5] += vST[1];
    // (fix) c_0_5 = a1b4 + a4b1 + a2b4 + a4b2
    c.xF6[0].xBF[5] += vST[0];

    // (fix) c_0_3 += a2b4 + a4b2
    c.xF6[0].xBF[3] += vST[0];

    // (fix) c_0_2 += a2b4 + a4b2
    c.xF6[0].xBF[2] += vST[0];

    // (fix) c_0_1 += a4b4
    c.xF6[0].xBF[1] += v1;

    // c_0_0 += a2b4 + a4b2
    c.xF6[0].xBF[0] += vST[0];
    // (fix) c_0_0 += a4b4
    c.xF6[0].xBF[0] += v1;

    T::mulCombR2L_uintSize(vST[1], a.xBF[0], b.xBF[4]);
    // c_0_4 += a0b4
    c.xF6[0].xBF[4] += vST[1];

    T::mulCombR2L_uintSize(vST[1], a.xBF[4], b.xBF[0]);
    // (fix) c_0_4 += a4b0
    c.xF6[0].xBF[4] += vST[1];

    // c_0 + w^5 + w^3
    c.xF6[0].xBF[3].plusOne();
    c.xF6[0].xBF[5].plusOne();
  }

  template<int wSize>
  __host__ __device__ static void alpha_beta6_2_withWindow(ExtField6_2& c, const ExtField6<T>& a, const ExtField6<T>& b) {

    // f + g
    c.xF6[1] = a + b;
    // (fix) c_1 = f + g + 1
    c.xF6[1].xBF[0].plusOne();

    // c_0 := (a0,a1,a2)(b0,b1,b2)
    // Karatsuba 3
    T v1, vST[3];

    T::template mulCombL2RwithWindow<wSize>(c.xF6[0].xBF[0], a.xBF[0], b.xBF[0]);
    T::template mulCombL2RwithWindow<wSize>(v1, a.xBF[1], b.xBF[1]);
    T::template mulCombL2RwithWindow<wSize>(c.xF6[0].xBF[4], a.xBF[2], b.xBF[2]);

    // vST[k] = (a[i]+a[j])(b[i]+b[j])
    // c_0_0
    T::template mulCombL2RwithWindow<wSize>(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
    T::template mulCombL2RwithWindow<wSize>(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
    // c_0_4
    T::template mulCombL2RwithWindow<wSize>(vST[2], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);

    // c_0_1
    c.xF6[0].xBF[1] = vST[0] - c.xF6[0].xBF[0];
    c.xF6[0].xBF[1] -= v1;

    // c_0_2
    c.xF6[0].xBF[2] = vST[1] - c.xF6[0].xBF[0];
    c.xF6[0].xBF[2] -= c.xF6[0].xBF[4];
    c.xF6[0].xBF[2] += v1;

    // c_0_3
    c.xF6[0].xBF[3] = vST[2] - v1;
    c.xF6[0].xBF[3] -= c.xF6[0].xBF[4];

    //(a0,a1,a2)(b0,b1,b2)  + (a1b4 + a2b4 + a4b1 + a4b2)w^5 + (a0b4 + a4b0)w^4 + (a2b4 + a4b2)w^3 + (a2b4 + a4b2)w^2 + (a4b4)w + a2b4 + a4b2 + a4b4
    // (fix) v1 = a4b4
    T::template mulCombL2RwithWindow<wSize>(v1, a.xBF[4], b.xBF[4]);
    // (fix) vST[0] = a2b4 + a4b2
    T::template mulCombL2RwithWindow<wSize>(vST[0], a.xBF[2], b.xBF[4]);
    T::template mulCombL2RwithWindow<wSize>(vST[1], a.xBF[4], b.xBF[2]);
    vST[0] += vST[1];

    // c_0_5 = a1b4（初代入）
    T::template mulCombL2RwithWindow<wSize>(c.xF6[0].xBF[5], a.xBF[1], b.xBF[4]);
    T::template mulCombL2RwithWindow<wSize>(vST[1], a.xBF[4], b.xBF[1]);
    // c_0_5 = a1b4 + a4b1
    c.xF6[0].xBF[5] += vST[1];
    // (fix) c_0_5 = a1b4 + a4b1 + a2b4 + a4b2
    c.xF6[0].xBF[5] += vST[0];

    // (fix) c_0_3 += a2b4 + a4b2
    c.xF6[0].xBF[3] += vST[0];

    // (fix) c_0_2 += a2b4 + a4b2
    c.xF6[0].xBF[2] += vST[0];

    // (fix) c_0_1 += a4b4
    c.xF6[0].xBF[1] += v1;

    // c_0_0 += a2b4 + a4b2
    c.xF6[0].xBF[0] += vST[0];
    // (fix) c_0_0 += a4b4
    c.xF6[0].xBF[0] += v1;

    T::template mulCombL2RwithWindow<wSize>(vST[1], a.xBF[0], b.xBF[4]);
    // c_0_4 += a0b4
    c.xF6[0].xBF[4] += vST[1];

    T::template mulCombL2RwithWindow<wSize>(vST[1], a.xBF[4], b.xBF[0]);
    // (fix) c_0_4 += a4b0
    c.xF6[0].xBF[4] += vST[1];

    // c_0 + w^5 + w^3
    c.xF6[0].xBF[3].plusOne();
    c.xF6[0].xBF[5].plusOne();
  }

  /*

  final addition / doublings におけるoperandの一方がsparseな元の乗算
  c = aB; a: normal, B: sparse
  B = (b,1) = s + b
  aB = (a0,a1)(b,1) = (a0 + a1(b+1))s + a0b + a1(w^5 + w^3)

  !! 乗算はregSizeのComb methodで行う !!

  */
  __host__ __device__ static void sparseMul6_2(ExtField6_2& c, const ExtField6_2& a, const ExtField6<T>& b) {

    ExtField6<T> tmp;

    // c_0 = b + 1
    c.xF6[0] = b;
    c.xF6[0].xBF[0].plusOne();
    // c_1 = a1(b + 1)
    ExtField6<T>::mulCombR2L_uintSize6(c.xF6[1], a.xF6[1], c.xF6[0]);
    // (fix) c_1 = a1(b + 1) + a0
    c.xF6[1] += a.xF6[0];



    // tmp = a1(w^5 + w^3)
    ExtField6<T>::mulConstTerm6(tmp, a.xF6[1]);
    // c_0 = a0b
    ExtField6<T>::mulCombR2L_uintSize6(c.xF6[0], a.xF6[0], b);
    // (fix) c_0 a0b + a1(w^5 + w^3)
    c.xF6[0] += tmp;
  }

  template<int wSize>
  __host__ __device__ static void sparseMul6_2_withWindow(ExtField6_2& c, const ExtField6_2& a, const ExtField6<T>& b) {

    ExtField6<T> tmp;

    // c_0 = b + 1
    c.xF6[0] = b;
    c.xF6[0].xBF[0].plusOne();
    // c_1 = a1(b + 1)
    ExtField6<T>::template mulCombL2RwithWindow6<wSize>(c.xF6[1], a.xF6[1], c.xF6[0]);
    // (fix) c_1 = a1(b + 1) + a0
    c.xF6[1] += a.xF6[0];



    // tmp = a1(w^5 + w^3)
    ExtField6<T>::mulConstTerm6(tmp, a.xF6[1]);
    // c_0 = a0b
    ExtField6<T>::template mulCombL2RwithWindow6<wSize>(c.xF6[0], a.xF6[0], b);
    // (fix) c_0 a0b + a1(w^5 + w^3)
    c.xF6[0] += tmp;
  }
};




template<class T, int number> class ExtField6_Multiple {

public:

  static const int wordSize = T::wordSize;
  static const int nUint = T::nUint;
  typedef typename T::uint_type uint_type;

  ExtField6<T> xF6[number];



  __host__ __device__ ExtField6_Multiple() {}

  __host__ __device__ void clear() {
    for (unsigned int i = 0; i < number; ++i) xF6[i].clear();
  }

  __host__ __device__ void plusOne() {
    for (unsigned int i = 0; i < number; ++i) xF6[i].plusOne();
  }

  void setRandom() {
    for (unsigned int i = 0; i < number; ++i) xF6[i].setRandom();
  }
};

template<class T, int number> class ExtField6_2_Multiple {

public:

  static const int wordSize = T::wordSize;
  static const int nUint = T::nUint;
  typedef typename T::uint_type uint_type;

  ExtField6_2<T> xF6_2[number];
  //ExtField6_2<T> *xF6_2;



  __host__ __device__ ExtField6_2_Multiple() {}

#if 0
  __host__ __device__ ~ExtField6_2_Multiple() {
    if (xF6_2 != NULL) delete[] xF6_2;
  }
#endif

  __host__ __device__ void clear() {
    for (unsigned int i = 0; i < number; ++i) xF6_2[i].clear();
  }

  __host__ __device__ void plusOne() {
    for (unsigned int i = 0; i < number; ++i) xF6_2[i].plusOne();
  }

  void setRandom() {
    for (unsigned int i = 0; i < number; ++i) xF6_2[i].setRandom();
  }
};
