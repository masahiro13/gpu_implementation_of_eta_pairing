#pragma once

#include "base_field.h"

/*
extension field: $\mathbb F_{2^{3m}}$の定義
\mathbb F_{2^{3m}} = \mathbb F_2[x]/(x^3+x+1)
*/
template<class T> class ExtField3 {

public:

  static const int ext_degree = 3;
  static const int base_degree = T::ext_degree;
  static const int wordSize = T::wordSize;
  static const int nUint = T::nUint;
  typedef typename T::uint_type uint_type;

  T xBF[3];



  __host__ __device__ ExtField3() {}

  __host__ __device__ void clear() {
    for (unsigned int i = 0; i < 3; ++i) xBF[i].clear();
  }

  __host__ __device__ void clearUpDeg() {
    for (unsigned int i = 0; i < 3; ++i) xBF[i].clearUpDeg();
  }

  void setRandom() {
    for (unsigned int i = 0; i < 3; ++i) xBF[i].setRandom();
  }

  __host__ __device__ void copy(const ExtField3& a) {
    for (unsigned int i = 0; i < 3; ++i) xBF[i] = a.xBF[i];
  }

  __host__ __device__ void plusOne() {
      xBF[0].plusOne();
  }

  __host__ __device__ friend
  ExtField3 operator&(const ExtField3& a, const ExtField3& b) {

    ExtField3 c;
    for (unsigned int i = 0; i < 3; ++i) c.xBF[i] = a.xBF[i] & b.xBF[i];
    return c;
  }

  __host__ __device__ friend
  ExtField3 operator|(const ExtField3& a, const ExtField3& b) {

    ExtField3 c;
    for (unsigned int i = 0; i < 3; ++i) c.xBF[i] = a.xBF[i] | b.xBF[i];
    return c;
  }

  __host__ __device__ friend
  ExtField3 operator^(const ExtField3& a, const ExtField3& b) {

    ExtField3 c;
    for (unsigned int i = 0; i < 3; ++i) c.xBF[i] = a.xBF[i] ^ b.xBF[i];
    return c;
  }

  __host__ __device__ friend
  ExtField3 operator+(const ExtField3& a, const ExtField3& b) {

    return a ^ b;
  }

  __host__ __device__ friend
  ExtField3 operator-(const ExtField3& a, const ExtField3& b) {

    return a ^ b;
  }

  __host__ __device__ friend bool operator==(const ExtField3& a, const ExtField3& b) {

    bool result = true;
    for (unsigned int i = 0; i < 3; ++i) result &= (a.xBF[i] == b.xBF[i]);
      return result;
  }

  __host__ __device__
  ExtField3 operator&=(const ExtField3& b) {

    for (unsigned int i = 0; i < 3; ++i) xBF[i] &= b.xBF[i];
    return *this;
  }

  __host__ __device__
  ExtField3 operator|=(const ExtField3& b) {

    for (unsigned int i = 0; i < 3; ++i) xBF[i] |= b.xBF[i];
    return *this;
  }

  __host__ __device__
  ExtField3 operator^=(const ExtField3& b) {

    for (unsigned int i = 0; i < 3; ++i) xBF[i] ^= b.xBF[i];
    return *this;
  }

  __host__ __device__
  ExtField3 operator+=(const ExtField3& b) {

    return *this ^= b;
  }

  __host__ __device__
  ExtField3 operator-=(const ExtField3& b) {

    return *this ^= b;

  }

  /*
  reduction with defining polynomial x^3 + x + 1

  a_4w^4 + a_3w^3 + a_2w^2 + a_1w + a_0
  = (a_2 + a_4)w^2 + (a_1 + a_3 + a_4)w + a_0 + a_3
  */
  __host__ __device__ void reduction(const ExtField_Double<ExtField3>& c) {

    T tmp;

    // BF[2] = a2 + a4
    this->xBF[2] = c.low.xBF[2] + c.high.xBF[1];

    // BF[1] = a1 + a3 + a4
    this->xBF[1] = c.low.xBF[1] + c.high.xBF[1];
    this->xBF[1] += c.high.xBF[0];

    // BF[0] = a0 + a3
    this->xBF[0] = c.low.xBF[0] + c.high.xBF[0];
  }

  // Karatsuba 3
  __host__ __device__ static void mulShAdd3(ExtField3& c, const ExtField3& a, const ExtField3& b) {

    T v1, vST[3];
    ExtField_Double<ExtField3> result;

    // res[0] = a[0]b[0]
    T::mulShAdd(result.low.xBF[0], a.xBF[0], b.xBF[0]);

    // res[4] = a[2]b[2]
    T::mulShAdd(result.high.xBF[1], a.xBF[2], b.xBF[2]);

    // v1 = a[1]b[1]
    T::mulShAdd(v1, a.xBF[1], b.xBF[1]);

    // vST[k] = (a[i]+a[j])(b[i]+b[j])
    T::mulShAdd(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
    T::mulShAdd(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
    T::mulShAdd(vST[2], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);

    result.low.xBF[1] = vST[0] - result.low.xBF[0];
    result.low.xBF[1] -= v1;

    result.low.xBF[2] = vST[1] - result.low.xBF[0];
    result.low.xBF[2] -= result.high.xBF[1];
    result.low.xBF[2] += v1;

    result.high.xBF[0] = vST[2] - v1;
    result.high.xBF[0] -= result.high.xBF[1];

    c.reduction(result);
  }

  __host__ __device__ static void mulCombR2L_uintSize3(ExtField3& c, const ExtField3& a, const ExtField3& b) {

    T v1, vST[3];
    ExtField_Double<ExtField3> result;

    // res[0] = a[0]b[0]
    T::mulCombR2L_uintSize(result.low.xBF[0], a.xBF[0], b.xBF[0]);

    // res[4] = a[2]b[2]
    T::mulCombR2L_uintSize(result.high.xBF[1], a.xBF[2], b.xBF[2]);

    // v1 = a[1]b[1]
    T::mulCombR2L_uintSize(v1, a.xBF[1], b.xBF[1]);

    // vST[k] = (a[i]+a[j])(b[i]+b[j])
    T::mulCombR2L_uintSize(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
    T::mulCombR2L_uintSize(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
    T::mulCombR2L_uintSize(vST[2], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);

    result.low.xBF[1] = vST[0] - result.low.xBF[0];
    result.low.xBF[1] -= v1;

    result.low.xBF[2] = vST[1] - result.low.xBF[0];
    result.low.xBF[2] -= result.high.xBF[1];
    result.low.xBF[2] += v1;

    result.high.xBF[0] = vST[2] - v1;
    result.high.xBF[0] -= result.high.xBF[1];

    c.reduction(result);
  }

  template<int wSize>
  __host__ __device__ static void mulCombL2RwithWindow3(ExtField3& c, const ExtField3& a, const ExtField3& b) {

    T v1, vST[3];
    ExtField_Double<ExtField3> result;

    // res[0] = a[0]b[0]
    T::template mulCombL2RwithWindow<wSize>(result.low.xBF[0], a.xBF[0], b.xBF[0]);

    // res[4] = a[2]b[2]
    T::template mulCombL2RwithWindow<wSize>(result.high.xBF[1], a.xBF[2], b.xBF[2]);

    // v1 = a[1]b[1]
    T::template mulCombL2RwithWindow<wSize>(v1, a.xBF[1], b.xBF[1]);

    // vST[k] = (a[i]+a[j])(b[i]+b[j])
    T::template mulCombL2RwithWindow<wSize>(vST[0], a.xBF[0] + a.xBF[1], b.xBF[0] + b.xBF[1]);
    T::template mulCombL2RwithWindow<wSize>(vST[1], a.xBF[0] + a.xBF[2], b.xBF[0] + b.xBF[2]);
    T::template mulCombL2RwithWindow<wSize>(vST[2], a.xBF[1] + a.xBF[2], b.xBF[1] + b.xBF[2]);

    result.low.xBF[1] = vST[0] - result.low.xBF[0];
    result.low.xBF[1] -= v1;

    result.low.xBF[2] = vST[1] - result.low.xBF[0];
    result.low.xBF[2] -= result.high.xBF[1];
    result.low.xBF[2] += v1;

    result.high.xBF[0] = vST[2] - v1;
    result.high.xBF[0] -= result.high.xBF[1];

    c.reduction(result);
  }

  /*
  (a_2w^2 + a_1w + a_0)^2 = (a_1^2 + a_2^2)w^2 + a_2^2w + a_0^2
  */
  __host__ __device__ static void square3(ExtField3& c, const ExtField3& a) {

    // c_2 = a_1^2
    T::square(c.xBF[2], a.xBF[1]);

    // (fix) c_1 = a_2^2
    T::square(c.xBF[1], a.xBF[2]);
    c.xBF[2] += c.xBF[1];

    // (fix) c_0 = a_0^2
    T::square(c.xBF[0], a.xBF[0]);
  }

  /*
  (a_2w^2 + a_1w + a_0) * w
  = a_1w^2 + (a_0 + a_2)w + a_2
  */
  __host__ __device__ static void mulConstTerm3(ExtField3& c, const ExtField3& a) {

      c.xBF[2] = a.xBF[1];
      c.xBF[0] = a.xBF[2];
      c.xBF[1] = a.xBF[2] + a.xBF[0];
  }
};

/*************************************************************************************/
/*************************************************************************************/

/*
extension field: $\mathbb F_{2^{6m}}$の定義
\mathbb F_{2^{6m}} = \mathbb F_{2^{3m}}[y]/(y^2+y+w+1)
*/
template<class T> class ExtField3_2 {

public:

  static const int ext_degree = 6;
  static const int base_degree = T::ext_degree;
  static const int wordSize = T::wordSize;
  static const int nUint = T::nUint;
  typedef typename T::uint_type uint_type;

  ExtField3<T> xF3[2];



  __host__ __device__ ExtField3_2() {}

  __host__ __device__ void clear() {
    xF3[0].clear();
    xF3[1].clear();
  }

  __host__ __device__ void clearUpDeg() {
    xF3[0].clearUpDeg();
    xF3[1].clearUpDeg();
  }

  void setRandom() {
    xF3[0].setRandom();
    xF3[1].setRandom();
  }

  __host__ __device__ void copy(const ExtField3_2& a) {
    xF3[0].copy(a.xF3[0]);
    xF3[1].copy(a.xF3[1]);
  }

  __host__ __device__ void plusOne() {
    xF3[0].plusOne();
  }

  __host__ __device__ friend ExtField3_2 operator&(const ExtField3_2& a, const ExtField3_2& b) {

    ExtField3_2 c;
    c.xF3[0] = a.xF3[0] & b.xF3[0];
    c.xF3[1] = a.xF3[1] & b.xF3[1];
    return c;
  }

  __host__ __device__ friend ExtField3_2 operator|(const ExtField3_2& a, const ExtField3_2& b) {

    ExtField3_2 c;
    c.xF3[0] = a.xF3[0] | b.xF3[0];
    c.xF3[1] = a.xF3[1] | b.xF3[1];
    return c;
  }

  __host__ __device__ friend ExtField3_2 operator^(const ExtField3_2& a, const ExtField3_2& b) {

    ExtField3_2 c;
    c.xF3[0] = a.xF3[0] ^ b.xF3[0];
    c.xF3[1] = a.xF3[1] ^ b.xF3[1];
    return c;
  }

  __host__ __device__ friend ExtField3_2 operator+(const ExtField3_2& a, const ExtField3_2& b) {

    ExtField3_2 c;
    c.xF3[0] = a.xF3[0] ^ b.xF3[0];
    c.xF3[1] = a.xF3[1] ^ b.xF3[1];
    return c;
  }

  __host__ __device__ friend ExtField3_2 operator-(const ExtField3_2& a, const ExtField3_2& b) {

    ExtField3_2 c;
    c.xF3[0] = a.xF3[0] ^ b.xF3[0];
    c.xF3[1] = a.xF3[1] ^ b.xF3[1];
    return c;
  }

  __host__ __device__ friend bool operator==(const ExtField3_2& a, const ExtField3_2& b) {

    return (a.xF3[0] == b.xF3[0]) && (a.xF3[1] == b.xF3[1]);
  }

  __host__ __device__
  ExtField3_2 operator+=(const ExtField3_2& b) {

    xF3[0] ^= b.xF3[0];
    xF3[1] ^= b.xF3[1];
    return *this;
  }

  __host__ __device__
  ExtField3_2 operator-=(const ExtField3_2& b) {

    xF3[0] ^= b.xF3[0];
    xF3[1] ^= b.xF3[1];
    return *this;
  }

  /*
  reduction with defining polynomial s^2 + s + w + 1

  a_2s^2 + a_1s + a_0
  = (a_1 + a_2)s + w*a_2 + a_0 + a_2
  */
  __host__ __device__
  void reduction(const ExtField_Double<ExtField3_2>& c) {

    ExtField3<T>::mulConstTerm3(this->xF3[0], c.high.xF3[0]);
    this->xF3[0] += c.low.xF3[0];
    this->xF3[0] += c.high.xF3[0];

    this->xF3[1] = c.low.xF3[1] + c.low.xF3[2];
  }

  // Karatsuba
  __host__ __device__
  static void mulShAdd3_2(ExtField3_2& c, const ExtField3_2& a, const ExtField3_2& b) {

    ExtField3<T> vST;
    ExtField_Double<ExtField3_2> result;

    ExtField3<T>::mulShAdd3(result.low.xF3[0], a.xF3[0], b.xF3[0]);

    ExtField3<T>::mulShAdd3(result.high.xF3[0], a.xF3[1], b.xF3[1]);


    // vST[k] = (a[i]+a[j])(b[i]+b[j])
    ExtField3<T>::mulShAdd3(vST, a.xF3[0] + a.xF3[1], b.xF3[0] + b.xF3[1]);

    result.low.xF3[1] = vST - result.low.xF3[0];
    result.low.xF3[1] -= result.high.xF3[0];

    c.reduction(result);
  }

  __host__ __device__
  static void mulCombR2L_uintSize3_2(ExtField3_2& c, const ExtField3_2& a, const ExtField3_2& b) {

    ExtField3<T> vST;
    ExtField_Double<ExtField3_2> result;

    ExtField3<T>::mulCombR2L_uintSize3(result.low.xF3[0], a.xF3[0], b.xF3[0]);

    ExtField3<T>::mulCombR2L_uintSize3(result.high.xF3[0], a.xF3[1], b.xF3[1]);


    // vST[k] = (a[i]+a[j])(b[i]+b[j])
    ExtField3<T>::mulCombR2L_uintSize3(vST, a.xF3[0] + a.xF3[1], b.xF3[0] + b.xF3[1]);

    result.low.xF3[1] = vST - result.low.xF3[0];
    result.low.xF3[1] -= result.high.xF3[0];

    c.reduction(result);
  }

  template<int wSize>
  __host__ __device__
  static void mulCombL2RwithWindow3_2(ExtField3_2& c, const ExtField3_2& a, const ExtField3_2& b) {

    ExtField3<T> vST;
    ExtField_Double<ExtField3_2> result;

    ExtField3<T>::template mulCombL2RwithWindow3<wSize>(result.low.xF3[0], a.xF3[0], b.xF3[0]);

    ExtField3<T>::template mulCombL2RwithWindow3<wSize>(result.high.xF3[0], a.xF3[1], b.xF3[1]);


    // vST[k] = (a[i]+a[j])(b[i]+b[j])
    ExtField3<T>::template mulCombL2RwithWindow3<wSize>(vST, a.xF3[0] + a.xF3[1], b.xF3[0] + b.xF3[1]);

    result.low.xF3[1] = vST - result.low.xF3[0];
    result.low.xF3[1] -= result.high.xF3[0];

    c.reduction(result);
  }

  // for sparse multiplication $f\cdot\alpha\beta$ in Miller loop
  /*
   ab = (a0, a1)(b0, 0) = a0b0 + (a1b0)s
   !!! オペランドの順に注意 $\alpha\beta$は右側 !!!
   */
  __host__ __device__
  static void mul_sp_ShAdd3_2(ExtField3_2& c, const ExtField3_2& a, const ExtField3_2& b) {

    ExtField3<T>::mulShAdd3(c.xF3[0], a.xF3[0], b.xF3[0]);
    ExtField3<T>::mulShAdd3(c.xF3[1], a.xF3[1], b.xF3[0]);
  }

  __host__ __device__
  static void mul_sp_CombR2L_uintSize3_2(ExtField3_2& c, const ExtField3_2& a, const ExtField3_2& b) {

    ExtField3<T>::mulCombR2L_uintSize3(c.xF3[0], a.xF3[0], b.xF3[0]);
    ExtField3<T>::mulCombR2L_uintSize3(c.xF3[1], a.xF3[1], b.xF3[0]);
  }

  template<int wSize>
  __host__ __device__
  static void mul_sp_CombL2RwithWindow3_2(ExtField3_2& c, const ExtField3_2& a, const ExtField3_2& b) {

    ExtField3<T>::template mulCombL2RwithWindow3<wSize>(c.xF3[0], a.xF3[0], b.xF3[0]);
    ExtField3<T>::template mulCombL2RwithWindow3<wSize>(c.xF3[1], a.xF3[1], b.xF3[0]);
  }

  /*
  (a_1s + a_0)^2 = a_1^2s + (a_1^2w + a_1^2 + a_0^2)
  */
  __host__ __device__
  static void square3_2(ExtField3_2& c, const ExtField3_2& a) {

    ExtField3<T> tmp;

    ExtField3<T>::square3(c.xF3[1], a.xF3[1]);
    ExtField3<T>::mulConstTerm3(c.xF3[0], c.xF3[1]);
    c.xF3[0] += c.xF3[1];

    ExtField3<T>::square3(tmp, a.xF3[0]);
    c.xF3[0] += tmp;
  }

  /*
  (a_5s*w^2 + a_4s*w + a_3s  + a_2w^2 + a_1w + a_0)(s + s*w^2) = (a0 + a3)s*w^2 + (a2 + a5)s*w + (a0 + a1 + a3 + a4)s + (a3 + a5)w^2 + (a4 + a5)w + a4
  */
  __host__ __device__
  static void mulConstTerm3_2(ExtField3_2& c, const ExtField3_2& a) {

    c.xF3[1].xBF[2] = a.xF3[0].xBF[0] + a.xF3[1].xBF[0];
    c.xF3[1].xBF[1] = a.xF3[0].xBF[2] + a.xF3[1].xBF[2];
    c.xF3[1].xBF[0] = a.xF3[0].xBF[0] + a.xF3[0].xBF[1];
    c.xF3[1].xBF[0] += a.xF3[1].xBF[0];
    c.xF3[1].xBF[0] += a.xF3[1].xBF[1];
    c.xF3[0].xBF[2] = a.xF3[1].xBF[0] + a.xF3[1].xBF[2];
    c.xF3[0].xBF[1] = a.xF3[1].xBF[2] + a.xF3[1].xBF[2];
    c.xF3[0].xBF[0] = a.xF3[1].xBF[2];
  }
};

/*
extension field: $\mathbb F_{2^{12m}}$の定義
\mathbb F_{2^{12m}} = \mathbb F_{2^{6m}}[z]/(z^2+z+s+sw^2)
*/
template<class T> class ExtField3_2_2 {

public:

  static const int ext_degree = 12;
  static const int base_degree = T::ext_degree;
  static const int wordSize = T::wordSize;
  static const int nUint = T::nUint;
  typedef typename T::uint_type uint_type;

  ExtField3_2<T> xF3_2[2];



  __host__ __device__ ExtField3_2_2() {}

  __host__ __device__ void clear() {
    xF3_2[0].clear();
    xF3_2[1].clear();
  }

  __host__ __device__ void clearUpDeg() {
    xF3_2[0].clearUpDeg();
    xF3_2[1].clearUpDeg();
  }

  void setRandom() {
    xF3_2[0].setRandom();
    xF3_2[1].setRandom();
  }

  __host__ __device__ void copy(const ExtField3_2_2& a) {
    xF3_2[0].copy(a.xF3_2[0]);
    xF3_2[1].copy(a.xF3_2[1]);
  }

  __host__ __device__ void plusOne() {
    xF3_2[0].plusOne();
  }

  __host__ __device__ friend ExtField3_2_2 operator+(const ExtField3_2_2& a, const ExtField3_2_2& b) {

    ExtField3_2_2 c;
    c.xF3_2[0] = a.xF3_2[0] ^ b.xF3_2[0];
    c.xF3_2[1] = a.xF3_2[1] ^ b.xF3_2[1];
    return c;
  }

  __host__ __device__ friend ExtField3_2_2 operator-(const ExtField3_2_2& a, const ExtField3_2_2& b) {

    ExtField3_2_2 c;
    c.xF3_2[0] = a.xF3_2[0] ^ b.xF3_2[0];
    c.xF3_2[1] = a.xF3_2[1] ^ b.xF3_2[1];
    return c;
  }

  __host__ __device__ friend bool operator==(const ExtField3_2_2& a, const ExtField3_2_2& b) {

    return (a.xF3_2[0] == b.xF3_2[0]) && (a.xF3_2[1] == b.xF3_2[1]);
  }

  __host__ __device__
  ExtField3_2_2 operator+=(const ExtField3_2_2& b) {

    xF3_2[0] ^= b.xF3_2[0];
    xF3_2[1] ^= b.xF3_2[1];
    return *this;
  }

  __host__ __device__
  ExtField3_2_2 operator-=(const ExtField3_2_2& b) {

    xF3_2[0] ^= b.xF3_2[0];
    xF3_2[1] ^= b.xF3_2[1];
    return *this;
  }

  friend std::ostream& operator<<(std::ostream& os, const ExtField3_2_2& a) {

    os << std::hex;
    os << "-------------------- ext3_2_2 start --------------------" << std::endl;
    for (unsigned int k = 0; k < 2; ++k) {
      for (unsigned int j = 0; j < 2; ++j) {
        for (unsigned int i = 0; i < 3; ++i) {
          os << "(" << i << "," << j << "," << k << ")" << std::endl;
          os << a.xF3_2[k].xF3[j].xBF[i];
        }
      }
    }
    os << "-------------------- ext3_2_2  end  --------------------" << std::endl;
    return os;
  }

  /*
  reduction with defining polynomial t^2+z+t+sw^2

  a_2t^2 + a_1t + a_0
  = (a_1 + a_2)t + a_0 + (s*w^2 + s)*a_2
  */
  __host__ __device__
  void reduction(const ExtField_Double<ExtField3_2_2>& c) {

    ExtField3_2<T>::mulConstTerm3_2(this->xF3_2[0], c.high.xF3_2[0]);
    this->xF3_2[0] += c.low.xF3_2[0];
    this->xF3_2[1] = c.high.xF3_2[0] + c.low.xF3_2[1];
  }

  // Karatsuba
  __host__ __device__
  static void mulShAdd3_2_2(ExtField3_2_2& c, const ExtField3_2_2& a, const ExtField3_2_2& b) {

    ExtField_Double<ExtField3_2_2> result;

    ExtField3_2<T>::mulShAdd3_2(result.low.xF3_2[0], a.xF3_2[0], b.xF3_2[0]);
    ExtField3_2<T>::mulShAdd3_2(result.high.xF3_2[0], a.xF3_2[1], b.xF3_2[1]);

    ExtField3_2<T>::mulShAdd3_2(result.low.xF3_2[1], a.xF3_2[0] + a.xF3_2[1], b.xF3_2[0] + b.xF3_2[1]);
    result.low.xF3_2[1] -= result.low.xF3_2[0];
    result.low.xF3_2[1] -= result.high.xF3_2[0];

    c.reduction(result);
  }

  // Karatsuba
  __host__ __device__
  static void mulCombR2L_uintSize3_2_2(ExtField3_2_2& c, const ExtField3_2_2& a, const ExtField3_2_2& b) {

    ExtField_Double<ExtField3_2_2> result;

    ExtField3_2<T>::mulCombR2L_uintSize3_2(result.low.xF3_2[0], a.xF3_2[0], b.xF3_2[0]);
    ExtField3_2<T>::mulCombR2L_uintSize3_2(result.high.xF3_2[0], a.xF3_2[1], b.xF3_2[1]);

    ExtField3_2<T>::mulCombR2L_uintSize3_2(result.low.xF3_2[1], a.xF3_2[0] + a.xF3_2[1], b.xF3_2[0] + b.xF3_2[1]);
    result.low.xF3_2[1] -= result.low.xF3_2[0];
    result.low.xF3_2[1] -= result.high.xF3_2[0];

    c.reduction(result);
  }

  template<int wSize>
  __host__ __device__
  static void mulCombL2RwithWindow3_2_2(ExtField3_2_2& c, const ExtField3_2_2& a, const ExtField3_2_2& b) {

    ExtField_Double<ExtField3_2_2> result;

    ExtField3_2<T>::template mulCombL2RwithWindow3_2<wSize>(result.low.xF3_2[0], a.xF3_2[0], b.xF3_2[0]);
    ExtField3_2<T>::template mulCombL2RwithWindow3_2<wSize>(result.high.xF3_2[0], a.xF3_2[1], b.xF3_2[1]);

    ExtField3_2<T>::template mulCombL2RwithWindow3_2<wSize>(result.low.xF3_2[1], a.xF3_2[0] + a.xF3_2[1], b.xF3_2[0] + b.xF3_2[1]);
    result.low.xF3_2[1] -= result.low.xF3_2[0];
    result.low.xF3_2[1] -= result.high.xF3_2[0];

    c.reduction(result);
  }

  // for sparse multiplication $f\cdot\alpha\beta$ in Miller loop
  /*
   ab = (a0, a1)(b0, 1), b1 = (*, 0). a1b1を ExtField3_2<T>::template mul_sp_* で行う
   !!! オペランドの順に注意 $\alpha\beta$は右側 !!!
   */
  __host__ __device__
  static void mul_sp_ShAdd3_2_2(ExtField3_2_2& c, const ExtField3_2_2& a, const ExtField3_2_2& b) {

    ExtField_Double<ExtField3_2_2> result;

    ExtField3_2<T>::mulShAdd3_2(result.low.xF3_2[0], a.xF3_2[0], b.xF3_2[0]);
    ExtField3_2<T>::mul_sp_ShAdd3_2(result.high.xF3_2[0], a.xF3_2[1], b.xF3_2[1]);

    ExtField3_2<T>::mulShAdd3_2(result.low.xF3_2[1], a.xF3_2[0] + a.xF3_2[1], b.xF3_2[0] + b.xF3_2[1]);
    result.low.xF3_2[1] -= result.low.xF3_2[0];
    result.low.xF3_2[1] -= result.high.xF3_2[0];

    c.reduction(result);
  }

  __host__ __device__
  static void mul_sp_CombR2L_uintSize3_2_2(ExtField3_2_2& c, const ExtField3_2_2& a, const ExtField3_2_2& b) {

    ExtField_Double<ExtField3_2_2> result;

    ExtField3_2<T>::mulCombR2L_uintSize3_2(result.low.xF3_2[0], a.xF3_2[0], b.xF3_2[0]);
    ExtField3_2<T>::mul_sp_CombR2L_uintSize3_2(result.high.xF3_2[0], a.xF3_2[1], b.xF3_2[1]);

    ExtField3_2<T>::mulCombR2L_uintSize3_2(result.low.xF3_2[1], a.xF3_2[0] + a.xF3_2[1], b.xF3_2[0] + b.xF3_2[1]);
    result.low.xF3_2[1] -= result.low.xF3_2[0];
    result.low.xF3_2[1] -= result.high.xF3_2[0];

    c.reduction(result);
  }

  template<int wSize>
  __host__ __device__
  static void mul_sp_CombL2RwithWindow3_2_2(ExtField3_2_2& c, const ExtField3_2_2& a, const ExtField3_2_2& b) {

    ExtField_Double<ExtField3_2_2> result;

    ExtField3_2<T>::template mulCombL2RwithWindow3_2<wSize>(result.low.xF3_2[0], a.xF3_2[0], b.xF3_2[0]);
    ExtField3_2<T>::template mul_sp_CombL2RwithWindow3_2<wSize>(result.high.xF3_2[0], a.xF3_2[1], b.xF3_2[1]);

    ExtField3_2<T>::template mulCombL2RwithWindow3_2<wSize>(result.low.xF3_2[1], a.xF3_2[0] + a.xF3_2[1], b.xF3_2[0] + b.xF3_2[1]);
    result.low.xF3_2[1] -= result.low.xF3_2[0];
    result.low.xF3_2[1] -= result.high.xF3_2[0];

    c.reduction(result);
  }

  /*
  (a_1t + a_0)^2 = a_1^2t + a_1^2(s*w^2 + s) + a_0^2
  */
  __host__ __device__
  static void square3_2_2(ExtField3_2_2& c, const ExtField3_2_2& a) {

    ExtField3_2<T> tmp;

    ExtField3_2<T>::square3_2(c.xF3_2[1], a.xF3_2[1]);
    ExtField3_2<T>::mulConstTerm3_2(c.xF3_2[0], c.xF3_2[1]);

    ExtField3_2<T>::square3_2(tmp, a.xF3_2[0]);
    c.xF3_2[0] += tmp;
  }


  // \alpha\beta
  /*
  !! 乗算はregSizeのComb methodで行う !!
  (a0,a1,a2,a3,a4,a5) = a0 + a1w+ a2w^2 + a3s + a4sw + a5sw^2とする

  a=(a0,a1,a2,c,0,0, (1,0,0,0,0,0)) = (f,1)
  b=(b0,b1,b2,c,0,0, (1,0,0,0,0,0)) = (g,1)
  ==> ab = (fg,f+g,1) = (fg + s + sw^2, f + g + 1)
         = (f + g + 1)z + fg + s + sw^2

  d=c^2, dはペアリングアルゴリズムの事前計算で求められた値を使えるため引数として渡す

  fg = (a0,a1,a2,c,0,0)(b0,b1,b2,c,0,0)
     = (a0,a1,a2)(b0,b1,b2) + ( c((a0,a1,a2) + (b0,b1,b2)) )y + (d=c^2,0,0)y^2
     = (a0,a1,a2)(b0,b1,b2) + (d,d,0) + ( c((a0,a1,a2) + (b0,b1,b2)) + d )y (mod y^2+y+w+1)

  (a0,a1,a2)(b0,b1,b2)をExtField3の乗算 (Karatusba 3) を用いて計算する
  */

  __host__ __device__
  static void alpha_beta3_2_2(ExtField3_2_2& c, const ExtField3_2<T>& a, const ExtField3_2<T>& b, const T& d) {

    /* コストはK3+3M+11A (ExtField3でのmod演算による加算等も含む) */
    ExtField3_2<T> fg;
    fg.clear();

    ExtField3<T> tmpF3;

    // c := (c_0,c_1,c_2,c_3)

    // fg
    // c_0 := (a0,a1,a2)(b0,b1,b2)
    ExtField3<T>::mulCombR2L_uintSize3(fg.xF3[0], a.xF3[0], b.xF3[0]);
    fg.xF3[0].xBF[0] += d;
    fg.xF3[0].xBF[1] += d;

    tmpF3 = a.xF3[0] + b.xF3[0];

    // (c_2,c_3) := f + g = (a0+b0,a1+b1,a2+b2,0,0,0)
    c.xF3_2[1].xF3[0] = tmpF3;
    c.xF3_2[1].xF3[1].clear();

    // (fix) (c_2,c_3) = f + g + 1
    c.xF3_2[1].plusOne();

    // tmpF3 = (a0+b0,a1+b1,a2+b2)
    T::mulCombR2L_uintSize(fg.xF3[1].xBF[0], a.xF3[1].xBF[0], tmpF3.xBF[0]);
    T::mulCombR2L_uintSize(fg.xF3[1].xBF[1], a.xF3[1].xBF[0], tmpF3.xBF[1]);
    T::mulCombR2L_uintSize(fg.xF3[1].xBF[2], a.xF3[1].xBF[0], tmpF3.xBF[2]);

    fg.xF3[1].xBF[0] += d;

    //(c_0,c_1) = fg + s + sw^2
    c.xF3_2[0] = fg;
    c.xF3_2[0].xF3[1].xBF[0].plusOne();
    c.xF3_2[0].xF3[1].xBF[2].plusOne();
  }

  template<int wSize>
  __host__ __device__
  static void alpha_beta3_2_2_withWindow(ExtField3_2_2& c, const ExtField3_2<T>& a, const ExtField3_2<T>& b, const T& d) {

    /* コストはK3+3M+11A (ExtField3でのmod演算による加算等も含む) */
    ExtField3_2<T> fg;
    fg.clear();

    ExtField3<T> tmpF3;

    // c := (c_0,c_1,c_2,c_3)

    // fg
    // c_0 := (a0,a1,a2)(b0,b1,b2)
    ExtField3<T>::template mulCombL2RwithWindow3<wSize>(fg.xF3[0], a.xF3[0], b.xF3[0]);
    fg.xF3[0].xBF[0] += d;
    fg.xF3[0].xBF[1] += d;

    tmpF3 = a.xF3[0] + b.xF3[0];

    // (c_2,c_3) := f + g = (a0+b0,a1+b1,a2+b2,0,0,0)
    c.xF3_2[1].xF3[0] = tmpF3;
    c.xF3_2[1].xF3[1].clear();

    // (fix) (c_2,c_3) = f + g + 1
    c.xF3_2[1].plusOne();

    // tmpF3 = (a0+b0,a1+b1,a2+b2)
    T::template mulCombL2RwithWindow<wSize>(fg.xF3[1].xBF[0], a.xF3[1].xBF[0], tmpF3.xBF[0]);
    T::template mulCombL2RwithWindow<wSize>(fg.xF3[1].xBF[1], a.xF3[1].xBF[0], tmpF3.xBF[1]);
    T::template mulCombL2RwithWindow<wSize>(fg.xF3[1].xBF[2], a.xF3[1].xBF[0], tmpF3.xBF[2]);

    fg.xF3[1].xBF[0] += d;

    //(c_0,c_1) = fg + s + sw^2
    c.xF3_2[0] = fg;
    c.xF3_2[0].xF3[1].xBF[0].plusOne();
    c.xF3_2[0].xF3[1].xBF[2].plusOne();
  }


  /*
  final addition / doublings におけるoperandの一方がsparseな元の乗算
  c = aB; a: normal, B: sparse
  B = (b,1) = b + t
  aB = (a0,a1)(b,1) = (a0 + a1(b+1))z + a0b + a1(s + sw^2)

  !! 乗算はregSizeのComb methodで行う !!
  */
  __host__ __device__
  static void sparseMul3_2_2(ExtField3_2_2& c, const ExtField3_2_2& a, const ExtField3_2<T>& b) {

    ExtField3_2<T> tmp;

    // c_0 = b + 1
    c.xF3_2[0] = b;
    c.xF3_2[0].xF3[0].xBF[0].plusOne();
    // c_1 = a1(b + 1)
    ExtField3_2<T>::mulCombR2L_uintSize3_2(c.xF3_2[0], a.xF3_2[1], c.xF3_2[0]);
    // (fix) c_1 = a1(b + 1) + a0
    c.xF3_2[1] += a.xF3_2[0];



    // tmp = a1(s + sw^2)
    ExtField3_2<T>::mulConstTerm3_2(tmp, a.xF3_2[1]);
    // c_0 = a0b
    ExtField3_2<T>::mulCombR2L_uintSize3_2(c.xF3_2[0], a.xF3_2[0], b);
    // (fix) c_0 a0b + a1(w^5 + w^3)
    c.xF3_2[0] += tmp;
  }

  template<int wSize>
  __host__ __device__
  static void sparseMul3_2_2_withWindow(ExtField3_2_2& c, const ExtField3_2_2& a, const ExtField3_2<T>& b) {

    ExtField3_2<T> tmp;

    // c_0 = b + 1
    c.xF3_2[0] = b;
    c.xF3_2[0].xF3[0].xBF[0].plusOne();
    // c_1 = a1(b + 1)
    ExtField3_2<T>::template mulCombL2RwithWindow3_2<wSize>(c.xF3_2[0], a.xF3_2[1], c.xF3_2[0]);
    // (fix) c_1 = a1(b + 1) + a0
    c.xF3_2[1] += a.xF3_2[0];



    // tmp = a1(s + sw^2)
    ExtField3_2<T>::mulConstTerm3_2(tmp, a.xF3_2[1]);
    // c_0 = a0b
    ExtField3_2<T>::template mulCombL2RwithWindow3_2<wSize>(c.xF3_2[0], a.xF3_2[0], b);
    // (fix) c_0 a0b + a1(w^5 + w^3)
    c.xF3_2[0] += tmp;
  }
};



template<class T, int number> class ExtField3_Multiple {

public:

  static const int wordSize = T::wordSize;
  static const int nUint = T::nUint;
  typedef typename T::uint_type uint_type;

  ExtField3<T> xF3[number];



  __host__ __device__ ExtField3_Multiple() {}

  __host__ __device__ void clear() {
    for (unsigned int i = 0; i < number; ++i) xF3[i].clear();
  }

  __host__ __device__ void plusOne() {
    for (unsigned int i = 0; i < number; ++i) xF3[i].plusOne();
  }

  void setRandom() {
    for (unsigned int i = 0; i < number; ++i) xF3[i].setRandom();
  }
};

template<class T, int number> class ExtField3_2_Multiple {

public:

  static const int wordSize = T::wordSize;
  static const int nUint = T::nUint;
  typedef typename T::uint_type uint_type;

  ExtField3_2<T> xF3_2[number];



  __host__ __device__ ExtField3_2_Multiple() {}

  __host__ __device__ void clear() {
    for (unsigned int i = 0; i < number; ++i) xF3_2[i].clear();
  }

  __host__ __device__ void plusOne() {
    for (unsigned int i = 0; i < number; ++i) xF3_2[i].plusOne();
  }

  void setRandom() {
    for (unsigned int i = 0; i < number; ++i) xF3_2[i].setRandom();
  }
};

template<class T, int number> class ExtField3_2_2_Multiple {

public:

  static const int wordSize = T::wordSize;
  static const int nUint = T::nUint;
  typedef typename T::uint_type uint_type;

  ExtField3_2_2<T> xF3_2_2[number];



  __host__ __device__ ExtField3_2_2_Multiple() {}

  __host__ __device__ void clear() {
    for (unsigned int i = 0; i < number; ++i) xF3_2_2[i].clear();
  }

  __host__ __device__ void plusOne() {
    for (unsigned int i = 0; i < number; ++i) xF3_2_2[i].plusOne();
  }

  void setRandom() {
    for (unsigned int i = 0; i < number; ++i) xF3_2_2[i].setRandom();
  }
};
