//#include <gmp.h>
#include <stdio.h>
#include <time.h>
#include <stdint.h>
#include <sys/time.h>
#include <cuda_runtime.h>

#include <iostream>

#include "callTemplate_Multiple.h"
#include "F2_487.h"
#include "ext_field6_2.h"
#include "pairing.h"
#include "pairingGpu.h"
#include "dummy_pairingGpu.h"



void testAddMul() {

  struct timeval start, end;
  double timing;

  F2_487_u64 a, b, add, mul, addGPU, mulShAddGPU, mulCombGPU, mulWindowGPU, sq, sqGPU;
  a.setRandom();
  b.setRandom();

  std::cout << "add on host" << std::endl;
  std::cout << a + b;

  addP<F2_487_u64>(addGPU, a, b);
  std::cout << "add on device (parallel)" << std::endl;
  std::cout << addGPU;

  F2_487_u64::mulCombR2L_uintSize(mul, a, b);
  std::cout << "mul on host" << std::endl;
  std::cout << mul;

  mulShAddP<F2_487_u64>(mulShAddGPU, a, b);
  std::cout << "mul shift addition on device (parallel)" << std::endl;
  std::cout << mulShAddGPU;

  mulCombR2L_uintSizeP(mulCombGPU, a, b);
  std::cout << "mul Comb method on device (parallel)" << std::endl;
  std::cout << mulCombGPU;

  const int wSize = 5;
  //mulCombL2RwithWindowP<F2_487_u64>(mulWindowGPU, a, b, wSize);
  mulCombL2RwithWindowP<wSize>(mulWindowGPU, a, b);
  std::cout << "mul Comb method with window method on device (parallel)" << std::endl;
  std::cout << mulWindowGPU;

  F2_487_u64::square(sq, a);
  std::cout << "square on host" << std::endl;
  std::cout << sq;

  squareP<F2_487_u64>(sqGPU, a);
  std::cout << "square on device (parallel)" << std::endl;
  std::cout << sqGPU;



  F2_967_u64 test967, sq967, sq967GPU;
  test967.setRandom();

  F2_967_u64::square(sq967, test967);
  std::cout << "square on host, m=967" << std::endl;
  std::cout << sq967;

  squareP<F2_967_u64>(sq967GPU, test967);
  std::cout << "square on device, m=967 (parallel)" << std::endl;
  std::cout << sq967GPU;



  ExtField6_2<F2_487_u64> a487_62, b487_62, c487_62;
  ExtField6<F2_487_u64> a487_6, b487_6;
  ExtField6_2<F2_967_u64> a967_62, b967_62, c967_62;
  ExtField6<F2_967_u64> a967_6, b967_6;

  ExtField3_2_2<F2_487_u64> a487_322, b487_322, c487_322;
  ExtField3_2<F2_487_u64> a487_32, b487_32;
  ExtField3_2_2<F2_967_u64> a967_322, b967_322, c967_322;
  ExtField3_2<F2_967_u64> a967_32, b967_32;
  F2_487_u64 a487, c487, d487;
  F2_967_u64 a967, c967, d967;
  const int count = 100;


  std::cout << "mult. with window method (window size: 5) ext6_2 (m=487) on host" << std::endl;

  timing = 0.0;
  for (int i = 0; i < count; ++i) {
    a487_62.setRandom();
    b487_62.setRandom();
    gettimeofday(&start, 0);
    ExtField6_2<F2_487_u64>::mulCombL2RwithWindow6_2<5>(c487_62, a487_62, b487_62);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << std::dec << "timing: " << timing / count << " (microsec)" << std::endl;



  std::cout << "mult. with window method (window size: 5) ext6_2 (m=967) on host" << std::endl;
  timing = 0.0;
  for (int i = 0; i < count; ++i) {
    a967_62.setRandom();
    b967_62.setRandom();
    gettimeofday(&start, 0);
    ExtField6_2<F2_967_u64>::mulCombL2RwithWindow6_2<5>(c967_62, a967_62, b967_62);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << std::dec << "timing: " << timing / count << " (microsec)" << std::endl;



  std::cout << "sparse mult. (in pairing) with window method (window size: 5) ext3_2_2 (m=487) on host" << std::endl;

  timing = 0.0;
  for (int i = 0; i < count; ++i) {
    a487_322.setRandom();
    b487_322.setRandom();
    gettimeofday(&start, 0);
    ExtField3_2_2<F2_487_u64>::mul_sp_CombL2RwithWindow3_2_2<5>(c487_322, a487_322, b487_322);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << std::dec << "timing: " << timing / count << " (microsec)" << std::endl;



  std::cout << "sparse mult. (in pairing) with window method (window size: 5) ext3_2_2 (m=967) on host" << std::endl;
  timing = 0.0;
  for (int i = 0; i < count; ++i) {
    a967_322.setRandom();
    b967_322.setRandom();
    gettimeofday(&start, 0);
    ExtField3_2_2<F2_967_u64>::mul_sp_CombL2RwithWindow3_2_2<5>(c967_322, a967_322, b967_322);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << std::dec << "timing: " << timing / count << " (microsec)" << std::endl;



  std::cout << "AlphaBeta with window method (window size: 5) ext6_2 (m=487) on host" << std::endl;

  timing = 0.0;
  for (int i = 0; i < count; ++i) {
    a487_6.setRandom();
    b487_6.setRandom();
    gettimeofday(&start, 0);
    ExtField6_2<F2_487_u64>::alpha_beta6_2_withWindow<5>(c487_62, a487_6, b487_6);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << std::dec << "timing: " << timing / count << " (microsec)" << std::endl;



  std::cout << "AlphaBeta with window method (window size: 5) ext6_2 (m=967) on host" << std::endl;
  timing = 0.0;
  for (int i = 0; i < count; ++i) {
    a967_6.setRandom();
    b967_6.setRandom();
    gettimeofday(&start, 0);
    ExtField6_2<F2_967_u64>::alpha_beta6_2_withWindow<5>(c967_62, a967_6, b967_6);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << std::dec << "timing: " << timing / count << " (microsec)" << std::endl;



  std::cout << "AlphaBeta with window method (window size: 5) ext3_2_2 (m=487) on host" << std::endl;

  timing = 0.0;
  for (int i = 0; i < count; ++i) {
    a487_32.setRandom();
    b487_32.setRandom();
    d487.setRandom();
    gettimeofday(&start, 0);
    ExtField3_2_2<F2_487_u64>::alpha_beta3_2_2_withWindow<5>(c487_322, a487_32, b487_32, d487);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << std::dec << "timing: " << timing / count << " (microsec)" << std::endl;



  std::cout << "AlphaBeta with window method (window size: 5) ext3_2_2 (m=967) on host" << std::endl;
  timing = 0.0;
  for (int i = 0; i < count; ++i) {
    a967_32.setRandom();
    b967_32.setRandom();
    d967.setRandom();
    gettimeofday(&start, 0);
    ExtField3_2_2<F2_967_u64>::alpha_beta3_2_2_withWindow<5>(c967_322, a967_32, b967_32, d967);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << std::dec << "timing: " << timing / count << " (microsec)" << std::endl;



  std::cout << "square baesfield (m=487) on host" << std::endl;

  timing = 0.0;
  a487.setRandom();
  gettimeofday(&start, 0);
  for (int i = 0; i < count; ++i) {
    F2_487_u64::square(c487, a487);
  }
  gettimeofday(&end, 0);
  timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing (" << count << " times): " << timing / count << " (microsec)" << std::endl;



  std::cout << "square basefield (m=967) on host" << std::endl;
  timing = 0.0;
  a967.setRandom();
  gettimeofday(&start, 0);
  for (int i = 0; i < count; ++i) {
    F2_967_u64::square(c967, a967);
  }
  gettimeofday(&end, 0);
  timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing (" << count << " times): " << timing / count << " (microsec)" << std::endl;



  std::cout << "square ext3_2_2 (m=487) on host" << std::endl;

  timing = 0.0;
  for (int i = 0; i < count; ++i) {
    a487_322.setRandom();
    gettimeofday(&start, 0);
    ExtField3_2_2<F2_487_u64>::square3_2_2(c487_322, a487_322);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << std::dec << "timing: " << timing / count << " (microsec)" << std::endl;



  std::cout << "square ext3_2_2 (m=967) on host" << std::endl;
  timing = 0.0;
  for (int i = 0; i < count; ++i) {
    a967_322.setRandom();
    gettimeofday(&start, 0);
    ExtField3_2_2<F2_967_u64>::square3_2_2(c967_322, a967_322);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << std::dec << "timing: " << timing / count << " (microsec)" << std::endl;
}

void testAddMul_Multiple() {

  /* 1024程度で変数の数から静的配列確保の限界が見える */
  const int number = 1024;
  const int wSize = 5;

  /*** multiple class の変数を定義しすぎるとsegmentation fault ***/
  BaseField_Multiple<F2_487_u64, number> a, b, add, mul, mulWindow, sq, addGPU, mulShAddGPU, mulCombGPU, mulWindowGPU, sqGPU;
  ExtField6_Multiple<F2_487_u64, number> a6, b6, c6, c6GPU;
  ExtField3_2_2_Multiple<F2_487_u64, number> a322, b322, c322, c322GPU;
  ExtField6_2_Multiple<F2_487_u64, number> a62, b62, c62; /* この辺が限界 */

  F2_487_u64 mulWindowGPU_uni;
  bool eq;

  struct timeval start, end;
  double timing;

  a.setRandom();
  b.setRandom();
  a6.setRandom();
  b6.setRandom();
  a62.setRandom();
  b62.setRandom();
  a322.setRandom();
  b322.setRandom();

  int *testG;
  cudaMalloc( (void**)&testG, sizeof(int));

  std::cout << "add (multiple) on host" << std::endl;
  for (unsigned int i = 0; i < number; ++i) add.xBF[i] = a.xBF[i] + b.xBF[i];
  std::cout << std::dec;
  std::cout << number << "-th element: " << std::endl << add.xBF[number - 1] << std::endl;



  std::cout << "add (multiple) on device (parallel)" << std::endl;
  add_multipleP(addGPU.xBF, a.xBF, b.xBF, number);
  std::cout << std::dec;
  std::cout << number << "-th element: " << std::endl<< addGPU.xBF[number - 1] << std::endl;



  std::cout << "add (multiple) ext3_2_2 on host" << std::endl;
  gettimeofday(&start, 0);
  for (unsigned int i = 0; i < number; ++i) c322.xF3_2_2[i] = a322.xF3_2_2[i] + b322.xF3_2_2[i];
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element (first BF):" << std::endl << c322.xF3_2_2[number - 1].xF3_2[0].xF3[0].xBF[0] << std::endl;



  std::cout << "add ext3_2_2 (multiple) on device (parallel)" << std::endl;
  gettimeofday(&start, 0);
  add3_2_2_multipleP<F2_487_u64, number>(c322GPU, a322, b322);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element (first BF):" << std::endl << c322GPU.xF3_2_2[number - 1].xF3_2[0].xF3[0].xBF[0] << std::endl;



  std::cout << "mul Comb (multiple) on host" << std::endl;
  gettimeofday(&start, 0);
  for (unsigned int i = 0; i < number; ++i) F2_487_u64::mulCombR2L_uintSize(mul.xBF[i], a.xBF[i], b.xBF[i]);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << mul.xBF[number - 1] << std::endl;



  std::cout << "mul Comb (multiple) on device (parallel)" << std::endl;
  gettimeofday(&start, 0);
  mulCombR2L_uintSize_multipleP(mulCombGPU.xBF, a.xBF, b.xBF, number);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << mulCombGPU.xBF[number - 1] << std::endl;



  std::cout << "mul Comb method with window method (unit) on device (parallel)" << std::endl;
  mulCombL2RwithWindowP<wSize>(mulWindowGPU_uni, a.xBF[number - 1], b.xBF[number - 1]);
  //mulCombL2RwithWindow(mulWindowGPU_uni, a.xBF[0], b.xBF[0], wSize);
  std::cout << std::dec;
  std::cout << number << "-th operand:" << std::endl << mulWindowGPU_uni << std::endl;



  std::cout << "mul Comb window method (multiple) on host" << std::endl;
  gettimeofday(&start, 0);
  for (unsigned int i = 0; i < number; ++i) F2_487_u64::mulCombL2RwithWindow(mulWindow.xBF[i], a.xBF[i], b.xBF[i], wSize);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << mulWindow.xBF[number - 1] << std::endl;



  std::cout << "mul Comb method with window method (multiple) on device (parallel)" << std::endl;
  gettimeofday(&start, 0);
  mulCombL2RwithWindow_multipleP<F2_487_u64, wSize, number>(mulWindowGPU, a, b);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << mulWindowGPU.xBF[number - 1] << std::endl;



  std::cout << "square (multiple) on host" << std::endl;
  gettimeofday(&start, 0);
  for (unsigned int i = 0; i < number; ++i) F2_487_u64::square(sq.xBF[i], a.xBF[i]);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << sq.xBF[number - 1] << std::endl;



  std::cout << "square (multiple) on device (block parallel)" << std::endl;
  gettimeofday(&start, 0);
  square_multipleP<F2_487_u64, number>(sqGPU, a);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << sqGPU.xBF[number - 1] << std::endl;



  std::cout << "mul ext6 (Karatsuba6) (multiple) on host" << std::endl;
  gettimeofday(&start, 0);
  for (unsigned int i = 0; i < number; ++i) ExtField6<F2_487_u64>::mulCombL2RwithWindow6<wSize>(c6.xF6[i], a6.xF6[i], b6.xF6[i]);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c6.xF6[number - 1].xBF[0] << std::endl;



  std::cout << "mul ext6 (Karatsuba6) (multiple) on device (block parallel)" << std::endl;
  gettimeofday(&start, 0);
  mulCombL2RwithWindow6_multipleP<wSize, number>(c6GPU, a6, b6);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c6GPU.xF6[number - 1].xBF[0] << std::endl;



  std::cout << "mul ext6_2 (multiple) on host" << std::endl;
  gettimeofday(&start, 0);
  for (unsigned int i = 0; i < number; ++i) ExtField6_2<F2_487_u64>::mulCombL2RwithWindow6_2<wSize>(c62.xF6_2[i], a62.xF6_2[i], b62.xF6_2[i]);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c62.xF6_2[number - 1].xF6[0].xBF[0] << std::endl;



  std::cout << "mul ext6_2 (multiple) on device (block parallel)" << std::endl;
  gettimeofday(&start, 0);
  mulCombL2RwithWindow6_2_multipleP<wSize, number>(c62, a62, b62);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c62.xF6_2[number - 1].xF6[0].xBF[0] << std::endl;



  std::cout << "AlphBbeta6_2 (multiple) on host" << std::endl;
  gettimeofday(&start, 0);
  for (unsigned int i = 0; i < number; ++i) ExtField6_2<F2_487_u64>::alpha_beta6_2_withWindow<wSize>(c62.xF6_2[i], a6.xF6[i], b6.xF6[i]);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c62.xF6_2[number - 1].xF6[0].xBF[0] << std::endl;


  std::cout << "AlphBbeta6_2 (multiple) on device (block parallel)" << std::endl;
  gettimeofday(&start, 0);
  alpha_beta6_2_withWindow_multipleP<wSize, number>(c62, a6, b6);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c62.xF6_2[number - 1].xF6[0].xBF[0] << std::endl;

  cudaFree(testG);
}

void testAddMul_Multiple2() {

  /* 1024程度で変数の数から静的配列確保の限界が見える */
  const int number = 1024;
  const int wSize = 5;

  /*** multiple class の変数を定義しすぎるとsegmentation fault ***/
  BaseField_Multiple<F2_487_u64, number> d;
  ExtField3_Multiple<F2_487_u64, number> a3, b3, c3;
  ExtField3_2_Multiple<F2_487_u64, number> a32, b32, c32;
  ExtField3_2_2_Multiple<F2_487_u64, number> a322, b322, c322;

  struct timeval start, end;
  double timing;

  a3.setRandom();
  b3.setRandom();
  a32.setRandom();
  b32.setRandom();
  a322.setRandom();
  b322.setRandom();

  int *testG;
  cudaMalloc( (void**)&testG, sizeof(int));



  std::cout << "mul ext3 (Karatsuba3) (multiple) on host" << std::endl;
  gettimeofday(&start, 0);
  for (unsigned int i = 0; i < number; ++i) ExtField3<F2_487_u64>::mulCombL2RwithWindow3<wSize>(c3.xF3[i], a3.xF3[i], b3.xF3[i]);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c3.xF3[number - 1].xBF[0] << std::endl;

  std::cout << "mul ext3 (Karatsuba3) (multiple) on device (block parallel)" << std::endl;
  gettimeofday(&start, 0);
  mulCombL2RwithWindow3_multipleP<wSize, number>(c3, a3, b3);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c3.xF3[number - 1].xBF[0] << std::endl;



  std::cout << "mul ext3_2 (multiple) on host" << std::endl;
  gettimeofday(&start, 0);
  for (unsigned int i = 0; i < number; ++i) ExtField3_2<F2_487_u64>::mulCombL2RwithWindow3_2<wSize>(c32.xF3_2[i], a32.xF3_2[i], b32.xF3_2[i]);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c32.xF3_2[number - 1].xF3[0].xBF[0] << std::endl;

  std::cout << "mul ext3_2 (multiple) on device (block parallel)" << std::endl;
  gettimeofday(&start, 0);
  mulCombL2RwithWindow3_2_multipleP<wSize, number>(c32, a32, b32);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c32.xF3_2[number - 1].xF3[0].xBF[0] << std::endl;



  std::cout << "mul ext3_2_2 (multiple) on host" << std::endl;
  gettimeofday(&start, 0);
  for (unsigned int i = 0; i < number; ++i) ExtField3_2_2<F2_487_u64>::mulCombL2RwithWindow3_2_2<wSize>(c322.xF3_2_2[i], a322.xF3_2_2[i], b322.xF3_2_2[i]);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c322.xF3_2_2[number - 1].xF3_2[0].xF3[0].xBF[0] << std::endl;

  std::cout << "mul ext3_2_2 (multiple) on device (block parallel)" << std::endl;
  gettimeofday(&start, 0);
  mulCombL2RwithWindow3_2_2_multipleP<wSize, number>(c322, a322, b322);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c322.xF3_2_2[number - 1].xF3_2[0].xF3[0].xBF[0] << std::endl;



  std::cout << "mul ext3_2_2 sparse (multiple) on host" << std::endl;
  gettimeofday(&start, 0);
  for (unsigned int i = 0; i < number; ++i) ExtField3_2_2<F2_487_u64>::mul_sp_CombL2RwithWindow3_2_2<wSize>(c322.xF3_2_2[i], a322.xF3_2_2[i], b322.xF3_2_2[i]);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c322.xF3_2_2[number - 1].xF3_2[0].xF3[0].xBF[0] << std::endl;

  std::cout << "mul ext3_2_2 sparse (multiple) on device (block parallel)" << std::endl;
  gettimeofday(&start, 0);
  mul_sp_CombL2RwithWindow3_2_2_multipleP<wSize, number>(c322, a322, b322);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c322.xF3_2_2[number - 1].xF3_2[0].xF3[0].xBF[0] << std::endl;



  std::cout << "AlphBbeta3_2_2 (multiple) on host" << std::endl;
  gettimeofday(&start, 0);
  for (unsigned int i = 0; i < number; ++i) ExtField3_2_2<F2_487_u64>::alpha_beta3_2_2_withWindow<wSize>(c322.xF3_2_2[i], a32.xF3_2[i], b32.xF3_2[i], d.xBF[i]);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c322.xF3_2_2[number - 1].xF3_2[0].xF3[0].xBF[0] << std::endl;


  std::cout << "AlphBbeta3_2_2 (multiple) on device (block parallel)" << std::endl;
  gettimeofday(&start, 0);
  alpha_beta3_2_2_withWindow_multipleP<wSize, number>(c322, a32, b32, d);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c322.xF3_2_2[number - 1].xF3_2[0].xF3[0].xBF[0] << std::endl;

  cudaFree(testG);
}

void testSquare_Multiple() {

  const int number = 1024;
  const int wSize = 5;

  /*** multiple class の変数を定義しすぎるとsegmentation fault ***/
  ExtField6_Multiple<F2_487_u64, number> a6, c6;
  ExtField6_2_Multiple<F2_487_u64, number> a62, c62;
  ExtField3_Multiple<F2_487_u64, number> a3, c3;
  ExtField3_2_Multiple<F2_487_u64, number> a32, c32;
  ExtField3_2_2_Multiple<F2_487_u64, number> a322, c322;

  struct timeval start, end;
  double timing;

  a6.setRandom();
  a62.setRandom();
  a3.setRandom();
  a32.setRandom();
  a322.setRandom();

  int *testG;
  cudaMalloc( (void**)&testG, sizeof(int));



  std::cout << "square6 (multiple) on host" << std::endl;
  gettimeofday(&start, 0);
  for (unsigned int i = 0; i < number; ++i) ExtField6<F2_487_u64>::square6(c6.xF6[i], a6.xF6[i]);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c6.xF6[number - 1].xBF[0] << std::endl;



  std::cout << "square6 (multiple) on device (block parallel)" << std::endl;
  gettimeofday(&start, 0);
  square6_multipleP<number>(c6, a6);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c6.xF6[number - 1].xBF[0] << std::endl;



  std::cout << "square6_2 (multiple) on host" << std::endl;
  gettimeofday(&start, 0);
  for (unsigned int i = 0; i < number; ++i) ExtField6_2<F2_487_u64>::square6_2(c62.xF6_2[i], a62.xF6_2[i]);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c62.xF6_2[number - 1].xF6[0].xBF[0] << std::endl;



  std::cout << "square6_2 (multiple) on device (block parallel)" << std::endl;
  gettimeofday(&start, 0);
  square6_2_multipleP<number>(c62, a62);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c62.xF6_2[number - 1].xF6[0].xBF[0] << std::endl;



  std::cout << "square3 (multiple) on host" << std::endl;
  gettimeofday(&start, 0);
  for (unsigned int i = 0; i < number; ++i) ExtField3<F2_487_u64>::square3(c3.xF3[i], a3.xF3[i]);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c3.xF3[number - 1].xBF[0] << std::endl;



  std::cout << "square3 (multiple) on device (block parallel)" << std::endl;
  gettimeofday(&start, 0);
  square3_multipleP<number>(c3, a3);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c3.xF3[number - 1].xBF[0] << std::endl;




  std::cout << "square3_2 (multiple) on host" << std::endl;
  gettimeofday(&start, 0);
  for (unsigned int i = 0; i < number; ++i) ExtField3_2<F2_487_u64>::square3_2(c32.xF3_2[i], a32.xF3_2[i]);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c32.xF3_2[number - 1].xF3[0].xBF[0] << std::endl;



  std::cout << "square3_2 (multiple) on device (block parallel)" << std::endl;
  gettimeofday(&start, 0);
  square3_2_multipleP<number>(c32, a32);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c32.xF3_2[number - 1].xF3[0].xBF[0] << std::endl;



  std::cout << "square3_2_2 (multiple) on host" << std::endl;
  gettimeofday(&start, 0);
  for (unsigned int i = 0; i < number; ++i) ExtField3_2_2<F2_487_u64>::square3_2_2(c322.xF3_2_2[i], a322.xF3_2_2[i]);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c322.xF3_2_2[number - 1].xF3_2[0].xF3[0].xBF[0] << std::endl;



  std::cout << "square3_2_2 (multiple) on device (block parallel)" << std::endl;
  gettimeofday(&start, 0);
  square3_2_2_multipleP<number>(c322, a322);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c322.xF3_2_2[number - 1].xF3_2[0].xF3[0].xBF[0] << std::endl;

  cudaFree(testG);
}

void test967AddMul_Multiple() {

  const int number = 1024;
  const int wSize = 4;

  /*** multiple class の変数を定義しすぎるとsegmentation fault ***/
  BaseField_Multiple<F2_967_u64, number> a, b, mulWindow, mulWindowGPU;
  ExtField3_2_2_Multiple<F2_967_u64, number> a3229, b3229, c3229;

  struct timeval start, end;
  double timing;

  a3229.setRandom();
  b3229.setRandom();

  int *testG;
  cudaMalloc( (void**)&testG, sizeof(int));



  std::cout << "mul window method (multiple) on host" << std::endl;
  gettimeofday(&start, 0);
  for (unsigned int i = 0; i < number; ++i) F2_967_u64::mulCombL2RwithWindow(mulWindow.xBF[i], a.xBF[i], b.xBF[i], wSize);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << mulWindow.xBF[number - 1] << std::endl;



  std::cout << "mul Comb method with window method (multiple) on device (parallel)" << std::endl;
  gettimeofday(&start, 0);
  mulCombL2RwithWindow_multipleP<F2_967_u64, wSize, number>(mulWindowGPU, a, b);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << mulWindowGPU.xBF[number - 1] << std::endl;



  std::cout << "mul (m=967) ext3_2_2 sparse (multiple) on host" << std::endl;
  gettimeofday(&start, 0);
  for (unsigned int i = 0; i < number; ++i) ExtField3_2_2<F2_967_u64>::mul_sp_CombL2RwithWindow3_2_2<wSize>(c3229.xF3_2_2[i], a3229.xF3_2_2[i], b3229.xF3_2_2[i]);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c3229.xF3_2_2[number - 1].xF3_2[0].xF3[0].xBF[0] << std::endl;

  std::cout << "mul (m=967) ext3_2_2 sparse (multiple) on device (block parallel)" << std::endl;
  gettimeofday(&start, 0);
  mul_sp_CombL2RwithWindow3_2_2_multipleP<wSize, number>(c3229, a3229, b3229);
  gettimeofday(&end, 0);
  timing = (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  std::cout << std::dec << "timing: " << timing << " (microsec)" << std::endl;
  std::cout << number << "-th element:" << std::endl << c3229.xF3_2_2[number - 1].xF3_2[0].xF3[0].xBF[0] << std::endl;

  cudaFree(testG);
}

void test_pairingMultiple_profile() {

  const int number = 1024;
  const int wSize = 5;

  const int Count = 1;
  struct timeval start, end;
  double timing;

  BaseField_Multiple<F2_487_u64, number> xP, xQ, yP, yQ;
  ExtField6_2_Multiple<F2_487_u64, number> pairing6_2;
  ExtField3_2_2_Multiple<F2_487_u64, number> pairing3_2_2;

  BaseField_Multiple<F2_967_u64, number> xP9, xQ9, yP9, yQ9;
  ExtField6_2_Multiple<F2_967_u64, number> pairing6_29;
  ExtField3_2_2_Multiple<F2_967_u64, number> pairing3_2_29;

  // GPU 起動 for timing
  int *testGPU_;
  cudaMalloc((void**)&testGPU_, sizeof(int));



  // m=487
  // ext3_2_2 GPU
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    xP.setRandom();
    xQ.setRandom();
    yP.setRandom();
    yQ.setRandom();

    gettimeofday(&start, 0);
    pairing3_2_2 = dummy_pairing3_2_2_multipleP<F2_487_u64, wSize, number>(xP, xQ, yP, yQ);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << "ext3_2_2, m=487 --- window size: " << wSize << ", number of pairings: " << number << std::endl;
  std::cout << "total time: " << timing / Count / 1.0e+6 << std::endl;
  std::cout << "timing per pairing: " << timing / Count / 1.0e+6 / number << std::endl;

  cudaFree(testGPU_);
}

void test_basicFunction_profile() {

  const int number = 1024;
  const int wSize = 5;
  const int Count = 100;

  struct timeval start, end;
  double timing;


  BaseField_Multiple<F2_487_u64, number> a, b, c;
  ExtField6_Multiple<F2_487_u64, number> a6, b6, c6;
  ExtField6_2_Multiple<F2_487_u64, number> a62, b62, c62;
  ExtField3_Multiple<F2_487_u64, number> a3, b3, c3;
  ExtField3_2_2_Multiple<F2_487_u64, number> a322, b322, c322;

  // GPU 起動 for timing
  int *testGPU_;
  cudaMalloc((void**)&testGPU_, sizeof(int));



  std::cout << "Count: " << Count << std::endl;

  // m=487
  // multiplication with window method in base field
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    a.setRandom();
    b.setRandom();

    gettimeofday(&start, 0);
    mulCombL2RwithWindow_multipleP<F2_487_u64, wSize, number>(c, a, b);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << "multiplication in base field, m=487 --- window size: " << wSize << ", number of multiplications: " << number << std::endl;
  std::cout << "total time: " << timing / Count / 1.0e+6 << std::endl;
  std::cout << "timing per multiplication: " << timing / Count / 1.0e+6 / number << std::endl;


#if 1
  // Karatsuba6
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    a6.setRandom();
    b6.setRandom();

    gettimeofday(&start, 0);
    mulCombL2RwithWindow6_multipleP<wSize, number>(c6, a6, b6);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << "multiplication in 6-th extension field (Karatsuba6), m=487 --- window size: " << wSize << ", number of multiplications: " << number << std::endl;
  std::cout << "total time: " << timing / Count / 1.0e+6 << std::endl;
  std::cout << "timing per multiplication: " << timing / Count / 1.0e+6 / number << std::endl;



  // Karatsuba3
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    a3.setRandom();
    b3.setRandom();

    gettimeofday(&start, 0);
    mulCombL2RwithWindow3_multipleP<wSize, number>(c3, a3, b3);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << "multiplication in cubic extension field (Karatsuba3), m=487 --- window size: " << wSize << ", number of multiplications: " << number << std::endl;
  std::cout << "total time: " << timing / Count / 1.0e+6 << std::endl;
  std::cout << "timing per multiplication: " << timing / Count / 1.0e+6 / number << std::endl;



  // mult. in ext6_2
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    a62.setRandom();
    b62.setRandom();

    gettimeofday(&start, 0);
    mulCombL2RwithWindow6_2_multipleP<wSize, number>(c62, a62, b62);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << "multiplication extension field 6_2, m=487 --- window size: " << wSize << ", number of multiplications: " << number << std::endl;
  std::cout << "total time: " << timing / Count / 1.0e+6 << std::endl;
  std::cout << "timing per multiplication: " << timing / Count / 1.0e+6 / number << std::endl;



  // mult. in ext3_2_2
  timing = 0.0;
  for (unsigned int i = 0; i < Count; ++i) {

    a322.setRandom();
    b322.setRandom();

    gettimeofday(&start, 0);
    mulCombL2RwithWindow3_2_2_multipleP<wSize, number>(c322, a322, b322);
    gettimeofday(&end, 0);
    timing += (end.tv_sec - start.tv_sec) * 1.0e+6 + (end.tv_usec - start.tv_usec);
  }
  std::cout << "multiplication extension field 3_2_2, m=487 --- window size: " << wSize << ", number of multiplications: " << number << std::endl;
  std::cout << "total time: " << timing / Count / 1.0e+6 << std::endl;
  std::cout << "timing per multiplication: " << timing / Count / 1.0e+6 / number << std::endl;

#else
#endif

  cudaFree(testGPU_);
}
