#pragma once

#include "F2_487.h"
#include "F2_967.h"
#include "ext_field6_2.h"
#include "ext_field3_2_2.h"

// GPU implementation of operation in base field
extern "C" {
// ext field

void alpha_beta6_2multipleP(ExtField6_2<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b);
}



// unit
template<class Basefield>
void addP(Basefield& c, const Basefield& a, const Basefield& b);

template<class Basefield>
void mulShAddP(Basefield& c, const Basefield& a, const Basefield& b);

template<class Basefield>
void mulCombR2L_uintSizeP(Basefield& c, const Basefield& a, const Basefield& b);

template<int wSize>
void mulCombL2RwithWindowP(F2_487_u64& c, const F2_487_u64& a, const F2_487_u64& b);

template<int wSize>
void mulCombL2RwithWindowP(F2_967_u64& c, const F2_967_u64& a, const F2_967_u64& b);

template<class Basefield>
void squareP(Basefield& c, const Basefield& a);

// multiple
template<class Basefield>
void add_multipleP(Basefield *c, const Basefield *a, const Basefield *b, const int number);

template<class Basefield>
void add6_multipleP(ExtField6<Basefield> *c, const ExtField6<Basefield> *a, const ExtField6<Basefield> *b, const int number);

template<class Basefield>
void add6_2_multipleP(ExtField6_2<Basefield> *c, const ExtField6_2<Basefield> *a, const ExtField6_2<Basefield> *b, const int number);

template<class Basefield>
void add3_multipleP(ExtField3<Basefield> *c, const ExtField3<Basefield> *a, const ExtField3<Basefield> *b, const int number);

template<class Basefield>
void add3_2_multipleP(ExtField3_2<Basefield> *c, const ExtField3_2<Basefield> *a, const ExtField3_2<Basefield> *b, const int number);

template<class Basefield>
void add3_2_2_multipleP(ExtField3_2_2<Basefield> *c, const ExtField3_2_2<Basefield> *a, const ExtField3_2_2<Basefield> *b, const int number);

template<class Basefield>
void mulCombR2L_uintSize_multipleP(Basefield *c, const Basefield *a, const Basefield *b, const int number);

template<class Basefield, int wSize>
void mulCombL2RwithWindow_multipleP(Basefield *c, const Basefield *a, const Basefield *b, const int number);

template<class Basefield>
void square_multipleP(Basefield *c, const Basefield *a, const int number);

//Karatsuba 6
template<int wSize>
void mulCombL2RwithWindow6_multipleP(ExtField6<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b, const int number);

void square6_multipleP(ExtField6<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const int number);

template<int wSize>
void mulCombL2RwithWindow6_2_multipleP(ExtField6_2<F2_487_u64> *c, const ExtField6_2<F2_487_u64> *a, const ExtField6_2<F2_487_u64> *b, const int number);

void square6_2_multipleP(ExtField6_2<F2_487_u64> *c, const ExtField6_2<F2_487_u64> *a, const int number);

template<int wSize>
void alpha_beta6_2_withWindow_multipleP(ExtField6_2<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b, const int number);

template<int wSize>
void mulCombL2RwithWindow6_multipleP(ExtField6<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const ExtField6<F2_967_u64> *b, const int number);

void square6_multipleP(ExtField6<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const int number);

template<int wSize>
void mulCombL2RwithWindow6_2_multipleP(ExtField6_2<F2_967_u64> *c, const ExtField6_2<F2_967_u64> *a, const ExtField6_2<F2_967_u64> *b, const int number);

void square6_2_multipleP(ExtField6_2<F2_967_u64> *c, const ExtField6_2<F2_967_u64> *a, const int number);

template<int wSize>
void alpha_beta6_2_withWindow_multipleP(ExtField6_2<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const ExtField6<F2_967_u64> *b, const int number);



//Karatsuba 3
template<int wSize>
void mulCombL2RwithWindow3_multipleP(ExtField3<F2_487_u64> *c, const ExtField3<F2_487_u64> *a, const ExtField3<F2_487_u64> *b, const int number);

void square3_multipleP(ExtField3<F2_487_u64> *c, const ExtField3<F2_487_u64> *a, const int number);

template<int wSize>
void mulCombL2RwithWindow3_2_multipleP(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const int number);

template<int wSize>
void mul_sp_CombL2RwithWindow3_2_multipleP(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const int number);

void square3_2_multipleP(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const int number);

template<int wSize>
void mulCombL2RwithWindow3_2_2_multipleP(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const ExtField3_2_2<F2_487_u64> *b, const int number);

template<int wSize>
void mul_sp_CombL2RwithWindow3_2_2_multipleP(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const ExtField3_2_2<F2_487_u64> *b, const int number);

void square3_2_2_multipleP(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const int number);

template<int wSize>
void alpha_beta3_2_2_withWindow_multipleP(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const F2_487_u64 *d, const int number);

template<int wSize>
void mulCombL2RwithWindow3_multipleP(ExtField3<F2_967_u64> *c, const ExtField3<F2_967_u64> *a, const ExtField3<F2_967_u64> *b, const int number);

void square3_multipleP(ExtField3<F2_967_u64> *c, const ExtField3<F2_967_u64> *a, const int number);

template<int wSize>
void mulCombL2RwithWindow3_2_multipleP(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const int number);

template<int wSize>
void mul_sp_CombL2RwithWindow3_2_multipleP(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const int number);

void square3_2_multipleP(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const int number);

template<int wSize>
void mulCombL2RwithWindow3_2_2_multipleP(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const ExtField3_2_2<F2_967_u64> *b, const int number);

template<int wSize>
void mul_sp_CombL2RwithWindow3_2_2_multipleP(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const ExtField3_2_2<F2_967_u64> *b, const int number);

void square3_2_2_multipleP(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const int number);

template<int wSize>
void alpha_beta3_2_2_withWindow_multipleP(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const F2_967_u64 *d, const int number);



//dummy pairing
//template<class Basefield, int number>
//ExtField6_2_Multiple<Basefield, number> dummyPairing6_2_multiple (const BaseField_Multiple<Basefield ,number>& xp0, const BaseField_Multiple<Basefield ,number>& xq0, const BaseField_Multiple<Basefield ,number>& yp0, const BaseField_Multiple<Basefield ,number>& yq0, int wSize);

//template<class Basefield, int number>
//ExtField3_2_2_Multiple<Basefield, number> dummyPairing3_2_2_multiple (const BaseField_Multiple<Basefield ,number>& xp0, const BaseField_Multiple<Basefield ,number>& xq0, const BaseField_Multiple<Basefield ,number>& yp0, const BaseField_Multiple<Basefield ,number>& yq0, int wSize);



//timing
void pairing_timing();
void pairingCPU_timing();
void pairingMUltiple_timing();
void pairingMUltiple_timingProfile();
void pairing_getTiming();
void pairingHost_getTiming();
void dummy_pairing_getTiming();
void dummy_pairingMultiple_getTiming();

// debug
void sandbox();
void pairing_host();
void testDeviceFunc();
void testTmplateCall();

// debug device
extern "C" {
void test_basic();
void test_basic2();
void test_AddMul_BF();
void testMultipleCall();
}

template<class Basefield, int number>
void testMultipleCall_Template();

template<class Basefield>
void mulCombR2L_uintSizeP2(Basefield& c, const Basefield& a, const Basefield& b);

// test
void testAddMul();
void testAddMul_Multiple();
void testAddMul_Multiple2();
void testSquare_Multiple();
void test967AddMul_Multiple();

void test_pairingMultiple_profile();
void test_basicFunction_profile();



// test ptx
extern "C"
void mulGPU64();

extern "C"
void mulGPU64High();

extern "C"
void mulGPU32Wide();

extern "C"
void mulGPU32();
