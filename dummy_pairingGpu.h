#pragma once

#include "callTemplate_Multiple.h"
#include "F2_487.h"
#include "F2_967.h"
#include "ext_field6_2.h"
#include "ext_field3_2_2.h"

/*
 * 変数を使い回し計算結果に関わらずスケーラビリティ等の評価
 */

template<class Basefield, int wSize, int number>
__host__ __device__
ExtField6_2_Multiple<Basefield, number> dummy_pairing6_2_multipleP (const BaseField_Multiple<Basefield ,number>& xp0, const BaseField_Multiple<Basefield ,number>& xq0, const BaseField_Multiple<Basefield ,number>& yp0, const BaseField_Multiple<Basefield ,number>& yq0) {

  static const int degBF = Basefield::ext_degree;

  ExtField6_2_Multiple<Basefield, number> tmp6_2, tmp6_2A;
  tmp6_2.clear();
  tmp6_2.plusOne();

  ExtField6_Multiple<Basefield, number> b4_F6;
  b4_F6.clear();
  b4_F6.clear();
  b4_F6.clear();

  BaseField_Multiple<Basefield, number> tmpBF_1;
  BaseField_Multiple<Basefield, number> tmpCopy;

  int k1, k2, k3, k4, k5, k6;

  // precomputation
  tmpBF_1 = xp0;
  tmpBF_1 = xq0;
  tmpBF_1 = yp0;
  tmpBF_1 = yq0;

  for (int i = 1; i < degBF; ++i) {
    //Basefield::square(xP[i], xP[i - 1]);
    //Basefield::square(xQ[i], xQ[i - 1]);
    //Basefield::square(yP[i], yP[i - 1]);
    //Basefield::square(yQ[i], yQ[i - 1]);

    square_multipleP<Basefield, number>(tmpBF_1, xp0);
    square_multipleP<Basefield, number>(tmpBF_1, xp0);
    square_multipleP<Basefield, number>(tmpBF_1, xp0);
    square_multipleP<Basefield, number>(tmpBF_1, xp0);
  }

  //for (unsigned int i = 0; i < degBF; ++i) {
  //  std::cout << "xP[" << i << "]:" << xP[i];
  //}

  // Miller loop
  for (int i = 0; i < (degBF - 3) / 2; ++i) {


    k1 = ((3 * degBF - 9 - 6 * i) / 2) % degBF;
    k2 = (k1 + 1) % degBF;
    k3 = (k2 + 1) % degBF;
    k4 = ((3 * degBF - 3 - 6 * i) / 2) % degBF;
    k5 = (k4 + 1) % degBF;
    k6 = (k5 + 1) % degBF;

    for (unsigned int j = 0; j < number; ++j) {
      // compute \alpha = (alpha,1)
      // alpha = a0 + a1w + a2w^2 + a4w^4

      // a4 = xP[k4] + xP[k5]
      //alpha.xBF[4] = xP[k4] + xP[k5];
      b4_F6.xF6[j].xBF[4] = xp0.xBF[j] + xq0.xBF[j];

      // a2 = xP[k4] + 1 + xQ[k3]
      //alpha.xBF[2] = xP[k4] + xQ[k3];
      b4_F6.xF6[j].xBF[2] = xp0.xBF[j] + xq0.xBF[j];

      //alpha.xBF[2].plusOne();
      b4_F6.xF6[j].xBF[2].plusOne();

      // a1 = xQ[k2] + xQ[k3]
      //alpha.xBF[1] = xQ[k2] + xQ[k3];
      b4_F6.xF6[j].xBF[1] = xp0.xBF[j] + xq0.xBF[j];
    }

    //a0 = yQ[k2] + a2xQ[k2] + a4xQ[k3] + yP[k4]
    //Basefield::mulCombR2L_uintSize(tmpBF_1, alpha.xBF[2], xQ[k2]);
    for (unsigned int j = 0; j < number; ++j) tmpCopy.xBF[j] = b4_F6.xF6[j].xBF[2];
    mulCombL2RwithWindow_multipleP<Basefield, wSize, number>(tmpBF_1, tmpCopy, xp0);

    //alpha.xBF[0] = yQ[k2] + tmpBF_1;
    for (unsigned int j = 0; j < number; ++j) b4_F6.xF6[j].xBF[0] = yp0.xBF[j] + tmpBF_1.xBF[j];


    //Basefield::mulCombR2L_uintSize(tmpBF_1, alpha.xBF[4], xQ[k3]);
    for (unsigned int j = 0; j < number; ++j) tmpCopy.xBF[j] = b4_F6.xF6[j].xBF[4];
    mulCombL2RwithWindow_multipleP<Basefield, wSize, number>(tmpBF_1, tmpCopy, xp0);

    for (unsigned int j = 0; j < number; ++j) {
      //alpha.xBF[0] += tmpBF_1;
      b4_F6.xF6[j].xBF[0] += tmpBF_1.xBF[j];

      //alpha.xBF[0] += yP[k4];
      b4_F6.xF6[j].xBF[0] += yp0.xBF[j];



      // compute \beta = (beta,1)
      // beta = b0 + b1w + b2w^2 + b4w^4

      // b1 = xP[k5] + xP[k6]
      //beta.xBF[1] = xP[k5] + xP[k6];
      b4_F6.xF6[j].xBF[1] = xp0.xBF[j] + xq0.xBF[j];


      // b2 = xP[k6] + xQ[k1] + 1
      //beta.xBF[2] = xP[k6] + xQ[k1];
      b4_F6.xF6[j].xBF[2] = xp0.xBF[j] + xq0.xBF[j];

      //beta.xBF[2].plusOne();
      b4_F6.xF6[j].xBF[2].plusOne();


      // b4 = xQ[k1] + xQ[k2] + 1
      //beta.xBF[4] = xQ[k1] + xQ[k2];
      b4_F6.xF6[j].xBF[4] = xp0.xBF[j] + xq0.xBF[j];

      //beta.xBF[4].plusOne();
      b4_F6.xF6[j].xBF[4].plusOne();
    }

    // b0 = yQ[k1] + b1xQ[k1] + yP[k5] + xP[k6](xP[k5] + xQ[k2]) + xP[k5]
    //Basefield::mulCombR2L_uintSize(tmpBF_1, beta.xBF[1], xQ[k1]);
    for (unsigned int j = 0; j < number; ++j) tmpCopy.xBF[j] = b4_F6.xF6[j].xBF[1];
    mulCombL2RwithWindow_multipleP<Basefield, wSize, number>(tmpBF_1, tmpCopy, xp0);

    for (unsigned int j = 0; j < number; ++j) {
      //beta.xBF[0] = yQ[k1] + tmpBF_1;
      b4_F6.xF6[j].xBF[0] = yp0.xBF[j] + tmpBF_1.xBF[j];

      //beta.xBF[0] += yP[k5];
      b4_F6.xF6[j].xBF[0] += yp0.xBF[j];

      //beta.xBF[0] += xP[k5];
      b4_F6.xF6[j].xBF[0] += xp0.xBF[j];
    }

    //Basefield::mulCombR2L_uintSize(tmpBF_1, xP[k6], xP[k5] + xQ[k2]);
    for (unsigned int j = 0; j < number; ++j) tmpCopy.xBF[j] = xp0.xBF[j] + xq0.xBF[j];
    mulCombL2RwithWindow_multipleP<Basefield, wSize, number>(tmpBF_1, xp0, tmpCopy);

    //beta.xBF[0] += tmpBF_1;
    for (unsigned int j = 0; j < number; ++j) b4_F6.xF6[j].xBF[0] += tmpBF_1.xBF[j];


    // \alpha\beta
    //ExtField6_2<Basefield>::alpha_beta6_2(alphaBeta, alpha, beta);
    alpha_beta6_2_withWindow_multipleP<wSize, number>(tmp6_2, b4_F6, b4_F6);

    //ExtField6_2<Basefield>::mulCombR2L_uintSize6_2(tmp6_2, pairingValue, alphaBeta);
    mulCombL2RwithWindow6_2_multipleP<wSize, number>(tmp6_2, tmp6_2A, tmp6_2A);
    for (unsigned int j = 0; j < number; ++j) tmp6_2A.xF6_2[j] = tmp6_2.xF6_2[j];
  }

  for (unsigned int j = 0; j < number; ++j) {
    // extract current point (xP,yP)
    //xpBF = xP[degBF - 3];
    //xpBF.plusOne();
    tmpBF_1.xBF[j] = xp0.xBF[j];
    tmpBF_1.xBF[j].plusOne();


    //ypBF = yP[degBF - 3] + xP[degBF - 2];
    tmpBF_1.xBF[j] = yp0.xBF[j] + xp0.xBF[j];


    // final doublings / addition
    /*
    b4_F6 = (yQ + xQ[1](1 + xQ + xpBF^8 + xpBF^4) + xpBF^4xQ + ypBF^4) +
            (xQ[1] + xpBF^4)w + (xpBF^8 + xpBF^4)w^2 + w^3 + (xQ[1] + xQ)w^4
    */

    // b4_3
    //b4_F6.xBF[3].plusOne();
    b4_F6.xF6[j].xBF[3].plusOne();

    // b4_4
    //b4_F6.xBF[4] = xQ[1] + xq0;
    b4_F6.xF6[j].xBF[4] = xp0.xBF[j] + xq0.xBF[j];
  }

  // xpBF := xpBF^4
  //Basefield::square(tmpBF_1, xpBF);
  square_multipleP<Basefield, number>(tmpBF_1, xp0);

  //Basefield::square(xpBF, tmpBF_1);
  square_multipleP<Basefield, number>(tmpBF_1, xp0);

  // b4_1
  //b4_F6.xBF[1] = xQ[1] + tmpBF_1;
  for (unsigned int j = 0; j < number; ++j) b4_F6.xF6[j].xBF[1] = xp0.xBF[j] + tmpBF_1.xBF[j];

  // tmpBF_1 := xpBF^8
  //Basefield::square(tmpBF_1, xpBF);
  square_multipleP<Basefield, number>(tmpBF_1, xp0);

  for (unsigned int j = 0; j < number; ++j) {
    // b4_2;
    //b4_F6.xBF[2] = tmpBF_1 + xpBF;
    b4_F6.xF6[j].xBF[2] = tmpBF_1.xBF[j] + xp0.xBF[j];

    // b4_0
    // tmpBF_1 := b4_2 + xq0
    //tmpBF_1 = b4_F6.xBF[2] + xq0;
    tmpBF_1.xBF[j] = b4_F6.xF6[j].xBF[2] + xq0.xBF[j];

    //tmpBF_1.plusOne();
    tmpBF_1.plusOne();
  }

  //Basefield::mulCombR2L_uintSize(b4_F6.xBF[0],xQ[1],tmpBF_1);
  mulCombL2RwithWindow_multipleP<Basefield, wSize, number>(tmpCopy, xp0, tmpBF_1);
  for (unsigned int j = 0; j < number; ++j) b4_F6.xF6[j].xBF[0] = tmpCopy.xBF[j];

  //tmpBF_1 = xpBF^4xq0
  //Basefield::mulCombR2L_uintSize(tmpBF_1, xpBF, xq0);
  mulCombL2RwithWindow_multipleP<Basefield, wSize, number>(tmpBF_1, xp0, xq0);



  for (unsigned int j = 0; j < number; ++j) {
    //b4_F6.xBF[0] += tmpBF_1;
    b4_F6.xF6[j].xBF[0] += tmpBF_1.xBF[j];

    //b4_F6.xBF[0] += yq0;
    b4_F6.xF6[j].xBF[0] += yq0.xBF[j];
  }

  // ypBF := ypBF^4
  //Basefield::square(tmpBF_1, ypBF);
  square_multipleP<Basefield, number>(tmpBF_1, yp0);

  //Basefield::square(ypBF, tmpBF_1);
  square_multipleP<Basefield, number>(tmpBF_1, yp0);

  //b4_F6.xBF[0] += ypBF;
  for (unsigned int j = 0; j < number; ++j) b4_F6.xF6[j].xBF[0] += yp0.xBF[j];

  // f^4
  //ExtField6_2<Basefield>::square6_2(tmp6_2, pairingValue);
  square6_2_multipleP<number>(tmp6_2, tmp6_2A);

  //ExtField6_2<Basefield>::square6_2(pairingValue, tmp6_2);
  square6_2_multipleP<number>(tmp6_2A, tmp6_2);

  // f^4(y+b4(x))
  /* 未実装 host函数で行う */
  for (unsigned int j = 0; j < number; ++j) {
    ExtField6_2<Basefield>::sparseMul6_2(tmp6_2.xF6_2[j], tmp6_2A.xF6_2[j], b4_F6.xF6[j]);
    tmp6_2A.xF6_2[j].copy(tmp6_2.xF6_2[j]);
  }


  // final exponentiation
  /* !! perform only (degBF+1)/2 squarings !! */

  for (int i = 0; i < (degBF + 1) / 2; ++i) {
    //ExtField6_2<Basefield>::square6_2(tmp6_2, pairingValue);
    square6_2_multipleP<number>(tmp6_2, tmp6_2A);
    //pairingValue.copy(tmp6_2);
    for (unsigned int j = 0; j < number; ++j) tmp6_2A.xF6_2[j] = tmp6_2.xF6_2[j];
  }

  return tmp6_2;
}

template<class Basefield, int wSize, int number>
__host__ __device__
ExtField3_2_2_Multiple<Basefield, number> dummy_pairing3_2_2_multipleP (const BaseField_Multiple<Basefield ,number>& xp0, const BaseField_Multiple<Basefield ,number>& xq0, const BaseField_Multiple<Basefield ,number>& yp0, const BaseField_Multiple<Basefield ,number>& yq0) {

  static const int degBF = Basefield::ext_degree;

  ExtField3_2_2_Multiple<Basefield, number> tmp3_2_2, tmp3_2_2A;
  tmp3_2_2.clear();
  tmp3_2_2.plusOne();

  ExtField3_2_Multiple<Basefield, number> b4_F6;
  b4_F6.clear();
  b4_F6.clear();
  b4_F6.clear();

  BaseField_Multiple<Basefield, number> tmpBF_1;
  BaseField_Multiple<Basefield, number> tmpCopy;

  int k1, k2, k3, k4, k5, k6;

  // precomputation
  tmpBF_1 = xp0;
  tmpBF_1 = xq0;
  tmpBF_1 = yp0;
  tmpBF_1 = yq0;

  for (int i = 1; i < degBF; ++i) {
    //Basefield::square(xP[i], xP[i - 1]);
    //Basefield::square(xQ[i], xQ[i - 1]);
    //Basefield::square(yP[i], yP[i - 1]);
    //Basefield::square(yQ[i], yQ[i - 1]);

    square_multipleP<Basefield, number>(tmpBF_1, xp0);
    square_multipleP<Basefield, number>(tmpBF_1, xp0);
    square_multipleP<Basefield, number>(tmpBF_1, xp0);
    square_multipleP<Basefield, number>(tmpBF_1, xp0);
  }
  // Miller loop
  for (int i = 0; i < (degBF - 3) / 2; ++i) {
    k1 = ((3 * degBF - 9 - 6 * i) / 2) % degBF;
    k2 = (k1 + 1) % degBF;
    k3 = (k2 + 1) % degBF;
    k4 = ((3 * degBF - 3 - 6 * i) / 2) % degBF;
    k5 = (k4 + 1) % degBF;
    k6 = (k5 + 1) % degBF;

    for (unsigned int j = 0; j < number; ++j) {
      // compute \alpha = (alpha,1)
      // alpha = a0 + a1w + a2w^2 + a3s (a3=c)

      // a2 = xP[k4] + xP[k5]
      b4_F6.xF3_2[j].xF3[0].xBF[2] = xp0.xBF[j] + xq0.xBF[j];

      // a3 = xQ[k2] + xP[k5] + 1
      b4_F6.xF3_2[j].xF3[0].xBF[1] = xq0.xBF[j] + xp0.xBF[j];
    }

    // !!! use a3+1 for a0 !!!
    //Basefield::mulCombR2L_uintSize(tmpBF_1, alpha.xF3[0].xBF[1], xQ[k3]); //tmpBF_1 = (a3 + 1)xQ[k3]
    mulCombL2RwithWindow_multipleP<Basefield, wSize, number>(tmpBF_1, xp0, yp0);

    //alpha.xF3[0].plusOne();

    for (unsigned int j = 0; j < number; ++j) {
      // a1 = xQ[k3] + xP[k5] + 1 + \gamma_1
      b4_F6.xF3_2[j].xF3[0].xBF[1] = xp0.xBF[j] + xq0.xBF[j];
      b4_F6.xF3_2[j].xF3[0].xBF[1].plusOne();

      // a0 = yQ[k2] + (a3 + 1)xQ[k3] + (xP[k4] + 1 + xQ[k3])xQ[k2] + yP[k4] + xP[k4] + \gamma_1
      b4_F6.xF3_2[j].xF3[0].xBF[0] = yp0.xBF[j] + tmpBF_1.xBF[j];
      b4_F6.xF3_2[j].xF3[0].xBF[0] += xp0.xBF[j];
      b4_F6.xF3_2[j].xF3[0].xBF[0] += yp0.xBF[j];
      tmpBF_1.xBF[j] = xq0.xBF[j] + xp0.xBF[j];
      tmpBF_1.xBF[j].plusOne();
    }

    //Basefield::mulCombR2L_uintSize(tmpBF_2, tmpBF_1, xQ[k2]);
    mulCombL2RwithWindow_multipleP<Basefield, wSize, number>(tmpBF_1, xp0, xq0);

    for (unsigned int j = 0; j < number; ++j) {
      b4_F6.xF3_2[j].xF3[0].xBF[0] += tmpBF_1.xBF[j];


      // compute \beta = (beta,1)
      // beta = b0 + b1w + b2w^2 + b3s (a3=b3=c)

      // b1 = xQ[k2] + xP[k6] + 1 + \gamma_1
      b4_F6.xF3_2[j].xF3[0].xBF[1] = xq0.xBF[j] + xp0.xBF[j];
      b4_F6.xF3_2[j].xF3[0].xBF[1].plusOne();

      // b2 = xQ[k2] + xQ[k1] + \tgamma_1
      b4_F6.xF3_2[j].xF3[0].xBF[2] = xq0.xBF[j] + xp0.xBF[j];

      // b0 = yQ[k1] + yP[k5] + xQ[k1](xP[k5] + xP[k6] + 1) + a3*xP[k6] + xP[k5]+ \gamma_1
      tmpBF_1.xBF[j] = xq0.xBF[j] + xp0.xBF[j];
      tmpBF_1.xBF[j].plusOne();
    }

    //Basefield::mulCombR2L_uintSize(beta.xF3[0].xBF[0], tmpBF_1, xQ[k1]);
    mulCombL2RwithWindow_multipleP<Basefield, wSize, number>(tmpCopy, tmpBF_1, xq0);
    for (unsigned int j = 0; j < number; ++j) b4_F6.xF3_2[j].xF3[0].xBF[0] = tmpCopy.xBF[j];

    for (unsigned int j = 0; j < number; ++j) {
      b4_F6.xF3_2[j].xF3[0].xBF[0] += yp0.xBF[j];
      b4_F6.xF3_2[j].xF3[0].xBF[0] += yp0.xBF[j];
      b4_F6.xF3_2[j].xF3[0].xBF[0] += xp0.xBF[j];
    }

    //Basefield::mulCombR2L_uintSize(tmpBF_1, xP[k6], alpha.xF3[1].xBF[0]);
    for (unsigned int j = 0; j < number; ++j) tmpCopy.xBF[j] = b4_F6.xF3_2[j].xF3[1].xBF[0];
    mulCombL2RwithWindow_multipleP<Basefield, wSize, number>(tmpBF_1, xp0, tmpCopy);

    for (unsigned int j = 0; j < number; ++j) b4_F6.xF3_2[j].xF3[0].xBF[0] += tmpBF_1.xBF[j];



    // \alpha\beta
    //ExtField3_2_2<Basefield>::alpha_beta3_2_2(alphaBeta, alpha, beta, d);
    alpha_beta3_2_2_withWindow_multipleP<wSize, number>(tmp3_2_2, b4_F6, b4_F6, xp0);

    //ExtField3_2_2<Basefield>::mulCombR2L_uintSize3_2_2(tmp3_2_2, pairingValue, alphaBeta);
    mul_sp_CombL2RwithWindow3_2_2_multipleP<wSize, number>(tmp3_2_2, tmp3_2_2A, tmp3_2_2A);

    //pairingValue.copy(tmp3_2_2);
    for (unsigned int j = 0; j < number; ++j) tmp3_2_2A.xF3_2_2[j] = tmp3_2_2.xF3_2_2[j];
  }

  for (unsigned int j = 0; j < number; ++j) {
    // extract current point (xP,yP)
    tmpBF_1.xBF[j] = xp0.xBF[j];
    tmpBF_1.xBF[j].plusOne();
    tmpBF_1.xBF[j] = yq0.xBF[j] + xp0.xBF[j];

    // final doublings / addition
    /*
    b4_F6 = (yQ + xQ[1](1 + xQ + xpBF^8 + xpBF^4) + xpBF^4xQ + ypBF^4) +
            (xQ[1] + xpBF^4)w + (xpBF^8 + xpBF^4)w^2 + w^3 + (xQ[1] + xQ)w^4
    */

    // b4_3
    b4_F6.xF3_2[j].xF3[0].xBF[2].plusOne();

    // b4_4
    b4_F6.xF3_2[j].xF3[0].xBF[1] = xp0.xBF[j] + xq0.xBF[j];
  }

  // xpBF := xpBF^4
  //Basefield::square(tmpBF_1, xpBF);
  square_multipleP<Basefield, number>(tmpBF_1, xp0);

  //Basefield::square(xpBF, tmpBF_1);
  square_multipleP<Basefield, number>(tmpBF_1, xp0);

  // b4_1
  for (unsigned int j = 0; j < number; ++j) b4_F6.xF3_2[j].xF3[1].xBF[1] = xp0.xBF[j] + tmpBF_1.xBF[j];

  // tmpBF_1 := xpBF^8
  //Basefield::square(tmpBF_1, xpBF);
  square_multipleP<Basefield, number>(tmpBF_1, xp0);

  for (unsigned int j = 0; j < number; ++j) {
    // b4_2;
    b4_F6.xF3_2[j].xF3[0].xBF[2] = tmpBF_1.xBF[j] + xp0.xBF[j];

    // b4_0
    // tmpBF_1 := b4_2 + xq0
    tmpBF_1.xBF[j] = b4_F6.xF3_2[j].xF3[1].xBF[2] + xq0.xBF[j];
    tmpBF_1.xBF[j].plusOne();
  }

  //Basefield::mulCombR2L_uintSize(b4_F6.xF3[0].xBF[0], xQ[1], tmpBF_1);
  mulCombL2RwithWindow_multipleP<Basefield, wSize, number>(tmpCopy, xp0, tmpBF_1);
  for (unsigned int j = 0; j < number; ++j) b4_F6.xF3_2[j].xF3[0].xBF[0] = tmpCopy.xBF[j];

  //tmpBF_1 = xpBF^4xq0
  //Basefield::mulCombR2L_uintSize(tmpBF_1, xpBF, xq0);
  mulCombL2RwithWindow_multipleP<Basefield, wSize, number>(tmpBF_1, xp0, xq0);

  for (unsigned int j = 0; j < number; ++j) {
    b4_F6.xF3_2[j].xF3[1].xBF[2] += tmpBF_1.xBF[j];
    b4_F6.xF3_2[j].xF3[1].xBF[0] += yq0.xBF[j];
  }

  // ypBF := ypBF^4
  //Basefield::square(tmpBF_1, ypBF);
  square_multipleP<Basefield, number>(tmpBF_1, yp0);

  //Basefield::square(ypBF, tmpBF_1);
  square_multipleP<Basefield, number>(tmpBF_1, yp0);

  for (unsigned int j = 0; j < number; ++j) b4_F6.xF3_2[j].xF3[0].xBF[0] += yp0.xBF[j];

  // f^4
  //ExtField3_2_2<Basefield>::square3_2_2(tmp3_2_2, pairingValue);
  square3_2_2_multipleP<number>(tmp3_2_2, tmp3_2_2A);

  //ExtField3_2_2<Basefield>::square3_2_2(pairingValue, tmp3_2_2);
  square3_2_2_multipleP<number>(tmp3_2_2A, tmp3_2_2);

  // f^4(y+b4(x))
  /* 未実装 host函数で行う */
  for (unsigned int j = 0; j < number; ++j) {
    ExtField3_2_2<Basefield>::sparseMul3_2_2(tmp3_2_2.xF3_2_2[j], tmp3_2_2A.xF3_2_2[j], b4_F6.xF3_2[j]);
    tmp3_2_2A.xF3_2_2[j].copy(tmp3_2_2.xF3_2_2[j]);
  }

  // final exponentiation
  /* !! perform only (degBF+1)/2 squarings !! */

  for (int i = 0; i < (degBF + 1) / 2; ++i) {
      //ExtField3_2_2<Basefield>::square3_2_2(tmp3_2_2, pairingValue);
      square3_2_2_multipleP<number>(tmp3_2_2, tmp3_2_2A);
      //pairingValue.copy(tmp3_2_2);
      for (unsigned int j = 0; j < number; ++j) tmp3_2_2A.xF3_2_2[j] = tmp3_2_2.xF3_2_2[j];
  }

  return tmp3_2_2;
}
