#pragma once

#include "kernel.h"
#include "F2_487.h"
#include "ext_field6_2.h"
#include "ext_field3_2_2.h"
#include "Declaration.h"

template<class Basefield, int number>
void add_multipleP(BaseField_Multiple<Basefield, number>& c, BaseField_Multiple<Basefield, number>& a, BaseField_Multiple<Basefield, number>& b) {

  add_multipleP(c.xBF, a.xBF, b.xBF, number);
}

template<class Basefield, int number>
void add6_multipleP(ExtField6_Multiple<Basefield, number>& c, const ExtField6_Multiple<Basefield, number>& a, const ExtField6_Multiple<Basefield, number>& b) {

  add6_multipleP(c.xF6, a.xF6, b.xF6, number);
}

template<class Basefield, int number>
void add6_2_multipleP(ExtField6_2_Multiple<Basefield, number>& c, const ExtField6_2_Multiple<Basefield, number>& a, const ExtField6_2_Multiple<Basefield, number>& b) {

  add6_2_multipleP(c.xF6_2, a.xF6_2, b.xF6_2, number);
}

template<class Basefield, int number>
void add3_multipleP(ExtField3_Multiple<Basefield, number>& c, const ExtField3_Multiple<Basefield, number>& a, const ExtField3_Multiple<Basefield, number>& b) {

  add3_multipleP(c.xF3, a.xF3, b.xF3, number);
}

template<class Basefield, int number>
void add3_2_multipleP(ExtField3_2_Multiple<Basefield, number>& c, const ExtField3_2_Multiple<Basefield, number>& a, const ExtField3_2_Multiple<Basefield, number>& b) {

  add3_2_multipleP(c.xF3_2, a.xF3_2, b.xF3_2, number);
}

template<class Basefield, int number>
void add3_2_2_multipleP(ExtField3_2_2_Multiple<Basefield, number>& c, const ExtField3_2_2_Multiple<Basefield, number>& a, const ExtField3_2_2_Multiple<Basefield, number>& b) {

  add3_2_2_multipleP(c.xF3_2_2, a.xF3_2_2, b.xF3_2_2, number);
}



template<class Basefield, int number>
void mulCombR2L_uintSize_multipleP(BaseField_Multiple<Basefield, number>& c, BaseField_Multiple<Basefield, number>& a, BaseField_Multiple<Basefield, number>& b) {

  mulCombR2L_uintSize_multipleP(c.xBF, a.xBF, b.xBF, number);
}

template<class Basefield, int wSize, int number>
void mulCombL2RwithWindow_multipleP(BaseField_Multiple<Basefield, number>& c, const BaseField_Multiple<Basefield, number>& a, const BaseField_Multiple<Basefield, number>& b) {

  mulCombL2RwithWindow_multipleP<Basefield, wSize>(c.xBF, a.xBF, b.xBF, number);
}

template<class Basefield, int number>
void square_multipleP(BaseField_Multiple<Basefield, number>& c, const BaseField_Multiple<Basefield, number>& a) {

  square_multipleP(c.xBF, a.xBF, number);
}



template<int wSize, int number>
void mulCombL2RwithWindow6_multipleP(ExtField6_Multiple<F2_487_u64, number>& c, const ExtField6_Multiple<F2_487_u64, number>& a, const ExtField6_Multiple<F2_487_u64, number>& b) {

  mulCombL2RwithWindow6_multipleP<wSize>(c.xF6, a.xF6, b.xF6, number);
}

template<int number>
void square6_multipleP(ExtField6_Multiple<F2_487_u64, number>& c, const ExtField6_Multiple<F2_487_u64, number>& a) {

  square6_multipleP(c.xF6, a.xF6, number);
}

template<int wSize, int number>
void mulCombL2RwithWindow6_2_multipleP(ExtField6_2_Multiple<F2_487_u64, number>& c, const ExtField6_2_Multiple<F2_487_u64, number>& a, const ExtField6_2_Multiple<F2_487_u64, number>& b) {

  mulCombL2RwithWindow6_2_multipleP<wSize>(c.xF6_2, a.xF6_2, b.xF6_2, number);
}

template<int number>
void square6_2_multipleP(ExtField6_2_Multiple<F2_487_u64, number>& c, const ExtField6_2_Multiple<F2_487_u64, number>& a) {

  square6_2_multipleP(c.xF6_2, a.xF6_2, number);
}

template<int wSize, int number>
void alpha_beta6_2_withWindow_multipleP(ExtField6_2_Multiple<F2_487_u64, number>& c, const ExtField6_Multiple<F2_487_u64, number>& a, const ExtField6_Multiple<F2_487_u64, number>& b) {

  alpha_beta6_2_withWindow_multipleP<wSize>(c.xF6_2, a.xF6, b.xF6, number);
}



template<int wSize, int number>
void mulCombL2RwithWindow3_multipleP(ExtField3_Multiple<F2_487_u64, number>& c, const ExtField3_Multiple<F2_487_u64, number>& a, const ExtField3_Multiple<F2_487_u64, number>& b) {

  mulCombL2RwithWindow3_multipleP<wSize>(c.xF3, a.xF3, b.xF3, number);
}

template<int number>
void square3_multipleP(ExtField3_Multiple<F2_487_u64, number>& c, const ExtField3_Multiple<F2_487_u64, number>& a) {

  square3_multipleP(c.xF3, a.xF3, number);
}

template<int wSize, int number>
void mulCombL2RwithWindow3_2_multipleP(ExtField3_2_Multiple<F2_487_u64, number>& c, const ExtField3_2_Multiple<F2_487_u64, number>& a, const ExtField3_2_Multiple<F2_487_u64, number>& b) {

  mulCombL2RwithWindow3_2_multipleP<wSize>(c.xF3_2, a.xF3_2, b.xF3_2, number);
}

template<int wSize, int number>
void mul_sp_CombL2RwithWindow3_2_multipleP(ExtField3_2_Multiple<F2_487_u64, number>& c, const ExtField3_2_Multiple<F2_487_u64, number>& a, const ExtField3_2_Multiple<F2_487_u64, number>& b) {

  mul_sp_CombL2RwithWindow3_2_multipleP<wSize>(c.xF3_2, a.xF3_2, b.xF3_2, number);
}

template<int number>
void square3_2_multipleP(ExtField3_2_Multiple<F2_487_u64, number>& c, const ExtField3_2_Multiple<F2_487_u64, number>& a) {

  square3_2_multipleP(c.xF3_2, a.xF3_2, number);
}

template<int wSize, int number>
void mulCombL2RwithWindow3_2_2_multipleP(ExtField3_2_2_Multiple<F2_487_u64, number>& c, const ExtField3_2_2_Multiple<F2_487_u64, number>& a, const ExtField3_2_2_Multiple<F2_487_u64, number>& b) {

  mulCombL2RwithWindow3_2_2_multipleP<wSize>(c.xF3_2_2, a.xF3_2_2, b.xF3_2_2, number);
}

template<int wSize, int number>
void mul_sp_CombL2RwithWindow3_2_2_multipleP(ExtField3_2_2_Multiple<F2_487_u64, number>& c, const ExtField3_2_2_Multiple<F2_487_u64, number>& a, const ExtField3_2_2_Multiple<F2_487_u64, number>& b) {

  mul_sp_CombL2RwithWindow3_2_2_multipleP<wSize>(c.xF3_2_2, a.xF3_2_2, b.xF3_2_2, number);
}

template<int number>
void square3_2_2_multipleP(ExtField3_2_2_Multiple<F2_487_u64, number>& c, const ExtField3_2_2_Multiple<F2_487_u64, number>& a) {

  square3_2_2_multipleP(c.xF3_2_2, a.xF3_2_2, number);
}

template<int wSize, int number>
void alpha_beta3_2_2_withWindow_multipleP(ExtField3_2_2_Multiple<F2_487_u64, number>& c, const ExtField3_2_Multiple<F2_487_u64, number>& a, const ExtField3_2_Multiple<F2_487_u64, number>& b, const BaseField_Multiple<F2_487_u64, number>& d) {

  alpha_beta3_2_2_withWindow_multipleP<wSize>(c.xF3_2_2, a.xF3_2, b.xF3_2, d.xBF, number);
}



// F2_967_u64
template<int wSize, int number>
void mulCombL2RwithWindow6_multipleP(ExtField6_Multiple<F2_967_u64, number>& c, const ExtField6_Multiple<F2_967_u64, number>& a, const ExtField6_Multiple<F2_967_u64, number>& b) {

  mulCombL2RwithWindow6_multipleP<wSize>(c.xF6, a.xF6, b.xF6, number);
}

template<int number>
void square6_multipleP(ExtField6_Multiple<F2_967_u64, number>& c, const ExtField6_Multiple<F2_967_u64, number>& a) {

  square6_multipleP(c.xF6, a.xF6, number);
}

template<int wSize, int number>
void mulCombL2RwithWindow6_2_multipleP(ExtField6_2_Multiple<F2_967_u64, number>& c, const ExtField6_2_Multiple<F2_967_u64, number>& a, const ExtField6_2_Multiple<F2_967_u64, number>& b) {

  mulCombL2RwithWindow6_2_multipleP<wSize>(c.xF6_2, a.xF6_2, b.xF6_2, number);
}

template<int number>
void square6_2_multipleP(ExtField6_2_Multiple<F2_967_u64, number>& c, const ExtField6_2_Multiple<F2_967_u64, number>& a) {

  square6_2_multipleP(c.xF6_2, a.xF6_2, number);
}

template<int wSize, int number>
void alpha_beta6_2_withWindow_multipleP(ExtField6_2_Multiple<F2_967_u64, number>& c, const ExtField6_Multiple<F2_967_u64, number>& a, const ExtField6_Multiple<F2_967_u64, number>& b) {

  alpha_beta6_2_withWindow_multipleP<wSize>(c.xF6_2, a.xF6, b.xF6, number);
}



template<int wSize, int number>
void mulCombL2RwithWindow3_multipleP(ExtField3_Multiple<F2_967_u64, number>& c, const ExtField3_Multiple<F2_967_u64, number>& a, const ExtField3_Multiple<F2_967_u64, number>& b) {

  mulCombL2RwithWindow3_multipleP<wSize>(c.xF3, a.xF3, b.xF3, number);
}

template<int number>
void square3_multipleP(ExtField3_Multiple<F2_967_u64, number>& c, const ExtField3_Multiple<F2_967_u64, number>& a) {

  square3_multipleP(c.xF3, a.xF3, number);
}

template<int wSize, int number>
void mulCombL2RwithWindow3_2_multipleP(ExtField3_2_Multiple<F2_967_u64, number>& c, const ExtField3_2_Multiple<F2_967_u64, number>& a, const ExtField3_2_Multiple<F2_967_u64, number>& b) {

  mulCombL2RwithWindow3_2_multipleP<wSize>(c.xF3_2, a.xF3_2, b.xF3_2, number);
}

template<int wSize, int number>
void mul_sp_CombL2RwithWindow3_2_multipleP(ExtField3_2_Multiple<F2_967_u64, number>& c, const ExtField3_2_Multiple<F2_967_u64, number>& a, const ExtField3_2_Multiple<F2_967_u64, number>& b) {

  mul_sp_CombL2RwithWindow3_2_multipleP<wSize>(c.xF3_2, a.xF3_2, b.xF3_2, number);
}

template<int number>
void square3_2_multipleP(ExtField3_2_Multiple<F2_967_u64, number>& c, const ExtField3_2_Multiple<F2_967_u64, number>& a) {

  square3_2_multipleP(c.xF3_2, a.xF3_2, number);
}

template<int wSize, int number>
void mulCombL2RwithWindow3_2_2_multipleP(ExtField3_2_2_Multiple<F2_967_u64, number>& c, const ExtField3_2_2_Multiple<F2_967_u64, number>& a, const ExtField3_2_2_Multiple<F2_967_u64, number>& b) {

  mulCombL2RwithWindow3_2_2_multipleP<wSize>(c.xF3_2_2, a.xF3_2_2, b.xF3_2_2, number);
}

template<int wSize, int number>
void mul_sp_CombL2RwithWindow3_2_2_multipleP(ExtField3_2_2_Multiple<F2_967_u64, number>& c, const ExtField3_2_2_Multiple<F2_967_u64, number>& a, const ExtField3_2_2_Multiple<F2_967_u64, number>& b) {

  mul_sp_CombL2RwithWindow3_2_2_multipleP<wSize>(c.xF3_2_2, a.xF3_2_2, b.xF3_2_2, number);
}

template<int number>
void square3_2_2_multipleP(ExtField3_2_2_Multiple<F2_967_u64, number>& c, const ExtField3_2_2_Multiple<F2_967_u64, number>& a) {

  square3_2_2_multipleP(c.xF3_2_2, a.xF3_2_2, number);
}

template<int wSize, int number>
void alpha_beta3_2_2_withWindow_multipleP(ExtField3_2_2_Multiple<F2_967_u64, number>& c, const ExtField3_2_Multiple<F2_967_u64, number>& a, const ExtField3_2_Multiple<F2_967_u64, number>& b, const BaseField_Multiple<F2_967_u64, number>& d) {

  alpha_beta3_2_2_withWindow_multipleP<wSize>(c.xF3_2_2, a.xF3_2, b.xF3_2, d.xBF, number);
}
