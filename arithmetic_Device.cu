/*
 *データの並列化的に，計算結果を並列に書き込むことを考えるとnUint分割程度になる
 */

/* 12次拡大体の乗算等において、karatsuba6,3を並べるだけでなく、事前計算としての基礎体上の乗算のkernel函数を並べなるべく並列計算を行う */
#define _Align_Kernel 1

#include <vector>

#include "kernel.h"
#include "F2_487.h"
#include "F2_967.h"
#include "ext_field6_2.h"
#include "ext_field3_2_2.h"
#include "callTemplate_Multiple.h"

/****************************************         ****************************************/
/**************************************** Kernel ****************************************/
/****************************************        ****************************************/

__global__ void testKernel() {}

template<class Basefield>
__global__ void addKernel(Basefield *c, Basefield *a, Basefield *b) {

  c->x[threadIdx.x] = a->x[threadIdx.x] ^ b->x[threadIdx.x];
}

/* ほぼ非効率 */
template<class Basefield>
__global__ void mulShAddKernel(BaseField_Double<Basefield> *result, Basefield *a, Basefield *b) {

  if (a->Basefield::getBit(0) == 1) result->low.x[threadIdx.x] ^= b->x[threadIdx.x];
  __shared__ Basefield bL, bH;
  bL = *b;
  bH = bL;
  bL.template shiftLeftBit<1>();

  for (int i = 1; i < 487; ++i) {

    if ( a->Basefield::getBit(i) == 1 ) result->low.x[threadIdx.x] ^= bL.x[threadIdx.x];
    bL.template shiftLeftBit<1>();
  }

  bH.template shiftRightBit<1>();
  for (int i = 486; i >= 1; --i) {

    if ( a->Basefield::getBit(i) == 1 ) result->high.x[threadIdx.x] ^= bH.x[threadIdx.x];
    bH.template shiftRightBit<1>();
  }
}

template<class Basefield>
__global__ void mulCombR2L_uintSizeKernel(BaseField_Double<Basefield> *result, Basefield *a, Basefield *b) {

  __shared__ Basefield bL, bH;
  bL = *b;
  bH = bL;

  for (int i = 0; i < Basefield::wordSize; ++i) {
    for (int j = 0; j < Basefield::nUint; ++j) {

      /* aは定められた次数より大きい項の係数が0であることを仮定する */
      if ( a->getBit(i + (j * Basefield::wordSize)) == 1 ) {
        if (threadIdx.x >= j) result->low.x[threadIdx.x] ^= bL.x[threadIdx.x - j];
      }
    }
    bL.template shiftLeftBit<1>();
  }

  /* 上位1ビットを空けてbの先頭係数を代入しておくと下記の実装でO.K. */
  bH.template shiftLeftBit<24>();
  for (int i = Basefield::wordSize - 1; i >= 0; --i) {
    for (int j = 0; j < Basefield::nUint; ++j) {

      /* aは定められた次数より大きい項の係数が0であることを仮定する */
      if ( a->getBit(i + (j * Basefield::wordSize)) == 1 ) {
        //threadIdx.x + Basefield::nUint - 1 - j < Basefield::nUint
        if (threadIdx.x < 1 + j) result->high.x[threadIdx.x] ^= bH.x[threadIdx.x + Basefield::nUint - 1 - j];
      }
    }
    bH.template shiftRightBit<1>();
  }
}

template<class Basefield, int wSize>
__global__ void mulCombL2RwithWindowKernel(BaseField_Double<Basefield> *result, Basefield *a, Basefield *bTable) {

  const int n_wSize = Basefield::wordSize / wSize;
  const int rem_wSize = Basefield::wordSize - wSize * n_wSize;
  const int nW = Basefield::nUint;

  // remainder
  if (rem_wSize != 0) {
    for (int j = 0; j < nW; ++j) {

      //result.low ^= Basefield::shiftLeftWordSize(bTable[Basefield::index(a, n_wSizewSize + j*Basefield::wordSize, rem_wSize)], j);
      if (threadIdx.x >= j) result->low.x[threadIdx.x] ^= bTable[Basefield::index(*a, n_wSize * (wSize) + j*Basefield::wordSize, rem_wSize)].x[threadIdx.x - j];

      //result.high ^= Basefield::shiftRightWordSize(bTable[Basefield::index(a, n_wSizewSize + j*Basefield::wordSize, rem_wSize)], nW - j);
      if (threadIdx.x < j) result->high.x[threadIdx.x] ^= bTable[Basefield::index(*a, n_wSize * (wSize) + j*Basefield::wordSize, rem_wSize)].x[threadIdx.x + Basefield::nUint - j];

    }

    // synchronize thereads
    __syncthreads();
    result->high.shiftLeftBit(wSize);
    result->high ^= Basefield::shiftRightBit(result->low, 512 - wSize); /*上記理由から487でなく512*/
    result->low.shiftLeftBit(wSize);
  }

  for (int i = n_wSize-1; i >= 0; --i) {
    for (int j = 0; j < nW; ++j) {

      //result.low ^= Basefield::shiftLeftWordSize(bTable[Basefield::index(a, iwSize+j*Basefield::wordSize, wSize)], j);
      if (threadIdx.x >= j) result->low.x[threadIdx.x] ^= bTable[Basefield::index(*a, i * (wSize) + j*Basefield::wordSize, wSize)].x[threadIdx.x - j];

      //result.high ^= Basefield::shiftRightWordSize(bTable[Basefield::index(a, iwSize+j*Basefield::wordSize, wSize)], nW - j);
      if (threadIdx.x < j) result->high.x[threadIdx.x] ^= bTable[Basefield::index(*a, i * (wSize) + j*Basefield::wordSize, wSize)].x[threadIdx.x + Basefield::nUint - j];
    }

    if (i != 0) {

      // synchronize thereads
      __syncthreads();
      result->high.shiftLeftBit(wSize);
      result->high ^= Basefield::shiftRightBit(result->low, 512 - wSize); /*上記理由から487でなく512*/
      result->low.shiftLeftBit(wSize);
    }
  }
}

template<class Basefield>
__global__ void squareKernel(BaseField_Double<Basefield> *result, Basefield *a) {

  for (int i = 0; i < 32; ++i) result->low.x[threadIdx.x] ^= a->getBit(i + threadIdx.x * 32) << (2 * i);
  for (int i = 0; i < 32; ++i) result->high.x[threadIdx.x] ^= a->getBit(i + ((Basefield::ext_degree + 1) / 2) + threadIdx.x * 32) << (2 * i + 1);
  //for (int i = 0; i < 32; ++i) result->high.x[threadIdx.x] ^= a->getBit(i + 244 + threadIdx.x * 32) << (2 * i + 1);
}



/* 以下，複数元の並列計算kernel函数 */
/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/

template<class Basefield>
__global__ void add_multipleKernel(Basefield *c, Basefield *a, Basefield *b) {

  c[blockIdx.x].x[threadIdx.x] = a[blockIdx.x].x[threadIdx.x] ^ b[blockIdx.x].x[threadIdx.x];
}

template<class Basefield>
__global__ void add6_multipleKernel(ExtField6<Basefield> *c, ExtField6<Basefield> *a, ExtField6<Basefield> *b) {

  for (unsigned int i = 0; i < 6; ++i) c[blockIdx.x].xBF[i].x[threadIdx.x] = a[blockIdx.x].xBF[i].x[threadIdx.x] ^ b[blockIdx.x].xBF[i].x[threadIdx.x];
}

template<class Basefield>
__global__ void add6_2_multipleKernel(ExtField6_2<Basefield> *c, ExtField6_2<Basefield> *a, ExtField6_2<Basefield> *b) {

  for (unsigned int i = 0; i < 2; ++i) {
    for (unsigned int j = 0; j < 6; ++j) c[blockIdx.x].xF6[i].xBF[j].x[threadIdx.x] = a[blockIdx.x].xF6[i].xBF[j].x[threadIdx.x] ^ b[blockIdx.x].xF6[i].xBF[j].x[threadIdx.x];
  }
}

template<class Basefield>
__global__ void add3_multipleKernel(ExtField3<Basefield> *c, ExtField3<Basefield> *a, ExtField3<Basefield> *b) {

  for (unsigned int i = 0; i < 3; ++i) c[blockIdx.x].xBF[i].x[threadIdx.x] = a[blockIdx.x].xBF[i].x[threadIdx.x] ^ b[blockIdx.x].xBF[i].x[threadIdx.x];
}

template<class Basefield>
__global__ void add3_2_multipleKernel(ExtField3_2<Basefield> *c, ExtField3_2<Basefield> *a, ExtField3_2<Basefield> *b) {

  for (unsigned int i = 0; i < 2; ++i) {
    for (unsigned int j = 0; j < 3; ++j) c[blockIdx.x].xF3[i].xBF[j].x[threadIdx.x] = a[blockIdx.x].xF3[i].xBF[j].x[threadIdx.x] ^ b[blockIdx.x].xF3[i].xBF[j].x[threadIdx.x];
  }
}

template<class Basefield>
__global__ void add3_2_2_multipleKernel(ExtField3_2_2<Basefield> *c, ExtField3_2_2<Basefield> *a, ExtField3_2_2<Basefield> *b) {

  for (unsigned int i = 0; i < 2; ++i) {
    for (unsigned int j = 0; j < 2; ++j) {
      for (unsigned int k = 0; k < 3; ++k) c[blockIdx.x].xF3_2[i].xF3[j].xBF[k].x[threadIdx.x] = a[blockIdx.x].xF3_2[i].xF3[j].xBF[k].x[threadIdx.x] ^ b[blockIdx.x].xF3_2[i].xF3[j].xBF[k].x[threadIdx.x];
    }
  }
}

template<class Basefield>
__global__ void mulCombR2L_uintSize_multipleKernel(BaseField_Double<Basefield> *result, Basefield *a, Basefield *b) {

  __shared__ Basefield bL, bH;
  bL = b[blockIdx.x];
  bH = bL;

  for (int i = 0; i < Basefield::wordSize; ++i) {
    for (int j = 0; j < Basefield::nUint; ++j) {

      /* aは定められた次数より大きい項の係数が0であることを仮定する */
      if ( a[blockIdx.x].getBit(i + (j * Basefield::wordSize)) == 1 ) {
        if (threadIdx.x >= j) result[blockIdx.x].low.x[threadIdx.x] ^= bL.x[threadIdx.x - j];
      }
    }
    bL.template shiftLeftBit<1>();
  }

  /* 上位1ビットを空けてbの先頭係数を代入しておくと下記の実装でO.K. */
  bH.template shiftLeftBit<24>();
  for (int i = Basefield::wordSize - 1; i >= 0; --i) {
    for (int j = 0; j < Basefield::nUint; ++j) {

      /* aは定められた次数より大きい項の係数が0であることを仮定する */
      if ( a[blockIdx.x].getBit(i + (j * Basefield::wordSize)) == 1 ) {
        //threadIdx.x + Basefield::nUint - 1 - j < Basefield::nUint
        if (threadIdx.x < 1 + j) result[blockIdx.x].high.x[threadIdx.x] ^= bH.x[threadIdx.x + Basefield::nUint - 1 - j];
      }
    }
    bH.template shiftRightBit<1>();
  }
}

/*** mulCombL2RwithWindow_multipleKernel ***/
/*** precompute tableがglobal memoryにあるとさすがに遅いか.. ***/
/* result on global memory, reduction in host code */
#if 0
template<class Basefield>
__global__ void mulCombL2RwithWindow_multipleKernel(Basefield *c, BaseField_Double<Basefield> *result, const Basefield *a, const Basefield *b, Basefield *bTable, const int *wSize) {

  const int n_wSize = Basefield::wordSize / *wSize;
  const int rem_wSize = Basefield::wordSize - *wSize * n_wSize;
  const int nW = Basefield::nUint;

  Basefield::precomputeTable(bTable + blockIdx.x * Basefield::pow2func(*wSize), b[blockIdx.x], *wSize);

  // remainder
  if (rem_wSize != 0) {
    for (int j = 0; j < nW; ++j) {

      //result.low ^= Basefield::shiftLeftWordSize(bTable[Basefield::index(a, n_wSize*wSize + j*Basefield::wordSize, rem_wSize)], j);
      if (threadIdx.x >= j) result[blockIdx.x].low.x[threadIdx.x] ^= (bTable + blockIdx.x * Basefield::pow2func(*wSize))[Basefield::index(a[blockIdx.x], n_wSize * (*wSize) + j*Basefield::wordSize, rem_wSize)].x[threadIdx.x - j];

      //result.high ^= Basefield::shiftRightWordSize(bTable[Basefield::index(a, n_wSize*wSize + j*Basefield::wordSize, rem_wSize)], nW - j);
      if (threadIdx.x < j) result[blockIdx.x].high.x[threadIdx.x] ^= (bTable + blockIdx.x * Basefield::pow2func(*wSize))[Basefield::index(a[blockIdx.x], n_wSize * (*wSize) + j*Basefield::wordSize, rem_wSize)].x[threadIdx.x + Basefield::nUint - j];

    }

    // synchronize thereads
    //__syncthreads();
    result[blockIdx.x].high.shiftLeftBit(*wSize);
    result[blockIdx.x].high ^= Basefield::shiftRightBit(result[blockIdx.x].low, 512 - *wSize); /*上記理由から487でなく512*/
    result[blockIdx.x].low.shiftLeftBit(*wSize);
  }

  for (int i = n_wSize-1; i >= 0; --i) {
    for (int j = 0; j < nW; ++j) {

      //result[blockIdx.x].low ^= Basefield::shiftLeftWordSize(bTable[Basefield::index(a[blockIdx.x], i * (*wSize) + j*Basefield::wordSize, *wSize)], j);
      if (threadIdx.x >= j) result[blockIdx.x].low.x[threadIdx.x] ^= (bTable + blockIdx.x * Basefield::pow2func(*wSize))[Basefield::index(a[blockIdx.x], i * (*wSize) + j*Basefield::wordSize, *wSize)].x[threadIdx.x - j];

      //result[blockIdx.x].high ^= Basefield::shiftRightWordSize(bTable[Basefield::index(a[blockIdx.x], i * (*wSize) + j*Basefield::wordSize, *wSize)], nW - j);
      if (threadIdx.x < j) result[blockIdx.x].high.x[threadIdx.x] ^= (bTable + blockIdx.x * Basefield::pow2func(*wSize))[Basefield::index(a[blockIdx.x], i * (*wSize) + j*Basefield::wordSize, *wSize)].x[threadIdx.x + Basefield::nUint - j];
    }

    if (i != 0) {

      // synchronize thereads
      //__syncthreads();
      result[blockIdx.x].high.shiftLeftBit(*wSize);
      result[blockIdx.x].high ^= Basefield::shiftRightBit(result[blockIdx.x].low, 512 - *wSize); /*上記理由から487でなく512*/
      result[blockIdx.x].low.shiftLeftBit(*wSize);
    }
  }

  result[blockIdx.x].high.template shiftLeftBit<25>();
  result[blockIdx.x].high ^= Basefield::shiftRightBit<487>(result[blockIdx.x].low);
}
/*** 各スレッドでtableの事前計算 ***/
#else
template<class Basefield, int wSize>
__global__ void mulCombL2RwithWindow_multipleKernel(Basefield *c, const Basefield *a, const Basefield *b) {

  const int n_wSize = Basefield::wordSize / wSize;
  const int rem_wSize = Basefield::wordSize - wSize * n_wSize;
  const int nW = Basefield::nUint;

  //Basefield::precomputeTable(bTable + blockIdx.x * power2<wSize>::num, b[blockIdx.x], wSize);
  __shared__ Basefield bTable_shared[power2<wSize>::num];
  Basefield::precomputeTable(bTable_shared, b[blockIdx.x], wSize);

  __shared__ BaseField_Double<Basefield> result;
  //result.clear();
  result.low.x[threadIdx.x] = 0;
  result.high.x[threadIdx.x] = 0;

  // remainder
  if (rem_wSize != 0) {
    for (int j = 0; j < nW; ++j) {

      //result.low ^= Basefield::shiftLeftWordSize(bTable[Basefield::index(a, n_wSizewSize + j*Basefield::wordSize, rem_wSize)], j);
      //if (threadIdx.x >= j) result.low.x[threadIdx.x] ^= (bTable + blockIdx.x * power2<wSize>::num)[Basefield::index(a[blockIdx.x], n_wSize * (wSize) + j*Basefield::wordSize, rem_wSize)].x[threadIdx.x - j];
      if (threadIdx.x >= j) result.low.x[threadIdx.x] ^= bTable_shared[Basefield::index(a[blockIdx.x], n_wSize * (wSize) + j*Basefield::wordSize, rem_wSize)].x[threadIdx.x - j];

      //result.high ^= Basefield::shiftRightWordSize(bTable[Basefield::index(a, n_wSizewSize + j*Basefield::wordSize, rem_wSize)], nW - j);
      //if (threadIdx.x < j) result.high.x[threadIdx.x] ^= (bTable + blockIdx.x * power2<wSize>::num)[Basefield::index(a[blockIdx.x], n_wSize * (wSize) + j*Basefield::wordSize, rem_wSize)].x[threadIdx.x + Basefield::nUint - j];
      if (threadIdx.x < j) result.high.x[threadIdx.x] ^= bTable_shared[Basefield::index(a[blockIdx.x], n_wSize * (wSize) + j*Basefield::wordSize, rem_wSize)].x[threadIdx.x + Basefield::nUint - j];
    }

    // synchronize thereads
    //__syncthreads();
    result.high.shiftLeftBit(wSize);
    result.high ^= Basefield::shiftRightBit(result.low, 512 - wSize); /*上記理由から487でなく512*/
    result.low.shiftLeftBit(wSize);
  }

  for (int i = n_wSize-1; i >= 0; --i) {
    for (int j = 0; j < nW; ++j) {

      //result[blockIdx.x].low ^= Basefield::shiftLeftWordSize(bTable[Basefield::index(a[blockIdx.x], i * (wSize) + j*Basefield::wordSize, wSize)], j);
      //if (threadIdx.x >= j) result.low.x[threadIdx.x] ^= (bTable + blockIdx.x * power2<wSize>::num)[Basefield::index(a[blockIdx.x], i * (wSize) + j*Basefield::wordSize, wSize)].x[threadIdx.x - j];
      if (threadIdx.x >= j) result.low.x[threadIdx.x] ^= bTable_shared[Basefield::index(a[blockIdx.x], i * (wSize) + j*Basefield::wordSize, wSize)].x[threadIdx.x - j];

      //result[blockIdx.x].high ^= Basefield::shiftRightWordSize(bTable[Basefield::index(a[blockIdx.x], i * (wSize) + j*Basefield::wordSize, wSize)], nW - j);
      //if (threadIdx.x < j) result.high.x[threadIdx.x] ^= (bTable + blockIdx.x * power2<wSize>::num)[Basefield::index(a[blockIdx.x], i * (wSize) + j*Basefield::wordSize, wSize)].x[threadIdx.x + Basefield::nUint - j];
      if (threadIdx.x < j) result.high.x[threadIdx.x] ^= bTable_shared[Basefield::index(a[blockIdx.x], i * (wSize) + j*Basefield::wordSize, wSize)].x[threadIdx.x + Basefield::nUint - j];
    }

    if (i != 0) {

      // synchronize thereads
      //__syncthreads();
      result.high.shiftLeftBit(wSize);
      result.high ^= Basefield::shiftRightBit(result.low, 512 - wSize); /*上記理由から487でなく512*/
      result.low.shiftLeftBit(wSize);
    }
  }

  /* !! 要修正 m=967のとき <<25 ではないはず !! */
  result.high.template shiftLeftBit<25>();
  //result.high ^= Basefield::shiftRightBit<Basefield::ext_degree>(result.low);
  result.high.x[threadIdx.x] ^= Basefield::shiftRightBit<Basefield::ext_degree>(result.low).x[threadIdx.x];

  //typename Basefield::uint_type mask = 0x0000007fffffffff;
  //result.low.x[Basefield::nUint - 1] &= mask;
  result.low.clearUpDeg();
  result.reduction(c[blockIdx.x]);
}
#endif

template<class Basefield, int wSize>
__global__ void AddMul_for_Karatsuba_multipleKernel(Basefield *vST, const Basefield *aS, const Basefield *aT, const Basefield *bS, const Basefield *bT) {

  const int n_wSize = Basefield::wordSize / wSize;
  const int rem_wSize = Basefield::wordSize - wSize * n_wSize;
  const int nW = Basefield::nUint;

  __shared__ Basefield aST, bST;
  __shared__ Basefield bTable_shared[power2<wSize>::num];
  __shared__ BaseField_Double<Basefield> result;
  result.clear();

  aST.x[threadIdx.x] = aS[blockIdx.x].x[threadIdx.x] ^ aT[blockIdx.x].x[threadIdx.x];
  bST.x[threadIdx.x] = bS[blockIdx.x].x[threadIdx.x] ^ bT[blockIdx.x].x[threadIdx.x];

  Basefield::precomputeTable(bTable_shared, bST, wSize);

  // remainder
  if (rem_wSize != 0) {
    for (int j = 0; j < nW; ++j) {

      if (threadIdx.x >= j) result.low.x[threadIdx.x] ^= bTable_shared[Basefield::index(aST, n_wSize * (wSize) + j*Basefield::wordSize, rem_wSize)].x[threadIdx.x - j];
      if (threadIdx.x < j) result.high.x[threadIdx.x] ^= bTable_shared[Basefield::index(aST, n_wSize * (wSize) + j*Basefield::wordSize, rem_wSize)].x[threadIdx.x + Basefield::nUint - j];
    }
    // synchronize thereads
    //__syncthreads();
    result.high.shiftLeftBit(wSize);
    result.high ^= Basefield::shiftRightBit(result.low, 512 - wSize); /*上記理由から487でなく512*/
    result.low.shiftLeftBit(wSize);
  }

  for (int i = n_wSize-1; i >= 0; --i) {
    for (int j = 0; j < nW; ++j) {

      if (threadIdx.x >= j) result.low.x[threadIdx.x] ^= bTable_shared[Basefield::index(aST, i * (wSize) + j*Basefield::wordSize, wSize)].x[threadIdx.x - j];
      if (threadIdx.x < j) result.high.x[threadIdx.x] ^= bTable_shared[Basefield::index(aST, i * (wSize) + j*Basefield::wordSize, wSize)].x[threadIdx.x + Basefield::nUint - j];
    }

    if (i != 0) {
      // synchronize thereads
      //__syncthreads();
      result.high.shiftLeftBit(wSize);
      result.high ^= Basefield::shiftRightBit(result.low, 512 - wSize); /*上記理由から487でなく512*/
      result.low.shiftLeftBit(wSize);
    }
  }

  /* m=967のとき25ではない! 要修正! */
  result.high.template shiftLeftBit<25>();
  result.high ^= Basefield::shiftRightBit<Basefield::ext_degree>(result.low);

  //typename Basefield::uint_type mask = 0x0000007fffffffff;
  //result.low.x[Basefield::nUint - 1] &= mask;
  result.low.clearUpDeg();
  result.reduction(vST[blockIdx.x]);
}

template<class Basefield>
__global__ void square_multipleKernel(Basefield *c, const Basefield *a) {

  __shared__ BaseField_Double<Basefield> result;
  result.clear();

  for (int i = 0; i < 32; ++i) result.low.x[threadIdx.x] ^= a[blockIdx.x].getBit(i + threadIdx.x * 32) << (2 * i);
  for (int i = 0; i < 32; ++i) result.high.x[threadIdx.x] ^= a[blockIdx.x].getBit(i + 244 + threadIdx.x * 32) << (2 * i + 1);

  result.low.clearUpDeg();
  result.reduction(c[blockIdx.x]);
}



/****************************************             ****************************************/
/**************************************** Kernel Call ****************************************/
/****************************************             ****************************************/

template<class Basefield>
void addP(Basefield& c, const Basefield& a, const Basefield& b) {

  Basefield *a_dev, *b_dev, *c_dev;

  cudaMalloc((void**)&a_dev, sizeof(Basefield));
  cudaMalloc((void**)&b_dev, sizeof(Basefield));
  cudaMalloc((void**)&c_dev, sizeof(Basefield));

  cudaMemcpy(a_dev, &a, sizeof(Basefield), cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, &b, sizeof(Basefield), cudaMemcpyHostToDevice);

  dim3 blocks(1,1);
  dim3 threads(Basefield::nUint,1);

  addKernel<<< blocks, threads >>>(c_dev,a_dev,b_dev);

  cudaMemcpy(&c, c_dev, sizeof(Basefield), cudaMemcpyDeviceToHost);

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(c_dev);
}
template void addP<F2_487_u64>(F2_487_u64& c, const F2_487_u64& a, const F2_487_u64& b);

template<class Basefield>
void mulShAddP(Basefield& c, const Basefield& a, const Basefield& b) {

#if 0
  if (a.testZero() || b.testZero()) {

    c.clear();
    return;
  }
#endif

  BaseField_Double<Basefield> result;
  result.clear();

  Basefield *a_dev, *b_dev;
  BaseField_Double<Basefield> *result_dev;

  cudaMalloc((void**)&a_dev, sizeof(Basefield));
  cudaMalloc((void**)&b_dev, sizeof(Basefield));
  cudaMalloc((void**)&result_dev, sizeof(BaseField_Double<Basefield>));

  cudaMemcpy(a_dev, &a, sizeof(Basefield), cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, &b, sizeof(Basefield), cudaMemcpyHostToDevice);
  cudaMemcpy(result_dev, &result, sizeof(BaseField_Double<Basefield>), cudaMemcpyHostToDevice);

  dim3 blocks(1,1);
  dim3 threads(Basefield::nUint,1);

  mulShAddKernel<<< blocks, threads >>>(result_dev,a_dev,b_dev);

  cudaMemcpy(&result, result_dev, sizeof(BaseField_Double<Basefield>), cudaMemcpyDeviceToHost);

  //typename Basefield::uint_type mask = 0x0000007fffffffff;
  //result.low.x[Basefield::nUint - 1] &= mask;
  result.low.clearUpDeg();
  result.reduction(c);

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(result_dev);
}
template void mulShAddP<F2_487_u64>(F2_487_u64& c, const F2_487_u64& a, const F2_487_u64& b);
template void mulShAddP<F2_967_u64>(F2_967_u64& c, const F2_967_u64& a, const F2_967_u64& b);

template<class Basefield>
void mulCombR2L_uintSizeP(Basefield& c, const Basefield& a, const Basefield& b) {

#if 0
  if (a.testZero() || b.testZero()) {

    c.clear();
    return;
  }
#endif

  BaseField_Double<Basefield> result;
  result.clear();

  Basefield *a_dev, *b_dev;
  BaseField_Double<Basefield> *result_dev;

  cudaMalloc((void**)&a_dev, sizeof(Basefield));
  cudaMalloc((void**)&b_dev, sizeof(Basefield));
  cudaMalloc((void**)&result_dev, sizeof(BaseField_Double<Basefield>));

  cudaMemcpy(a_dev, &a, sizeof(Basefield), cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, &b, sizeof(Basefield), cudaMemcpyHostToDevice);
  cudaMemcpy(result_dev, &result, sizeof(BaseField_Double<Basefield>), cudaMemcpyHostToDevice);

  dim3 blocks(1,1);
  dim3 threads(Basefield::nUint,1);

  mulCombR2L_uintSizeKernel<<< blocks, threads >>>(result_dev,a_dev,b_dev);

  cudaMemcpy(&result, result_dev, sizeof(BaseField_Double<Basefield>), cudaMemcpyDeviceToHost);

  //typename Basefield::uint_type mask = 0x0000007fffffffff;
  //result.low.x[Basefield::nUint - 1] &= mask;
  result.low.clearUpDeg();
  result.reduction(c);

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(result_dev);
}
template void mulCombR2L_uintSizeP<F2_487_u64>(F2_487_u64& c, const F2_487_u64& a, const F2_487_u64& b);
template void mulCombR2L_uintSizeP<F2_967_u64>(F2_967_u64& c, const F2_967_u64& a, const F2_967_u64& b);

/*
 * template<class Basefield>としない（templateとして定義すると new Basefield[Basefield::pow2func(wSize)] がvariable-length array[-Wvla]警告となる）
 */
template<int wSize>
void mulCombL2RwithWindowP(F2_487_u64& c, const F2_487_u64& a, const F2_487_u64& b) {

#if 0
  if (a.testZero() || b.testZero()) {

    c.clear();
    return;
  }
#endif

  BaseField_Double<F2_487_u64> result;
  result.clear();

  F2_487_u64 *bTable = new F2_487_u64[F2_487_u64::pow2func(wSize)];
  F2_487_u64::precomputeTable(bTable, b, wSize);

  F2_487_u64 *a_dev, *bTable_dev;
  BaseField_Double<F2_487_u64> *result_dev;
  //int *ws_dev;

  cudaMalloc((void**)&a_dev, sizeof(F2_487_u64));
  cudaMalloc((void**)&bTable_dev, sizeof(F2_487_u64)*F2_487_u64::pow2func(wSize));
  cudaMalloc((void**)&result_dev, sizeof(BaseField_Double<F2_487_u64>));
  //cudaMalloc((void**)&ws_dev, sizeof(int));

  cudaMemcpy(a_dev, &a, sizeof(F2_487_u64), cudaMemcpyHostToDevice);
  cudaMemcpy(bTable_dev, bTable, sizeof(F2_487_u64)*F2_487_u64::pow2func(wSize), cudaMemcpyHostToDevice);
  cudaMemcpy(result_dev, &result, sizeof(BaseField_Double<F2_487_u64>), cudaMemcpyHostToDevice);
  //cudaMemcpy(ws_dev, &wSize, sizeof(int), cudaMemcpyHostToDevice);

  dim3 blocks(1,1);
  dim3 threads(F2_487_u64::nUint,1);

  mulCombL2RwithWindowKernel<F2_487_u64, wSize><<< blocks, threads >>>(result_dev, a_dev, bTable_dev);

  cudaMemcpy(&result, result_dev, sizeof(BaseField_Double<F2_487_u64>), cudaMemcpyDeviceToHost);

  /*
  F2_F2_487_u64_Double<F2_487_u64>としての表現に合わせて整形
  */
  result.high.shiftLeftBit<25>();
  result.high ^= F2_487_u64::shiftRightBit<487>(result.low);

  //typename F2_487_u64::uint_type mask = 0x0000007fffffffff;
  //result.low.x[F2_487_u64::nUint - 1] &= mask;
  result.low.clearUpDeg();
  result.reduction(c);

  //delete[] bTable;
  cudaFree(a_dev);
  cudaFree(result_dev);
  //cudaFree(ws_dev);
}
template void mulCombL2RwithWindowP<3>(F2_487_u64& c, const F2_487_u64& a, const F2_487_u64& b);
template void mulCombL2RwithWindowP<4>(F2_487_u64& c, const F2_487_u64& a, const F2_487_u64& b);
template void mulCombL2RwithWindowP<5>(F2_487_u64& c, const F2_487_u64& a, const F2_487_u64& b);
template void mulCombL2RwithWindowP<6>(F2_487_u64& c, const F2_487_u64& a, const F2_487_u64& b);

template<class Basefield>
void squareP(Basefield& c, const Basefield& a) {

#if 0
  if (a.testZero() || b.testZero()) {

    c.clear();
    return;
  }
#endif

  BaseField_Double<Basefield> result;
  result.clear();

  Basefield *a_dev;
  BaseField_Double<Basefield> *result_dev;

  cudaMalloc((void**)&a_dev, sizeof(Basefield));
  cudaMalloc((void**)&result_dev, sizeof(BaseField_Double<Basefield>));

  cudaMemcpy(a_dev, &a, sizeof(Basefield), cudaMemcpyHostToDevice);
  cudaMemcpy(result_dev, &result, sizeof(BaseField_Double<Basefield>), cudaMemcpyHostToDevice);

  dim3 blocks(1,1);
  dim3 threads(Basefield::nUint,1);

  squareKernel<<< blocks, threads >>>(result_dev,a_dev);

  cudaMemcpy(&result, result_dev, sizeof(BaseField_Double<Basefield>), cudaMemcpyDeviceToHost);

  //typename Basefield::uint_type mask = 0x0000007fffffffff;
  //result.low.x[Basefield::nUint - 1] &= mask;
  result.low.clearUpDeg();
  result.reduction(c);

  cudaFree(a_dev);
  cudaFree(result_dev);
}
template void squareP<F2_487_u64>(F2_487_u64& c, const F2_487_u64& a);
template void squareP<F2_967_u64>(F2_967_u64& c, const F2_967_u64& a);



/****************************************************************************************************/
/****************************************************************************************************/
/****************************************************************************************************/
/* 以下，複数元の並列演算 */
template<class Basefield>
void add_multipleP(Basefield *c, const Basefield *a, const Basefield *b, const int number) {

  Basefield *a_dev, *b_dev, *c_dev;

  cudaMalloc((void**)&a_dev, sizeof(Basefield)*number);
  cudaMalloc((void**)&b_dev, sizeof(Basefield)*number);
  cudaMalloc((void**)&c_dev, sizeof(Basefield)*number);

  cudaMemcpy(a_dev, a, sizeof(Basefield)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, b, sizeof(Basefield)*number, cudaMemcpyHostToDevice);

  dim3 blocks(number,1);
  dim3 threads(Basefield::nUint,1);

  add_multipleKernel<<< blocks, threads >>>(c_dev,a_dev,b_dev);

  cudaMemcpy(c, c_dev, sizeof(Basefield)*number, cudaMemcpyDeviceToHost);

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(c_dev);
}
template void add_multipleP<F2_487_u64>(F2_487_u64 *c, const F2_487_u64 *a, const F2_487_u64 *b, const int number);
template void add_multipleP<F2_967_u64>(F2_967_u64 *c, const F2_967_u64 *a, const F2_967_u64 *b, const int number);

/* 二重並列化しない */
template<class Basefield>
void add6_multipleP(ExtField6<Basefield> *c, const ExtField6<Basefield> *a, const ExtField6<Basefield> *b, const int number) {

  ExtField6<Basefield> *a_dev, *b_dev, *c_dev;

  cudaMalloc((void**)&a_dev, sizeof(ExtField6<Basefield>)*number);
  cudaMalloc((void**)&b_dev, sizeof(ExtField6<Basefield>)*number);
  cudaMalloc((void**)&c_dev, sizeof(ExtField6<Basefield>)*number);

  cudaMemcpy(a_dev, a, sizeof(ExtField6<Basefield>)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, b, sizeof(ExtField6<Basefield>)*number, cudaMemcpyHostToDevice);

  dim3 blocks(number,1);
  dim3 threads(Basefield::nUint,1);

  add6_multipleKernel<<< blocks, threads >>>(c_dev,a_dev,b_dev);

  cudaMemcpy(c, c_dev, sizeof(ExtField6<Basefield>)*number, cudaMemcpyDeviceToHost);

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(c_dev);
}
template void add6_multipleP<F2_487_u64>(ExtField6<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b, const int number);
template void add6_multipleP<F2_967_u64>(ExtField6<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const ExtField6<F2_967_u64> *b, const int number);

/* 二重並列化しない */
template<class Basefield>
void add6_2_multipleP(ExtField6_2<Basefield> *c, const ExtField6_2<Basefield> *a, const ExtField6_2<Basefield> *b, const int number) {

  ExtField6_2<Basefield> *a_dev, *b_dev, *c_dev;

  cudaMalloc((void**)&a_dev, sizeof(ExtField6_2<Basefield>)*number);
  cudaMalloc((void**)&b_dev, sizeof(ExtField6_2<Basefield>)*number);
  cudaMalloc((void**)&c_dev, sizeof(ExtField6_2<Basefield>)*number);

  cudaMemcpy(a_dev, a, sizeof(ExtField6_2<Basefield>)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, b, sizeof(ExtField6_2<Basefield>)*number, cudaMemcpyHostToDevice);

  dim3 blocks(number,1);
  dim3 threads(Basefield::nUint,1);

  add6_2_multipleKernel<<< blocks, threads >>>(c_dev,a_dev,b_dev);

  cudaMemcpy(c, c_dev, sizeof(ExtField6_2<Basefield>)*number, cudaMemcpyDeviceToHost);

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(c_dev);
}
template void add6_2_multipleP<F2_487_u64>(ExtField6_2<F2_487_u64> *c, const ExtField6_2<F2_487_u64> *a, const ExtField6_2<F2_487_u64> *b, const int number);
template void add6_2_multipleP<F2_967_u64>(ExtField6_2<F2_967_u64> *c, const ExtField6_2<F2_967_u64> *a, const ExtField6_2<F2_967_u64> *b, const int number);

/* 二重並列化しない */
template<class Basefield>
void add3_multipleP(ExtField3<Basefield> *c, const ExtField3<Basefield> *a, const ExtField3<Basefield> *b, const int number) {

  ExtField3<Basefield> *a_dev, *b_dev, *c_dev;

  cudaMalloc((void**)&a_dev, sizeof(ExtField3<Basefield>)*number);
  cudaMalloc((void**)&b_dev, sizeof(ExtField3<Basefield>)*number);
  cudaMalloc((void**)&c_dev, sizeof(ExtField3<Basefield>)*number);

  cudaMemcpy(a_dev, a, sizeof(ExtField3<Basefield>)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, b, sizeof(ExtField3<Basefield>)*number, cudaMemcpyHostToDevice);

  dim3 blocks(number,1);
  dim3 threads(Basefield::nUint,1);

  add3_multipleKernel<<< blocks, threads >>>(c_dev,a_dev,b_dev);

  cudaMemcpy(c, c_dev, sizeof(ExtField3<Basefield>)*number, cudaMemcpyDeviceToHost);

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(c_dev);
}
template void add3_multipleP<F2_487_u64>(ExtField3<F2_487_u64> *c, const ExtField3<F2_487_u64> *a, const ExtField3<F2_487_u64> *b, const int number);
template void add3_multipleP<F2_967_u64>(ExtField3<F2_967_u64> *c, const ExtField3<F2_967_u64> *a, const ExtField3<F2_967_u64> *b, const int number);

/* 二重並列化しない */
template<class Basefield>
void add3_2_multipleP(ExtField3_2<Basefield> *c, const ExtField3_2<Basefield> *a, const ExtField3_2<Basefield> *b, const int number) {

  ExtField3_2<Basefield> *a_dev, *b_dev, *c_dev;

  cudaMalloc((void**)&a_dev, sizeof(ExtField3_2<Basefield>)*number);
  cudaMalloc((void**)&b_dev, sizeof(ExtField3_2<Basefield>)*number);
  cudaMalloc((void**)&c_dev, sizeof(ExtField3_2<Basefield>)*number);

  cudaMemcpy(a_dev, a, sizeof(ExtField3_2<Basefield>)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, b, sizeof(ExtField3_2<Basefield>)*number, cudaMemcpyHostToDevice);

  dim3 blocks(number,1);
  dim3 threads(Basefield::nUint,1);

  add3_2_multipleKernel<<< blocks, threads >>>(c_dev,a_dev,b_dev);

  cudaMemcpy(c, c_dev, sizeof(ExtField3_2<Basefield>)*number, cudaMemcpyDeviceToHost);

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(c_dev);
}
template void add3_2_multipleP<F2_487_u64>(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const int number);
template void add3_2_multipleP<F2_967_u64>(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const int number);

/* 二重並列化しない */
template<class Basefield>
void add3_2_2_multipleP(ExtField3_2_2<Basefield> *c, const ExtField3_2_2<Basefield> *a, const ExtField3_2_2<Basefield> *b, const int number) {

  ExtField3_2_2<Basefield> *a_dev, *b_dev, *c_dev;

  cudaMalloc((void**)&a_dev, sizeof(ExtField3_2_2<Basefield>)*number);
  cudaMalloc((void**)&b_dev, sizeof(ExtField3_2_2<Basefield>)*number);
  cudaMalloc((void**)&c_dev, sizeof(ExtField3_2_2<Basefield>)*number);

  cudaMemcpy(a_dev, a, sizeof(ExtField3_2_2<Basefield>)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, b, sizeof(ExtField3_2_2<Basefield>)*number, cudaMemcpyHostToDevice);

  dim3 blocks(number,1);
  dim3 threads(Basefield::nUint,1);

  add3_2_2_multipleKernel<<< blocks, threads >>>(c_dev,a_dev,b_dev);

  cudaMemcpy(c, c_dev, sizeof(ExtField3_2_2<Basefield>)*number, cudaMemcpyDeviceToHost);

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(c_dev);
}
template void add3_2_2_multipleP<F2_487_u64>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const ExtField3_2_2<F2_487_u64> *b, const int number);
template void add3_2_2_multipleP<F2_967_u64>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const ExtField3_2_2<F2_967_u64> *b, const int number);



template<class Basefield>
void mulCombR2L_uintSize_multipleP(Basefield *c, const Basefield *a, const Basefield *b, const int number) {

#if 0
  if (a.testZero() || b.testZero()) {

    c.clear();
    return;
  }
#endif

  BaseField_Double<Basefield> *result = new BaseField_Double<Basefield>[number];
  for (unsigned int i = 0; i < number; ++i) result[i].clear();

  Basefield *a_dev, *b_dev;
  BaseField_Double<Basefield> *result_dev;

  cudaMalloc((void**)&a_dev, sizeof(Basefield)*number);
  cudaMalloc((void**)&b_dev, sizeof(Basefield)*number);
  cudaMalloc((void**)&result_dev, sizeof(BaseField_Double<Basefield>)*number);

  cudaMemcpy(a_dev, a, sizeof(Basefield)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, b, sizeof(Basefield)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(result_dev, result, sizeof(BaseField_Double<Basefield>)*number, cudaMemcpyHostToDevice);

  dim3 blocks(number,1);
  dim3 threads(Basefield::nUint,1);

  mulCombR2L_uintSize_multipleKernel<<< blocks, threads >>>(result_dev,a_dev,b_dev);

  cudaMemcpy(result, result_dev, sizeof(BaseField_Double<Basefield>)*number, cudaMemcpyDeviceToHost);

  //typename Basefield::uint_type mask = 0x0000007fffffffff;
  for (unsigned int i = 0; i < number; ++i) {
    //result[i].low.x[Basefield::nUint - 1] &= mask;
    result[i].low.clearUpDeg();
    result[i].reduction(c[i]);
  }

  delete[] result;
  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(result_dev);
}
template void mulCombR2L_uintSize_multipleP(F2_487_u64 *c, const F2_487_u64 *a, const F2_487_u64 *b, const int number);
template void mulCombR2L_uintSize_multipleP(F2_967_u64 *c, const F2_967_u64 *a, const F2_967_u64 *b, const int number);

/*** mulCombL2RwithWindow_multipleP ***/
#if 0
template<class Basefield>
void mulCombL2RwithWindow_multipleP(Basefield *c, const Basefield *a, const Basefield *b, const int wSize, const int number) {

#if 0
  if (a.testZero() || b.testZero()) {

    c.clear();
    return;
  }
#endif

  BaseField_Double<Basefield> *result = new BaseField_Double<Basefield>[number];
  for (unsigned int i = 0; i < number; ++i) result[i].clear();

  Basefield *a_dev, *b_dev, *c_dev, *bTable_dev;
  BaseField_Double<Basefield> *result_dev;
  int *ws_dev;

  /* !! 遅い !! */
  /* hostでtableの計算 */
  //Basefield *bTable = new Basefield[number*Basefield::pow2func(wSize)];
  //for (unsigned int i = 0; i < number; ++i) Basefield::precomputeTable(bTable + i * Basefield::pow2func(wSize), b[i], wSize);

  cudaMalloc((void**)&a_dev, sizeof(Basefield)*number);
  cudaMalloc((void**)&b_dev, sizeof(Basefield)*number);
  cudaMalloc((void**)&c_dev, sizeof(Basefield)*number);
  cudaMalloc((void**)&bTable_dev, sizeof(Basefield) * number * Basefield::pow2func(wSize));
  cudaMalloc((void**)&result_dev, sizeof(BaseField_Double<Basefield>)*number);
  cudaMalloc((void**)&ws_dev, sizeof(int));

  cudaMemcpy(a_dev, a, sizeof(Basefield)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, b, sizeof(Basefield)*number, cudaMemcpyHostToDevice);
  //cudaMemcpy(bTable_dev, bTable, sizeof(Basefield) * number * Basefield::pow2func(wSize), cudaMemcpyHostToDevice);
  cudaMemcpy(result_dev, result, sizeof(BaseField_Double<Basefield>)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(ws_dev, &wSize, sizeof(int), cudaMemcpyHostToDevice);

  dim3 blocks(number,1);
  dim3 threads(Basefield::nUint,1);

  mulCombL2RwithWindow_multipleKernel<<< blocks, threads >>>(c_dev, result_dev, a_dev, b_dev, bTable_dev, ws_dev);

  cudaMemcpy(result, result_dev, sizeof(BaseField_Double<Basefield>)*number, cudaMemcpyDeviceToHost);

  for (unsigned int i = 0; i < number; ++i) {
    result[i].low.clearUpDeg();
    result[i].reduction(c[i]);
  }


  delete[] result;
  //delete[] bTable;
  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(c_dev);
  cudaFree(bTable_dev);
  cudaFree(result_dev);
  cudaFree(ws_dev);
}
#else
template<class Basefield, int wSize>
void mulCombL2RwithWindow_multipleP(Basefield *c, const Basefield *a, const Basefield *b, const int number) {

#if 0
  if (a.testZero() || b.testZero()) {

    c.clear();
    return;
  }
#endif

  //BaseField_Double<Basefield> *result = new BaseField_Double<Basefield>[number];
  //for (unsigned int i = 0; i < number; ++i) result[i].clear();

  //Basefield *bTable = new Basefield[Basefield::pow2func(wSize)];
  //Basefield::precomputeTable(bTable, b, wSize);

  Basefield *a_dev, *b_dev, *c_dev;
  //BaseField_Double<Basefield> *result_dev;
  //int *ws_dev;

  /* !! 遅い !! */
  /* hostでtableの計算 */
  //Basefield *bTable = new Basefield[number*Basefield::pow2func(wSize)];
  //for (unsigned int i = 0; i < number; ++i) Basefield::precomputeTable(bTable + i * Basefield::pow2func(wSize), b[i], wSize);

  cudaMalloc((void**)&a_dev, sizeof(Basefield)*number);
  cudaMalloc((void**)&b_dev, sizeof(Basefield)*number);
  cudaMalloc((void**)&c_dev, sizeof(Basefield)*number);

  //cudaMalloc((void**)&bTable_dev, sizeof(Basefield) * number * Basefield::pow2func(wSize));
  //cudaMalloc((void**)&result_dev, sizeof(BaseField_Double<Basefield>)*number);

  //cudaMalloc((void**)&ws_dev, sizeof(int));

  cudaMemcpy(a_dev, a, sizeof(Basefield)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b_dev, b, sizeof(Basefield)*number, cudaMemcpyHostToDevice);
  //cudaMemcpy(bTable_dev, bTable, sizeof(Basefield) * number * Basefield::pow2func(wSize), cudaMemcpyHostToDevice);
  //cudaMemcpy(result_dev, result, sizeof(BaseField_Double<Basefield>)*number, cudaMemcpyHostToDevice);
  //cudaMemcpy(ws_dev, &wSize, sizeof(int), cudaMemcpyHostToDevice);

  dim3 blocks(number,1);
  dim3 threads(Basefield::nUint,1);

  //mulCombL2RwithWindow_multipleKernel<<< blocks, threads >>>(c_dev, result_dev, a_dev, b_dev, bTable_dev, ws_dev);
  mulCombL2RwithWindow_multipleKernel<Basefield, wSize><<< blocks, threads >>>(c_dev, a_dev, b_dev);

  //cudaMemcpy(result, result_dev, sizeof(BaseField_Double<Basefield>)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(c, c_dev, sizeof(Basefield)*number, cudaMemcpyDeviceToHost);

  //delete[] result;
  //delete[] bTable;

  cudaFree(a_dev);
  cudaFree(b_dev);
  cudaFree(c_dev);

  //cudaFree(bTable_dev);
  //cudaFree(result_dev);
  //cudaFree(ws_dev);
}
#endif
template void mulCombL2RwithWindow_multipleP<F2_487_u64, 3>(F2_487_u64 *c, const F2_487_u64 *a, const F2_487_u64 *b, const int number);
template void mulCombL2RwithWindow_multipleP<F2_487_u64, 4>(F2_487_u64 *c, const F2_487_u64 *a, const F2_487_u64 *b, const int number);
template void mulCombL2RwithWindow_multipleP<F2_487_u64, 5>(F2_487_u64 *c, const F2_487_u64 *a, const F2_487_u64 *b, const int number);
template void mulCombL2RwithWindow_multipleP<F2_487_u64, 6>(F2_487_u64 *c, const F2_487_u64 *a, const F2_487_u64 *b, const int number);
template void mulCombL2RwithWindow_multipleP<F2_487_u64, 7>(F2_487_u64 *c, const F2_487_u64 *a, const F2_487_u64 *b, const int number);
template void mulCombL2RwithWindow_multipleP<F2_487_u64, 8>(F2_487_u64 *c, const F2_487_u64 *a, const F2_487_u64 *b, const int number);
//template void mulCombL2RwithWindow_multipleP<F2_487_u64, 9>(F2_487_u64 *c, const F2_487_u64 *a, const F2_487_u64 *b, const int number);

template<class Basefield>
void square_multipleP(Basefield *c, const Basefield *a, const int number) {

  Basefield *a_dev, *c_dev;

  cudaMalloc((void**)&a_dev, sizeof(Basefield)*number);
  cudaMalloc((void**)&c_dev, sizeof(Basefield)*number);

  cudaMemcpy(a_dev, a, sizeof(Basefield)*number, cudaMemcpyHostToDevice);

  dim3 blocks(number,1);
  dim3 threads(Basefield::nUint,1);

  square_multipleKernel<<< blocks, threads >>>(c_dev, a_dev);

  cudaMemcpy(c, c_dev, sizeof(Basefield)*number, cudaMemcpyDeviceToHost);

  cudaFree(a_dev);
  cudaFree(c_dev);
}
template void square_multipleP<F2_487_u64>(F2_487_u64 *c, const F2_487_u64 *a, const int number);
template void square_multipleP<F2_967_u64>(F2_967_u64 *c, const F2_967_u64 *a, const int number);


/* 一先ずKaratusba methodの二重並列化は実装せず，乗算については，データをglobal memoryにコピーし基礎体上の乗算のkernel call函数を非同期に呼び出し（並べて）単純に構成する（二重並列化と要検証） */
/* window methodのみ実装 */
/* !!! template<class Basefield>だとnew Basefield[number]で-Wvla...基礎体classは指定する 後で要調査 !!! */
/* 一つのkernel函数で各aST, bSTをdeviceで計算した後乗算vSTを行う */
#if 1
template<int wSize>
void mulCombL2RwithWindow6_multipleP(ExtField6<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b, const int number) {

  ExtField_Double< ExtField6<F2_487_u64> > *result = new ExtField_Double< ExtField6<F2_487_u64> >[number];

  F2_487_u64 *a0 = new F2_487_u64[number];
  F2_487_u64 *a1 = new F2_487_u64[number];
  F2_487_u64 *a2 = new F2_487_u64[number];
  F2_487_u64 *a3 = new F2_487_u64[number];
  F2_487_u64 *a4 = new F2_487_u64[number];
  F2_487_u64 *a5 = new F2_487_u64[number];

  F2_487_u64 *b0 = new F2_487_u64[number];
  F2_487_u64 *b1 = new F2_487_u64[number];
  F2_487_u64 *b2 = new F2_487_u64[number];
  F2_487_u64 *b3 = new F2_487_u64[number];
  F2_487_u64 *b4 = new F2_487_u64[number];
  F2_487_u64 *b5 = new F2_487_u64[number];

  F2_487_u64 *v0 = new F2_487_u64[number];
  F2_487_u64 *v1 = new F2_487_u64[number];
  F2_487_u64 *v2 = new F2_487_u64[number];
  F2_487_u64 *v3 = new F2_487_u64[number];
  F2_487_u64 *v4 = new F2_487_u64[number];
  F2_487_u64 *v5 = new F2_487_u64[number];

  F2_487_u64 *vST0 = new F2_487_u64[number];
  F2_487_u64 *vST1 = new F2_487_u64[number];
  F2_487_u64 *vST2 = new F2_487_u64[number];
  F2_487_u64 *vST3 = new F2_487_u64[number];
  F2_487_u64 *vST4 = new F2_487_u64[number];
  F2_487_u64 *vST5 = new F2_487_u64[number];
  F2_487_u64 *vST6 = new F2_487_u64[number];
  F2_487_u64 *vST7 = new F2_487_u64[number];
  F2_487_u64 *vST8 = new F2_487_u64[number];
  F2_487_u64 *vST9 = new F2_487_u64[number];
  F2_487_u64 *vST10 = new F2_487_u64[number];
  F2_487_u64 *vST11 = new F2_487_u64[number];
  F2_487_u64 *vST12 = new F2_487_u64[number];
  F2_487_u64 *vST13 = new F2_487_u64[number];
  F2_487_u64 *vST14 = new F2_487_u64[number];

  // copy to BF array
  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xBF[0];
    a1[i] = a[i].xBF[1];
    a2[i] = a[i].xBF[2];
    a3[i] = a[i].xBF[3];
    a4[i] = a[i].xBF[4];
    a5[i] = a[i].xBF[5];

    b0[i] = b[i].xBF[0];
    b1[i] = b[i].xBF[1];
    b2[i] = b[i].xBF[2];
    b3[i] = b[i].xBF[3];
    b4[i] = b[i].xBF[4];
    b5[i] = b[i].xBF[5];
  }



  F2_487_u64 *a0_dev, *a1_dev, *a2_dev, *a3_dev, *a4_dev, *a5_dev;
  F2_487_u64 *b0_dev, *b1_dev, *b2_dev, *b3_dev, *b4_dev, *b5_dev;
  F2_487_u64 *v0_dev, *v1_dev, *v2_dev, *v3_dev, *v4_dev, *v5_dev;
  F2_487_u64 *vST0_dev, *vST1_dev, *vST2_dev, *vST3_dev, *vST4_dev, *vST5_dev, *vST6_dev, *vST7_dev, *vST8_dev, *vST9_dev, *vST10_dev, *vST11_dev, *vST12_dev, *vST13_dev, *vST14_dev;

  cudaMalloc((void**)&a0_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a1_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a2_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a3_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a4_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a5_dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&b0_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b1_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b2_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b3_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b4_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b5_dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&v0_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v1_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v2_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v3_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v4_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v5_dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&vST0_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST1_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST2_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST3_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST4_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST5_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST6_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST7_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST8_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST9_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST10_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST11_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST12_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST13_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST14_dev, sizeof(F2_487_u64)*number);

  cudaMemcpy(a0_dev, a0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a1_dev, a1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a2_dev, a2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a3_dev, a3, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a4_dev, a4, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a5_dev, a5, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);

  cudaMemcpy(b0_dev, b0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b1_dev, b1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b2_dev, b2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b3_dev, b3, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b4_dev, b4, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b5_dev, b5, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);

  dim3 blocks(number,1);
  dim3 threads(F2_487_u64::nUint,1);

  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v0_dev, a0_dev, b0_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v1_dev, a1_dev, b1_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v2_dev, a2_dev, b2_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v3_dev, a3_dev, b3_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v4_dev, a4_dev, b4_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v5_dev, a5_dev, b5_dev);

  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST0_dev, a0_dev, a1_dev, b0_dev, b1_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST1_dev, a0_dev, a2_dev, b0_dev, b2_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST2_dev, a0_dev, a3_dev, b0_dev, b3_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST3_dev, a0_dev, a4_dev, b0_dev, b4_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST4_dev, a0_dev, a5_dev, b0_dev, b5_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST5_dev, a1_dev, a2_dev, b1_dev, b2_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST6_dev, a1_dev, a3_dev, b1_dev, b3_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST7_dev, a1_dev, a4_dev, b1_dev, b4_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST8_dev, a1_dev, a5_dev, b1_dev, b5_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST9_dev, a2_dev, a3_dev, b2_dev, b3_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST10_dev, a2_dev, a4_dev, b2_dev, b4_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST11_dev, a2_dev, a5_dev, b2_dev, b5_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST12_dev, a3_dev, a4_dev, b3_dev, b4_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST13_dev, a3_dev, a5_dev, b3_dev, b5_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST14_dev, a4_dev, a5_dev, b4_dev, b5_dev);

  cudaMemcpy(v0, v0_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v1, v1_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v2, v2_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v3, v3_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v4, v4_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v5, v5_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);

  cudaMemcpy(vST0, vST0_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST1, vST1_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST2, vST2_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST3, vST3_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST4, vST4_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST5, vST5_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST6, vST6_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST7, vST7_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST8, vST8_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST9, vST9_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST10, vST10_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST11, vST11_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST12, vST12_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST13, vST13_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST14, vST14_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);



  for (unsigned int i = 0; i < number; ++i) {
    // res[0] = a[0]b[0]
    result[i].low.xBF[0] = v0[i];

    //res[10] = a[5]b[5]
    result[i].high.xBF[4] = v5[i];

    // res[1]
    result[i].low.xBF[1] = vST0[i] - result[i].low.xBF[0];
    result[i].low.xBF[1] -= v1[i];

    // res[2]
    result[i].low.xBF[2] = vST1[i] - result[i].low.xBF[0];
    result[i].low.xBF[2] -= v2[i];
    result[i].low.xBF[2] += v1[i];

    // res[3]
    result[i].low.xBF[3] = vST2[i] + vST5[i];
    result[i].low.xBF[3] -= result[i].low.xBF[0];
    result[i].low.xBF[3] -= v3[i];
    result[i].low.xBF[3] -= v1[i];
    result[i].low.xBF[3] -= v2[i];

    // res[4]
    result[i].low.xBF[4] = vST3[i] + vST6[i];
    result[i].low.xBF[4] -= result[i].low.xBF[0];
    result[i].low.xBF[4] -= v4[i];
    result[i].low.xBF[4] -= v1[i];
    result[i].low.xBF[4] -= v3[i];
    result[i].low.xBF[4] += v2[i];

    // res[5]
    result[i].low.xBF[5] = vST4[i] + vST7[i];
    result[i].low.xBF[5] += vST9[i];
    result[i].low.xBF[5] -= result[i].low.xBF[0];
    result[i].low.xBF[5] -= result[i].high.xBF[4];
    result[i].low.xBF[5] -= v1[i];
    result[i].low.xBF[5] -= v4[i];
    result[i].low.xBF[5] -= v2[i];
    result[i].low.xBF[5] -= v3[i];

    // res[6]
    result[i].high.xBF[0] = vST8[i] + vST10[i];
    result[i].high.xBF[0] -= v1[i];
    result[i].high.xBF[0] -= result[i].high.xBF[4];
    result[i].high.xBF[0] -= v2[i];
    result[i].high.xBF[0] -= v4[i];
    result[i].high.xBF[0] += v3[i];

    // res[7]
    result[i].high.xBF[1] = vST11[i] + vST12[i];
    result[i].high.xBF[1] -= v2[i];
    result[i].high.xBF[1] -= result[i].high.xBF[4];
    result[i].high.xBF[1] -= v3[i];
    result[i].high.xBF[1] -= v4[i];

    // res[8]
    result[i].high.xBF[2] = vST13[i] - v3[i];
    result[i].high.xBF[2] -= result[i].high.xBF[4];
    result[i].high.xBF[2] += v4[i];

    // res[9]
    result[i].high.xBF[3] = vST14[i] - v4[i];
    result[i].high.xBF[3] -= result[i].high.xBF[4];

    c[i].reduction(result[i]);
  }

  delete[] result;

  delete[] a0;
  delete[] a1;
  delete[] a2;
  delete[] a3;
  delete[] a4;
  delete[] a5;

  delete[] b0;
  delete[] b1;
  delete[] b2;
  delete[] b3;
  delete[] b4;
  delete[] b5;

  delete[] v0;
  delete[] v1;
  delete[] v2;
  delete[] v3;
  delete[] v4;
  delete[] v5;

  delete[] vST0;
  delete[] vST1;
  delete[] vST2;
  delete[] vST3;
  delete[] vST4;
  delete[] vST5;
  delete[] vST6;
  delete[] vST7;
  delete[] vST8;
  delete[] vST9;
  delete[] vST10;
  delete[] vST11;
  delete[] vST12;
  delete[] vST13;
  delete[] vST14;

  cudaFree(a0_dev);
  cudaFree(a1_dev);
  cudaFree(a2_dev);
  cudaFree(a3_dev);
  cudaFree(a4_dev);
  cudaFree(a5_dev);

  cudaFree(b0_dev);
  cudaFree(b1_dev);
  cudaFree(b2_dev);
  cudaFree(b3_dev);
  cudaFree(b4_dev);
  cudaFree(b5_dev);

  cudaFree(v0_dev);
  cudaFree(v1_dev);
  cudaFree(v2_dev);
  cudaFree(v3_dev);
  cudaFree(v4_dev);
  cudaFree(v5_dev);

  cudaFree(vST0_dev);
  cudaFree(vST1_dev);
  cudaFree(vST2_dev);
  cudaFree(vST3_dev);
  cudaFree(vST4_dev);
  cudaFree(vST5_dev);
  cudaFree(vST6_dev);
  cudaFree(vST7_dev);
  cudaFree(vST8_dev);
  cudaFree(vST9_dev);
  cudaFree(vST10_dev);
  cudaFree(vST11_dev);
  cudaFree(vST12_dev);
  cudaFree(vST13_dev);
  cudaFree(vST14_dev);
}
/* Karatsubaの事前計算いおいて、hostで加算を行い、乗算のみkernel函数を呼ぶ */
#else
template<int wSize>
void mulCombL2RwithWindow6_multipleP(ExtField6<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b, const int number) {

  ExtField_Double< ExtField6<F2_487_u64> > *result = new ExtField_Double< ExtField6<F2_487_u64> >[number];

  F2_487_u64 *a0 = new F2_487_u64[number];
  F2_487_u64 *a1 = new F2_487_u64[number];
  F2_487_u64 *a2 = new F2_487_u64[number];
  F2_487_u64 *a3 = new F2_487_u64[number];
  F2_487_u64 *a4 = new F2_487_u64[number];
  F2_487_u64 *a5 = new F2_487_u64[number];

  F2_487_u64 *b0 = new F2_487_u64[number];
  F2_487_u64 *b1 = new F2_487_u64[number];
  F2_487_u64 *b2 = new F2_487_u64[number];
  F2_487_u64 *b3 = new F2_487_u64[number];
  F2_487_u64 *b4 = new F2_487_u64[number];
  F2_487_u64 *b5 = new F2_487_u64[number];

  F2_487_u64 *v0 = new F2_487_u64[number];
  F2_487_u64 *v1 = new F2_487_u64[number];
  F2_487_u64 *v2 = new F2_487_u64[number];
  F2_487_u64 *v3 = new F2_487_u64[number];
  F2_487_u64 *v4 = new F2_487_u64[number];
  F2_487_u64 *v5 = new F2_487_u64[number];

  F2_487_u64 *aST0 = new F2_487_u64[number];
  F2_487_u64 *aST1 = new F2_487_u64[number];
  F2_487_u64 *aST2 = new F2_487_u64[number];
  F2_487_u64 *aST3 = new F2_487_u64[number];
  F2_487_u64 *aST4 = new F2_487_u64[number];
  F2_487_u64 *aST5 = new F2_487_u64[number];
  F2_487_u64 *aST6 = new F2_487_u64[number];
  F2_487_u64 *aST7 = new F2_487_u64[number];
  F2_487_u64 *aST8 = new F2_487_u64[number];
  F2_487_u64 *aST9 = new F2_487_u64[number];
  F2_487_u64 *aST10 = new F2_487_u64[number];
  F2_487_u64 *aST11 = new F2_487_u64[number];
  F2_487_u64 *aST12 = new F2_487_u64[number];
  F2_487_u64 *aST13 = new F2_487_u64[number];
  F2_487_u64 *aST14 = new F2_487_u64[number];

  F2_487_u64 *bST0 = new F2_487_u64[number];
  F2_487_u64 *bST1 = new F2_487_u64[number];
  F2_487_u64 *bST2 = new F2_487_u64[number];
  F2_487_u64 *bST3 = new F2_487_u64[number];
  F2_487_u64 *bST4 = new F2_487_u64[number];
  F2_487_u64 *bST5 = new F2_487_u64[number];
  F2_487_u64 *bST6 = new F2_487_u64[number];
  F2_487_u64 *bST7 = new F2_487_u64[number];
  F2_487_u64 *bST8 = new F2_487_u64[number];
  F2_487_u64 *bST9 = new F2_487_u64[number];
  F2_487_u64 *bST10 = new F2_487_u64[number];
  F2_487_u64 *bST11 = new F2_487_u64[number];
  F2_487_u64 *bST12 = new F2_487_u64[number];
  F2_487_u64 *bST13 = new F2_487_u64[number];
  F2_487_u64 *bST14 = new F2_487_u64[number];

  F2_487_u64 *vST0 = new F2_487_u64[number];
  F2_487_u64 *vST1 = new F2_487_u64[number];
  F2_487_u64 *vST2 = new F2_487_u64[number];
  F2_487_u64 *vST3 = new F2_487_u64[number];
  F2_487_u64 *vST4 = new F2_487_u64[number];
  F2_487_u64 *vST5 = new F2_487_u64[number];
  F2_487_u64 *vST6 = new F2_487_u64[number];
  F2_487_u64 *vST7 = new F2_487_u64[number];
  F2_487_u64 *vST8 = new F2_487_u64[number];
  F2_487_u64 *vST9 = new F2_487_u64[number];
  F2_487_u64 *vST10 = new F2_487_u64[number];
  F2_487_u64 *vST11 = new F2_487_u64[number];
  F2_487_u64 *vST12 = new F2_487_u64[number];
  F2_487_u64 *vST13 = new F2_487_u64[number];
  F2_487_u64 *vST14 = new F2_487_u64[number];

  // copy to BF array
  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xBF[0];
    a1[i] = a[i].xBF[1];
    a2[i] = a[i].xBF[2];
    a3[i] = a[i].xBF[3];
    a4[i] = a[i].xBF[4];
    a5[i] = a[i].xBF[5];

    b0[i] = b[i].xBF[0];
    b1[i] = b[i].xBF[1];
    b2[i] = b[i].xBF[2];
    b3[i] = b[i].xBF[3];
    b4[i] = b[i].xBF[4];
    b5[i] = b[i].xBF[5];
  }

  // precompute, addition
  for (unsigned int i = 0; i < number; ++i) {
    aST0[i] = a0[i] + a1[i];
    aST1[i] = a0[i] + a2[i];
    aST2[i] = a0[i] + a3[i];
    aST3[i] = a0[i] + a4[i];
    aST4[i] = a0[i] + a5[i];
    aST5[i] = a1[i] + a2[i];
    aST6[i] = a1[i] + a3[i];
    aST7[i] = a1[i] + a4[i];
    aST8[i] = a1[i] + a5[i];
    aST9[i] = a2[i] + a3[i];
    aST10[i] = a2[i] + a4[i];
    aST11[i] = a2[i] + a5[i];
    aST12[i] = a3[i] + a4[i];
    aST13[i] = a3[i] + a5[i];
    aST14[i] = a4[i] + a5[i];

    bST0[i] = b0[i] + b1[i];
    bST1[i] = b0[i] + b2[i];
    bST2[i] = b0[i] + b3[i];
    bST3[i] = b0[i] + b4[i];
    bST4[i] = b0[i] + b5[i];
    bST5[i] = b1[i] + b2[i];
    bST6[i] = b1[i] + b3[i];
    bST7[i] = b1[i] + b4[i];
    bST8[i] = b1[i] + b5[i];
    bST9[i] = b2[i] + b3[i];
    bST10[i] = b2[i] + b4[i];
    bST11[i] = b2[i] + b5[i];
    bST12[i] = b3[i] + b4[i];
    bST13[i] = b3[i] + b5[i];
    bST14[i] = b4[i] + b5[i];
  }



  F2_487_u64 *a0_dev, *a1_dev, *a2_dev, *a3_dev, *a4_dev, *a5_dev;
  F2_487_u64 *b0_dev, *b1_dev, *b2_dev, *b3_dev, *b4_dev, *b5_dev;
  F2_487_u64 *v0_dev, *v1_dev, *v2_dev, *v3_dev, *v4_dev, *v5_dev;
  F2_487_u64 *aST0_dev, *aST1_dev, *aST2_dev, *aST3_dev, *aST4_dev, *aST5_dev, *aST6_dev, *aST7_dev, *aST8_dev, *aST9_dev, *aST10_dev, *aST11_dev, *aST12_dev, *aST13_dev, *aST14_dev;
  F2_487_u64 *bST0_dev, *bST1_dev, *bST2_dev, *bST3_dev, *bST4_dev, *bST5_dev, *bST6_dev, *bST7_dev, *bST8_dev, *bST9_dev, *bST10_dev, *bST11_dev, *bST12_dev, *bST13_dev, *bST14_dev;
  F2_487_u64 *vST0_dev, *vST1_dev, *vST2_dev, *vST3_dev, *vST4_dev, *vST5_dev, *vST6_dev, *vST7_dev, *vST8_dev, *vST9_dev, *vST10_dev, *vST11_dev, *vST12_dev, *vST13_dev, *vST14_dev;

  cudaMalloc((void**)&a0_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a1_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a2_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a3_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a4_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a5_dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&b0_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b1_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b2_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b3_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b4_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b5_dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&v0_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v1_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v2_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v3_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v4_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v5_dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&aST0_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&aST1_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&aST2_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&aST3_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&aST4_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&aST5_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&aST6_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&aST7_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&aST8_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&aST9_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&aST10_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&aST11_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&aST12_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&aST13_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&aST14_dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&bST0_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&bST1_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&bST2_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&bST3_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&bST4_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&bST5_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&bST6_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&bST7_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&bST8_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&bST9_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&bST10_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&bST11_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&bST12_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&bST13_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&bST14_dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&vST0_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST1_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST2_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST3_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST4_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST5_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST6_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST7_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST8_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST9_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST10_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST11_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST12_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST13_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST14_dev, sizeof(F2_487_u64)*number);

  cudaMemcpy(a0_dev, a0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a1_dev, a1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a2_dev, a2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a3_dev, a3, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a4_dev, a4, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a5_dev, a5, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);

  cudaMemcpy(b0_dev, b0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b1_dev, b1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b2_dev, b2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b3_dev, b3, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b4_dev, b4, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b5_dev, b5, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);

  cudaMemcpy(aST0_dev, aST0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST1_dev, aST1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST2_dev, aST2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST3_dev, aST3, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST4_dev, aST4, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST5_dev, aST5, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST6_dev, aST6, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST7_dev, aST7, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST8_dev, aST8, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST9_dev, aST9, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST10_dev, aST10, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST11_dev, aST11, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST12_dev, aST12, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST13_dev, aST13, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST14_dev, aST14, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);

  cudaMemcpy(bST0_dev, bST0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST1_dev, bST1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST2_dev, bST2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST3_dev, bST3, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST4_dev, bST4, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST5_dev, bST5, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST6_dev, bST6, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST7_dev, bST7, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST8_dev, bST8, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST9_dev, bST9, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST10_dev, bST10, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST11_dev, bST11, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST12_dev, bST12, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST13_dev, bST13, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST14_dev, bST14, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);

  dim3 blocks(number,1);
  dim3 threads(F2_487_u64::nUint,1);

  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v0_dev, a0_dev, b0_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v1_dev, a1_dev, b1_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v2_dev, a2_dev, b2_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v3_dev, a3_dev, b3_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v4_dev, a4_dev, b4_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v5_dev, a5_dev, b5_dev);

  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST0_dev, aST0_dev, bST0_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST1_dev, aST1_dev, bST1_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST2_dev, aST2_dev, bST2_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST3_dev, aST3_dev, bST3_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST4_dev, aST4_dev, bST4_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST5_dev, aST5_dev, bST5_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST6_dev, aST6_dev, bST6_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST7_dev, aST7_dev, bST7_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST8_dev, aST8_dev, bST8_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST9_dev, aST9_dev, bST9_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST10_dev, aST10_dev, bST10_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST11_dev, aST11_dev, bST11_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST12_dev, aST12_dev, bST12_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST13_dev, aST13_dev, bST13_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST14_dev, aST14_dev, bST14_dev);

  cudaMemcpy(v0, v0_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v1, v1_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v2, v2_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v3, v3_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v4, v4_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v5, v5_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);

  cudaMemcpy(vST0, vST0_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST1, vST1_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST2, vST2_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST3, vST3_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST4, vST4_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST5, vST5_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST6, vST6_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST7, vST7_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST8, vST8_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST9, vST9_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST10, vST10_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST11, vST11_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST12, vST12_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST13, vST13_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST14, vST14_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);



  for (unsigned int i = 0; i < number; ++i) {
    // res[0] = a[0]b[0]
    result[i].low.xBF[0] = v0[i];

    //res[10] = a[5]b[5]
    result[i].high.xBF[4] = v5[i];

    // res[1]
    result[i].low.xBF[1] = vST0[i] - result[i].low.xBF[0];
    result[i].low.xBF[1] -= v1[i];

    // res[2]
    result[i].low.xBF[2] = vST1[i] - result[i].low.xBF[0];
    result[i].low.xBF[2] -= v2[i];
    result[i].low.xBF[2] += v1[i];

    // res[3]
    result[i].low.xBF[3] = vST2[i] + vST5[i];
    result[i].low.xBF[3] -= result[i].low.xBF[0];
    result[i].low.xBF[3] -= v3[i];
    result[i].low.xBF[3] -= v1[i];
    result[i].low.xBF[3] -= v2[i];

    // res[4]
    result[i].low.xBF[4] = vST3[i] + vST6[i];
    result[i].low.xBF[4] -= result[i].low.xBF[0];
    result[i].low.xBF[4] -= v4[i];
    result[i].low.xBF[4] -= v1[i];
    result[i].low.xBF[4] -= v3[i];
    result[i].low.xBF[4] += v2[i];

    // res[5]
    result[i].low.xBF[5] = vST4[i] + vST7[i];
    result[i].low.xBF[5] += vST9[i];
    result[i].low.xBF[5] -= result[i].low.xBF[0];
    result[i].low.xBF[5] -= result[i].high.xBF[4];
    result[i].low.xBF[5] -= v1[i];
    result[i].low.xBF[5] -= v4[i];
    result[i].low.xBF[5] -= v2[i];
    result[i].low.xBF[5] -= v3[i];

    // res[6]
    result[i].high.xBF[0] = vST8[i] + vST10[i];
    result[i].high.xBF[0] -= v1[i];
    result[i].high.xBF[0] -= result[i].high.xBF[4];
    result[i].high.xBF[0] -= v2[i];
    result[i].high.xBF[0] -= v4[i];
    result[i].high.xBF[0] += v3[i];

    // res[7]
    result[i].high.xBF[1] = vST11[i] + vST12[i];
    result[i].high.xBF[1] -= v2[i];
    result[i].high.xBF[1] -= result[i].high.xBF[4];
    result[i].high.xBF[1] -= v3[i];
    result[i].high.xBF[1] -= v4[i];

    // res[8]
    result[i].high.xBF[2] = vST13[i] - v3[i];
    result[i].high.xBF[2] -= result[i].high.xBF[4];
    result[i].high.xBF[2] += v4[i];

    // res[9]
    result[i].high.xBF[3] = vST14[i] - v4[i];
    result[i].high.xBF[3] -= result[i].high.xBF[4];

    c[i].reduction(result[i]);
  }

  delete[] result;

  delete[] a0;
  delete[] a1;
  delete[] a2;
  delete[] a3;
  delete[] a4;
  delete[] a5;

  delete[] b0;
  delete[] b1;
  delete[] b2;
  delete[] b3;
  delete[] b4;
  delete[] b5;

  delete[] v0;
  delete[] v1;
  delete[] v2;
  delete[] v3;
  delete[] v4;
  delete[] v5;

  delete[] aST0;
  delete[] aST1;
  delete[] aST2;
  delete[] aST3;
  delete[] aST4;
  delete[] aST5;
  delete[] aST6;
  delete[] aST7;
  delete[] aST8;
  delete[] aST9;
  delete[] aST10;
  delete[] aST11;
  delete[] aST12;
  delete[] aST13;
  delete[] aST14;

  delete[] bST0;
  delete[] bST1;
  delete[] bST2;
  delete[] bST3;
  delete[] bST4;
  delete[] bST5;
  delete[] bST6;
  delete[] bST7;
  delete[] bST8;
  delete[] bST9;
  delete[] bST10;
  delete[] bST11;
  delete[] bST12;
  delete[] bST13;
  delete[] bST14;

  delete[] vST0;
  delete[] vST1;
  delete[] vST2;
  delete[] vST3;
  delete[] vST4;
  delete[] vST5;
  delete[] vST6;
  delete[] vST7;
  delete[] vST8;
  delete[] vST9;
  delete[] vST10;
  delete[] vST11;
  delete[] vST12;
  delete[] vST13;
  delete[] vST14;

  cudaFree(a0_dev);
  cudaFree(a1_dev);
  cudaFree(a2_dev);
  cudaFree(a3_dev);
  cudaFree(a4_dev);
  cudaFree(a5_dev);

  cudaFree(b0_dev);
  cudaFree(b1_dev);
  cudaFree(b2_dev);
  cudaFree(b3_dev);
  cudaFree(b4_dev);
  cudaFree(b5_dev);

  cudaFree(v0_dev);
  cudaFree(v1_dev);
  cudaFree(v2_dev);
  cudaFree(v3_dev);
  cudaFree(v4_dev);
  cudaFree(v5_dev);

  cudaFree(aST0_dev);
  cudaFree(aST1_dev);
  cudaFree(aST2_dev);
  cudaFree(aST3_dev);
  cudaFree(aST4_dev);
  cudaFree(aST5_dev);
  cudaFree(aST6_dev);
  cudaFree(aST7_dev);
  cudaFree(aST8_dev);
  cudaFree(aST9_dev);
  cudaFree(aST10_dev);
  cudaFree(aST11_dev);
  cudaFree(aST12_dev);
  cudaFree(aST13_dev);
  cudaFree(aST14_dev);

  cudaFree(bST0_dev);
  cudaFree(bST1_dev);
  cudaFree(bST2_dev);
  cudaFree(bST3_dev);
  cudaFree(bST4_dev);
  cudaFree(bST5_dev);
  cudaFree(bST6_dev);
  cudaFree(bST7_dev);
  cudaFree(bST8_dev);
  cudaFree(bST9_dev);
  cudaFree(bST10_dev);
  cudaFree(bST11_dev);
  cudaFree(bST12_dev);
  cudaFree(bST13_dev);
  cudaFree(bST14_dev);

  cudaFree(vST0_dev);
  cudaFree(vST1_dev);
  cudaFree(vST2_dev);
  cudaFree(vST3_dev);
  cudaFree(vST4_dev);
  cudaFree(vST5_dev);
  cudaFree(vST6_dev);
  cudaFree(vST7_dev);
  cudaFree(vST8_dev);
  cudaFree(vST9_dev);
  cudaFree(vST10_dev);
  cudaFree(vST11_dev);
  cudaFree(vST12_dev);
  cudaFree(vST13_dev);
  cudaFree(vST14_dev);
}
#endif
template void mulCombL2RwithWindow6_multipleP<3>(ExtField6<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow6_multipleP<4>(ExtField6<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow6_multipleP<5>(ExtField6<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow6_multipleP<6>(ExtField6<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow6_multipleP<7>(ExtField6<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow6_multipleP<8>(ExtField6<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b, const int number);
//template void mulCombL2RwithWindow6_multipleP<9>(ExtField6<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b, const int number);

void square6_multipleP(ExtField6<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const int number) {

  F2_487_u64 *a0 = new F2_487_u64[number];
  F2_487_u64 *a1 = new F2_487_u64[number];
  F2_487_u64 *a2 = new F2_487_u64[number];
  F2_487_u64 *a3 = new F2_487_u64[number];
  F2_487_u64 *a4 = new F2_487_u64[number];
  F2_487_u64 *a5 = new F2_487_u64[number];
  F2_487_u64 *tmp = new F2_487_u64[number];

  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xBF[0];
    a1[i] = a[i].xBF[1];
    a2[i] = a[i].xBF[2];
    a3[i] = a[i].xBF[3];
    a4[i] = a[i].xBF[4];
    a5[i] = a[i].xBF[5];
  }

  // (fix) c_5 = a_3^2
  square_multipleP(tmp, a3, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[5] = tmp[i];

  // (fix) c_4 = a_2^2
  square_multipleP(tmp, a2, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[4] = tmp[i];

  // c_3 = a_5^2
  square_multipleP(tmp, a5, number);
  // (fix) c_3 = a_5^2 + a_3^2
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[3] = tmp[i] + c[i].xBF[5];

  // c_0 = a_1^2
  square_multipleP(tmp, a1, number);

  // (fix) c_2 = a_5^2 + a_3^2 + a_1^2
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[2] = tmp[i] + c[i].xBF[3];

  // (fix) c_1 = a_4^2
  square_multipleP(tmp, a4, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[1] = tmp[i];

  // c_0 = a_0^2
  square_multipleP(tmp, a0, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[0] = tmp[i];

  // c_0 = a_4^2 + a_3^2 + a_0^2
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[0] += c[i].xBF[1];
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[0] += c[i].xBF[5];

  delete[] a0;
  delete[] a1;
  delete[] a2;
  delete[] a3;
  delete[] a4;
  delete[] a5;
  delete[] tmp;
}

template<int wSize>
void mulCombL2RwithWindow6_2_multipleP(ExtField6_2<F2_487_u64> *c, const ExtField6_2<F2_487_u64> *a, const ExtField6_2<F2_487_u64> *b, const int number) {

#if _Align_Kernel
  ExtField_Double< ExtField6_2<F2_487_u64> > *result = new ExtField_Double< ExtField6_2<F2_487_u64> >[number];

  ExtField_Double< ExtField6<F2_487_u64> > *result_0 = new ExtField_Double< ExtField6<F2_487_u64> >[number];
  ExtField_Double< ExtField6<F2_487_u64> > *result_1 = new ExtField_Double< ExtField6<F2_487_u64> >[number];
  ExtField_Double< ExtField6<F2_487_u64> > *result_2 = new ExtField_Double< ExtField6<F2_487_u64> >[number];

  F2_487_u64 *a0_0 = new F2_487_u64[number];
  F2_487_u64 *a1_0 = new F2_487_u64[number];
  F2_487_u64 *a2_0 = new F2_487_u64[number];
  F2_487_u64 *a3_0 = new F2_487_u64[number];
  F2_487_u64 *a4_0 = new F2_487_u64[number];
  F2_487_u64 *a5_0 = new F2_487_u64[number];

  F2_487_u64 *a0_1 = new F2_487_u64[number];
  F2_487_u64 *a1_1 = new F2_487_u64[number];
  F2_487_u64 *a2_1 = new F2_487_u64[number];
  F2_487_u64 *a3_1 = new F2_487_u64[number];
  F2_487_u64 *a4_1 = new F2_487_u64[number];
  F2_487_u64 *a5_1 = new F2_487_u64[number];

  F2_487_u64 *a0_2 = new F2_487_u64[number];
  F2_487_u64 *a1_2 = new F2_487_u64[number];
  F2_487_u64 *a2_2 = new F2_487_u64[number];
  F2_487_u64 *a3_2 = new F2_487_u64[number];
  F2_487_u64 *a4_2 = new F2_487_u64[number];
  F2_487_u64 *a5_2 = new F2_487_u64[number];

  F2_487_u64 *b0_0 = new F2_487_u64[number];
  F2_487_u64 *b1_0 = new F2_487_u64[number];
  F2_487_u64 *b2_0 = new F2_487_u64[number];
  F2_487_u64 *b3_0 = new F2_487_u64[number];
  F2_487_u64 *b4_0 = new F2_487_u64[number];
  F2_487_u64 *b5_0 = new F2_487_u64[number];

  F2_487_u64 *b0_1 = new F2_487_u64[number];
  F2_487_u64 *b1_1 = new F2_487_u64[number];
  F2_487_u64 *b2_1 = new F2_487_u64[number];
  F2_487_u64 *b3_1 = new F2_487_u64[number];
  F2_487_u64 *b4_1 = new F2_487_u64[number];
  F2_487_u64 *b5_1 = new F2_487_u64[number];

  F2_487_u64 *b0_2 = new F2_487_u64[number];
  F2_487_u64 *b1_2 = new F2_487_u64[number];
  F2_487_u64 *b2_2 = new F2_487_u64[number];
  F2_487_u64 *b3_2 = new F2_487_u64[number];
  F2_487_u64 *b4_2 = new F2_487_u64[number];
  F2_487_u64 *b5_2 = new F2_487_u64[number];

  F2_487_u64 *v0_0 = new F2_487_u64[number];
  F2_487_u64 *v1_0 = new F2_487_u64[number];
  F2_487_u64 *v2_0 = new F2_487_u64[number];
  F2_487_u64 *v3_0 = new F2_487_u64[number];
  F2_487_u64 *v4_0 = new F2_487_u64[number];
  F2_487_u64 *v5_0 = new F2_487_u64[number];

  F2_487_u64 *v0_1 = new F2_487_u64[number];
  F2_487_u64 *v1_1 = new F2_487_u64[number];
  F2_487_u64 *v2_1 = new F2_487_u64[number];
  F2_487_u64 *v3_1 = new F2_487_u64[number];
  F2_487_u64 *v4_1 = new F2_487_u64[number];
  F2_487_u64 *v5_1 = new F2_487_u64[number];

  F2_487_u64 *v0_2 = new F2_487_u64[number];
  F2_487_u64 *v1_2 = new F2_487_u64[number];
  F2_487_u64 *v2_2 = new F2_487_u64[number];
  F2_487_u64 *v3_2 = new F2_487_u64[number];
  F2_487_u64 *v4_2 = new F2_487_u64[number];
  F2_487_u64 *v5_2 = new F2_487_u64[number];

  F2_487_u64 *vST0_0 = new F2_487_u64[number];
  F2_487_u64 *vST1_0 = new F2_487_u64[number];
  F2_487_u64 *vST2_0 = new F2_487_u64[number];
  F2_487_u64 *vST3_0 = new F2_487_u64[number];
  F2_487_u64 *vST4_0 = new F2_487_u64[number];
  F2_487_u64 *vST5_0 = new F2_487_u64[number];
  F2_487_u64 *vST6_0 = new F2_487_u64[number];
  F2_487_u64 *vST7_0 = new F2_487_u64[number];
  F2_487_u64 *vST8_0 = new F2_487_u64[number];
  F2_487_u64 *vST9_0 = new F2_487_u64[number];
  F2_487_u64 *vST10_0 = new F2_487_u64[number];
  F2_487_u64 *vST11_0 = new F2_487_u64[number];
  F2_487_u64 *vST12_0 = new F2_487_u64[number];
  F2_487_u64 *vST13_0 = new F2_487_u64[number];
  F2_487_u64 *vST14_0 = new F2_487_u64[number];

  F2_487_u64 *vST0_1 = new F2_487_u64[number];
  F2_487_u64 *vST1_1 = new F2_487_u64[number];
  F2_487_u64 *vST2_1 = new F2_487_u64[number];
  F2_487_u64 *vST3_1 = new F2_487_u64[number];
  F2_487_u64 *vST4_1 = new F2_487_u64[number];
  F2_487_u64 *vST5_1 = new F2_487_u64[number];
  F2_487_u64 *vST6_1 = new F2_487_u64[number];
  F2_487_u64 *vST7_1 = new F2_487_u64[number];
  F2_487_u64 *vST8_1 = new F2_487_u64[number];
  F2_487_u64 *vST9_1 = new F2_487_u64[number];
  F2_487_u64 *vST10_1 = new F2_487_u64[number];
  F2_487_u64 *vST11_1 = new F2_487_u64[number];
  F2_487_u64 *vST12_1 = new F2_487_u64[number];
  F2_487_u64 *vST13_1 = new F2_487_u64[number];
  F2_487_u64 *vST14_1 = new F2_487_u64[number];

  F2_487_u64 *vST0_2 = new F2_487_u64[number];
  F2_487_u64 *vST1_2 = new F2_487_u64[number];
  F2_487_u64 *vST2_2 = new F2_487_u64[number];
  F2_487_u64 *vST3_2 = new F2_487_u64[number];
  F2_487_u64 *vST4_2 = new F2_487_u64[number];
  F2_487_u64 *vST5_2 = new F2_487_u64[number];
  F2_487_u64 *vST6_2 = new F2_487_u64[number];
  F2_487_u64 *vST7_2 = new F2_487_u64[number];
  F2_487_u64 *vST8_2 = new F2_487_u64[number];
  F2_487_u64 *vST9_2 = new F2_487_u64[number];
  F2_487_u64 *vST10_2 = new F2_487_u64[number];
  F2_487_u64 *vST11_2 = new F2_487_u64[number];
  F2_487_u64 *vST12_2 = new F2_487_u64[number];
  F2_487_u64 *vST13_2 = new F2_487_u64[number];
  F2_487_u64 *vST14_2 = new F2_487_u64[number];

  // copy to BF array
  for (unsigned int i = 0; i < number; ++i) {
    a0_0[i] = a[i].xF6[0].xBF[0];
    a1_0[i] = a[i].xF6[0].xBF[1];
    a2_0[i] = a[i].xF6[0].xBF[2];
    a3_0[i] = a[i].xF6[0].xBF[3];
    a4_0[i] = a[i].xF6[0].xBF[4];
    a5_0[i] = a[i].xF6[0].xBF[5];

    b0_0[i] = b[i].xF6[0].xBF[0];
    b1_0[i] = b[i].xF6[0].xBF[1];
    b2_0[i] = b[i].xF6[0].xBF[2];
    b3_0[i] = b[i].xF6[0].xBF[3];
    b4_0[i] = b[i].xF6[0].xBF[4];
    b5_0[i] = b[i].xF6[0].xBF[5];

    a0_1[i] = a[i].xF6[1].xBF[0];
    a1_1[i] = a[i].xF6[1].xBF[1];
    a2_1[i] = a[i].xF6[1].xBF[2];
    a3_1[i] = a[i].xF6[1].xBF[3];
    a4_1[i] = a[i].xF6[1].xBF[4];
    a5_1[i] = a[i].xF6[1].xBF[5];

    b0_1[i] = b[i].xF6[1].xBF[0];
    b1_1[i] = b[i].xF6[1].xBF[1];
    b2_1[i] = b[i].xF6[1].xBF[2];
    b3_1[i] = b[i].xF6[1].xBF[3];
    b4_1[i] = b[i].xF6[1].xBF[4];
    b5_1[i] = b[i].xF6[1].xBF[5];

    a0_2[i] = a[i].xF6[0].xBF[0] + a[i].xF6[1].xBF[0];
    a1_2[i] = a[i].xF6[0].xBF[1] + a[i].xF6[1].xBF[1];
    a2_2[i] = a[i].xF6[0].xBF[2] + a[i].xF6[1].xBF[2];
    a3_2[i] = a[i].xF6[0].xBF[3] + a[i].xF6[1].xBF[3];
    a4_2[i] = a[i].xF6[0].xBF[4] + a[i].xF6[1].xBF[4];
    a5_2[i] = a[i].xF6[0].xBF[5] + a[i].xF6[1].xBF[5];

    b0_2[i] = b[i].xF6[0].xBF[0] + b[i].xF6[1].xBF[0];
    b1_2[i] = b[i].xF6[0].xBF[1] + b[i].xF6[1].xBF[1];
    b2_2[i] = b[i].xF6[0].xBF[2] + b[i].xF6[1].xBF[2];
    b3_2[i] = b[i].xF6[0].xBF[3] + b[i].xF6[1].xBF[3];
    b4_2[i] = b[i].xF6[0].xBF[4] + b[i].xF6[1].xBF[4];
    b5_2[i] = b[i].xF6[0].xBF[5] + b[i].xF6[1].xBF[5];
  }




  F2_487_u64 *a0_0dev, *a1_0dev, *a2_0dev, *a3_0dev, *a4_0dev, *a5_0dev;
  F2_487_u64 *a0_1dev, *a1_1dev, *a2_1dev, *a3_1dev, *a4_1dev, *a5_1dev;
  F2_487_u64 *a0_2dev, *a1_2dev, *a2_2dev, *a3_2dev, *a4_2dev, *a5_2dev;

  F2_487_u64 *b0_0dev, *b1_0dev, *b2_0dev, *b3_0dev, *b4_0dev, *b5_0dev;
  F2_487_u64 *b0_1dev, *b1_1dev, *b2_1dev, *b3_1dev, *b4_1dev, *b5_1dev;
  F2_487_u64 *b0_2dev, *b1_2dev, *b2_2dev, *b3_2dev, *b4_2dev, *b5_2dev;

  F2_487_u64 *v0_0dev, *v1_0dev, *v2_0dev, *v3_0dev, *v4_0dev, *v5_0dev;
  F2_487_u64 *v0_1dev, *v1_1dev, *v2_1dev, *v3_1dev, *v4_1dev, *v5_1dev;
  F2_487_u64 *v0_2dev, *v1_2dev, *v2_2dev, *v3_2dev, *v4_2dev, *v5_2dev;

  F2_487_u64 *vST0_0dev, *vST1_0dev, *vST2_0dev, *vST3_0dev, *vST4_0dev, *vST5_0dev, *vST6_0dev, *vST7_0dev, *vST8_0dev, *vST9_0dev, *vST10_0dev, *vST11_0dev, *vST12_0dev, *vST13_0dev, *vST14_0dev;
  F2_487_u64 *vST0_1dev, *vST1_1dev, *vST2_1dev, *vST3_1dev, *vST4_1dev, *vST5_1dev, *vST6_1dev, *vST7_1dev, *vST8_1dev, *vST9_1dev, *vST10_1dev, *vST11_1dev, *vST12_1dev, *vST13_1dev, *vST14_1dev;
  F2_487_u64 *vST0_2dev, *vST1_2dev, *vST2_2dev, *vST3_2dev, *vST4_2dev, *vST5_2dev, *vST6_2dev, *vST7_2dev, *vST8_2dev, *vST9_2dev, *vST10_2dev, *vST11_2dev, *vST12_2dev, *vST13_2dev, *vST14_2dev;

  cudaMalloc((void**)&a0_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a1_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a2_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a3_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a4_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a5_0dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&a0_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a1_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a2_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a3_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a4_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a5_1dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&a0_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a1_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a2_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a3_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a4_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a5_2dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&b0_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b1_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b2_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b3_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b4_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b5_0dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&b0_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b1_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b2_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b3_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b4_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b5_1dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&b0_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b1_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b2_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b3_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b4_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b5_2dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&v0_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v1_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v2_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v3_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v4_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v5_0dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&v0_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v1_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v2_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v3_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v4_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v5_1dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&v0_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v1_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v2_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v3_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v4_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v5_2dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&vST0_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST1_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST2_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST3_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST4_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST5_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST6_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST7_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST8_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST9_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST10_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST11_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST12_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST13_0dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST14_0dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&vST0_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST1_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST2_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST3_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST4_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST5_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST6_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST7_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST8_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST9_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST10_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST11_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST12_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST13_1dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST14_1dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&vST0_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST1_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST2_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST3_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST4_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST5_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST6_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST7_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST8_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST9_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST10_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST11_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST12_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST13_2dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST14_2dev, sizeof(F2_487_u64)*number);

  cudaMemcpy(a0_0dev, a0_0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a1_0dev, a1_0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a2_0dev, a2_0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a3_0dev, a3_0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a4_0dev, a4_0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a5_0dev, a5_0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);

  cudaMemcpy(a0_1dev, a0_1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a1_1dev, a1_1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a2_1dev, a2_1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a3_1dev, a3_1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a4_1dev, a4_1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a5_1dev, a5_1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);

  cudaMemcpy(a0_2dev, a0_2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a1_2dev, a1_2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a2_2dev, a2_2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a3_2dev, a3_2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a4_2dev, a4_2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a5_2dev, a5_2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);

  cudaMemcpy(b0_0dev, b0_0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b1_0dev, b1_0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b2_0dev, b2_0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b3_0dev, b3_0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b4_0dev, b4_0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b5_0dev, b5_0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);

  cudaMemcpy(b0_1dev, b0_1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b1_1dev, b1_1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b2_1dev, b2_1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b3_1dev, b3_1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b4_1dev, b4_1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b5_1dev, b5_1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);

  cudaMemcpy(b0_2dev, b0_2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b1_2dev, b1_2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b2_2dev, b2_2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b3_2dev, b3_2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b4_2dev, b4_2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b5_2dev, b5_2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);



  dim3 blocks(number,1);
  dim3 threads(F2_487_u64::nUint,1);

  // a0b0
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v0_0dev, a0_0dev, b0_0dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v1_0dev, a1_0dev, b1_0dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v2_0dev, a2_0dev, b2_0dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v3_0dev, a3_0dev, b3_0dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v4_0dev, a4_0dev, b4_0dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v5_0dev, a5_0dev, b5_0dev);

  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST0_0dev, a0_0dev, a1_0dev, b0_0dev, b1_0dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST1_0dev, a0_0dev, a2_0dev, b0_0dev, b2_0dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST2_0dev, a0_0dev, a3_0dev, b0_0dev, b3_0dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST3_0dev, a0_0dev, a4_0dev, b0_0dev, b4_0dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST4_0dev, a0_0dev, a5_0dev, b0_0dev, b5_0dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST5_0dev, a1_0dev, a2_0dev, b1_0dev, b2_0dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST6_0dev, a1_0dev, a3_0dev, b1_0dev, b3_0dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST7_0dev, a1_0dev, a4_0dev, b1_0dev, b4_0dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST8_0dev, a1_0dev, a5_0dev, b1_0dev, b5_0dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST9_0dev, a2_0dev, a3_0dev, b2_0dev, b3_0dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST10_0dev, a2_0dev, a4_0dev, b2_0dev, b4_0dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST11_0dev, a2_0dev, a5_0dev, b2_0dev, b5_0dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST12_0dev, a3_0dev, a4_0dev, b3_0dev, b4_0dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST13_0dev, a3_0dev, a5_0dev, b3_0dev, b5_0dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST14_0dev, a4_0dev, a5_0dev, b4_0dev, b5_0dev);



  // a1b1
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v0_1dev, a0_1dev, b0_1dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v1_1dev, a1_1dev, b1_1dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v2_1dev, a2_1dev, b2_1dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v3_1dev, a3_1dev, b3_1dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v4_1dev, a4_1dev, b4_1dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v5_1dev, a5_1dev, b5_1dev);

  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST0_1dev, a0_1dev, a1_1dev, b0_1dev, b1_1dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST1_1dev, a0_1dev, a2_1dev, b0_1dev, b2_1dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST2_1dev, a0_1dev, a3_1dev, b0_1dev, b3_1dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST3_1dev, a0_1dev, a4_1dev, b0_1dev, b4_1dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST4_1dev, a0_1dev, a5_1dev, b0_1dev, b5_1dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST5_1dev, a1_1dev, a2_1dev, b1_1dev, b2_1dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST6_1dev, a1_1dev, a3_1dev, b1_1dev, b3_1dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST7_1dev, a1_1dev, a4_1dev, b1_1dev, b4_1dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST8_1dev, a1_1dev, a5_1dev, b1_1dev, b5_1dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST9_1dev, a2_1dev, a3_1dev, b2_1dev, b3_1dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST10_1dev, a2_1dev, a4_1dev, b2_1dev, b4_1dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST11_1dev, a2_1dev, a5_1dev, b2_1dev, b5_1dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST12_1dev, a3_1dev, a4_1dev, b3_1dev, b4_1dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST13_1dev, a3_1dev, a5_1dev, b3_1dev, b5_1dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST14_1dev, a4_1dev, a5_1dev, b4_1dev, b5_1dev);



  // (a0+a1)(b0+b1)
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v0_2dev, a0_2dev, b0_2dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v1_2dev, a1_2dev, b1_2dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v2_2dev, a2_2dev, b2_2dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v3_2dev, a3_2dev, b3_2dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v4_2dev, a4_2dev, b4_2dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v5_2dev, a5_2dev, b5_2dev);

  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST0_2dev, a0_2dev, a1_2dev, b0_2dev, b1_2dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST1_2dev, a0_2dev, a2_2dev, b0_2dev, b2_2dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST2_2dev, a0_2dev, a3_2dev, b0_2dev, b3_2dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST3_2dev, a0_2dev, a4_2dev, b0_2dev, b4_2dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST4_2dev, a0_2dev, a5_2dev, b0_2dev, b5_2dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST5_2dev, a1_2dev, a2_2dev, b1_2dev, b2_2dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST6_2dev, a1_2dev, a3_2dev, b1_2dev, b3_2dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST7_2dev, a1_2dev, a4_2dev, b1_2dev, b4_2dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST8_2dev, a1_2dev, a5_2dev, b1_2dev, b5_2dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST9_2dev, a2_2dev, a3_2dev, b2_2dev, b3_2dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST10_2dev, a2_2dev, a4_2dev, b2_2dev, b4_2dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST11_2dev, a2_2dev, a5_2dev, b2_2dev, b5_2dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST12_2dev, a3_2dev, a4_2dev, b3_2dev, b4_2dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST13_2dev, a3_2dev, a5_2dev, b3_2dev, b5_2dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST14_2dev, a4_2dev, a5_2dev, b4_2dev, b5_2dev);



  cudaMemcpy(v0_0, v0_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v1_0, v1_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v2_0, v2_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v3_0, v3_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v4_0, v4_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v5_0, v5_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);

  cudaMemcpy(v0_1, v0_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v1_1, v1_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v2_1, v2_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v3_1, v3_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v4_1, v4_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v5_1, v5_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);

  cudaMemcpy(v0_2, v0_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v1_2, v1_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v2_2, v2_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v3_2, v3_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v4_2, v4_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v5_2, v5_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);

  cudaMemcpy(vST0_0, vST0_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST1_0, vST1_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST2_0, vST2_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST3_0, vST3_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST4_0, vST4_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST5_0, vST5_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST6_0, vST6_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST7_0, vST7_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST8_0, vST8_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST9_0, vST9_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST10_0, vST10_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST11_0, vST11_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST12_0, vST12_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST13_0, vST13_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST14_0, vST14_0dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);

  cudaMemcpy(vST0_1, vST0_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST1_1, vST1_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST2_1, vST2_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST3_1, vST3_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST4_1, vST4_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST5_1, vST5_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST6_1, vST6_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST7_1, vST7_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST8_1, vST8_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST9_1, vST9_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST10_1, vST10_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST11_1, vST11_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST12_1, vST12_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST13_1, vST13_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST14_1, vST14_1dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);

  cudaMemcpy(vST0_2, vST0_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST1_2, vST1_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST2_2, vST2_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST3_2, vST3_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST4_2, vST4_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST5_2, vST5_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST6_2, vST6_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST7_2, vST7_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST8_2, vST8_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST9_2, vST9_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST10_2, vST10_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST11_2, vST11_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST12_2, vST12_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST13_2, vST13_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST14_2, vST14_2dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);



  for (unsigned int i = 0; i < number; ++i) {
    // a0b0
    // res[0] = a[0]b[0]
    result_0[i].low.xBF[0] = v0_0[i];

    //res[10] = a[5]b[5]
    result_0[i].high.xBF[4] = v5_0[i];

    // res[1]
    result_0[i].low.xBF[1] = vST0_0[i] - result_0[i].low.xBF[0];
    result_0[i].low.xBF[1] -= v1_0[i];

    // res[2]
    result_0[i].low.xBF[2] = vST1_0[i] - result_0[i].low.xBF[0];
    result_0[i].low.xBF[2] -= v2_0[i];
    result_0[i].low.xBF[2] += v1_0[i];

    // res[3]
    result_0[i].low.xBF[3] = vST2_0[i] + vST5_0[i];
    result_0[i].low.xBF[3] -= result_0[i].low.xBF[0];
    result_0[i].low.xBF[3] -= v3_0[i];
    result_0[i].low.xBF[3] -= v1_0[i];
    result_0[i].low.xBF[3] -= v2_0[i];

    // res[4]
    result_0[i].low.xBF[4] = vST3_0[i] + vST6_0[i];
    result_0[i].low.xBF[4] -= result_0[i].low.xBF[0];
    result_0[i].low.xBF[4] -= v4_0[i];
    result_0[i].low.xBF[4] -= v1_0[i];
    result_0[i].low.xBF[4] -= v3_0[i];
    result_0[i].low.xBF[4] += v2_0[i];

    // res[5]
    result_0[i].low.xBF[5] = vST4_0[i] + vST7_0[i];
    result_0[i].low.xBF[5] += vST9_0[i];
    result_0[i].low.xBF[5] -= result_0[i].low.xBF[0];
    result_0[i].low.xBF[5] -= result_0[i].high.xBF[4];
    result_0[i].low.xBF[5] -= v1_0[i];
    result_0[i].low.xBF[5] -= v4_0[i];
    result_0[i].low.xBF[5] -= v2_0[i];
    result_0[i].low.xBF[5] -= v3_0[i];

    // res[6]
    result_0[i].high.xBF[0] = vST8_0[i] + vST10_0[i];
    result_0[i].high.xBF[0] -= v1_0[i];
    result_0[i].high.xBF[0] -= result_0[i].high.xBF[4];
    result_0[i].high.xBF[0] -= v2_0[i];
    result_0[i].high.xBF[0] -= v4_0[i];
    result_0[i].high.xBF[0] += v3_0[i];

    // res[7]
    result_0[i].high.xBF[1] = vST11_0[i] + vST12_0[i];
    result_0[i].high.xBF[1] -= v2_0[i];
    result_0[i].high.xBF[1] -= result_0[i].high.xBF[4];
    result_0[i].high.xBF[1] -= v3_0[i];
    result_0[i].high.xBF[1] -= v4_0[i];

    // res[8]
    result_0[i].high.xBF[2] = vST13_0[i] - v3_0[i];
    result_0[i].high.xBF[2] -= result_0[i].high.xBF[4];
    result_0[i].high.xBF[2] += v4_0[i];

    // res[9]
    result_0[i].high.xBF[3] = vST14_0[i] - v4_0[i];
    result_0[i].high.xBF[3] -= result_0[i].high.xBF[4];



    // a1b1
    // res[0] = a[0]b[0]
    result_1[i].low.xBF[0] = v0_1[i];

    //res[10] = a[5]b[5]
    result_1[i].high.xBF[4] = v5_1[i];

    // res[1]
    result_1[i].low.xBF[1] = vST0_1[i] - result_1[i].low.xBF[0];
    result_1[i].low.xBF[1] -= v1_1[i];

    // res[2]
    result_1[i].low.xBF[2] = vST1_1[i] - result_1[i].low.xBF[0];
    result_1[i].low.xBF[2] -= v2_1[i];
    result_1[i].low.xBF[2] += v1_1[i];

    // res[3]
    result_1[i].low.xBF[3] = vST2_1[i] + vST5_1[i];
    result_1[i].low.xBF[3] -= result_1[i].low.xBF[0];
    result_1[i].low.xBF[3] -= v3_1[i];
    result_1[i].low.xBF[3] -= v1_1[i];
    result_1[i].low.xBF[3] -= v2_1[i];

    // res[4]
    result_1[i].low.xBF[4] = vST3_1[i] + vST6_1[i];
    result_1[i].low.xBF[4] -= result_1[i].low.xBF[0];
    result_1[i].low.xBF[4] -= v4_1[i];
    result_1[i].low.xBF[4] -= v1_1[i];
    result_1[i].low.xBF[4] -= v3_1[i];
    result_1[i].low.xBF[4] += v2_1[i];

    // res[5]
    result_1[i].low.xBF[5] = vST4_1[i] + vST7_1[i];
    result_1[i].low.xBF[5] += vST9_1[i];
    result_1[i].low.xBF[5] -= result_1[i].low.xBF[0];
    result_1[i].low.xBF[5] -= result_1[i].high.xBF[4];
    result_1[i].low.xBF[5] -= v1_1[i];
    result_1[i].low.xBF[5] -= v4_1[i];
    result_1[i].low.xBF[5] -= v2_1[i];
    result_1[i].low.xBF[5] -= v3_1[i];

    // res[6]
    result_1[i].high.xBF[0] = vST8_1[i] + vST10_1[i];
    result_1[i].high.xBF[0] -= v1_1[i];
    result_1[i].high.xBF[0] -= result_1[i].high.xBF[4];
    result_1[i].high.xBF[0] -= v2_1[i];
    result_1[i].high.xBF[0] -= v4_1[i];
    result_1[i].high.xBF[0] += v3_1[i];

    // res[7]
    result_1[i].high.xBF[1] = vST11_1[i] + vST12_1[i];
    result_1[i].high.xBF[1] -= v2_1[i];
    result_1[i].high.xBF[1] -= result_1[i].high.xBF[4];
    result_1[i].high.xBF[1] -= v3_1[i];
    result_1[i].high.xBF[1] -= v4_1[i];

    // res[8]
    result_1[i].high.xBF[2] = vST13_1[i] - v3_1[i];
    result_1[i].high.xBF[2] -= result_1[i].high.xBF[4];
    result_1[i].high.xBF[2] += v4_1[i];

    // res[9]
    result_1[i].high.xBF[3] = vST14_1[i] - v4_1[i];
    result_1[i].high.xBF[3] -= result_1[i].high.xBF[4];



    // (a0+a1)(a1+b1)
    // res[0] = a[0]b[0]
    result_2[i].low.xBF[0] = v0_2[i];

    //res[10] = a[5]b[5]
    result_2[i].high.xBF[4] = v5_2[i];

    // res[1]
    result_2[i].low.xBF[1] = vST0_2[i] - result_2[i].low.xBF[0];
    result_2[i].low.xBF[1] -= v1_2[i];

    // res[2]
    result_2[i].low.xBF[2] = vST1_2[i] - result_2[i].low.xBF[0];
    result_2[i].low.xBF[2] -= v2_2[i];
    result_2[i].low.xBF[2] += v1_2[i];

    // res[3]
    result_2[i].low.xBF[3] = vST2_2[i] + vST5_2[i];
    result_2[i].low.xBF[3] -= result_2[i].low.xBF[0];
    result_2[i].low.xBF[3] -= v3_2[i];
    result_2[i].low.xBF[3] -= v1_2[i];
    result_2[i].low.xBF[3] -= v2_2[i];

    // res[4]
    result_2[i].low.xBF[4] = vST3_2[i] + vST6_2[i];
    result_2[i].low.xBF[4] -= result_2[i].low.xBF[0];
    result_2[i].low.xBF[4] -= v4_2[i];
    result_2[i].low.xBF[4] -= v1_2[i];
    result_2[i].low.xBF[4] -= v3_2[i];
    result_2[i].low.xBF[4] += v2_2[i];

    // res[5]
    result_2[i].low.xBF[5] = vST4_2[i] + vST7_2[i];
    result_2[i].low.xBF[5] += vST9_2[i];
    result_2[i].low.xBF[5] -= result_2[i].low.xBF[0];
    result_2[i].low.xBF[5] -= result_2[i].high.xBF[4];
    result_2[i].low.xBF[5] -= v1_2[i];
    result_2[i].low.xBF[5] -= v4_2[i];
    result_2[i].low.xBF[5] -= v2_2[i];
    result_2[i].low.xBF[5] -= v3_2[i];

    // res[6]
    result_2[i].high.xBF[0] = vST8_2[i] + vST10_2[i];
    result_2[i].high.xBF[0] -= v1_2[i];
    result_2[i].high.xBF[0] -= result_2[i].high.xBF[4];
    result_2[i].high.xBF[0] -= v2_2[i];
    result_2[i].high.xBF[0] -= v4_2[i];
    result_2[i].high.xBF[0] += v3_2[i];

    // res[7]
    result_2[i].high.xBF[1] = vST11_2[i] + vST12_2[i];
    result_2[i].high.xBF[1] -= v2_2[i];
    result_2[i].high.xBF[1] -= result_2[i].high.xBF[4];
    result_2[i].high.xBF[1] -= v3_2[i];
    result_2[i].high.xBF[1] -= v4_2[i];

    // res[8]
    result_2[i].high.xBF[2] = vST13_2[i] - v3_2[i];
    result_2[i].high.xBF[2] -= result_2[i].high.xBF[4];
    result_2[i].high.xBF[2] += v4_2[i];

    // res[9]
    result_2[i].high.xBF[3] = vST14_2[i] - v4_2[i];
    result_2[i].high.xBF[3] -= result_2[i].high.xBF[4];



    result[i].low.xF6[0].reduction(result_0[i]);
    result[i].high.xF6[0].reduction(result_1[i]);
    result[i].low.xF6[1].reduction(result_2[i]);
  }



  // Karatsuba on ext6
  for (unsigned int i = 0; i < number; ++i) {
    result[i].low.xF6[1] -= result[i].low.xF6[0];
    result[i].low.xF6[1] -= result[i].high.xF6[0];

    c[i].reduction(result[i]);
  }



  delete[] result;
  delete[] result_0;
  delete[] result_1;
  delete[] result_2;

  delete[] a0_0;
  delete[] a1_0;
  delete[] a2_0;
  delete[] a3_0;
  delete[] a4_0;
  delete[] a5_0;

  delete[] a0_1;
  delete[] a1_1;
  delete[] a2_1;
  delete[] a3_1;
  delete[] a4_1;
  delete[] a5_1;

  delete[] a0_2;
  delete[] a1_2;
  delete[] a2_2;
  delete[] a3_2;
  delete[] a4_2;
  delete[] a5_2;

  delete[] b0_0;
  delete[] b1_0;
  delete[] b2_0;
  delete[] b3_0;
  delete[] b4_0;
  delete[] b5_0;

  delete[] b0_1;
  delete[] b1_1;
  delete[] b2_1;
  delete[] b3_1;
  delete[] b4_1;
  delete[] b5_1;

  delete[] b0_2;
  delete[] b1_2;
  delete[] b2_2;
  delete[] b3_2;
  delete[] b4_2;
  delete[] b5_2;

  delete[] v0_0;
  delete[] v1_0;
  delete[] v2_0;
  delete[] v3_0;
  delete[] v4_0;
  delete[] v5_0;

  delete[] v0_1;
  delete[] v1_1;
  delete[] v2_1;
  delete[] v3_1;
  delete[] v4_1;
  delete[] v5_1;

  delete[] v0_2;
  delete[] v1_2;
  delete[] v2_2;
  delete[] v3_2;
  delete[] v4_2;
  delete[] v5_2;

  delete[] vST0_0;
  delete[] vST1_0;
  delete[] vST2_0;
  delete[] vST3_0;
  delete[] vST4_0;
  delete[] vST5_0;
  delete[] vST6_0;
  delete[] vST7_0;
  delete[] vST8_0;
  delete[] vST9_0;
  delete[] vST10_0;
  delete[] vST11_0;
  delete[] vST12_0;
  delete[] vST13_0;
  delete[] vST14_0;

  delete[] vST0_1;
  delete[] vST1_1;
  delete[] vST2_1;
  delete[] vST3_1;
  delete[] vST4_1;
  delete[] vST5_1;
  delete[] vST6_1;
  delete[] vST7_1;
  delete[] vST8_1;
  delete[] vST9_1;
  delete[] vST10_1;
  delete[] vST11_1;
  delete[] vST12_1;
  delete[] vST13_1;
  delete[] vST14_1;

  delete[] vST0_2;
  delete[] vST1_2;
  delete[] vST2_2;
  delete[] vST3_2;
  delete[] vST4_2;
  delete[] vST5_2;
  delete[] vST6_2;
  delete[] vST7_2;
  delete[] vST8_2;
  delete[] vST9_2;
  delete[] vST10_2;
  delete[] vST11_2;
  delete[] vST12_2;
  delete[] vST13_2;
  delete[] vST14_2;

  cudaFree(a0_0dev);
  cudaFree(a1_0dev);
  cudaFree(a2_0dev);
  cudaFree(a3_0dev);
  cudaFree(a4_0dev);
  cudaFree(a5_0dev);

  cudaFree(a0_1dev);
  cudaFree(a1_1dev);
  cudaFree(a2_1dev);
  cudaFree(a3_1dev);
  cudaFree(a4_1dev);
  cudaFree(a5_1dev);

  cudaFree(a0_2dev);
  cudaFree(a1_2dev);
  cudaFree(a2_2dev);
  cudaFree(a3_2dev);
  cudaFree(a4_2dev);
  cudaFree(a5_2dev);

  cudaFree(b0_0dev);
  cudaFree(b1_0dev);
  cudaFree(b2_0dev);
  cudaFree(b3_0dev);
  cudaFree(b4_0dev);
  cudaFree(b5_0dev);

  cudaFree(b0_1dev);
  cudaFree(b1_1dev);
  cudaFree(b2_1dev);
  cudaFree(b3_1dev);
  cudaFree(b4_1dev);
  cudaFree(b5_1dev);

  cudaFree(b0_2dev);
  cudaFree(b1_2dev);
  cudaFree(b2_2dev);
  cudaFree(b3_2dev);
  cudaFree(b4_2dev);
  cudaFree(b5_2dev);

  cudaFree(v0_0dev);
  cudaFree(v1_0dev);
  cudaFree(v2_0dev);
  cudaFree(v3_0dev);
  cudaFree(v4_0dev);
  cudaFree(v5_0dev);

  cudaFree(v0_1dev);
  cudaFree(v1_1dev);
  cudaFree(v2_1dev);
  cudaFree(v3_1dev);
  cudaFree(v4_1dev);
  cudaFree(v5_1dev);

  cudaFree(v0_2dev);
  cudaFree(v1_2dev);
  cudaFree(v2_2dev);
  cudaFree(v3_2dev);
  cudaFree(v4_2dev);
  cudaFree(v5_2dev);

  cudaFree(vST0_0dev);
  cudaFree(vST1_0dev);
  cudaFree(vST2_0dev);
  cudaFree(vST3_0dev);
  cudaFree(vST4_0dev);
  cudaFree(vST5_0dev);
  cudaFree(vST6_0dev);
  cudaFree(vST7_0dev);
  cudaFree(vST8_0dev);
  cudaFree(vST9_0dev);
  cudaFree(vST10_0dev);
  cudaFree(vST11_0dev);
  cudaFree(vST12_0dev);
  cudaFree(vST13_0dev);
  cudaFree(vST14_0dev);

  cudaFree(vST0_1dev);
  cudaFree(vST1_1dev);
  cudaFree(vST2_1dev);
  cudaFree(vST3_1dev);
  cudaFree(vST4_1dev);
  cudaFree(vST5_1dev);
  cudaFree(vST6_1dev);
  cudaFree(vST7_1dev);
  cudaFree(vST8_1dev);
  cudaFree(vST9_1dev);
  cudaFree(vST10_1dev);
  cudaFree(vST11_1dev);
  cudaFree(vST12_1dev);
  cudaFree(vST13_1dev);
  cudaFree(vST14_1dev);

  cudaFree(vST0_2dev);
  cudaFree(vST1_2dev);
  cudaFree(vST2_2dev);
  cudaFree(vST3_2dev);
  cudaFree(vST4_2dev);
  cudaFree(vST5_2dev);
  cudaFree(vST6_2dev);
  cudaFree(vST7_2dev);
  cudaFree(vST8_2dev);
  cudaFree(vST9_2dev);
  cudaFree(vST10_2dev);
  cudaFree(vST11_2dev);
  cudaFree(vST12_2dev);
  cudaFree(vST13_2dev);
  cudaFree(vST14_2dev);



#else

  ExtField_Double< ExtField6_2<F2_487_u64> > *result = new ExtField_Double< ExtField6_2<F2_487_u64> >[number];
  ExtField6<F2_487_u64> *a0 = new ExtField6<F2_487_u64>[number];
  ExtField6<F2_487_u64> *a1 = new ExtField6<F2_487_u64>[number];
  ExtField6<F2_487_u64> *b0 = new ExtField6<F2_487_u64>[number];
  ExtField6<F2_487_u64> *b1 = new ExtField6<F2_487_u64>[number];
  ExtField6<F2_487_u64> *aST = new ExtField6<F2_487_u64>[number];
  ExtField6<F2_487_u64> *bST = new ExtField6<F2_487_u64>[number];
  ExtField6<F2_487_u64> *tmp6 = new ExtField6<F2_487_u64>[number];

  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xF6[0];
    a1[i] = a[i].xF6[1];
    b0[i] = b[i].xF6[0];
    b1[i] = b[i].xF6[1];
  }

  for (unsigned int i = 0; i < number; ++i) {
    aST[i] = a[i].xF6[0] + a[i].xF6[1];
    bST[i] = b[i].xF6[0] + b[i].xF6[1];
  }

  mulCombL2RwithWindow6_multipleP<wSize>(tmp6, a0, b0, number);
  for (unsigned int i = 0; i < number; ++i) result[i].low.xF6[0] = tmp6[i];

  mulCombL2RwithWindow6_multipleP<wSize>(tmp6, a1, b1, number);
  for (unsigned int i = 0; i < number; ++i) result[i].high.xF6[0] = tmp6[i];

  mulCombL2RwithWindow6_multipleP<wSize>(tmp6, aST, bST, number);
  for (unsigned int i = 0; i < number; ++i) result[i].low.xF6[1] = tmp6[i];

  for (unsigned int i = 0; i < number; ++i) {
    result[i].low.xF6[1] -= result[i].low.xF6[0];
    result[i].low.xF6[1] -= result[i].high.xF6[0];

    c[i].reduction(result[i]);
  }

  delete[] result;
  delete[] a0;
  delete[] a1;
  delete[] b0;
  delete[] b1;
  delete[] aST;
  delete[] bST;
  delete[] tmp6;
#endif
}

template void mulCombL2RwithWindow6_2_multipleP<3>(ExtField6_2<F2_487_u64> *c, const ExtField6_2<F2_487_u64> *a, const ExtField6_2<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow6_2_multipleP<4>(ExtField6_2<F2_487_u64> *c, const ExtField6_2<F2_487_u64> *a, const ExtField6_2<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow6_2_multipleP<5>(ExtField6_2<F2_487_u64> *c, const ExtField6_2<F2_487_u64> *a, const ExtField6_2<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow6_2_multipleP<6>(ExtField6_2<F2_487_u64> *c, const ExtField6_2<F2_487_u64> *a, const ExtField6_2<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow6_2_multipleP<7>(ExtField6_2<F2_487_u64> *c, const ExtField6_2<F2_487_u64> *a, const ExtField6_2<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow6_2_multipleP<8>(ExtField6_2<F2_487_u64> *c, const ExtField6_2<F2_487_u64> *a, const ExtField6_2<F2_487_u64> *b, const int number);
//template void mulCombL2RwithWindow6_2_multipleP<9>(ExtField6_2<F2_487_u64> *c, const ExtField6_2<F2_487_u64> *a, const ExtField6_2<F2_487_u64> *b, const int number);

void square6_2_multipleP(ExtField6_2<F2_487_u64> *c, const ExtField6_2<F2_487_u64> *a, const int number) {

  ExtField6<F2_487_u64> *a0 = new ExtField6<F2_487_u64>[number];
  ExtField6<F2_487_u64> *a1 = new ExtField6<F2_487_u64>[number];
  ExtField6<F2_487_u64> *tmp = new ExtField6<F2_487_u64>[number];

  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xF6[0];
    a1[i] = a[i].xF6[1];
  }

  // c_1 = a_1^2
  square6_multipleP(tmp, a1, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xF6[1] = tmp[i];

  square6_multipleP(tmp, a0, number);

  // c_0 = a_1^2(w^5 + w^3)
  for (unsigned int i = 0; i < number; ++i) ExtField6<F2_487_u64>::mulConstTerm6(c[i].xF6[0], c[i].xF6[1]);

  // c_0 = a_1^2(w^5 + w^3) + a_0^2
  for (unsigned int i = 0; i < number; ++i) c[i].xF6[0] += tmp[i];

  delete[] a0;
  delete[] a1;
  delete[] tmp;
}

template<int wSize>
void alpha_beta6_2_withWindow_multipleP(ExtField6_2<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b, const int number) {

  for (unsigned int i = 0; i < number; ++i) {
    // f + g
    c[i].xF6[1] = a[i] + b[i];
    // (fix) c_1 = f + g + 1
    c[i].xF6[1].xBF[0].plusOne();
  }

  // c_0 := (a0,a1,a2)(b0,b1,b2)
  // Karatsuba 3
  F2_487_u64 *a0 = new F2_487_u64[number];
  F2_487_u64 *a1 = new F2_487_u64[number];
  F2_487_u64 *a2 = new F2_487_u64[number];
  F2_487_u64 *a4 = new F2_487_u64[number];


  F2_487_u64 *b0 = new F2_487_u64[number];
  F2_487_u64 *b1 = new F2_487_u64[number];
  F2_487_u64 *b2 = new F2_487_u64[number];
  F2_487_u64 *b4 = new F2_487_u64[number];

  F2_487_u64 *v0 = new F2_487_u64[number];
  F2_487_u64 *v1 = new F2_487_u64[number];
  F2_487_u64 *v2 = new F2_487_u64[number];

  F2_487_u64 *vST0 = new F2_487_u64[number];
  F2_487_u64 *vST1 = new F2_487_u64[number];
  F2_487_u64 *vST2 = new F2_487_u64[number];

  // copy to BF array
  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xBF[0];
    a1[i] = a[i].xBF[1];
    a2[i] = a[i].xBF[2];
    a4[i] = a[i].xBF[4];

    b0[i] = b[i].xBF[0];
    b1[i] = b[i].xBF[1];
    b2[i] = b[i].xBF[2];
    b4[i] = b[i].xBF[4];
  }

  F2_487_u64 *a0_dev, *a1_dev, *a2_dev;
  F2_487_u64 *b0_dev, *b1_dev, *b2_dev;
  F2_487_u64 *v0_dev, *v1_dev, *v2_dev;
  F2_487_u64 *vST0_dev, *vST1_dev, *vST2_dev;

  cudaMalloc((void**)&a0_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a1_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a2_dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&b0_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b1_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b2_dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&v0_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v1_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v2_dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&vST0_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST1_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST2_dev, sizeof(F2_487_u64)*number);

  cudaMemcpy(a0_dev, a0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a1_dev, a1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a2_dev, a2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);

  cudaMemcpy(b0_dev, b0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b1_dev, b1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b2_dev, b2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);

  dim3 blocks(number,1);
  dim3 threads(F2_487_u64::nUint,1);

  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v0_dev, a0_dev, b0_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v1_dev, a1_dev, b1_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v2_dev, a2_dev, b2_dev);

  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST0_dev, a0_dev, a1_dev, b0_dev, b1_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST1_dev, a0_dev, a2_dev, b0_dev, b2_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST2_dev, a1_dev, a2_dev, b1_dev, b2_dev);

  cudaMemcpy(v0, v0_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v1, v1_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v2, v2_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);

  cudaMemcpy(vST0, vST0_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST1, vST1_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST2, vST2_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);

  for (unsigned int i = 0; i < number; ++i) {
    c[i].xF6[0].xBF[0] = v0[i];
    c[i].xF6[0].xBF[4] = v2[i];

    // c_0_1
    c[i].xF6[0].xBF[1] = vST0[i] - c[i].xF6[0].xBF[0];
    c[i].xF6[0].xBF[1] -= v1[i];

    // c_0_2
    c[i].xF6[0].xBF[2] = vST1[i] - c[i].xF6[0].xBF[0];
    c[i].xF6[0].xBF[2] -= c[i].xF6[0].xBF[4];
    c[i].xF6[0].xBF[2] += v1[i];

    // c_0_3
    c[i].xF6[0].xBF[3] = vST2[i] - v1[i];
    c[i].xF6[0].xBF[3] -= c[i].xF6[0].xBF[4];
  }

  //(a0,a1,a2)(b0,b1,b2)  + (a1b4 + a2b4 + a4b1 + a4b2)w^5 + (a0b4 + a4b0)w^4 + (a2b4 + a4b2)w^3 + (a2b4 + a4b2)w^2 + (a4b4)w + a2b4 + a4b2 + a4b4
  // (fix) v1 = a4b4
  mulCombL2RwithWindow_multipleP<F2_487_u64, wSize>(v1, a4, b4, number);

  // (fix) vST[0] = a2b4 + a4b2
  mulCombL2RwithWindow_multipleP<F2_487_u64, wSize>(vST0, a2, b4, number);
  mulCombL2RwithWindow_multipleP<F2_487_u64, wSize>(vST1, a4, b2, number);

  for (unsigned int i = 0; i < number; ++i) vST0[i] += vST1[i];

  // c_0_5 = a1b4（初代入）
  mulCombL2RwithWindow_multipleP<F2_487_u64, wSize>(vST2, a1, b4, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xF6[0].xBF[5] = vST2[i];

  mulCombL2RwithWindow_multipleP<F2_487_u64, wSize>(vST1, a4, b1, number);

  for (unsigned int i = 0; i < number; ++i) {

    // c_0_5 = a1b4 + a4b1
    c[i].xF6[0].xBF[5] += vST1[i];
    // (fix) c_0_5 = a1b4 + a4b1 + a2b4 + a4b2
    c[i].xF6[0].xBF[5] += vST0[i];

    // (fix) c_0_3 += a2b4 + a4b2
    c[i].xF6[0].xBF[3] += vST0[i];

    // (fix) c_0_2 += a2b4 + a4b2
    c[i].xF6[0].xBF[2] += vST0[i];

    // (fix) c_0_1 += a4b4
    c[i].xF6[0].xBF[1] += v1[i];

    // c_0_0 += a2b4 + a4b2
    c[i].xF6[0].xBF[0] += vST0[i];
    // (fix) c_0_0 += a4b4
    c[i].xF6[0].xBF[0] += v1[i];
  }

  mulCombL2RwithWindow_multipleP<F2_487_u64, wSize>(vST1, a0, b4, number);
  // c_0_4 += a0b4
  for (unsigned int i = 0; i < number; ++i) c[i].xF6[0].xBF[4] += vST1[i];

  mulCombL2RwithWindow_multipleP<F2_487_u64, wSize>(vST1, a4, b0, number);
  // (fix) c_0_4 += a4b0
  for (unsigned int i = 0; i < number; ++i) c[i].xF6[0].xBF[4] += vST1[i];

  for (unsigned int i = 0; i < number; ++i) {
    // c_0 + w^5 + w^3
    c[i].xF6[0].xBF[3].plusOne();
    c[i].xF6[0].xBF[5].plusOne();
  }
  delete[] a0;
  delete[] a1;
  delete[] a2;
  delete[] a4;

  delete[] b0;
  delete[] b1;
  delete[] b2;
  delete[] b4;

  delete[] v0;
  delete[] v1;
  delete[] v2;

  delete[] vST0;
  delete[] vST1;
  delete[] vST2;

  cudaFree(a0_dev);
  cudaFree(a1_dev);
  cudaFree(a2_dev);

  cudaFree(b0_dev);
  cudaFree(b1_dev);
  cudaFree(b2_dev);

  cudaFree(v0_dev);
  cudaFree(v1_dev);
  cudaFree(v2_dev);

  cudaFree(vST0_dev);
  cudaFree(vST1_dev);
  cudaFree(vST2_dev);
}
template void alpha_beta6_2_withWindow_multipleP<3>(ExtField6_2<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b, const int number);
template void alpha_beta6_2_withWindow_multipleP<4>(ExtField6_2<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b, const int number);
template void alpha_beta6_2_withWindow_multipleP<5>(ExtField6_2<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b, const int number);
template void alpha_beta6_2_withWindow_multipleP<6>(ExtField6_2<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b, const int number);
template void alpha_beta6_2_withWindow_multipleP<7>(ExtField6_2<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b, const int number);
template void alpha_beta6_2_withWindow_multipleP<8>(ExtField6_2<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b, const int number);
//template void alpha_beta6_2_withWindow_multipleP<9>(ExtField6_2<F2_487_u64> *c, const ExtField6<F2_487_u64> *a, const ExtField6<F2_487_u64> *b, const int number);



//Karatsuba 3
/* 一つのkernel函数で各aST, bSTをdeviceで計算した後乗算vSTを行う */
template<int wSize>
void mulCombL2RwithWindow3_multipleP(ExtField3<F2_487_u64> *c, const ExtField3<F2_487_u64> *a, const ExtField3<F2_487_u64> *b, const int number) {

  ExtField_Double< ExtField3<F2_487_u64> > *result = new ExtField_Double< ExtField3<F2_487_u64> >[number];

  F2_487_u64 *a0 = new F2_487_u64[number];
  F2_487_u64 *a1 = new F2_487_u64[number];
  F2_487_u64 *a2 = new F2_487_u64[number];

  F2_487_u64 *b0 = new F2_487_u64[number];
  F2_487_u64 *b1 = new F2_487_u64[number];
  F2_487_u64 *b2 = new F2_487_u64[number];

  F2_487_u64 *v0 = new F2_487_u64[number];
  F2_487_u64 *v1 = new F2_487_u64[number];
  F2_487_u64 *v2 = new F2_487_u64[number];

  F2_487_u64 *vST0 = new F2_487_u64[number];
  F2_487_u64 *vST1 = new F2_487_u64[number];
  F2_487_u64 *vST2 = new F2_487_u64[number];

  // copy to BF array
  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xBF[0];
    a1[i] = a[i].xBF[1];
    a2[i] = a[i].xBF[2];

    b0[i] = b[i].xBF[0];
    b1[i] = b[i].xBF[1];
    b2[i] = b[i].xBF[2];
  }



  F2_487_u64 *a0_dev, *a1_dev, *a2_dev;
  F2_487_u64 *b0_dev, *b1_dev, *b2_dev;
  F2_487_u64 *v0_dev, *v1_dev, *v2_dev;
  F2_487_u64 *vST0_dev, *vST1_dev, *vST2_dev;

  cudaMalloc((void**)&a0_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a1_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&a2_dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&b0_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b1_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&b2_dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&v0_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v1_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&v2_dev, sizeof(F2_487_u64)*number);

  cudaMalloc((void**)&vST0_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST1_dev, sizeof(F2_487_u64)*number);
  cudaMalloc((void**)&vST2_dev, sizeof(F2_487_u64)*number);

  cudaMemcpy(a0_dev, a0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a1_dev, a1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a2_dev, a2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);

  cudaMemcpy(b0_dev, b0, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b1_dev, b1, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b2_dev, b2, sizeof(F2_487_u64)*number, cudaMemcpyHostToDevice);

  dim3 blocks(number,1);
  dim3 threads(F2_487_u64::nUint,1);

  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v0_dev, a0_dev, b0_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v1_dev, a1_dev, b1_dev);
  mulCombL2RwithWindow_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(v2_dev, a2_dev, b2_dev);

  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST0_dev, a0_dev, a1_dev, b0_dev, b1_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST1_dev, a0_dev, a2_dev, b0_dev, b2_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_487_u64, wSize><<< blocks, threads >>>(vST2_dev, a1_dev, a2_dev, b1_dev, b2_dev);

  cudaMemcpy(v0, v0_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v1, v1_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v2, v2_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);

  cudaMemcpy(vST0, vST0_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST1, vST1_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST2, vST2_dev, sizeof(F2_487_u64)*number, cudaMemcpyDeviceToHost);



  for (unsigned int i = 0; i < number; ++i) {
    // res[0] = a[0]b[0]
    result[i].low.xBF[0] = v0[i];

    //res[4] = a[2]b[2]
    result[i].high.xBF[1] = v2[i];

    // res[1]
    result[i].low.xBF[1] = vST0[i] - result[i].low.xBF[0];
    result[i].low.xBF[1] -= v1[i];

    // res[2]
    result[i].low.xBF[2] = vST1[i] - result[i].low.xBF[0];
    result[i].low.xBF[2] -= result[i].high.xBF[1];
    result[i].low.xBF[2] += v1[i];

    // res[3]
    result[i].high.xBF[0] = vST2[i] - v1[i];
    result[i].high.xBF[0] -= result[i].high.xBF[1];

    c[i].reduction(result[i]);
  }

  delete[] result;

  delete[] a0;
  delete[] a1;
  delete[] a2;

  delete[] b0;
  delete[] b1;
  delete[] b2;

  delete[] v0;
  delete[] v1;
  delete[] v2;

  delete[] vST0;
  delete[] vST1;
  delete[] vST2;

  cudaFree(a0_dev);
  cudaFree(a1_dev);
  cudaFree(a2_dev);

  cudaFree(b0_dev);
  cudaFree(b1_dev);
  cudaFree(b2_dev);

  cudaFree(v0_dev);
  cudaFree(v1_dev);
  cudaFree(v2_dev);

  cudaFree(vST0_dev);
  cudaFree(vST1_dev);
  cudaFree(vST2_dev);
}
template void mulCombL2RwithWindow3_multipleP<3>(ExtField3<F2_487_u64> *c, const ExtField3<F2_487_u64> *a, const ExtField3<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow3_multipleP<4>(ExtField3<F2_487_u64> *c, const ExtField3<F2_487_u64> *a, const ExtField3<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow3_multipleP<5>(ExtField3<F2_487_u64> *c, const ExtField3<F2_487_u64> *a, const ExtField3<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow3_multipleP<6>(ExtField3<F2_487_u64> *c, const ExtField3<F2_487_u64> *a, const ExtField3<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow3_multipleP<7>(ExtField3<F2_487_u64> *c, const ExtField3<F2_487_u64> *a, const ExtField3<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow3_multipleP<8>(ExtField3<F2_487_u64> *c, const ExtField3<F2_487_u64> *a, const ExtField3<F2_487_u64> *b, const int number);
//template void mulCombL2RwithWindow3_multipleP<9>(ExtField3<F2_487_u64> *c, const ExtField3<F2_487_u64> *a, const ExtField3<F2_487_u64> *b, const int number);

void square3_multipleP(ExtField3<F2_487_u64> *c, const ExtField3<F2_487_u64> *a, const int number) {

  F2_487_u64 *a0 = new F2_487_u64[number];
  F2_487_u64 *a1 = new F2_487_u64[number];
  F2_487_u64 *a2 = new F2_487_u64[number];
  F2_487_u64 *tmp = new F2_487_u64[number];

  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xBF[0];
    a1[i] = a[i].xBF[1];
    a2[i] = a[i].xBF[2];
  }

  // c_2 = a_1^2
  square_multipleP(tmp, a1, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[2] = tmp[i];

  // c_1 = a_2^2
  square_multipleP(tmp, a2, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[1] += tmp[i];
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[2] += tmp[i];

  // (fix) c_0 = a_0^2
  square_multipleP(tmp, a0, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[0] = tmp[i];

  delete[] a0;
  delete[] a1;
  delete[] a2;
  delete[] tmp;
}

template<int wSize>
void mulCombL2RwithWindow3_2_multipleP(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const int number) {

  ExtField_Double< ExtField3_2<F2_487_u64> > *result = new ExtField_Double< ExtField3_2<F2_487_u64> >[number];
  ExtField3<F2_487_u64> *a0 = new ExtField3<F2_487_u64>[number];
  ExtField3<F2_487_u64> *a1 = new ExtField3<F2_487_u64>[number];
  ExtField3<F2_487_u64> *b0 = new ExtField3<F2_487_u64>[number];
  ExtField3<F2_487_u64> *b1 = new ExtField3<F2_487_u64>[number];
  ExtField3<F2_487_u64> *aST = new ExtField3<F2_487_u64>[number];
  ExtField3<F2_487_u64> *bST = new ExtField3<F2_487_u64>[number];
  ExtField3<F2_487_u64> *tmp3 = new ExtField3<F2_487_u64>[number];

  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xF3[0];
    a1[i] = a[i].xF3[1];
    b0[i] = b[i].xF3[0];
    b1[i] = b[i].xF3[1];
  }

  for (unsigned int i = 0; i < number; ++i) {
    aST[i] = a[i].xF3[0] + a[i].xF3[1];
    bST[i] = b[i].xF3[0] + b[i].xF3[1];
  }

  mulCombL2RwithWindow3_multipleP<wSize>(tmp3, a0, b0, number);
  for (unsigned int i = 0; i < number; ++i) result[i].low.xF3[0] = tmp3[i];

  mulCombL2RwithWindow3_multipleP<wSize>(tmp3, a1, b1, number);
  for (unsigned int i = 0; i < number; ++i) result[i].high.xF3[0] = tmp3[i];

  mulCombL2RwithWindow3_multipleP<wSize>(tmp3, aST, bST, number);
  for (unsigned int i = 0; i < number; ++i) result[i].low.xF3[1] = tmp3[i];

  for (unsigned int i = 0; i < number; ++i) {
    result[i].low.xF3[1] -= result[i].low.xF3[0];
    result[i].low.xF3[1] -= result[i].high.xF3[0];

    c[i].reduction(result[i]);
  }

  delete[] result;
  delete[] a0;
  delete[] a1;
  delete[] b0;
  delete[] b1;
  delete[] aST;
  delete[] bST;
  delete[] tmp3;
}
template void mulCombL2RwithWindow3_2_multipleP<3>(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_multipleP<4>(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_multipleP<5>(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_multipleP<6>(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_multipleP<7>(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_multipleP<8>(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const int number);
//template void mulCombL2RwithWindow3_2_multipleP<9>(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const int number);

template<int wSize>
void mul_sp_CombL2RwithWindow3_2_multipleP(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const int number) {

  ExtField3<F2_487_u64> *a0 = new ExtField3<F2_487_u64>[number];
  ExtField3<F2_487_u64> *a1 = new ExtField3<F2_487_u64>[number];
  ExtField3<F2_487_u64> *b0 = new ExtField3<F2_487_u64>[number];
  ExtField3<F2_487_u64> *tmp3 = new ExtField3<F2_487_u64>[number];

  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xF3[0];
    a1[i] = a[i].xF3[1];
    b0[i] = b[i].xF3[0];
  }

  mulCombL2RwithWindow3_multipleP<wSize>(tmp3, a0, b0, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xF3[0] = tmp3[i];

  mulCombL2RwithWindow3_multipleP<wSize>(tmp3, a1, b0, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xF3[1] = tmp3[i];


  delete[] a0;
  delete[] a1;
  delete[] b0;
  delete[] tmp3;
}
template void mul_sp_CombL2RwithWindow3_2_multipleP<3>(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const int number);
template void mul_sp_CombL2RwithWindow3_2_multipleP<4>(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const int number);
template void mul_sp_CombL2RwithWindow3_2_multipleP<5>(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const int number);
template void mul_sp_CombL2RwithWindow3_2_multipleP<6>(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const int number);
template void mul_sp_CombL2RwithWindow3_2_multipleP<7>(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const int number);
template void mul_sp_CombL2RwithWindow3_2_multipleP<8>(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const int number);
//template void mul_sp_CombL2RwithWindow3_2_multipleP<9>(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const int number);

void square3_2_multipleP(ExtField3_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const int number) {

  ExtField3<F2_487_u64> *a0 = new ExtField3<F2_487_u64>[number];
  ExtField3<F2_487_u64> *a1 = new ExtField3<F2_487_u64>[number];
  ExtField3<F2_487_u64> *tmp = new ExtField3<F2_487_u64>[number];

  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xF3[0];
    a1[i] = a[i].xF3[1];
  }

  square3_multipleP(tmp, a1, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xF3[1] = tmp[i];

  for (unsigned int i = 0; i < number; ++i) {
    ExtField3<F2_487_u64>::mulConstTerm3(c[i].xF3[0], c[i].xF3[1]);
    c[i].xF3[0] += c[i].xF3[1];
  }

  square3_multipleP(tmp, a0, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xF3[0] += tmp[i];

  delete[] a0;
  delete[] a1;
  delete[] tmp;
}

template<int wSize>
void mulCombL2RwithWindow3_2_2_multipleP(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const ExtField3_2_2<F2_487_u64> *b, const int number) {

  ExtField_Double< ExtField3_2_2<F2_487_u64> > *result = new ExtField_Double< ExtField3_2_2<F2_487_u64> >[number];
  ExtField3_2<F2_487_u64> *a0 = new ExtField3_2<F2_487_u64>[number];
  ExtField3_2<F2_487_u64> *a1 = new ExtField3_2<F2_487_u64>[number];
  ExtField3_2<F2_487_u64> *b0 = new ExtField3_2<F2_487_u64>[number];
  ExtField3_2<F2_487_u64> *b1 = new ExtField3_2<F2_487_u64>[number];
  ExtField3_2<F2_487_u64> *aST = new ExtField3_2<F2_487_u64>[number];
  ExtField3_2<F2_487_u64> *bST = new ExtField3_2<F2_487_u64>[number];
  ExtField3_2<F2_487_u64> *tmp3_2 = new ExtField3_2<F2_487_u64>[number];

  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xF3_2[0];
    a1[i] = a[i].xF3_2[1];
    b0[i] = b[i].xF3_2[0];
    b1[i] = b[i].xF3_2[1];
  }

  for (unsigned int i = 0; i < number; ++i) {
    aST[i] = a[i].xF3_2[0] + a[i].xF3_2[1];
    bST[i] = b[i].xF3_2[0] + b[i].xF3_2[1];
  }

  mulCombL2RwithWindow3_2_multipleP<wSize>(tmp3_2, a0, b0, number);
  for (unsigned int i = 0; i < number; ++i) result[i].low.xF3_2[0] = tmp3_2[i];

  mulCombL2RwithWindow3_2_multipleP<wSize>(tmp3_2, a1, b1, number);
  for (unsigned int i = 0; i < number; ++i) result[i].high.xF3_2[0] = tmp3_2[i];

  mulCombL2RwithWindow3_2_multipleP<wSize>(tmp3_2, aST, bST, number);
  for (unsigned int i = 0; i < number; ++i) result[i].low.xF3_2[1] = tmp3_2[i];

  for (unsigned int i = 0; i < number; ++i) {
    result[i].low.xF3_2[1] -= result[i].low.xF3_2[0];
    result[i].low.xF3_2[1] -= result[i].high.xF3_2[0];

    c[i].reduction(result[i]);
  }

  delete[] result;
  delete[] a0;
  delete[] a1;
  delete[] b0;
  delete[] b1;
  delete[] aST;
  delete[] bST;
  delete[] tmp3_2;
}
template void mulCombL2RwithWindow3_2_2_multipleP<3>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const ExtField3_2_2<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_2_multipleP<4>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const ExtField3_2_2<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_2_multipleP<5>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const ExtField3_2_2<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_2_multipleP<6>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const ExtField3_2_2<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_2_multipleP<7>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const ExtField3_2_2<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_2_multipleP<8>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const ExtField3_2_2<F2_487_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_2_multipleP<9>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const ExtField3_2_2<F2_487_u64> *b, const int number);

template<int wSize>
void mul_sp_CombL2RwithWindow3_2_2_multipleP(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const ExtField3_2_2<F2_487_u64> *b, const int number) {

  ExtField_Double< ExtField3_2_2<F2_487_u64> > *result = new ExtField_Double< ExtField3_2_2<F2_487_u64> >[number];
  ExtField3_2<F2_487_u64> *a0 = new ExtField3_2<F2_487_u64>[number];
  ExtField3_2<F2_487_u64> *a1 = new ExtField3_2<F2_487_u64>[number];
  ExtField3_2<F2_487_u64> *b0 = new ExtField3_2<F2_487_u64>[number];
  ExtField3_2<F2_487_u64> *b1 = new ExtField3_2<F2_487_u64>[number];
  ExtField3_2<F2_487_u64> *aST = new ExtField3_2<F2_487_u64>[number];
  ExtField3_2<F2_487_u64> *bST = new ExtField3_2<F2_487_u64>[number];
  ExtField3_2<F2_487_u64> *tmp3_2 = new ExtField3_2<F2_487_u64>[number];

  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xF3_2[0];
    a1[i] = a[i].xF3_2[1];
    b0[i] = b[i].xF3_2[0];
    b1[i] = b[i].xF3_2[1];
  }

  for (unsigned int i = 0; i < number; ++i) {
    aST[i] = a[i].xF3_2[0] + a[i].xF3_2[1];
    bST[i] = b[i].xF3_2[0] + b[i].xF3_2[1];
  }

  mulCombL2RwithWindow3_2_multipleP<wSize>(tmp3_2, a0, b0, number);
  for (unsigned int i = 0; i < number; ++i) result[i].low.xF3_2[0] = tmp3_2[i];

  mul_sp_CombL2RwithWindow3_2_multipleP<wSize>(tmp3_2, a1, b1, number);
  for (unsigned int i = 0; i < number; ++i) result[i].high.xF3_2[0] = tmp3_2[i];

  mulCombL2RwithWindow3_2_multipleP<wSize>(tmp3_2, aST, bST, number);
  for (unsigned int i = 0; i < number; ++i) result[i].low.xF3_2[1] = tmp3_2[i];

  for (unsigned int i = 0; i < number; ++i) {
    result[i].low.xF3_2[1] -= result[i].low.xF3_2[0];
    result[i].low.xF3_2[1] -= result[i].high.xF3_2[0];

    c[i].reduction(result[i]);
  }

  delete[] result;
  delete[] a0;
  delete[] a1;
  delete[] b0;
  delete[] b1;
  delete[] aST;
  delete[] bST;
  delete[] tmp3_2;
}
template void mul_sp_CombL2RwithWindow3_2_2_multipleP<3>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const ExtField3_2_2<F2_487_u64> *b, const int number);
template void mul_sp_CombL2RwithWindow3_2_2_multipleP<4>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const ExtField3_2_2<F2_487_u64> *b, const int number);
template void mul_sp_CombL2RwithWindow3_2_2_multipleP<5>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const ExtField3_2_2<F2_487_u64> *b, const int number);
template void mul_sp_CombL2RwithWindow3_2_2_multipleP<6>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const ExtField3_2_2<F2_487_u64> *b, const int number);
template void mul_sp_CombL2RwithWindow3_2_2_multipleP<7>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const ExtField3_2_2<F2_487_u64> *b, const int number);
template void mul_sp_CombL2RwithWindow3_2_2_multipleP<8>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const ExtField3_2_2<F2_487_u64> *b, const int number);
//template void mul_sp_CombL2RwithWindow3_2_2_multipleP<9>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const ExtField3_2_2<F2_487_u64> *b, const int number);

void square3_2_2_multipleP(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2_2<F2_487_u64> *a, const int number) {

  ExtField3_2<F2_487_u64> *a0 = new ExtField3_2<F2_487_u64>[number];
  ExtField3_2<F2_487_u64> *a1 = new ExtField3_2<F2_487_u64>[number];
  ExtField3_2<F2_487_u64> *tmp = new ExtField3_2<F2_487_u64>[number];

  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xF3_2[0];
    a1[i] = a[i].xF3_2[1];
  }

  square3_2_multipleP(tmp, a1, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xF3_2[1] = tmp[i];

  for (unsigned int i = 0; i < number; ++i) ExtField3_2<F2_487_u64>::mulConstTerm3_2(c[i].xF3_2[0], c[i].xF3_2[1]);

  square3_2_multipleP(tmp, a0, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xF3_2[0] += tmp[i];

  delete[] a0;
  delete[] a1;
  delete[] tmp;
}

template<int wSize>
void alpha_beta3_2_2_withWindow_multipleP(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const F2_487_u64 *d, const int number) {

  ExtField3<F2_487_u64> *fg0 = new ExtField3<F2_487_u64>[number];
  ExtField3<F2_487_u64> *a0 = new ExtField3<F2_487_u64>[number];
  ExtField3<F2_487_u64> *b0 = new ExtField3<F2_487_u64>[number];
  ExtField3<F2_487_u64> *tmpF3 = new ExtField3<F2_487_u64>[number];

  F2_487_u64 *aF3_1_0 = new F2_487_u64[number];
  F2_487_u64 *tmpF3_0 = new F2_487_u64[number];
  F2_487_u64 *tmpF3_1 = new F2_487_u64[number];
  F2_487_u64 *tmpF3_2 = new F2_487_u64[number];
  F2_487_u64 *fg1_0 = new F2_487_u64[number];
  F2_487_u64 *fg1_1 = new F2_487_u64[number];
  F2_487_u64 *fg1_2 = new F2_487_u64[number];

  for (unsigned int i = 0; i < number; ++i) aF3_1_0[i] = a[i].xF3[1].xBF[0];

  // fg
  // c_0 := (a0,a1,a2)(b0,b1,b2)
  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xF3[0];
    b0[i] = b[i].xF3[0];
  }

  mulCombL2RwithWindow3_multipleP<wSize>(fg0, a0, b0, number);
  for (unsigned int i = 0; i < number; ++i) {
    fg0[i].xBF[0] += d[i];
    fg0[i].xBF[1] += d[i];
  }

  for (unsigned int i = 0; i < number; ++i) {
    tmpF3[i] = a[i].xF3[0] + b[i].xF3[0];
    // (c_2,c_3) := f + g = (a0+b0,a1+b1,a2+b2,0,0,0)
    c[i].xF3_2[1].xF3[0] = tmpF3[i];
    c[i].xF3_2[1].xF3[1].clear();
    // (fix) (c_2,c_3) = f + g + 1
    c[i].xF3_2[1].plusOne();
  }


  // tmpF3 = (a0+b0,a1+b1,a2+b2)
  for (unsigned int i = 0; i < number; ++i) {
    tmpF3_0[i] = tmpF3[i].xBF[0];
    tmpF3_1[i] = tmpF3[i].xBF[1];
    tmpF3_2[i] = tmpF3[i].xBF[2];
  }

  mulCombL2RwithWindow_multipleP<F2_487_u64, wSize>(fg1_0, aF3_1_0, tmpF3_0, number);
  mulCombL2RwithWindow_multipleP<F2_487_u64, wSize>(fg1_1, aF3_1_0, tmpF3_1, number);
  mulCombL2RwithWindow_multipleP<F2_487_u64, wSize>(fg1_2, aF3_1_0, tmpF3_2, number);

  for (unsigned int i = 0; i < number; ++i) fg1_0[i] += d[i];

  //(c_0,c_1) = fg + s + sw^2
  for (unsigned int i = 0; i < number; ++i) {
    c[i].xF3_2[0].xF3[0] = fg0[i];
    c[i].xF3_2[0].xF3[1].xBF[0] = fg1_0[i];
    c[i].xF3_2[0].xF3[1].xBF[1] = fg1_1[i];
    c[i].xF3_2[0].xF3[1].xBF[2] = fg1_2[i];

    c[i].xF3_2[0].xF3[1].xBF[0].plusOne();
    c[i].xF3_2[0].xF3[1].xBF[2].plusOne();
  }

  delete[] fg0;
  delete[] a0;
  delete[] b0;
  delete[] tmpF3;
  delete[] aF3_1_0;
  delete[] tmpF3_0;
  delete[] tmpF3_1;
  delete[] tmpF3_2;
  delete[] fg1_0;
  delete[] fg1_1;
  delete[] fg1_2;
}
template void alpha_beta3_2_2_withWindow_multipleP<3>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const F2_487_u64 *d, const int number);
template void alpha_beta3_2_2_withWindow_multipleP<4>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const F2_487_u64 *d, const int number);
template void alpha_beta3_2_2_withWindow_multipleP<5>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const F2_487_u64 *d, const int number);
template void alpha_beta3_2_2_withWindow_multipleP<6>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const F2_487_u64 *d, const int number);
template void alpha_beta3_2_2_withWindow_multipleP<7>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const F2_487_u64 *d, const int number);
template void alpha_beta3_2_2_withWindow_multipleP<8>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const F2_487_u64 *d, const int number);
//template void alpha_beta3_2_2_withWindow_multipleP<9>(ExtField3_2_2<F2_487_u64> *c, const ExtField3_2<F2_487_u64> *a, const ExtField3_2<F2_487_u64> *b, const F2_487_u64 *d, const int number);



/**********************************************************************************************************************************************************************************/
/**********************************************************************************************************************************************************************************/
/**********************************************************************************************************************************************************************************/
/**********************************************************************************************************************************************************************************/
/*
 * F2_967_u64における函数定義
 */
/**********************************************************************************************************************************************************************************/
/**********************************************************************************************************************************************************************************/
/**********************************************************************************************************************************************************************************/
/**********************************************************************************************************************************************************************************/

/*
 * template<class Basefield>としない（templateとして定義すると new Basefield[Basefield::pow2func(wSize)] がvariable-length array[-Wvla]警告となる）
 */
template<int wSize>
void mulCombL2RwithWindowP(F2_967_u64& c, const F2_967_u64& a, const F2_967_u64& b) {

#if 0
  if (a.testZero() || b.testZero()) {

    c.clear();
    return;
  }
#endif

  BaseField_Double<F2_967_u64> result;
  result.clear();

  F2_967_u64 *bTable = new F2_967_u64[F2_967_u64::pow2func(wSize)];
  F2_967_u64::precomputeTable(bTable, b, wSize);

  F2_967_u64 *a_dev, *bTable_dev;
  BaseField_Double<F2_967_u64> *result_dev;
  //int *ws_dev;

  cudaMalloc((void**)&a_dev, sizeof(F2_967_u64));
  cudaMalloc((void**)&bTable_dev, sizeof(F2_967_u64)*F2_967_u64::pow2func(wSize));
  cudaMalloc((void**)&result_dev, sizeof(BaseField_Double<F2_967_u64>));
  //cudaMalloc((void**)&ws_dev, sizeof(int));

  cudaMemcpy(a_dev, &a, sizeof(F2_967_u64), cudaMemcpyHostToDevice);
  cudaMemcpy(bTable_dev, bTable, sizeof(F2_967_u64)*F2_967_u64::pow2func(wSize), cudaMemcpyHostToDevice);
  cudaMemcpy(result_dev, &result, sizeof(BaseField_Double<F2_967_u64>), cudaMemcpyHostToDevice);
  //cudaMemcpy(ws_dev, &wSize, sizeof(int), cudaMemcpyHostToDevice);

  dim3 blocks(1,1);
  dim3 threads(F2_967_u64::nUint,1);

  mulCombL2RwithWindowKernel<F2_967_u64, wSize><<< blocks, threads >>>(result_dev, a_dev, bTable_dev);

  cudaMemcpy(&result, result_dev, sizeof(BaseField_Double<F2_967_u64>), cudaMemcpyDeviceToHost);

  /*
  F2_F2_967_u64_Double<F2_967_u64>としての表現に合わせて整形
  */
  result.high.shiftLeftBit<25>();
  result.high ^= F2_967_u64::shiftRightBit<967>(result.low);

  //typename F2_967_u64::uint_type mask = 0x0000007fffffffff;
  //result.low.x[F2_967_u64::nUint - 1] &= mask;
  result.low.clearUpDeg();
  result.reduction(c);

  //delete[] bTable;
  cudaFree(a_dev);
  cudaFree(result_dev);
  //cudaFree(ws_dev);
}
template void mulCombL2RwithWindowP<3>(F2_967_u64& c, const F2_967_u64& a, const F2_967_u64& b);
template void mulCombL2RwithWindowP<4>(F2_967_u64& c, const F2_967_u64& a, const F2_967_u64& b);
template void mulCombL2RwithWindowP<5>(F2_967_u64& c, const F2_967_u64& a, const F2_967_u64& b);
template void mulCombL2RwithWindowP<6>(F2_967_u64& c, const F2_967_u64& a, const F2_967_u64& b);
template void mulCombL2RwithWindowP<7>(F2_967_u64& c, const F2_967_u64& a, const F2_967_u64& b);
template void mulCombL2RwithWindowP<8>(F2_967_u64& c, const F2_967_u64& a, const F2_967_u64& b);
//template void mulCombL2RwithWindowP<9>(F2_967_u64& c, const F2_967_u64& a, const F2_967_u64& b);

/* 一先ずKaratusba methodの二重並列化は実装せず，乗算については，データをglobal memoryにコピーし基礎体上の乗算のkernel call函数を非同期に呼び出し（並べて）単純に構成する（二重並列化と要検証） */
/* window methodのみ実装 */
/* !!! template<class Basefield>だとnew Basefield[number]で-Wvla...基礎体classは指定する 後で要調査 !!! */
/* 一つのkernel函数で各aST, bSTをdeviceで計算した後乗算vSTを行う */
#if 1
template<int wSize>
void mulCombL2RwithWindow6_multipleP(ExtField6<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const ExtField6<F2_967_u64> *b, const int number) {

  ExtField_Double< ExtField6<F2_967_u64> > *result = new ExtField_Double< ExtField6<F2_967_u64> >[number];

  F2_967_u64 *a0 = new F2_967_u64[number];
  F2_967_u64 *a1 = new F2_967_u64[number];
  F2_967_u64 *a2 = new F2_967_u64[number];
  F2_967_u64 *a3 = new F2_967_u64[number];
  F2_967_u64 *a4 = new F2_967_u64[number];
  F2_967_u64 *a5 = new F2_967_u64[number];

  F2_967_u64 *b0 = new F2_967_u64[number];
  F2_967_u64 *b1 = new F2_967_u64[number];
  F2_967_u64 *b2 = new F2_967_u64[number];
  F2_967_u64 *b3 = new F2_967_u64[number];
  F2_967_u64 *b4 = new F2_967_u64[number];
  F2_967_u64 *b5 = new F2_967_u64[number];

  F2_967_u64 *v0 = new F2_967_u64[number];
  F2_967_u64 *v1 = new F2_967_u64[number];
  F2_967_u64 *v2 = new F2_967_u64[number];
  F2_967_u64 *v3 = new F2_967_u64[number];
  F2_967_u64 *v4 = new F2_967_u64[number];
  F2_967_u64 *v5 = new F2_967_u64[number];

  F2_967_u64 *vST0 = new F2_967_u64[number];
  F2_967_u64 *vST1 = new F2_967_u64[number];
  F2_967_u64 *vST2 = new F2_967_u64[number];
  F2_967_u64 *vST3 = new F2_967_u64[number];
  F2_967_u64 *vST4 = new F2_967_u64[number];
  F2_967_u64 *vST5 = new F2_967_u64[number];
  F2_967_u64 *vST6 = new F2_967_u64[number];
  F2_967_u64 *vST7 = new F2_967_u64[number];
  F2_967_u64 *vST8 = new F2_967_u64[number];
  F2_967_u64 *vST9 = new F2_967_u64[number];
  F2_967_u64 *vST10 = new F2_967_u64[number];
  F2_967_u64 *vST11 = new F2_967_u64[number];
  F2_967_u64 *vST12 = new F2_967_u64[number];
  F2_967_u64 *vST13 = new F2_967_u64[number];
  F2_967_u64 *vST14 = new F2_967_u64[number];

  // copy to BF array
  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xBF[0];
    a1[i] = a[i].xBF[1];
    a2[i] = a[i].xBF[2];
    a3[i] = a[i].xBF[3];
    a4[i] = a[i].xBF[4];
    a5[i] = a[i].xBF[5];

    b0[i] = b[i].xBF[0];
    b1[i] = b[i].xBF[1];
    b2[i] = b[i].xBF[2];
    b3[i] = b[i].xBF[3];
    b4[i] = b[i].xBF[4];
    b5[i] = b[i].xBF[5];
  }



  F2_967_u64 *a0_dev, *a1_dev, *a2_dev, *a3_dev, *a4_dev, *a5_dev;
  F2_967_u64 *b0_dev, *b1_dev, *b2_dev, *b3_dev, *b4_dev, *b5_dev;
  F2_967_u64 *v0_dev, *v1_dev, *v2_dev, *v3_dev, *v4_dev, *v5_dev;
  F2_967_u64 *vST0_dev, *vST1_dev, *vST2_dev, *vST3_dev, *vST4_dev, *vST5_dev, *vST6_dev, *vST7_dev, *vST8_dev, *vST9_dev, *vST10_dev, *vST11_dev, *vST12_dev, *vST13_dev, *vST14_dev;

  cudaMalloc((void**)&a0_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&a1_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&a2_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&a3_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&a4_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&a5_dev, sizeof(F2_967_u64)*number);

  cudaMalloc((void**)&b0_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&b1_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&b2_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&b3_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&b4_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&b5_dev, sizeof(F2_967_u64)*number);

  cudaMalloc((void**)&v0_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&v1_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&v2_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&v3_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&v4_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&v5_dev, sizeof(F2_967_u64)*number);

  cudaMalloc((void**)&vST0_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST1_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST2_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST3_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST4_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST5_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST6_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST7_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST8_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST9_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST10_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST11_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST12_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST13_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST14_dev, sizeof(F2_967_u64)*number);

  cudaMemcpy(a0_dev, a0, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a1_dev, a1, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a2_dev, a2, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a3_dev, a3, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a4_dev, a4, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a5_dev, a5, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);

  cudaMemcpy(b0_dev, b0, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b1_dev, b1, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b2_dev, b2, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b3_dev, b3, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b4_dev, b4, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b5_dev, b5, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);

  dim3 blocks(number,1);
  dim3 threads(F2_967_u64::nUint,1);

  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(v0_dev, a0_dev, b0_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(v1_dev, a1_dev, b1_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(v2_dev, a2_dev, b2_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(v3_dev, a3_dev, b3_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(v4_dev, a4_dev, b4_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(v5_dev, a5_dev, b5_dev);

  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST0_dev, a0_dev, a1_dev, b0_dev, b1_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST1_dev, a0_dev, a2_dev, b0_dev, b2_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST2_dev, a0_dev, a3_dev, b0_dev, b3_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST3_dev, a0_dev, a4_dev, b0_dev, b4_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST4_dev, a0_dev, a5_dev, b0_dev, b5_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST5_dev, a1_dev, a2_dev, b1_dev, b2_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST6_dev, a1_dev, a3_dev, b1_dev, b3_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST7_dev, a1_dev, a4_dev, b1_dev, b4_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST8_dev, a1_dev, a5_dev, b1_dev, b5_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST9_dev, a2_dev, a3_dev, b2_dev, b3_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST10_dev, a2_dev, a4_dev, b2_dev, b4_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST11_dev, a2_dev, a5_dev, b2_dev, b5_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST12_dev, a3_dev, a4_dev, b3_dev, b4_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST13_dev, a3_dev, a5_dev, b3_dev, b5_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST14_dev, a4_dev, a5_dev, b4_dev, b5_dev);

  cudaMemcpy(v0, v0_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v1, v1_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v2, v2_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v3, v3_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v4, v4_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v5, v5_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);

  cudaMemcpy(vST0, vST0_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST1, vST1_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST2, vST2_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST3, vST3_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST4, vST4_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST5, vST5_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST6, vST6_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST7, vST7_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST8, vST8_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST9, vST9_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST10, vST10_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST11, vST11_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST12, vST12_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST13, vST13_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST14, vST14_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);



  for (unsigned int i = 0; i < number; ++i) {
    // res[0] = a[0]b[0]
    result[i].low.xBF[0] = v0[i];

    //res[10] = a[5]b[5]
    result[i].high.xBF[4] = v5[i];

    // res[1]
    result[i].low.xBF[1] = vST0[i] - result[i].low.xBF[0];
    result[i].low.xBF[1] -= v1[i];

    // res[2]
    result[i].low.xBF[2] = vST1[i] - result[i].low.xBF[0];
    result[i].low.xBF[2] -= v2[i];
    result[i].low.xBF[2] += v1[i];

    // res[3]
    result[i].low.xBF[3] = vST2[i] + vST5[i];
    result[i].low.xBF[3] -= result[i].low.xBF[0];
    result[i].low.xBF[3] -= v3[i];
    result[i].low.xBF[3] -= v1[i];
    result[i].low.xBF[3] -= v2[i];

    // res[4]
    result[i].low.xBF[4] = vST3[i] + vST6[i];
    result[i].low.xBF[4] -= result[i].low.xBF[0];
    result[i].low.xBF[4] -= v4[i];
    result[i].low.xBF[4] -= v1[i];
    result[i].low.xBF[4] -= v3[i];
    result[i].low.xBF[4] += v2[i];

    // res[5]
    result[i].low.xBF[5] = vST4[i] + vST7[i];
    result[i].low.xBF[5] += vST9[i];
    result[i].low.xBF[5] -= result[i].low.xBF[0];
    result[i].low.xBF[5] -= result[i].high.xBF[4];
    result[i].low.xBF[5] -= v1[i];
    result[i].low.xBF[5] -= v4[i];
    result[i].low.xBF[5] -= v2[i];
    result[i].low.xBF[5] -= v3[i];

    // res[6]
    result[i].high.xBF[0] = vST8[i] + vST10[i];
    result[i].high.xBF[0] -= v1[i];
    result[i].high.xBF[0] -= result[i].high.xBF[4];
    result[i].high.xBF[0] -= v2[i];
    result[i].high.xBF[0] -= v4[i];
    result[i].high.xBF[0] += v3[i];

    // res[7]
    result[i].high.xBF[1] = vST11[i] + vST12[i];
    result[i].high.xBF[1] -= v2[i];
    result[i].high.xBF[1] -= result[i].high.xBF[4];
    result[i].high.xBF[1] -= v3[i];
    result[i].high.xBF[1] -= v4[i];

    // res[8]
    result[i].high.xBF[2] = vST13[i] - v3[i];
    result[i].high.xBF[2] -= result[i].high.xBF[4];
    result[i].high.xBF[2] += v4[i];

    // res[9]
    result[i].high.xBF[3] = vST14[i] - v4[i];
    result[i].high.xBF[3] -= result[i].high.xBF[4];

    c[i].reduction(result[i]);
  }

  delete[] result;

  delete[] a0;
  delete[] a1;
  delete[] a2;
  delete[] a3;
  delete[] a4;
  delete[] a5;

  delete[] b0;
  delete[] b1;
  delete[] b2;
  delete[] b3;
  delete[] b4;
  delete[] b5;

  delete[] v0;
  delete[] v1;
  delete[] v2;
  delete[] v3;
  delete[] v4;
  delete[] v5;

  delete[] vST0;
  delete[] vST1;
  delete[] vST2;
  delete[] vST3;
  delete[] vST4;
  delete[] vST5;
  delete[] vST6;
  delete[] vST7;
  delete[] vST8;
  delete[] vST9;
  delete[] vST10;
  delete[] vST11;
  delete[] vST12;
  delete[] vST13;
  delete[] vST14;

  cudaFree(a0_dev);
  cudaFree(a1_dev);
  cudaFree(a2_dev);
  cudaFree(a3_dev);
  cudaFree(a4_dev);
  cudaFree(a5_dev);

  cudaFree(b0_dev);
  cudaFree(b1_dev);
  cudaFree(b2_dev);
  cudaFree(b3_dev);
  cudaFree(b4_dev);
  cudaFree(b5_dev);

  cudaFree(v0_dev);
  cudaFree(v1_dev);
  cudaFree(v2_dev);
  cudaFree(v3_dev);
  cudaFree(v4_dev);
  cudaFree(v5_dev);

  cudaFree(vST0_dev);
  cudaFree(vST1_dev);
  cudaFree(vST2_dev);
  cudaFree(vST3_dev);
  cudaFree(vST4_dev);
  cudaFree(vST5_dev);
  cudaFree(vST6_dev);
  cudaFree(vST7_dev);
  cudaFree(vST8_dev);
  cudaFree(vST9_dev);
  cudaFree(vST10_dev);
  cudaFree(vST11_dev);
  cudaFree(vST12_dev);
  cudaFree(vST13_dev);
  cudaFree(vST14_dev);
}
/* hostで加算を行い乗算のみkernel函数を呼ぶ */
#else
template<int wSize>
void mulCombL2RwithWindow6_multipleP(ExtField6<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const ExtField6<F2_967_u64> *b, const int number) {

  ExtField_Double< ExtField6<F2_967_u64> > *result = new ExtField_Double< ExtField6<F2_967_u64> >[number];

  F2_967_u64 *a0 = new F2_967_u64[number];
  F2_967_u64 *a1 = new F2_967_u64[number];
  F2_967_u64 *a2 = new F2_967_u64[number];
  F2_967_u64 *a3 = new F2_967_u64[number];
  F2_967_u64 *a4 = new F2_967_u64[number];
  F2_967_u64 *a5 = new F2_967_u64[number];

  F2_967_u64 *b0 = new F2_967_u64[number];
  F2_967_u64 *b1 = new F2_967_u64[number];
  F2_967_u64 *b2 = new F2_967_u64[number];
  F2_967_u64 *b3 = new F2_967_u64[number];
  F2_967_u64 *b4 = new F2_967_u64[number];
  F2_967_u64 *b5 = new F2_967_u64[number];

  F2_967_u64 *v0 = new F2_967_u64[number];
  F2_967_u64 *v1 = new F2_967_u64[number];
  F2_967_u64 *v2 = new F2_967_u64[number];
  F2_967_u64 *v3 = new F2_967_u64[number];
  F2_967_u64 *v4 = new F2_967_u64[number];
  F2_967_u64 *v5 = new F2_967_u64[number];

  F2_967_u64 *aST0 = new F2_967_u64[number];
  F2_967_u64 *aST1 = new F2_967_u64[number];
  F2_967_u64 *aST2 = new F2_967_u64[number];
  F2_967_u64 *aST3 = new F2_967_u64[number];
  F2_967_u64 *aST4 = new F2_967_u64[number];
  F2_967_u64 *aST5 = new F2_967_u64[number];
  F2_967_u64 *aST6 = new F2_967_u64[number];
  F2_967_u64 *aST7 = new F2_967_u64[number];
  F2_967_u64 *aST8 = new F2_967_u64[number];
  F2_967_u64 *aST9 = new F2_967_u64[number];
  F2_967_u64 *aST10 = new F2_967_u64[number];
  F2_967_u64 *aST11 = new F2_967_u64[number];
  F2_967_u64 *aST12 = new F2_967_u64[number];
  F2_967_u64 *aST13 = new F2_967_u64[number];
  F2_967_u64 *aST14 = new F2_967_u64[number];

  F2_967_u64 *bST0 = new F2_967_u64[number];
  F2_967_u64 *bST1 = new F2_967_u64[number];
  F2_967_u64 *bST2 = new F2_967_u64[number];
  F2_967_u64 *bST3 = new F2_967_u64[number];
  F2_967_u64 *bST4 = new F2_967_u64[number];
  F2_967_u64 *bST5 = new F2_967_u64[number];
  F2_967_u64 *bST6 = new F2_967_u64[number];
  F2_967_u64 *bST7 = new F2_967_u64[number];
  F2_967_u64 *bST8 = new F2_967_u64[number];
  F2_967_u64 *bST9 = new F2_967_u64[number];
  F2_967_u64 *bST10 = new F2_967_u64[number];
  F2_967_u64 *bST11 = new F2_967_u64[number];
  F2_967_u64 *bST12 = new F2_967_u64[number];
  F2_967_u64 *bST13 = new F2_967_u64[number];
  F2_967_u64 *bST14 = new F2_967_u64[number];

  F2_967_u64 *vST0 = new F2_967_u64[number];
  F2_967_u64 *vST1 = new F2_967_u64[number];
  F2_967_u64 *vST2 = new F2_967_u64[number];
  F2_967_u64 *vST3 = new F2_967_u64[number];
  F2_967_u64 *vST4 = new F2_967_u64[number];
  F2_967_u64 *vST5 = new F2_967_u64[number];
  F2_967_u64 *vST6 = new F2_967_u64[number];
  F2_967_u64 *vST7 = new F2_967_u64[number];
  F2_967_u64 *vST8 = new F2_967_u64[number];
  F2_967_u64 *vST9 = new F2_967_u64[number];
  F2_967_u64 *vST10 = new F2_967_u64[number];
  F2_967_u64 *vST11 = new F2_967_u64[number];
  F2_967_u64 *vST12 = new F2_967_u64[number];
  F2_967_u64 *vST13 = new F2_967_u64[number];
  F2_967_u64 *vST14 = new F2_967_u64[number];

  // copy to BF array
  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xBF[0];
    a1[i] = a[i].xBF[1];
    a2[i] = a[i].xBF[2];
    a3[i] = a[i].xBF[3];
    a4[i] = a[i].xBF[4];
    a5[i] = a[i].xBF[5];

    b0[i] = b[i].xBF[0];
    b1[i] = b[i].xBF[1];
    b2[i] = b[i].xBF[2];
    b3[i] = b[i].xBF[3];
    b4[i] = b[i].xBF[4];
    b5[i] = b[i].xBF[5];
  }

  // precompute, addition
  for (unsigned int i = 0; i < number; ++i) {
    aST0[i] = a0[i] + a1[i];
    aST1[i] = a0[i] + a2[i];
    aST2[i] = a0[i] + a3[i];
    aST3[i] = a0[i] + a4[i];
    aST4[i] = a0[i] + a5[i];
    aST5[i] = a1[i] + a2[i];
    aST6[i] = a1[i] + a3[i];
    aST7[i] = a1[i] + a4[i];
    aST8[i] = a1[i] + a5[i];
    aST9[i] = a2[i] + a3[i];
    aST10[i] = a2[i] + a4[i];
    aST11[i] = a2[i] + a5[i];
    aST12[i] = a3[i] + a4[i];
    aST13[i] = a3[i] + a5[i];
    aST14[i] = a4[i] + a5[i];

    bST0[i] = b0[i] + b1[i];
    bST1[i] = b0[i] + b2[i];
    bST2[i] = b0[i] + b3[i];
    bST3[i] = b0[i] + b4[i];
    bST4[i] = b0[i] + b5[i];
    bST5[i] = b1[i] + b2[i];
    bST6[i] = b1[i] + b3[i];
    bST7[i] = b1[i] + b4[i];
    bST8[i] = b1[i] + b5[i];
    bST9[i] = b2[i] + b3[i];
    bST10[i] = b2[i] + b4[i];
    bST11[i] = b2[i] + b5[i];
    bST12[i] = b3[i] + b4[i];
    bST13[i] = b3[i] + b5[i];
    bST14[i] = b4[i] + b5[i];
  }



  F2_967_u64 *a0_dev, *a1_dev, *a2_dev, *a3_dev, *a4_dev, *a5_dev;
  F2_967_u64 *b0_dev, *b1_dev, *b2_dev, *b3_dev, *b4_dev, *b5_dev;
  F2_967_u64 *v0_dev, *v1_dev, *v2_dev, *v3_dev, *v4_dev, *v5_dev;
  F2_967_u64 *aST0_dev, *aST1_dev, *aST2_dev, *aST3_dev, *aST4_dev, *aST5_dev, *aST6_dev, *aST7_dev, *aST8_dev, *aST9_dev, *aST10_dev, *aST11_dev, *aST12_dev, *aST13_dev, *aST14_dev;
  F2_967_u64 *bST0_dev, *bST1_dev, *bST2_dev, *bST3_dev, *bST4_dev, *bST5_dev, *bST6_dev, *bST7_dev, *bST8_dev, *bST9_dev, *bST10_dev, *bST11_dev, *bST12_dev, *bST13_dev, *bST14_dev;
  F2_967_u64 *vST0_dev, *vST1_dev, *vST2_dev, *vST3_dev, *vST4_dev, *vST5_dev, *vST6_dev, *vST7_dev, *vST8_dev, *vST9_dev, *vST10_dev, *vST11_dev, *vST12_dev, *vST13_dev, *vST14_dev;

  cudaMalloc((void**)&a0_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&a1_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&a2_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&a3_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&a4_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&a5_dev, sizeof(F2_967_u64)*number);

  cudaMalloc((void**)&b0_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&b1_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&b2_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&b3_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&b4_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&b5_dev, sizeof(F2_967_u64)*number);

  cudaMalloc((void**)&v0_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&v1_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&v2_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&v3_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&v4_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&v5_dev, sizeof(F2_967_u64)*number);

  cudaMalloc((void**)&aST0_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&aST1_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&aST2_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&aST3_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&aST4_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&aST5_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&aST6_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&aST7_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&aST8_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&aST9_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&aST10_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&aST11_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&aST12_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&aST13_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&aST14_dev, sizeof(F2_967_u64)*number);

  cudaMalloc((void**)&bST0_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&bST1_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&bST2_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&bST3_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&bST4_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&bST5_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&bST6_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&bST7_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&bST8_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&bST9_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&bST10_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&bST11_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&bST12_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&bST13_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&bST14_dev, sizeof(F2_967_u64)*number);

  cudaMalloc((void**)&vST0_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST1_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST2_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST3_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST4_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST5_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST6_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST7_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST8_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST9_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST10_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST11_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST12_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST13_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST14_dev, sizeof(F2_967_u64)*number);

  cudaMemcpy(a0_dev, a0, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a1_dev, a1, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a2_dev, a2, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a3_dev, a3, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a4_dev, a4, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a5_dev, a5, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);

  cudaMemcpy(b0_dev, b0, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b1_dev, b1, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b2_dev, b2, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b3_dev, b3, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b4_dev, b4, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b5_dev, b5, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);

  cudaMemcpy(aST0_dev, aST0, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST1_dev, aST1, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST2_dev, aST2, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST3_dev, aST3, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST4_dev, aST4, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST5_dev, aST5, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST6_dev, aST6, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST7_dev, aST7, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST8_dev, aST8, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST9_dev, aST9, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST10_dev, aST10, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST11_dev, aST11, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST12_dev, aST12, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST13_dev, aST13, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(aST14_dev, aST14, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);

  cudaMemcpy(bST0_dev, bST0, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST1_dev, bST1, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST2_dev, bST2, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST3_dev, bST3, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST4_dev, bST4, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST5_dev, bST5, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST6_dev, bST6, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST7_dev, bST7, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST8_dev, bST8, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST9_dev, bST9, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST10_dev, bST10, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST11_dev, bST11, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST12_dev, bST12, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST13_dev, bST13, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(bST14_dev, bST14, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);

  dim3 blocks(number,1);
  dim3 threads(F2_967_u64::nUint,1);

  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(v0_dev, a0_dev, b0_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(v1_dev, a1_dev, b1_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(v2_dev, a2_dev, b2_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(v3_dev, a3_dev, b3_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(v4_dev, a4_dev, b4_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(v5_dev, a5_dev, b5_dev);

  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST0_dev, aST0_dev, bST0_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST1_dev, aST1_dev, bST1_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST2_dev, aST2_dev, bST2_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST3_dev, aST3_dev, bST3_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST4_dev, aST4_dev, bST4_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST5_dev, aST5_dev, bST5_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST6_dev, aST6_dev, bST6_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST7_dev, aST7_dev, bST7_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST8_dev, aST8_dev, bST8_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST9_dev, aST9_dev, bST9_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST10_dev, aST10_dev, bST10_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST11_dev, aST11_dev, bST11_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST12_dev, aST12_dev, bST12_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST13_dev, aST13_dev, bST13_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST14_dev, aST14_dev, bST14_dev);

  cudaMemcpy(v0, v0_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v1, v1_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v2, v2_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v3, v3_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v4, v4_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v5, v5_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);

  cudaMemcpy(vST0, vST0_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST1, vST1_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST2, vST2_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST3, vST3_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST4, vST4_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST5, vST5_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST6, vST6_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST7, vST7_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST8, vST8_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST9, vST9_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST10, vST10_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST11, vST11_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST12, vST12_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST13, vST13_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST14, vST14_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);



  for (unsigned int i = 0; i < number; ++i) {
    // res[0] = a[0]b[0]
    result[i].low.xBF[0] = v0[i];

    //res[10] = a[5]b[5]
    result[i].high.xBF[4] = v5[i];

    // res[1]
    result[i].low.xBF[1] = vST0[i] - result[i].low.xBF[0];
    result[i].low.xBF[1] -= v1[i];

    // res[2]
    result[i].low.xBF[2] = vST1[i] - result[i].low.xBF[0];
    result[i].low.xBF[2] -= v2[i];
    result[i].low.xBF[2] += v1[i];

    // res[3]
    result[i].low.xBF[3] = vST2[i] + vST5[i];
    result[i].low.xBF[3] -= result[i].low.xBF[0];
    result[i].low.xBF[3] -= v3[i];
    result[i].low.xBF[3] -= v1[i];
    result[i].low.xBF[3] -= v2[i];

    // res[4]
    result[i].low.xBF[4] = vST3[i] + vST6[i];
    result[i].low.xBF[4] -= result[i].low.xBF[0];
    result[i].low.xBF[4] -= v4[i];
    result[i].low.xBF[4] -= v1[i];
    result[i].low.xBF[4] -= v3[i];
    result[i].low.xBF[4] += v2[i];

    // res[5]
    result[i].low.xBF[5] = vST4[i] + vST7[i];
    result[i].low.xBF[5] += vST9[i];
    result[i].low.xBF[5] -= result[i].low.xBF[0];
    result[i].low.xBF[5] -= result[i].high.xBF[4];
    result[i].low.xBF[5] -= v1[i];
    result[i].low.xBF[5] -= v4[i];
    result[i].low.xBF[5] -= v2[i];
    result[i].low.xBF[5] -= v3[i];

    // res[6]
    result[i].high.xBF[0] = vST8[i] + vST10[i];
    result[i].high.xBF[0] -= v1[i];
    result[i].high.xBF[0] -= result[i].high.xBF[4];
    result[i].high.xBF[0] -= v2[i];
    result[i].high.xBF[0] -= v4[i];
    result[i].high.xBF[0] += v3[i];

    // res[7]
    result[i].high.xBF[1] = vST11[i] + vST12[i];
    result[i].high.xBF[1] -= v2[i];
    result[i].high.xBF[1] -= result[i].high.xBF[4];
    result[i].high.xBF[1] -= v3[i];
    result[i].high.xBF[1] -= v4[i];

    // res[8]
    result[i].high.xBF[2] = vST13[i] - v3[i];
    result[i].high.xBF[2] -= result[i].high.xBF[4];
    result[i].high.xBF[2] += v4[i];

    // res[9]
    result[i].high.xBF[3] = vST14[i] - v4[i];
    result[i].high.xBF[3] -= result[i].high.xBF[4];

    c[i].reduction(result[i]);
  }

  delete[] result;

  delete[] a0;
  delete[] a1;
  delete[] a2;
  delete[] a3;
  delete[] a4;
  delete[] a5;

  delete[] b0;
  delete[] b1;
  delete[] b2;
  delete[] b3;
  delete[] b4;
  delete[] b5;

  delete[] v0;
  delete[] v1;
  delete[] v2;
  delete[] v3;
  delete[] v4;
  delete[] v5;

  delete[] aST0;
  delete[] aST1;
  delete[] aST2;
  delete[] aST3;
  delete[] aST4;
  delete[] aST5;
  delete[] aST6;
  delete[] aST7;
  delete[] aST8;
  delete[] aST9;
  delete[] aST10;
  delete[] aST11;
  delete[] aST12;
  delete[] aST13;
  delete[] aST14;

  delete[] bST0;
  delete[] bST1;
  delete[] bST2;
  delete[] bST3;
  delete[] bST4;
  delete[] bST5;
  delete[] bST6;
  delete[] bST7;
  delete[] bST8;
  delete[] bST9;
  delete[] bST10;
  delete[] bST11;
  delete[] bST12;
  delete[] bST13;
  delete[] bST14;

  delete[] vST0;
  delete[] vST1;
  delete[] vST2;
  delete[] vST3;
  delete[] vST4;
  delete[] vST5;
  delete[] vST6;
  delete[] vST7;
  delete[] vST8;
  delete[] vST9;
  delete[] vST10;
  delete[] vST11;
  delete[] vST12;
  delete[] vST13;
  delete[] vST14;

  cudaFree(a0_dev);
  cudaFree(a1_dev);
  cudaFree(a2_dev);
  cudaFree(a3_dev);
  cudaFree(a4_dev);
  cudaFree(a5_dev);

  cudaFree(b0_dev);
  cudaFree(b1_dev);
  cudaFree(b2_dev);
  cudaFree(b3_dev);
  cudaFree(b4_dev);
  cudaFree(b5_dev);

  cudaFree(v0_dev);
  cudaFree(v1_dev);
  cudaFree(v2_dev);
  cudaFree(v3_dev);
  cudaFree(v4_dev);
  cudaFree(v5_dev);

  cudaFree(aST0_dev);
  cudaFree(aST1_dev);
  cudaFree(aST2_dev);
  cudaFree(aST3_dev);
  cudaFree(aST4_dev);
  cudaFree(aST5_dev);
  cudaFree(aST6_dev);
  cudaFree(aST7_dev);
  cudaFree(aST8_dev);
  cudaFree(aST9_dev);
  cudaFree(aST10_dev);
  cudaFree(aST11_dev);
  cudaFree(aST12_dev);
  cudaFree(aST13_dev);
  cudaFree(aST14_dev);

  cudaFree(bST0_dev);
  cudaFree(bST1_dev);
  cudaFree(bST2_dev);
  cudaFree(bST3_dev);
  cudaFree(bST4_dev);
  cudaFree(bST5_dev);
  cudaFree(bST6_dev);
  cudaFree(bST7_dev);
  cudaFree(bST8_dev);
  cudaFree(bST9_dev);
  cudaFree(bST10_dev);
  cudaFree(bST11_dev);
  cudaFree(bST12_dev);
  cudaFree(bST13_dev);
  cudaFree(bST14_dev);

  cudaFree(vST0_dev);
  cudaFree(vST1_dev);
  cudaFree(vST2_dev);
  cudaFree(vST3_dev);
  cudaFree(vST4_dev);
  cudaFree(vST5_dev);
  cudaFree(vST6_dev);
  cudaFree(vST7_dev);
  cudaFree(vST8_dev);
  cudaFree(vST9_dev);
  cudaFree(vST10_dev);
  cudaFree(vST11_dev);
  cudaFree(vST12_dev);
  cudaFree(vST13_dev);
  cudaFree(vST14_dev);
}
#endif
template void mulCombL2RwithWindow6_multipleP<3>(ExtField6<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const ExtField6<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow6_multipleP<4>(ExtField6<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const ExtField6<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow6_multipleP<5>(ExtField6<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const ExtField6<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow6_multipleP<6>(ExtField6<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const ExtField6<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow6_multipleP<7>(ExtField6<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const ExtField6<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow6_multipleP<8>(ExtField6<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const ExtField6<F2_967_u64> *b, const int number);
//template void mulCombL2RwithWindow6_multipleP<9>(ExtField6<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const ExtField6<F2_967_u64> *b, const int number);

void square6_multipleP(ExtField6<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const int number) {

  F2_967_u64 *a0 = new F2_967_u64[number];
  F2_967_u64 *a1 = new F2_967_u64[number];
  F2_967_u64 *a2 = new F2_967_u64[number];
  F2_967_u64 *a3 = new F2_967_u64[number];
  F2_967_u64 *a4 = new F2_967_u64[number];
  F2_967_u64 *a5 = new F2_967_u64[number];
  F2_967_u64 *tmp = new F2_967_u64[number];

  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xBF[0];
    a1[i] = a[i].xBF[1];
    a2[i] = a[i].xBF[2];
    a3[i] = a[i].xBF[3];
    a4[i] = a[i].xBF[4];
    a5[i] = a[i].xBF[5];
  }

  // (fix) c_5 = a_3^2
  square_multipleP(tmp, a3, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[5] = tmp[i];

  // (fix) c_4 = a_2^2
  square_multipleP(tmp, a2, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[4] = tmp[i];

  // c_3 = a_5^2
  square_multipleP(tmp, a5, number);
  // (fix) c_3 = a_5^2 + a_3^2
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[3] = tmp[i] + c[i].xBF[5];

  // c_0 = a_1^2
  square_multipleP(tmp, a1, number);

  // (fix) c_2 = a_5^2 + a_3^2 + a_1^2
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[2] = tmp[i] + c[i].xBF[3];

  // (fix) c_1 = a_4^2
  square_multipleP(tmp, a4, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[1] = tmp[i];

  // c_0 = a_0^2
  square_multipleP(tmp, a0, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[0] = tmp[i];

  // c_0 = a_4^2 + a_3^2 + a_0^2
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[0] += c[i].xBF[1];
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[0] += c[i].xBF[5];

  delete[] a0;
  delete[] a1;
  delete[] a2;
  delete[] a3;
  delete[] a4;
  delete[] a5;
  delete[] tmp;
}

template<int wSize>
void mulCombL2RwithWindow6_2_multipleP(ExtField6_2<F2_967_u64> *c, const ExtField6_2<F2_967_u64> *a, const ExtField6_2<F2_967_u64> *b, const int number) {

  ExtField_Double< ExtField6_2<F2_967_u64> > *result = new ExtField_Double< ExtField6_2<F2_967_u64> >[number];
  ExtField6<F2_967_u64> *a0 = new ExtField6<F2_967_u64>[number];
  ExtField6<F2_967_u64> *a1 = new ExtField6<F2_967_u64>[number];
  ExtField6<F2_967_u64> *b0 = new ExtField6<F2_967_u64>[number];
  ExtField6<F2_967_u64> *b1 = new ExtField6<F2_967_u64>[number];
  ExtField6<F2_967_u64> *aST = new ExtField6<F2_967_u64>[number];
  ExtField6<F2_967_u64> *bST = new ExtField6<F2_967_u64>[number];
  ExtField6<F2_967_u64> *tmp6 = new ExtField6<F2_967_u64>[number];

  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xF6[0];
    a1[i] = a[i].xF6[1];
    b0[i] = b[i].xF6[0];
    b1[i] = b[i].xF6[1];
  }

  for (unsigned int i = 0; i < number; ++i) {
    aST[i] = a[i].xF6[0] + a[i].xF6[1];
    bST[i] = b[i].xF6[0] + b[i].xF6[1];
  }

  mulCombL2RwithWindow6_multipleP<wSize>(tmp6, a0, b0, number);
  for (unsigned int i = 0; i < number; ++i) result[i].low.xF6[0] = tmp6[i];

  mulCombL2RwithWindow6_multipleP<wSize>(tmp6, a1, b1, number);
  for (unsigned int i = 0; i < number; ++i) result[i].high.xF6[0] = tmp6[i];

  mulCombL2RwithWindow6_multipleP<wSize>(tmp6, aST, bST, number);
  for (unsigned int i = 0; i < number; ++i) result[i].low.xF6[1] = tmp6[i];

  for (unsigned int i = 0; i < number; ++i) {
    result[i].low.xF6[1] -= result[i].low.xF6[0];
    result[i].low.xF6[1] -= result[i].high.xF6[0];

    c[i].reduction(result[i]);
  }

  delete[] result;
  delete[] a0;
  delete[] a1;
  delete[] b0;
  delete[] b1;
  delete[] aST;
  delete[] bST;
  delete[] tmp6;
}
template void mulCombL2RwithWindow6_2_multipleP<3>(ExtField6_2<F2_967_u64> *c, const ExtField6_2<F2_967_u64> *a, const ExtField6_2<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow6_2_multipleP<4>(ExtField6_2<F2_967_u64> *c, const ExtField6_2<F2_967_u64> *a, const ExtField6_2<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow6_2_multipleP<5>(ExtField6_2<F2_967_u64> *c, const ExtField6_2<F2_967_u64> *a, const ExtField6_2<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow6_2_multipleP<6>(ExtField6_2<F2_967_u64> *c, const ExtField6_2<F2_967_u64> *a, const ExtField6_2<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow6_2_multipleP<7>(ExtField6_2<F2_967_u64> *c, const ExtField6_2<F2_967_u64> *a, const ExtField6_2<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow6_2_multipleP<8>(ExtField6_2<F2_967_u64> *c, const ExtField6_2<F2_967_u64> *a, const ExtField6_2<F2_967_u64> *b, const int number);
//template void mulCombL2RwithWindow6_2_multipleP<9>(ExtField6_2<F2_967_u64> *c, const ExtField6_2<F2_967_u64> *a, const ExtField6_2<F2_967_u64> *b, const int number);

void square6_2_multipleP(ExtField6_2<F2_967_u64> *c, const ExtField6_2<F2_967_u64> *a, const int number) {

  ExtField6<F2_967_u64> *a0 = new ExtField6<F2_967_u64>[number];
  ExtField6<F2_967_u64> *a1 = new ExtField6<F2_967_u64>[number];
  ExtField6<F2_967_u64> *tmp = new ExtField6<F2_967_u64>[number];

  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xF6[0];
    a1[i] = a[i].xF6[1];
  }

  // c_1 = a_1^2
  square6_multipleP(tmp, a1, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xF6[1] = tmp[i];

  square6_multipleP(tmp, a0, number);

  // c_0 = a_1^2(w^5 + w^3)
  for (unsigned int i = 0; i < number; ++i) ExtField6<F2_967_u64>::mulConstTerm6(c[i].xF6[0], c[i].xF6[1]);

  // c_0 = a_1^2(w^5 + w^3) + a_0^2
  for (unsigned int i = 0; i < number; ++i) c[i].xF6[0] += tmp[i];

  delete[] a0;
  delete[] a1;
  delete[] tmp;
}

template<int wSize>
void alpha_beta6_2_withWindow_multipleP(ExtField6_2<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const ExtField6<F2_967_u64> *b, const int number) {

  for (unsigned int i = 0; i < number; ++i) {
    // f + g
    c[i].xF6[1] = a[i] + b[i];
    // (fix) c_1 = f + g + 1
    c[i].xF6[1].xBF[0].plusOne();
  }

  // c_0 := (a0,a1,a2)(b0,b1,b2)
  // Karatsuba 3
  F2_967_u64 *a0 = new F2_967_u64[number];
  F2_967_u64 *a1 = new F2_967_u64[number];
  F2_967_u64 *a2 = new F2_967_u64[number];
  F2_967_u64 *a4 = new F2_967_u64[number];


  F2_967_u64 *b0 = new F2_967_u64[number];
  F2_967_u64 *b1 = new F2_967_u64[number];
  F2_967_u64 *b2 = new F2_967_u64[number];
  F2_967_u64 *b4 = new F2_967_u64[number];

  F2_967_u64 *v0 = new F2_967_u64[number];
  F2_967_u64 *v1 = new F2_967_u64[number];
  F2_967_u64 *v2 = new F2_967_u64[number];

  F2_967_u64 *vST0 = new F2_967_u64[number];
  F2_967_u64 *vST1 = new F2_967_u64[number];
  F2_967_u64 *vST2 = new F2_967_u64[number];

  // copy to BF array
  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xBF[0];
    a1[i] = a[i].xBF[1];
    a2[i] = a[i].xBF[2];
    a4[i] = a[i].xBF[4];

    b0[i] = b[i].xBF[0];
    b1[i] = b[i].xBF[1];
    b2[i] = b[i].xBF[2];
    b4[i] = b[i].xBF[4];
  }

  F2_967_u64 *a0_dev, *a1_dev, *a2_dev;
  F2_967_u64 *b0_dev, *b1_dev, *b2_dev;
  F2_967_u64 *v0_dev, *v1_dev, *v2_dev;
  F2_967_u64 *vST0_dev, *vST1_dev, *vST2_dev;

  cudaMalloc((void**)&a0_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&a1_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&a2_dev, sizeof(F2_967_u64)*number);

  cudaMalloc((void**)&b0_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&b1_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&b2_dev, sizeof(F2_967_u64)*number);

  cudaMalloc((void**)&v0_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&v1_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&v2_dev, sizeof(F2_967_u64)*number);

  cudaMalloc((void**)&vST0_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST1_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST2_dev, sizeof(F2_967_u64)*number);

  cudaMemcpy(a0_dev, a0, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a1_dev, a1, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a2_dev, a2, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);

  cudaMemcpy(b0_dev, b0, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b1_dev, b1, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b2_dev, b2, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);

  dim3 blocks(number,1);
  dim3 threads(F2_967_u64::nUint,1);

  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(v0_dev, a0_dev, b0_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(v1_dev, a1_dev, b1_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(v2_dev, a2_dev, b2_dev);

  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST0_dev, a0_dev, a1_dev, b0_dev, b1_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST1_dev, a0_dev, a2_dev, b0_dev, b2_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST2_dev, a1_dev, a2_dev, b1_dev, b2_dev);

  cudaMemcpy(v0, v0_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v1, v1_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v2, v2_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);

  cudaMemcpy(vST0, vST0_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST1, vST1_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST2, vST2_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);

  for (unsigned int i = 0; i < number; ++i) {
    c[i].xF6[0].xBF[0] = v0[i];
    c[i].xF6[0].xBF[4] = v2[i];

    // c_0_1
    c[i].xF6[0].xBF[1] = vST0[i] - c[i].xF6[0].xBF[0];
    c[i].xF6[0].xBF[1] -= v1[i];

    // c_0_2
    c[i].xF6[0].xBF[2] = vST1[i] - c[i].xF6[0].xBF[0];
    c[i].xF6[0].xBF[2] -= c[i].xF6[0].xBF[4];
    c[i].xF6[0].xBF[2] += v1[i];

    // c_0_3
    c[i].xF6[0].xBF[3] = vST2[i] - v1[i];
    c[i].xF6[0].xBF[3] -= c[i].xF6[0].xBF[4];
  }

  //(a0,a1,a2)(b0,b1,b2)  + (a1b4 + a2b4 + a4b1 + a4b2)w^5 + (a0b4 + a4b0)w^4 + (a2b4 + a4b2)w^3 + (a2b4 + a4b2)w^2 + (a4b4)w + a2b4 + a4b2 + a4b4
  // (fix) v1 = a4b4
  mulCombL2RwithWindow_multipleP<F2_967_u64, wSize>(v1, a4, b4, number);

  // (fix) vST[0] = a2b4 + a4b2
  mulCombL2RwithWindow_multipleP<F2_967_u64, wSize>(vST0, a2, b4, number);
  mulCombL2RwithWindow_multipleP<F2_967_u64, wSize>(vST1, a4, b2, number);

  for (unsigned int i = 0; i < number; ++i) vST0[i] += vST1[i];

  // c_0_5 = a1b4（初代入）
  mulCombL2RwithWindow_multipleP<F2_967_u64, wSize>(vST2, a1, b4, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xF6[0].xBF[5] = vST2[i];

  mulCombL2RwithWindow_multipleP<F2_967_u64, wSize>(vST1, a4, b1, number);

  for (unsigned int i = 0; i < number; ++i) {

    // c_0_5 = a1b4 + a4b1
    c[i].xF6[0].xBF[5] += vST1[i];
    // (fix) c_0_5 = a1b4 + a4b1 + a2b4 + a4b2
    c[i].xF6[0].xBF[5] += vST0[i];

    // (fix) c_0_3 += a2b4 + a4b2
    c[i].xF6[0].xBF[3] += vST0[i];

    // (fix) c_0_2 += a2b4 + a4b2
    c[i].xF6[0].xBF[2] += vST0[i];

    // (fix) c_0_1 += a4b4
    c[i].xF6[0].xBF[1] += v1[i];

    // c_0_0 += a2b4 + a4b2
    c[i].xF6[0].xBF[0] += vST0[i];
    // (fix) c_0_0 += a4b4
    c[i].xF6[0].xBF[0] += v1[i];
  }

  mulCombL2RwithWindow_multipleP<F2_967_u64, wSize>(vST1, a0, b4, number);
  // c_0_4 += a0b4
  for (unsigned int i = 0; i < number; ++i) c[i].xF6[0].xBF[4] += vST1[i];

  mulCombL2RwithWindow_multipleP<F2_967_u64, wSize>(vST1, a4, b0, number);
  // (fix) c_0_4 += a4b0
  for (unsigned int i = 0; i < number; ++i) c[i].xF6[0].xBF[4] += vST1[i];

  for (unsigned int i = 0; i < number; ++i) {
    // c_0 + w^5 + w^3
    c[i].xF6[0].xBF[3].plusOne();
    c[i].xF6[0].xBF[5].plusOne();
  }
  delete[] a0;
  delete[] a1;
  delete[] a2;
  delete[] a4;

  delete[] b0;
  delete[] b1;
  delete[] b2;
  delete[] b4;

  delete[] v0;
  delete[] v1;
  delete[] v2;

  delete[] vST0;
  delete[] vST1;
  delete[] vST2;

  cudaFree(a0_dev);
  cudaFree(a1_dev);
  cudaFree(a2_dev);

  cudaFree(b0_dev);
  cudaFree(b1_dev);
  cudaFree(b2_dev);

  cudaFree(v0_dev);
  cudaFree(v1_dev);
  cudaFree(v2_dev);

  cudaFree(vST0_dev);
  cudaFree(vST1_dev);
  cudaFree(vST2_dev);
}
template void alpha_beta6_2_withWindow_multipleP<3>(ExtField6_2<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const ExtField6<F2_967_u64> *b, const int number);
template void alpha_beta6_2_withWindow_multipleP<4>(ExtField6_2<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const ExtField6<F2_967_u64> *b, const int number);
template void alpha_beta6_2_withWindow_multipleP<5>(ExtField6_2<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const ExtField6<F2_967_u64> *b, const int number);
template void alpha_beta6_2_withWindow_multipleP<6>(ExtField6_2<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const ExtField6<F2_967_u64> *b, const int number);
template void alpha_beta6_2_withWindow_multipleP<7>(ExtField6_2<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const ExtField6<F2_967_u64> *b, const int number);
template void alpha_beta6_2_withWindow_multipleP<8>(ExtField6_2<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const ExtField6<F2_967_u64> *b, const int number);
//template void alpha_beta6_2_withWindow_multipleP<9>(ExtField6_2<F2_967_u64> *c, const ExtField6<F2_967_u64> *a, const ExtField6<F2_967_u64> *b, const int number);



//Karatsuba 3
/* 一つのkernel函数で各aST, bSTをdeviceで計算した後乗算vSTを行う */
template<int wSize>
void mulCombL2RwithWindow3_multipleP(ExtField3<F2_967_u64> *c, const ExtField3<F2_967_u64> *a, const ExtField3<F2_967_u64> *b, const int number) {

  ExtField_Double< ExtField3<F2_967_u64> > *result = new ExtField_Double< ExtField3<F2_967_u64> >[number];

  F2_967_u64 *a0 = new F2_967_u64[number];
  F2_967_u64 *a1 = new F2_967_u64[number];
  F2_967_u64 *a2 = new F2_967_u64[number];

  F2_967_u64 *b0 = new F2_967_u64[number];
  F2_967_u64 *b1 = new F2_967_u64[number];
  F2_967_u64 *b2 = new F2_967_u64[number];

  F2_967_u64 *v0 = new F2_967_u64[number];
  F2_967_u64 *v1 = new F2_967_u64[number];
  F2_967_u64 *v2 = new F2_967_u64[number];

  F2_967_u64 *vST0 = new F2_967_u64[number];
  F2_967_u64 *vST1 = new F2_967_u64[number];
  F2_967_u64 *vST2 = new F2_967_u64[number];

  // copy to BF array
  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xBF[0];
    a1[i] = a[i].xBF[1];
    a2[i] = a[i].xBF[2];

    b0[i] = b[i].xBF[0];
    b1[i] = b[i].xBF[1];
    b2[i] = b[i].xBF[2];
  }



  F2_967_u64 *a0_dev, *a1_dev, *a2_dev;
  F2_967_u64 *b0_dev, *b1_dev, *b2_dev;
  F2_967_u64 *v0_dev, *v1_dev, *v2_dev;
  F2_967_u64 *vST0_dev, *vST1_dev, *vST2_dev;

  cudaMalloc((void**)&a0_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&a1_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&a2_dev, sizeof(F2_967_u64)*number);

  cudaMalloc((void**)&b0_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&b1_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&b2_dev, sizeof(F2_967_u64)*number);

  cudaMalloc((void**)&v0_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&v1_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&v2_dev, sizeof(F2_967_u64)*number);

  cudaMalloc((void**)&vST0_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST1_dev, sizeof(F2_967_u64)*number);
  cudaMalloc((void**)&vST2_dev, sizeof(F2_967_u64)*number);

  cudaMemcpy(a0_dev, a0, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a1_dev, a1, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(a2_dev, a2, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);

  cudaMemcpy(b0_dev, b0, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b1_dev, b1, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);
  cudaMemcpy(b2_dev, b2, sizeof(F2_967_u64)*number, cudaMemcpyHostToDevice);

  dim3 blocks(number,1);
  dim3 threads(F2_967_u64::nUint,1);

  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(v0_dev, a0_dev, b0_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(v1_dev, a1_dev, b1_dev);
  mulCombL2RwithWindow_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(v2_dev, a2_dev, b2_dev);

  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST0_dev, a0_dev, a1_dev, b0_dev, b1_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST1_dev, a0_dev, a2_dev, b0_dev, b2_dev);
  AddMul_for_Karatsuba_multipleKernel<F2_967_u64, wSize><<< blocks, threads >>>(vST2_dev, a1_dev, a2_dev, b1_dev, b2_dev);

  cudaMemcpy(v0, v0_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v1, v1_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(v2, v2_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);

  cudaMemcpy(vST0, vST0_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST1, vST1_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);
  cudaMemcpy(vST2, vST2_dev, sizeof(F2_967_u64)*number, cudaMemcpyDeviceToHost);



  for (unsigned int i = 0; i < number; ++i) {
    // res[0] = a[0]b[0]
    result[i].low.xBF[0] = v0[i];

    //res[4] = a[2]b[2]
    result[i].high.xBF[1] = v2[i];

    // res[1]
    result[i].low.xBF[1] = vST0[i] - result[i].low.xBF[0];
    result[i].low.xBF[1] -= v1[i];

    // res[2]
    result[i].low.xBF[2] = vST1[i] - result[i].low.xBF[0];
    result[i].low.xBF[2] -= result[i].high.xBF[1];
    result[i].low.xBF[2] += v1[i];

    // res[3]
    result[i].high.xBF[0] = vST2[i] - v1[i];
    result[i].high.xBF[0] -= result[i].high.xBF[1];

    c[i].reduction(result[i]);
  }

  delete[] result;

  delete[] a0;
  delete[] a1;
  delete[] a2;

  delete[] b0;
  delete[] b1;
  delete[] b2;

  delete[] v0;
  delete[] v1;
  delete[] v2;

  delete[] vST0;
  delete[] vST1;
  delete[] vST2;

  cudaFree(a0_dev);
  cudaFree(a1_dev);
  cudaFree(a2_dev);

  cudaFree(b0_dev);
  cudaFree(b1_dev);
  cudaFree(b2_dev);

  cudaFree(v0_dev);
  cudaFree(v1_dev);
  cudaFree(v2_dev);

  cudaFree(vST0_dev);
  cudaFree(vST1_dev);
  cudaFree(vST2_dev);
}
template void mulCombL2RwithWindow3_multipleP<3>(ExtField3<F2_967_u64> *c, const ExtField3<F2_967_u64> *a, const ExtField3<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow3_multipleP<4>(ExtField3<F2_967_u64> *c, const ExtField3<F2_967_u64> *a, const ExtField3<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow3_multipleP<5>(ExtField3<F2_967_u64> *c, const ExtField3<F2_967_u64> *a, const ExtField3<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow3_multipleP<6>(ExtField3<F2_967_u64> *c, const ExtField3<F2_967_u64> *a, const ExtField3<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow3_multipleP<7>(ExtField3<F2_967_u64> *c, const ExtField3<F2_967_u64> *a, const ExtField3<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow3_multipleP<8>(ExtField3<F2_967_u64> *c, const ExtField3<F2_967_u64> *a, const ExtField3<F2_967_u64> *b, const int number);
//template void mulCombL2RwithWindow3_multipleP<9>(ExtField3<F2_967_u64> *c, const ExtField3<F2_967_u64> *a, const ExtField3<F2_967_u64> *b, const int number);

void square3_multipleP(ExtField3<F2_967_u64> *c, const ExtField3<F2_967_u64> *a, const int number) {

  F2_967_u64 *a0 = new F2_967_u64[number];
  F2_967_u64 *a1 = new F2_967_u64[number];
  F2_967_u64 *a2 = new F2_967_u64[number];
  F2_967_u64 *tmp = new F2_967_u64[number];

  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xBF[0];
    a1[i] = a[i].xBF[1];
    a2[i] = a[i].xBF[2];
  }

  // c_2 = a_1^2
  square_multipleP(tmp, a1, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[2] = tmp[i];

  // c_1 = a_2^2
  square_multipleP(tmp, a2, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[1] += tmp[i];
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[2] += tmp[i];

  // (fix) c_0 = a_0^2
  square_multipleP(tmp, a0, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xBF[0] = tmp[i];

  delete[] a0;
  delete[] a1;
  delete[] a2;
  delete[] tmp;
}

template<int wSize>
void mulCombL2RwithWindow3_2_multipleP(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const int number) {

  ExtField_Double< ExtField3_2<F2_967_u64> > *result = new ExtField_Double< ExtField3_2<F2_967_u64> >[number];
  ExtField3<F2_967_u64> *a0 = new ExtField3<F2_967_u64>[number];
  ExtField3<F2_967_u64> *a1 = new ExtField3<F2_967_u64>[number];
  ExtField3<F2_967_u64> *b0 = new ExtField3<F2_967_u64>[number];
  ExtField3<F2_967_u64> *b1 = new ExtField3<F2_967_u64>[number];
  ExtField3<F2_967_u64> *aST = new ExtField3<F2_967_u64>[number];
  ExtField3<F2_967_u64> *bST = new ExtField3<F2_967_u64>[number];
  ExtField3<F2_967_u64> *tmp3 = new ExtField3<F2_967_u64>[number];

  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xF3[0];
    a1[i] = a[i].xF3[1];
    b0[i] = b[i].xF3[0];
    b1[i] = b[i].xF3[1];
  }

  for (unsigned int i = 0; i < number; ++i) {
    aST[i] = a[i].xF3[0] + a[i].xF3[1];
    bST[i] = b[i].xF3[0] + b[i].xF3[1];
  }

  mulCombL2RwithWindow3_multipleP<wSize>(tmp3, a0, b0, number);
  for (unsigned int i = 0; i < number; ++i) result[i].low.xF3[0] = tmp3[i];

  mulCombL2RwithWindow3_multipleP<wSize>(tmp3, a1, b1, number);
  for (unsigned int i = 0; i < number; ++i) result[i].high.xF3[0] = tmp3[i];

  mulCombL2RwithWindow3_multipleP<wSize>(tmp3, aST, bST, number);
  for (unsigned int i = 0; i < number; ++i) result[i].low.xF3[1] = tmp3[i];

  for (unsigned int i = 0; i < number; ++i) {
    result[i].low.xF3[1] -= result[i].low.xF3[0];
    result[i].low.xF3[1] -= result[i].high.xF3[0];

    c[i].reduction(result[i]);
  }

  delete[] result;
  delete[] a0;
  delete[] a1;
  delete[] b0;
  delete[] b1;
  delete[] aST;
  delete[] bST;
  delete[] tmp3;
}
template void mulCombL2RwithWindow3_2_multipleP<3>(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_multipleP<4>(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_multipleP<5>(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_multipleP<6>(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_multipleP<7>(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_multipleP<8>(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const int number);
//template void mulCombL2RwithWindow3_2_multipleP<9>(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const int number);

template<int wSize>
void mul_sp_CombL2RwithWindow3_2_multipleP(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const int number) {

  ExtField3<F2_967_u64> *a0 = new ExtField3<F2_967_u64>[number];
  ExtField3<F2_967_u64> *a1 = new ExtField3<F2_967_u64>[number];
  ExtField3<F2_967_u64> *b0 = new ExtField3<F2_967_u64>[number];
  ExtField3<F2_967_u64> *tmp3 = new ExtField3<F2_967_u64>[number];

  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xF3[0];
    a1[i] = a[i].xF3[1];
    b0[i] = b[i].xF3[0];
  }

  mulCombL2RwithWindow3_multipleP<wSize>(tmp3, a0, b0, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xF3[0] = tmp3[i];

  mulCombL2RwithWindow3_multipleP<wSize>(tmp3, a1, b0, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xF3[1] = tmp3[i];


  delete[] a0;
  delete[] a1;
  delete[] b0;
  delete[] tmp3;
}
template void mul_sp_CombL2RwithWindow3_2_multipleP<3>(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const int number);
template void mul_sp_CombL2RwithWindow3_2_multipleP<4>(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const int number);
template void mul_sp_CombL2RwithWindow3_2_multipleP<5>(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const int number);
template void mul_sp_CombL2RwithWindow3_2_multipleP<6>(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const int number);
template void mul_sp_CombL2RwithWindow3_2_multipleP<7>(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const int number);
template void mul_sp_CombL2RwithWindow3_2_multipleP<8>(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const int number);
//template void mul_sp_CombL2RwithWindow3_2_multipleP<9>(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const int number);

void square3_2_multipleP(ExtField3_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const int number) {

  ExtField3<F2_967_u64> *a0 = new ExtField3<F2_967_u64>[number];
  ExtField3<F2_967_u64> *a1 = new ExtField3<F2_967_u64>[number];
  ExtField3<F2_967_u64> *tmp = new ExtField3<F2_967_u64>[number];

  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xF3[0];
    a1[i] = a[i].xF3[1];
  }

  square3_multipleP(tmp, a1, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xF3[1] = tmp[i];

  for (unsigned int i = 0; i < number; ++i) {
    ExtField3<F2_967_u64>::mulConstTerm3(c[i].xF3[0], c[i].xF3[1]);
    c[i].xF3[0] += c[i].xF3[1];
  }

  square3_multipleP(tmp, a0, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xF3[0] += tmp[i];

  delete[] a0;
  delete[] a1;
  delete[] tmp;
}

template<int wSize>
void mulCombL2RwithWindow3_2_2_multipleP(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const ExtField3_2_2<F2_967_u64> *b, const int number) {

  ExtField_Double< ExtField3_2_2<F2_967_u64> > *result = new ExtField_Double< ExtField3_2_2<F2_967_u64> >[number];
  ExtField3_2<F2_967_u64> *a0 = new ExtField3_2<F2_967_u64>[number];
  ExtField3_2<F2_967_u64> *a1 = new ExtField3_2<F2_967_u64>[number];
  ExtField3_2<F2_967_u64> *b0 = new ExtField3_2<F2_967_u64>[number];
  ExtField3_2<F2_967_u64> *b1 = new ExtField3_2<F2_967_u64>[number];
  ExtField3_2<F2_967_u64> *aST = new ExtField3_2<F2_967_u64>[number];
  ExtField3_2<F2_967_u64> *bST = new ExtField3_2<F2_967_u64>[number];
  ExtField3_2<F2_967_u64> *tmp3_2 = new ExtField3_2<F2_967_u64>[number];

  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xF3_2[0];
    a1[i] = a[i].xF3_2[1];
    b0[i] = b[i].xF3_2[0];
    b1[i] = b[i].xF3_2[1];
  }

  for (unsigned int i = 0; i < number; ++i) {
    aST[i] = a[i].xF3_2[0] + a[i].xF3_2[1];
    bST[i] = b[i].xF3_2[0] + b[i].xF3_2[1];
  }

  mulCombL2RwithWindow3_2_multipleP<wSize>(tmp3_2, a0, b0, number);
  for (unsigned int i = 0; i < number; ++i) result[i].low.xF3_2[0] = tmp3_2[i];

  mulCombL2RwithWindow3_2_multipleP<wSize>(tmp3_2, a1, b1, number);
  for (unsigned int i = 0; i < number; ++i) result[i].high.xF3_2[0] = tmp3_2[i];

  mulCombL2RwithWindow3_2_multipleP<wSize>(tmp3_2, aST, bST, number);
  for (unsigned int i = 0; i < number; ++i) result[i].low.xF3_2[1] = tmp3_2[i];

  for (unsigned int i = 0; i < number; ++i) {
    result[i].low.xF3_2[1] -= result[i].low.xF3_2[0];
    result[i].low.xF3_2[1] -= result[i].high.xF3_2[0];

    c[i].reduction(result[i]);
  }

  delete[] result;
  delete[] a0;
  delete[] a1;
  delete[] b0;
  delete[] b1;
  delete[] aST;
  delete[] bST;
  delete[] tmp3_2;
}
template void mulCombL2RwithWindow3_2_2_multipleP<3>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const ExtField3_2_2<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_2_multipleP<4>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const ExtField3_2_2<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_2_multipleP<5>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const ExtField3_2_2<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_2_multipleP<6>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const ExtField3_2_2<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_2_multipleP<7>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const ExtField3_2_2<F2_967_u64> *b, const int number);
template void mulCombL2RwithWindow3_2_2_multipleP<8>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const ExtField3_2_2<F2_967_u64> *b, const int number);
//template void mulCombL2RwithWindow3_2_2_multipleP<9>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const ExtField3_2_2<F2_967_u64> *b, const int number);

template<int wSize>
void mul_sp_CombL2RwithWindow3_2_2_multipleP(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const ExtField3_2_2<F2_967_u64> *b, const int number) {

  ExtField_Double< ExtField3_2_2<F2_967_u64> > *result = new ExtField_Double< ExtField3_2_2<F2_967_u64> >[number];
  ExtField3_2<F2_967_u64> *a0 = new ExtField3_2<F2_967_u64>[number];
  ExtField3_2<F2_967_u64> *a1 = new ExtField3_2<F2_967_u64>[number];
  ExtField3_2<F2_967_u64> *b0 = new ExtField3_2<F2_967_u64>[number];
  ExtField3_2<F2_967_u64> *b1 = new ExtField3_2<F2_967_u64>[number];
  ExtField3_2<F2_967_u64> *aST = new ExtField3_2<F2_967_u64>[number];
  ExtField3_2<F2_967_u64> *bST = new ExtField3_2<F2_967_u64>[number];
  ExtField3_2<F2_967_u64> *tmp3_2 = new ExtField3_2<F2_967_u64>[number];

  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xF3_2[0];
    a1[i] = a[i].xF3_2[1];
    b0[i] = b[i].xF3_2[0];
    b1[i] = b[i].xF3_2[1];
  }

  for (unsigned int i = 0; i < number; ++i) {
    aST[i] = a[i].xF3_2[0] + a[i].xF3_2[1];
    bST[i] = b[i].xF3_2[0] + b[i].xF3_2[1];
  }

  mulCombL2RwithWindow3_2_multipleP<wSize>(tmp3_2, a0, b0, number);
  for (unsigned int i = 0; i < number; ++i) result[i].low.xF3_2[0] = tmp3_2[i];

  mul_sp_CombL2RwithWindow3_2_multipleP<wSize>(tmp3_2, a1, b1, number);
  for (unsigned int i = 0; i < number; ++i) result[i].high.xF3_2[0] = tmp3_2[i];

  mulCombL2RwithWindow3_2_multipleP<wSize>(tmp3_2, aST, bST, number);
  for (unsigned int i = 0; i < number; ++i) result[i].low.xF3_2[1] = tmp3_2[i];

  for (unsigned int i = 0; i < number; ++i) {
    result[i].low.xF3_2[1] -= result[i].low.xF3_2[0];
    result[i].low.xF3_2[1] -= result[i].high.xF3_2[0];

    c[i].reduction(result[i]);
  }

  delete[] result;
  delete[] a0;
  delete[] a1;
  delete[] b0;
  delete[] b1;
  delete[] aST;
  delete[] bST;
  delete[] tmp3_2;
}
template void mul_sp_CombL2RwithWindow3_2_2_multipleP<3>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const ExtField3_2_2<F2_967_u64> *b, const int number);
template void mul_sp_CombL2RwithWindow3_2_2_multipleP<4>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const ExtField3_2_2<F2_967_u64> *b, const int number);
template void mul_sp_CombL2RwithWindow3_2_2_multipleP<5>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const ExtField3_2_2<F2_967_u64> *b, const int number);
template void mul_sp_CombL2RwithWindow3_2_2_multipleP<6>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const ExtField3_2_2<F2_967_u64> *b, const int number);
template void mul_sp_CombL2RwithWindow3_2_2_multipleP<7>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const ExtField3_2_2<F2_967_u64> *b, const int number);
template void mul_sp_CombL2RwithWindow3_2_2_multipleP<8>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const ExtField3_2_2<F2_967_u64> *b, const int number);
//template void mul_sp_CombL2RwithWindow3_2_2_multipleP<9>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const ExtField3_2_2<F2_967_u64> *b, const int number);

void square3_2_2_multipleP(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2_2<F2_967_u64> *a, const int number) {

  ExtField3_2<F2_967_u64> *a0 = new ExtField3_2<F2_967_u64>[number];
  ExtField3_2<F2_967_u64> *a1 = new ExtField3_2<F2_967_u64>[number];
  ExtField3_2<F2_967_u64> *tmp = new ExtField3_2<F2_967_u64>[number];

  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xF3_2[0];
    a1[i] = a[i].xF3_2[1];
  }

  square3_2_multipleP(tmp, a1, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xF3_2[1] = tmp[i];

  for (unsigned int i = 0; i < number; ++i) ExtField3_2<F2_967_u64>::mulConstTerm3_2(c[i].xF3_2[0], c[i].xF3_2[1]);

  square3_2_multipleP(tmp, a0, number);
  for (unsigned int i = 0; i < number; ++i) c[i].xF3_2[0] += tmp[i];

  delete[] a0;
  delete[] a1;
  delete[] tmp;
}

template<int wSize>
void alpha_beta3_2_2_withWindow_multipleP(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const F2_967_u64 *d, const int number) {

  ExtField3<F2_967_u64> *fg0 = new ExtField3<F2_967_u64>[number];
  ExtField3<F2_967_u64> *a0 = new ExtField3<F2_967_u64>[number];
  ExtField3<F2_967_u64> *b0 = new ExtField3<F2_967_u64>[number];
  ExtField3<F2_967_u64> *tmpF3 = new ExtField3<F2_967_u64>[number];

  F2_967_u64 *aF3_1_0 = new F2_967_u64[number];
  F2_967_u64 *tmpF3_0 = new F2_967_u64[number];
  F2_967_u64 *tmpF3_1 = new F2_967_u64[number];
  F2_967_u64 *tmpF3_2 = new F2_967_u64[number];
  F2_967_u64 *fg1_0 = new F2_967_u64[number];
  F2_967_u64 *fg1_1 = new F2_967_u64[number];
  F2_967_u64 *fg1_2 = new F2_967_u64[number];

  for (unsigned int i = 0; i < number; ++i) aF3_1_0[i] = a[i].xF3[1].xBF[0];

  // fg
  // c_0 := (a0,a1,a2)(b0,b1,b2)
  for (unsigned int i = 0; i < number; ++i) {
    a0[i] = a[i].xF3[0];
    b0[i] = b[i].xF3[0];
  }

  mulCombL2RwithWindow3_multipleP<wSize>(fg0, a0, b0, number);
  for (unsigned int i = 0; i < number; ++i) {
    fg0[i].xBF[0] += d[i];
    fg0[i].xBF[1] += d[i];
  }

  for (unsigned int i = 0; i < number; ++i) {
    tmpF3[i] = a[i].xF3[0] + b[i].xF3[0];
    // (c_2,c_3) := f + g = (a0+b0,a1+b1,a2+b2,0,0,0)
    c[i].xF3_2[1].xF3[0] = tmpF3[i];
    c[i].xF3_2[1].xF3[1].clear();
    // (fix) (c_2,c_3) = f + g + 1
    c[i].xF3_2[1].plusOne();
  }


  // tmpF3 = (a0+b0,a1+b1,a2+b2)
  for (unsigned int i = 0; i < number; ++i) {
    tmpF3_0[i] = tmpF3[i].xBF[0];
    tmpF3_1[i] = tmpF3[i].xBF[1];
    tmpF3_2[i] = tmpF3[i].xBF[2];
  }

  mulCombL2RwithWindow_multipleP<F2_967_u64, wSize>(fg1_0, aF3_1_0, tmpF3_0, number);
  mulCombL2RwithWindow_multipleP<F2_967_u64, wSize>(fg1_1, aF3_1_0, tmpF3_1, number);
  mulCombL2RwithWindow_multipleP<F2_967_u64, wSize>(fg1_2, aF3_1_0, tmpF3_2, number);

  for (unsigned int i = 0; i < number; ++i) fg1_0[i] += d[i];

  //(c_0,c_1) = fg + s + sw^2
  for (unsigned int i = 0; i < number; ++i) {
    c[i].xF3_2[0].xF3[0] = fg0[i];
    c[i].xF3_2[0].xF3[1].xBF[0] = fg1_0[i];
    c[i].xF3_2[0].xF3[1].xBF[1] = fg1_1[i];
    c[i].xF3_2[0].xF3[1].xBF[2] = fg1_2[i];

    c[i].xF3_2[0].xF3[1].xBF[0].plusOne();
    c[i].xF3_2[0].xF3[1].xBF[2].plusOne();
  }

  delete[] fg0;
  delete[] a0;
  delete[] b0;
  delete[] tmpF3;
  delete[] aF3_1_0;
  delete[] tmpF3_0;
  delete[] tmpF3_1;
  delete[] tmpF3_2;
  delete[] fg1_0;
  delete[] fg1_1;
  delete[] fg1_2;
}
template void alpha_beta3_2_2_withWindow_multipleP<3>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const F2_967_u64 *d, const int number);
template void alpha_beta3_2_2_withWindow_multipleP<4>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const F2_967_u64 *d, const int number);
template void alpha_beta3_2_2_withWindow_multipleP<5>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const F2_967_u64 *d, const int number);
template void alpha_beta3_2_2_withWindow_multipleP<6>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const F2_967_u64 *d, const int number);
template void alpha_beta3_2_2_withWindow_multipleP<7>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const F2_967_u64 *d, const int number);
template void alpha_beta3_2_2_withWindow_multipleP<8>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const F2_967_u64 *d, const int number);
//template void alpha_beta3_2_2_withWindow_multipleP<9>(ExtField3_2_2<F2_967_u64> *c, const ExtField3_2<F2_967_u64> *a, const ExtField3_2<F2_967_u64> *b, const F2_967_u64 *d, const int number);
