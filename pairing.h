#pragma once

#include<iostream>

#include "F2_487.h"
#include "ext_field6_2.h"
#include "ext_field3_2_2.h"

template<class Basefield>
__host__ __device__
ExtField6_2<Basefield> etaT_pairing6_2(const Basefield& xp0, const Basefield& xq0, const Basefield& yp0, const Basefield& yq0) {

  const int degBF = Basefield::ext_degree;

  ExtField6_2<Basefield> pairingValue, alphaBeta, tmp6_2;
  pairingValue.clear();
  pairingValue.plusOne();

  ExtField6<Basefield> alpha, beta, b4_F6;
  alpha.clear();
  beta.clear();
  b4_F6.clear();

  Basefield xpBF, ypBF, tmpBF_1;
  Basefield xP[degBF], xQ[degBF], yP[degBF], yQ[degBF];

  unsigned int k1, k2, k3, k4, k5, k6;

  // precomputation
  xP[0] = xp0;
  xQ[0] = xq0;
  yP[0] = yp0;
  yQ[0] = yq0;

  for (int i = 1; i < degBF; ++i) {

    Basefield::square(xP[i], xP[i - 1]);
    Basefield::square(xQ[i], xQ[i - 1]);
    Basefield::square(yP[i], yP[i - 1]);
    Basefield::square(yQ[i], yQ[i - 1]);
  }

  //for (unsigned int i = 0; i < degBF; ++i) {
  //  std::cout << "xP[" << i << "]:" << xP[i];
  //}

  // Miller loop
  for (int i = 0; i < (degBF - 3) / 2; ++i) {


    k1 = ((3 * degBF - 9 - 6 * i) / 2) % degBF;
    k2 = (k1 + 1) % degBF;
    k3 = (k2 + 1) % degBF;
    k4 = ((3 * degBF - 3 - 6 * i) / 2) % degBF;
    k5 = (k4 + 1) % degBF;
    k6 = (k5 + 1) % degBF;

    // compute \alpha = (alpha,1)
    // alpha = a0 + a1w + a2w^2 + a4w^4

    // a4 = xP[k4] + xP[k5]
    alpha.xBF[4] = xP[k4] + xP[k5];

    // a2 = xP[k4] + 1 + xQ[k3]
    alpha.xBF[2] = xP[k4] + xQ[k3];
    alpha.xBF[2].plusOne();

    // a1 = xQ[k2] + xQ[k3]
    alpha.xBF[1] = xQ[k2] + xQ[k3];

    //a0 = yQ[k2] + a2xQ[k2] + a4xQ[k3] + yP[k4]
    Basefield::mulCombR2L_uintSize(tmpBF_1, alpha.xBF[2], xQ[k2]);
    alpha.xBF[0] = yQ[k2] + tmpBF_1;
    Basefield::mulCombR2L_uintSize(tmpBF_1, alpha.xBF[4], xQ[k3]);
    alpha.xBF[0] += tmpBF_1;
    alpha.xBF[0] += yP[k4];


    // compute \beta = (beta,1)
    // beta = b0 + b1w + b2w^2 + b4w^4

    // b1 = xP[k5] + xP[k6]
    beta.xBF[1] = xP[k5] + xP[k6];

    // b2 = xP[k6] + xQ[k1] + 1
    beta.xBF[2] = xP[k6] + xQ[k1];
    beta.xBF[2].plusOne();

    // b4 = xQ[k1] + xQ[k2] + 1
    beta.xBF[4] = xQ[k1] + xQ[k2];
    beta.xBF[4].plusOne();

    // b0 = yQ[k1] + b1xQ[k1] + yP[k5] + xP[k6](xP[k5] + xQ[k2]) + xP[k5]
    Basefield::mulCombR2L_uintSize(tmpBF_1, beta.xBF[1], xQ[k1]);
    beta.xBF[0] = yQ[k1] + tmpBF_1;
    beta.xBF[0] += yP[k5];
    beta.xBF[0] += xP[k5];
    Basefield::mulCombR2L_uintSize( tmpBF_1, xP[k6], xP[k5] + xQ[k2] );
    beta.xBF[0] += tmpBF_1;

    // \alpha\beta
    ExtField6_2<Basefield>::alpha_beta6_2(alphaBeta, alpha, beta);

    ExtField6_2<Basefield>::mulCombR2L_uintSize6_2(tmp6_2, pairingValue, alphaBeta);
    pairingValue.copy(tmp6_2);

  }

  // extract current point (xP,yP)
  xpBF = xP[degBF - 3];
  xpBF.plusOne();
  ypBF = yP[degBF - 3] + xP[degBF - 2];

  // final doublings / addition
  /*
  b4_F6 = (yQ + xQ[1](1 + xQ + xpBF^8 + xpBF^4) + xpBF^4xQ + ypBF^4) +
          (xQ[1] + xpBF^4)w + (xpBF^8 + xpBF^4)w^2 + w^3 + (xQ[1] + xQ)w^4
  */

  // b4_3
  b4_F6.xBF[3].plusOne();

  // b4_4
  b4_F6.xBF[4] = xQ[1] + xq0;

  // xpBF := xpBF^4
  Basefield::square(tmpBF_1, xpBF);
  Basefield::square(xpBF, tmpBF_1);

  // b4_1
  b4_F6.xBF[1] = xQ[1] + tmpBF_1;

  // tmpBF_1 := xpBF^8
  Basefield::square(tmpBF_1, xpBF);

  // b4_2;
  b4_F6.xBF[2] = tmpBF_1 + xpBF;

  // b4_0
  // tmpBF_1 := b4_2 + xq0
  tmpBF_1 = b4_F6.xBF[2] + xq0;
  tmpBF_1.plusOne();
  Basefield::mulCombR2L_uintSize(b4_F6.xBF[0],xQ[1],tmpBF_1);

  //tmpBF_1 = xpBF^4xq0
  Basefield::mulCombR2L_uintSize(tmpBF_1, xpBF, xq0);
  b4_F6.xBF[0] += tmpBF_1;
  b4_F6.xBF[0] += yq0;

  // ypBF := ypBF^4
  Basefield::square(tmpBF_1, ypBF);
  Basefield::square(ypBF, tmpBF_1);
  b4_F6.xBF[0] += ypBF;

  // f^4
  ExtField6_2<Basefield>::square6_2(tmp6_2, pairingValue);
  ExtField6_2<Basefield>::square6_2(pairingValue, tmp6_2);

  // f^4(y+b4(x))
  ExtField6_2<Basefield>::sparseMul6_2(tmp6_2, pairingValue, b4_F6);
  pairingValue.copy(tmp6_2);


  // final exponentiation
  /* !! perform only (degBF+1)/2 squarings !! */

  for (int i = 0; i < (degBF + 1) / 2; ++i) {

    ExtField6_2<Basefield>::square6_2(tmp6_2, pairingValue);
    pairingValue.copy(tmp6_2);
  }

  return pairingValue;
}

// 3-2-2
/*\gamma_1未実装*/
template<class Basefield>
__host__ __device__
ExtField3_2_2<Basefield> etaT_pairing3_2_2(const Basefield& xp0, const Basefield& xq0, const Basefield& yp0, const Basefield& yq0) {

    const int degBF = Basefield::ext_degree;

    ExtField3_2_2<Basefield> pairingValue, alphaBeta, tmp3_2_2;
    pairingValue.clear();
    pairingValue.plusOne();

    ExtField3_2<Basefield> alpha, beta, b4_F6;
    alpha.clear();
    beta.clear();
    b4_F6.clear();

    Basefield xpBF, ypBF, d, tmpBF_1, tmpBF_2;
    Basefield xP[degBF], xQ[degBF], yP[degBF], yQ[degBF];

    unsigned int k1, k2, k3, k4, k5, k6;

    // precomputation
    xP[0] = xp0;
    xQ[0] = xq0;
    yP[0] = yp0;
    yQ[0] = yq0;

    for (int i = 1; i < degBF; ++i) {

      Basefield::square(xP[i], xP[i - 1]);
      Basefield::square(xQ[i], xQ[i - 1]);
      Basefield::square(yP[i], yP[i - 1]);
      Basefield::square(yQ[i], yQ[i - 1]);
    }
    // Miller loop
    for (int i = 0; i < (degBF - 3) / 2; ++i) {


        k1 = ((3 * degBF - 9 - 6 * i) / 2) % degBF;
        k2 = (k1 + 1) % degBF;
        k3 = (k2 + 1) % degBF;
        k4 = ((3 * degBF - 3 - 6 * i) / 2) % degBF;
        k5 = (k4 + 1) % degBF;
        k6 = (k5 + 1) % degBF;

        // compute \alpha = (alpha,1)
        // alpha = a0 + a1w + a2w^2 + a3s (a3=c)

        // a2 = xP[k4] + xP[k5]
        alpha.xF3[0].xBF[2] = xP[k4] + xP[k5];

        // a3 = xQ[k2] + xP[k5] + 1
        alpha.xF3[1].xBF[0] = xQ[k2] + xP[k5];

        // !!! use a3+1 for a0 !!!
        Basefield::mulCombR2L_uintSize(tmpBF_1, alpha.xF3[0].xBF[1], xQ[k3]); //tmpBF_1 = (a3 + 1)xQ[k3]
        alpha.xF3[0].plusOne();

        // a1 = xQ[k3] + xP[k5] + 1 + \gamma_1
        alpha.xF3[0].xBF[1] = xP[k5] + xQ[k3];
        alpha.xF3[0].xBF[1].plusOne();

        //a0 = yQ[k2] + (a3 + 1)xQ[k3] + (xP[k4] + 1 + xQ[k3])xQ[k2] + yP[k4] + xP[k4] + \gamma_1
        alpha.xF3[0].xBF[0] = yQ[k2] + tmpBF_1;
        alpha.xF3[0].xBF[0] += xP[k4];
        alpha.xF3[0].xBF[0] += yP[k4];
        tmpBF_1 = xP[k4] + xQ[k3];
        tmpBF_1.plusOne();
        Basefield::mulCombR2L_uintSize(tmpBF_2, tmpBF_1, xQ[k2]);
        alpha.xF3[0].xBF[0] += tmpBF_2;



        // compute \beta = (beta,1)
        // beta = b0 + b1w + b2w^2 + b3s (a3=b3=c)

        // b1 = xQ[k2] + xP[k6] + 1 + \gamma_1
        beta.xF3[0].xBF[1] = xQ[k2] + xP[k6];
        beta.xF3[0].xBF[1].plusOne();

        // b2 = xQ[k2] + xQ[k1] + \tgamma_1
        beta.xF3[0].xBF[2] = xQ[k2] + xQ[k1];

        // b0 = yQ[k1] + yP[k5] + xQ[k1](xP[k5] + xP[k6] + 1) + a3*xP[k6] + xP[k5]+ \gamma_1
        tmpBF_1 = xP[k6] + xP[k5];
        tmpBF_1.plusOne();
        Basefield::mulCombR2L_uintSize(beta.xF3[0].xBF[0], tmpBF_1, xQ[k1]);
        beta.xF3[0].xBF[0] += yQ[k1];
        beta.xF3[0].xBF[0] += yP[k5];
        beta.xF3[0].xBF[0] += xP[k5];
        Basefield::mulCombR2L_uintSize(tmpBF_1, xP[k6], alpha.xF3[1].xBF[0]);
        beta.xF3[0].xBF[0] += tmpBF_1;



        // \alpha\beta
        ExtField3_2_2<Basefield>::alpha_beta3_2_2(alphaBeta, alpha, beta, d);

        /* alphaBeta must be placed on the right side */
        ExtField3_2_2<Basefield>::mul_sp_CombR2L_uintSize3_2_2(tmp3_2_2, pairingValue, alphaBeta);
        //ExtField3_2_2<Basefield>::mulCombR2L_uintSize3_2_2(tmp3_2_2, pairingValue, alphaBeta);
        pairingValue.copy(tmp3_2_2);

    }

    // extract current point (xP,yP)
    xpBF = xP[degBF - 3];
    xpBF.plusOne();
    ypBF = yP[degBF - 3] + xP[degBF - 2];

    // final doublings / addition
    /*
    b4_F6 = (yQ + xQ[1](1 + xQ + xpBF^8 + xpBF^4) + xpBF^4xQ + ypBF^4) +
            (xQ[1] + xpBF^4)w + (xpBF^8 + xpBF^4)w^2 + w^3 + (xQ[1] + xQ)w^4
    */

    // b4_3
    b4_F6.xF3[0].xBF[2].plusOne();

    // b4_4
    b4_F6.xF3[0].xBF[1] = xQ[1] + xq0;

    // xpBF := xpBF^4
    Basefield::square(tmpBF_1, xpBF);
    Basefield::square(xpBF, tmpBF_1);

    // b4_1
    b4_F6.xF3[1].xBF[1] = xQ[1] + tmpBF_1;

    // tmpBF_1 := xpBF^8
    Basefield::square(tmpBF_1, xpBF);

    // b4_2;
    b4_F6.xF3[0].xBF[2] = tmpBF_1 + xpBF;

    // b4_0
    // tmpBF_1 := b4_2 + xq0
    tmpBF_1 = b4_F6.xF3[1].xBF[2] + xq0;
    tmpBF_1.plusOne();
    Basefield::mulCombR2L_uintSize(b4_F6.xF3[0].xBF[0],xQ[1],tmpBF_1);

    //tmpBF_1 = xpBF^4xq0
    Basefield::mulCombR2L_uintSize(tmpBF_1, xpBF, xq0);
    b4_F6.xF3[1].xBF[2] += tmpBF_1;
    b4_F6.xF3[1].xBF[0] += yq0;

    // ypBF := ypBF^4
    Basefield::square(tmpBF_1, ypBF);
    Basefield::square(ypBF, tmpBF_1);
    b4_F6.xF3[0].xBF[0] += ypBF;

    // f^4
    ExtField3_2_2<Basefield>::square3_2_2(tmp3_2_2, pairingValue);
    ExtField3_2_2<Basefield>::square3_2_2(pairingValue, tmp3_2_2);

    // f^4(y+b4(x))
    ExtField3_2_2<Basefield>::sparseMul3_2_2(tmp3_2_2, pairingValue, b4_F6);
    pairingValue.copy(tmp3_2_2);


    // final exponentiation
    /* !! perform only (degBF+1)/2 squarings !! */

    for (int i = 0; i < (degBF + 1) / 2; ++i) {

        ExtField3_2_2<Basefield>::square3_2_2(tmp3_2_2, pairingValue);
        pairingValue.copy(tmp3_2_2);
    }

    return pairingValue;
}



/* using window method */
template<class Basefield, int wSize>
__host__ __device__
ExtField6_2<Basefield> etaT_pairing6_2(const Basefield& xp0, const Basefield& xq0, const Basefield& yp0, const Basefield& yq0) {

  const int degBF = Basefield::ext_degree;

  ExtField6_2<Basefield> pairingValue, alphaBeta, tmp6_2;
  pairingValue.clear();
  pairingValue.plusOne();

  ExtField6<Basefield> alpha, beta, b4_F6;
  alpha.clear();
  beta.clear();
  b4_F6.clear();

  Basefield xpBF, ypBF, tmpBF_1;
  Basefield xP[degBF], xQ[degBF], yP[degBF], yQ[degBF];

  unsigned int k1, k2, k3, k4, k5, k6;

  // precomputation
  xP[0] = xp0;
  xQ[0] = xq0;
  yP[0] = yp0;
  yQ[0] = yq0;

  for (int i = 1; i < degBF; ++i) {

    Basefield::square(xP[i], xP[i - 1]);
    Basefield::square(xQ[i], xQ[i - 1]);
    Basefield::square(yP[i], yP[i - 1]);
    Basefield::square(yQ[i], yQ[i - 1]);
  }

  //for (unsigned int i = 0; i < degBF; ++i) {
  //  std::cout << "xP[" << i << "]:" << xP[i];
  //}

  // Miller loop
  for (int i = 0; i < (degBF - 3) / 2; ++i) {


    k1 = ((3 * degBF - 9 - 6 * i) / 2) % degBF;
    k2 = (k1 + 1) % degBF;
    k3 = (k2 + 1) % degBF;
    k4 = ((3 * degBF - 3 - 6 * i) / 2) % degBF;
    k5 = (k4 + 1) % degBF;
    k6 = (k5 + 1) % degBF;

    // compute \alpha = (alpha,1)
    // alpha = a0 + a1w + a2w^2 + a4w^4

    // a4 = xP[k4] + xP[k5]
    alpha.xBF[4] = xP[k4] + xP[k5];

    // a2 = xP[k4] + 1 + xQ[k3]
    alpha.xBF[2] = xP[k4] + xQ[k3];
    alpha.xBF[2].plusOne();

    // a1 = xQ[k2] + xQ[k3]
    alpha.xBF[1] = xQ[k2] + xQ[k3];

    //a0 = yQ[k2] + a2xQ[k2] + a4xQ[k3] + yP[k4]
    Basefield::template mulCombL2RwithWindow<wSize>(tmpBF_1, alpha.xBF[2], xQ[k2]);
    alpha.xBF[0] = yQ[k2] + tmpBF_1;
    Basefield::template mulCombL2RwithWindow<wSize>(tmpBF_1, alpha.xBF[4], xQ[k3]);
    alpha.xBF[0] += tmpBF_1;
    alpha.xBF[0] += yP[k4];


    // compute \beta = (beta,1)
    // beta = b0 + b1w + b2w^2 + b4w^4

    // b1 = xP[k5] + xP[k6]
    beta.xBF[1] = xP[k5] + xP[k6];

    // b2 = xP[k6] + xQ[k1] + 1
    beta.xBF[2] = xP[k6] + xQ[k1];
    beta.xBF[2].plusOne();

    // b4 = xQ[k1] + xQ[k2] + 1
    beta.xBF[4] = xQ[k1] + xQ[k2];
    beta.xBF[4].plusOne();

    // b0 = yQ[k1] + b1xQ[k1] + yP[k5] + xP[k6](xP[k5] + xQ[k2]) + xP[k5]
    Basefield::template mulCombL2RwithWindow<wSize>(tmpBF_1, beta.xBF[1], xQ[k1]);
    beta.xBF[0] = yQ[k1] + tmpBF_1;
    beta.xBF[0] += yP[k5];
    beta.xBF[0] += xP[k5];
    Basefield::template mulCombL2RwithWindow<wSize>( tmpBF_1, xP[k6], xP[k5] + xQ[k2] );
    beta.xBF[0] += tmpBF_1;

    // \alpha\beta
    ExtField6_2<Basefield>::template alpha_beta6_2_withWindow<wSize>(alphaBeta, alpha, beta);

    ExtField6_2<Basefield>::template mulCombL2RwithWindow6_2<wSize>(tmp6_2, pairingValue, alphaBeta);
    pairingValue.copy(tmp6_2);

  }

  // extract current point (xP,yP)
  xpBF = xP[degBF - 3];
  xpBF.plusOne();
  ypBF = yP[degBF - 3] + xP[degBF - 2];

  // final doublings / addition
  /*
  b4_F6 = (yQ + xQ[1](1 + xQ + xpBF^8 + xpBF^4) + xpBF^4xQ + ypBF^4) +
          (xQ[1] + xpBF^4)w + (xpBF^8 + xpBF^4)w^2 + w^3 + (xQ[1] + xQ)w^4
  */

  // b4_3
  b4_F6.xBF[3].plusOne();

  // b4_4
  b4_F6.xBF[4] = xQ[1] + xq0;

  // xpBF := xpBF^4
  Basefield::square(tmpBF_1, xpBF);
  Basefield::square(xpBF, tmpBF_1);

  // b4_1
  b4_F6.xBF[1] = xQ[1] + tmpBF_1;

  // tmpBF_1 := xpBF^8
  Basefield::square(tmpBF_1, xpBF);

  // b4_2;
  b4_F6.xBF[2] = tmpBF_1 + xpBF;

  // b4_0
  // tmpBF_1 := b4_2 + xq0
  tmpBF_1 = b4_F6.xBF[2] + xq0;
  tmpBF_1.plusOne();
  Basefield::template mulCombL2RwithWindow<wSize>(b4_F6.xBF[0],xQ[1],tmpBF_1);

  //tmpBF_1 = xpBF^4xq0
  Basefield::template mulCombL2RwithWindow<wSize>(tmpBF_1, xpBF, xq0);
  b4_F6.xBF[0] += tmpBF_1;
  b4_F6.xBF[0] += yq0;

  // ypBF := ypBF^4
  Basefield::square(tmpBF_1, ypBF);
  Basefield::square(ypBF, tmpBF_1);
  b4_F6.xBF[0] += ypBF;

  // f^4
  ExtField6_2<Basefield>::square6_2(tmp6_2, pairingValue);
  ExtField6_2<Basefield>::square6_2(pairingValue, tmp6_2);

  // f^4(y+b4(x))
  ExtField6_2<Basefield>::template sparseMul6_2_withWindow<wSize>(tmp6_2, pairingValue, b4_F6);
  pairingValue.copy(tmp6_2);


  // final exponentiation
  /* !! perform only (degBF+1)/2 squarings !! */

  for (int i = 0; i < (degBF + 1) / 2; ++i) {

    ExtField6_2<Basefield>::square6_2(tmp6_2, pairingValue);
    pairingValue.copy(tmp6_2);
  }

  return pairingValue;
}

// 3-2-2
/*\gamma_1未実装*/
template<class Basefield, int wSize>
__host__ __device__
ExtField3_2_2<Basefield> etaT_pairing3_2_2(const Basefield& xp0, const Basefield& xq0, const Basefield& yp0, const Basefield& yq0) {

    const int degBF = Basefield::ext_degree;
    //std::cout << 13;

    ExtField3_2_2<Basefield> pairingValue, alphaBeta, tmp3_2_2;
    pairingValue.clear();
    pairingValue.plusOne();

    ExtField3_2<Basefield> alpha, beta, b4_F6;
    alpha.clear();
    beta.clear();
    b4_F6.clear();

    Basefield xpBF, ypBF, d, tmpBF_1, tmpBF_2;
    Basefield xP[degBF], xQ[degBF], yP[degBF], yQ[degBF];

    unsigned int k1, k2, k3, k4, k5, k6;

    // precomputation
    xP[0] = xp0;
    xQ[0] = xq0;
    yP[0] = yp0;
    yQ[0] = yq0;

    for (int i = 1; i < degBF; ++i) {
      Basefield::square(xP[i], xP[i - 1]);
      Basefield::square(xQ[i], xQ[i - 1]);
      Basefield::square(yP[i], yP[i - 1]);
      Basefield::square(yQ[i], yQ[i - 1]);
    }

    // Miller loop
    for (int i = 0; i < (degBF - 3) / 2; ++i) {

        k1 = ((3 * degBF - 9 - 6 * i) / 2) % degBF;
        k2 = (k1 + 1) % degBF;
        k3 = (k2 + 1) % degBF;
        k4 = ((3 * degBF - 3 - 6 * i) / 2) % degBF;
        k5 = (k4 + 1) % degBF;
        k6 = (k5 + 1) % degBF;

        // compute \alpha = (alpha,1)
        // alpha = a0 + a1w + a2w^2 + a3s (a3=c)

        // a2 = xP[k4] + xP[k5]
        alpha.xF3[0].xBF[2] = xP[k4] + xP[k5];

        // a3 = xQ[k2] + xP[k5] + 1
        alpha.xF3[1].xBF[0] = xQ[k2] + xP[k5];

        // !!! use a3+1 for a0 !!!
        Basefield::template mulCombL2RwithWindow<wSize>(tmpBF_1, alpha.xF3[0].xBF[1], xQ[k3]); //tmpBF_1 = (a3 + 1)xQ[k3]
        alpha.xF3[0].plusOne();

        // a1 = xQ[k3] + xP[k5] + 1 + \gamma_1
        alpha.xF3[0].xBF[1] = xP[k5] + xQ[k3];
        alpha.xF3[0].xBF[1].plusOne();

        //a0 = yQ[k2] + (a3 + 1)xQ[k3] + (xP[k4] + 1 + xQ[k3])xQ[k2] + yP[k4] + xP[k4] + \gamma_1
        alpha.xF3[0].xBF[0] = yQ[k2] + tmpBF_1;
        alpha.xF3[0].xBF[0] += xP[k4];
        alpha.xF3[0].xBF[0] += yP[k4];
        tmpBF_1 = xP[k4] + xQ[k3];
        tmpBF_1.plusOne();
        Basefield::template mulCombL2RwithWindow<wSize>(tmpBF_2, tmpBF_1, xQ[k2]);
        alpha.xF3[0].xBF[0] += tmpBF_2;



        // compute \beta = (beta,1)
        // beta = b0 + b1w + b2w^2 + b3s (a3=b3=c)

        // b1 = xQ[k2] + xP[k6] + 1 + \gamma_1
        beta.xF3[0].xBF[1] = xQ[k2] + xP[k6];
        beta.xF3[0].xBF[1].plusOne();

        // b2 = xQ[k2] + xQ[k1] + \tgamma_1
        beta.xF3[0].xBF[2] = xQ[k2] + xQ[k1];

        // b0 = yQ[k1] + yP[k5] + xQ[k1](xP[k5] + xP[k6] + 1) + a3*xP[k6] + xP[k5]+ \gamma_1
        tmpBF_1 = xP[k6] + xP[k5];
        tmpBF_1.plusOne();
        Basefield::template mulCombL2RwithWindow<wSize>(beta.xF3[0].xBF[0], tmpBF_1, xQ[k1]);
        beta.xF3[0].xBF[0] += yQ[k1];
        beta.xF3[0].xBF[0] += yP[k5];
        beta.xF3[0].xBF[0] += xP[k5];
        Basefield::template mulCombL2RwithWindow<wSize>(tmpBF_1, xP[k6], alpha.xF3[1].xBF[0]);
        beta.xF3[0].xBF[0] += tmpBF_1;



        // \alpha\beta
        ExtField3_2_2<Basefield>::template alpha_beta3_2_2_withWindow<wSize>(alphaBeta, alpha, beta, d);

        /* alphaBeta must be placed on the right side */
        ExtField3_2_2<Basefield>::template mul_sp_CombL2RwithWindow3_2_2<wSize>(tmp3_2_2, pairingValue, alphaBeta);
        //ExtField3_2_2<Basefield>::template mulCombL2RwithWindow3_2_2<wSize>(tmp3_2_2, pairingValue, alphaBeta);
        pairingValue.copy(tmp3_2_2);

#if 0 // for debug
        if (i < 490) {
          std::cout << std::dec;
          std::cout << "i=" << i << std::endl;

          std::cout << alphaBeta;
          //std::cout << tmp3_2_2;
          //std::cout << pairingValue;
        }
#endif
    }

    // extract current point (xP,yP)
    xpBF = xP[degBF - 3];
    xpBF.plusOne();
    ypBF = yP[degBF - 3] + xP[degBF - 2];

    // final doublings / addition
    /*
    b4_F6 = (yQ + xQ[1](1 + xQ + xpBF^8 + xpBF^4) + xpBF^4xQ + ypBF^4) +
            (xQ[1] + xpBF^4)w + (xpBF^8 + xpBF^4)w^2 + w^3 + (xQ[1] + xQ)w^4
    */

    // b4_3
    b4_F6.xF3[0].xBF[2].plusOne();

    // b4_4
    b4_F6.xF3[0].xBF[1] = xQ[1] + xq0;

    // xpBF := xpBF^4
    Basefield::square(tmpBF_1, xpBF);
    Basefield::square(xpBF, tmpBF_1);

    // b4_1
    b4_F6.xF3[1].xBF[1] = xQ[1] + tmpBF_1;

    // tmpBF_1 := xpBF^8
    Basefield::square(tmpBF_1, xpBF);

    // b4_2;
    b4_F6.xF3[0].xBF[2] = tmpBF_1 + xpBF;

    // b4_0
    // tmpBF_1 := b4_2 + xq0
    tmpBF_1 = b4_F6.xF3[1].xBF[2] + xq0;
    tmpBF_1.plusOne();
    Basefield::template mulCombL2RwithWindow<wSize>(b4_F6.xF3[0].xBF[0],xQ[1],tmpBF_1);

    //tmpBF_1 = xpBF^4xq0
    Basefield::template mulCombL2RwithWindow<wSize>(tmpBF_1, xpBF, xq0);
    b4_F6.xF3[1].xBF[2] += tmpBF_1;
    b4_F6.xF3[1].xBF[0] += yq0;

    // ypBF := ypBF^4
    Basefield::square(tmpBF_1, ypBF);
    Basefield::square(ypBF, tmpBF_1);
    b4_F6.xF3[0].xBF[0] += ypBF;

    // f^4
    ExtField3_2_2<Basefield>::square3_2_2(tmp3_2_2, pairingValue);
    ExtField3_2_2<Basefield>::square3_2_2(pairingValue, tmp3_2_2);

    // f^4(y+b4(x))
    ExtField3_2_2<Basefield>::template sparseMul3_2_2_withWindow<wSize>(tmp3_2_2, pairingValue, b4_F6);
    pairingValue.copy(tmp3_2_2);


    // final exponentiation
    /* !! perform only (degBF+1)/2 squarings !! */

    for (int i = 0; i < (degBF + 1) / 2; ++i) {

      ExtField3_2_2<Basefield>::square3_2_2(tmp3_2_2, pairingValue);
      pairingValue.copy(tmp3_2_2);
    }

    return pairingValue;
}

