#pragma once

#include <cuda_runtime.h>
#include <stdint.h>

template<typename T, int size> struct my_uint {

  typedef T uint_type;
  static const unsigned int wordSize = size;
};

typedef my_uint<uint32_t,32> uint_32;
typedef my_uint<uint64_t,64> uint_64;



template<int n>
struct power2 {

    static const int num = 2 * power2<n-1>::num;
};

template<>
struct power2<0> {

  static const int num = 1;
};

static const int power2Arr[11] = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024 };

__forceinline__ __host__ __device__
unsigned int pow2funcGlobal(const int wSize) {

  return static_cast<unsigned int>(1 << wSize);
}
