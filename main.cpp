//#include <gmp.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/time.h>
#include <cuda_runtime.h>

#include <iostream>

#include "Declaration.h"
#include "timing.h"
#include "F2_487.h"



int main(int argc, char **argv)
{
  // test (and timing)
  //testAddMul();
  //testAddMul_Multiple();
  //testAddMul_Multiple2();
  //testSquare_Multiple();
  //test967AddMul_Multiple();

  //test_pairingMultiple_profile();
  //test_basicFunction_profile();

  // test for device imple.
  //test_basic();
  //test_basic2();
  //test_AddMul_BF();
  //testMultipleCall();
  //testMultipleCall_Template<F2_487_u64>(13);

  // debug
  //sandbox();
  //pairing_host();
  //testDeviceFunc();
  //testTmplateCall();

  // timing
  //pairing_timing();
  //pairingCPU_timing(); // pairing_timing()と同じ
  //pairingMUltiple_timing();

  //pairingMUltiple_timingProfile();

  //pairing_getTiming();
  pairingHost_getTiming(); // CPU
  //dummy_pairing_getTiming(); // GPU
  //dummy_pairingMultiple_getTiming(); // GPU, m=487,967

  //for (int i = 1; i <= 44; ++i) std::cout << "dummy_pairing487Multiple_getTiming<" << i*32 << ">();" << std::endl;

#if 0
  mulGPU64();
  mulGPU64High();
  mulGPU32Wide();
  mulGPU32();
#endif

  return 0;
}
