#pragma once

#include "F2_487.h"

// base field operation
//__global__ void addKernel(F2_487_u64 *c, F2_487_u64 *a, F2_487_u64 *b);

// test
__global__ void test_basicKernel(BaseField<uint_64,487>::uint_type *C, BaseField<uint_64,487>::uint_type *A, BaseField<uint_64,487>::uint_type *B);
__global__ void test_basicKernel2(BaseField<uint_64,487> *C, BaseField<uint_64,487> *A, BaseField<uint_64,487> *B);
__global__ void test_add(F2_487_u64 *C, F2_487_u64 *A, F2_487_u64 *B);
__global__ void test_mul(F2_487_u64 *C, F2_487_u64 *A, F2_487_u64 *B);

template<class Basefield, int number>
__global__ void testMultiple(BaseField_Multiple<Basefield, number> *c, BaseField_Multiple<Basefield, number> *a, BaseField_Multiple<Basefield, number> *b);
