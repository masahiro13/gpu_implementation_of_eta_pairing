//#include <gmp.h>
#include <iostream>
#include <stdint.h>
#include <sys/time.h>
#include <cuda_runtime.h>

#include "Declaration.h"
#include "F2_487.h"
#include "ext_field6_2.h"
#include "ext_field3_2_2.h"
#include "pairing.h"
#include "callTemplate_Multiple.h"


void sandbox() {

  F2_487_u64 F, G, H, I, J, K;
  BaseField_Double<F2_487_u64> Dbl;
  Dbl.clear();
  Dbl.high.plusOne();
  Dbl.high.shiftLeftBit<485>(); // x^972
  bool eq, eq2;

  ExtField3<F2_487_u64> a3, b3, c3, d3;
  a3.setRandom();
  b3.setRandom();
  ExtField3<F2_487_u64>::mulShAdd3(c3,a3,b3);
  ExtField3<F2_487_u64>::mulCombR2L_uintSize3(d3,a3,b3);
  eq = c3 == d3;
  std::cout << "ext3 mul (bool): "<< eq << std::endl;

  ExtField3<F2_487_u64>::mulShAdd3(c3,a3,a3);
  ExtField3<F2_487_u64>::square3(d3,a3);
  eq = c3 == d3;
  std::cout << "ext3 square (bool): "<< eq << std::endl;




  ExtField6_2<F2_487_u64> P6_2, Q6_2, R6_2, S6_2, T6_2;
  ExtField3_2_2<F2_487_u64> P3_2_2, Q3_2_2, R3_2_2, S3_2_2;
  P6_2.setRandom();
  Q6_2.setRandom();

  P3_2_2.setRandom();
  Q3_2_2.setRandom();

  ExtField6_2<F2_487_u64>::mulShAdd6_2(R6_2, P6_2, Q6_2);
  ExtField6_2<F2_487_u64>::mulCombR2L_uintSize6_2(S6_2, P6_2, Q6_2);
  eq = R6_2 == S6_2;
  eq2 = R6_2 == T6_2;
  std::cout << "ext6_2 mul (bool): "<< eq << std::endl;

  ExtField3_2_2<F2_487_u64>::mulShAdd3_2_2(R3_2_2, P3_2_2, Q3_2_2);
  ExtField3_2_2<F2_487_u64>::mulCombR2L_uintSize3_2_2(S3_2_2, P3_2_2, Q3_2_2);
  eq = R3_2_2 == S3_2_2;
  std::cout << "ext3_2_2 mul (bool): "<< eq << std::endl;

  ExtField3_2_2<F2_487_u64>::mulCombR2L_uintSize3_2_2(R3_2_2, P3_2_2, P3_2_2);
  ExtField3_2_2<F2_487_u64>::square3_2_2(S3_2_2, P3_2_2);
  eq = R3_2_2 == S3_2_2;
  std::cout << "ext3_2_2 square (bool): "<< eq << std::endl;

  Dbl.reduction(F);
  F.setRandom();
  G.setRandom();

  BaseField<uint_32,487> F32;
  F32.clear();
  F32.setRandom();

  //F.clear();
  //G.clear();
  //F.plusOne();
  //F.shiftLeftBit<486>();
  //G.plusOne();
  //G.shiftLeftBit<486>();

  //F.clear();
  //G.clear();
  //F.plusOne();
  //G.plusOne();
  //F.F2_487_u64::shiftLeftBit<256>();
  //G.F2_487_u64::shiftLeftBit<256>();
  F2_487_u64::mulShAdd(H,F,G);
  //F2_487_u64::square(I,F);
  F2_487_u64::mulCombR2L_uintSize(I,F,G);
  F2_487_u64::mulCombL2RwithWindow<6>(J,F,G);
  F2_487_u64::mulCombL2RwithWindow(K,F,G,6);
  eq = H == I;
  eq2 = H == J;
  std::cout << H;
  std::cout << I;
  std::cout << "template (window):" << std::endl << J;
  std::cout << "not template (window):" << std::endl << K;
  std::cout << eq << std::endl;
  std::cout << "ext mul compare with window method (bool): "<< eq2 << std::endl;



  F.setAll1Bit();
  F.shiftLeftBit<65>();
  F.setAll1Bit();
  F.shiftRightBit<255>();
  G = BaseField<uint_64,487>::shiftRightBit<33>(F);

  F.clearUpDeg();

  F.clear();
  F.setRandom();
  F.plusOne();
  G.clear();
  H = F + G;
  H = F + F;

  eq = H == F;
  eq = H == G;
}

void pairing_host() {

  F2_487_u64 xP, xQ, yP, yQ;
  ExtField6_2<F2_487_u64> pairing6_2;
  xP.setRandom();
  xQ.setRandom();
  yP.setRandom();
  yQ.setRandom();

  pairing6_2 = etaT_pairing6_2<F2_487_u64>(xP, xQ, yP, yQ);

}

void testDeviceFunc() {

  F2_487_u64 a, b, c, c2;
  a.setRandom();
  b.setRandom();

  mulCombR2L_uintSizeP(c,a,b);
  mulCombR2L_uintSizeP2<F2_487_u64>(c2,a,b);

  std::cout << "c1:" << std::endl << c;
  std::cout << "c2:" << std::endl << c2;
}

void testTmplateCall() {

  const int number = 4;
  BaseField_Multiple<F2_487_u64, number> a, b, c, c2;
  a.setRandom();
  b.setRandom();

  //device
  mulCombR2L_uintSize_multipleP<F2_487_u64, number>(c,a,b);

  // host
  for (int i = 0; i < number; ++i) F2_487_u64::mulCombR2L_uintSize(c2.xBF[i],a.xBF[i],b.xBF[i]);

  for (int i = 0; i < number; ++i) {

    std::cout << i << "-th element; c (device):" << std::endl << c.xBF[i];
    std::cout << "c (host):" << std::endl << c2.xBF[i];
    std::cout << std::endl;
  }
}
